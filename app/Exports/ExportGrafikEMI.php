<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Charts;

use App\Models\ProfilModel as ProfilModel;
use App\Models\PenilaianModel as PenilaianModel;
use App\Models\PerencanaanModel as PerencanaanModel;
use App\Models\IndikatorModel as IndikatorModel;
use App\Models\StandarModel as StandarModel;
use App\Models\StandarModelFile as StandarModelFile;
use App\Models\StandarAwalModel as StandarAwalModel;
use App\Models\CategoryModel as CategoryModel;
use App\Models\JenjangModel as JenjangModel;

use App\Models\Emi_StandarModel as EmiStandarModel;
use App\Models\Emi_IsianStandarModel as Emi_IsianStandarModel;
use App\Models\Emi_IsianStandarModel as EmiIsianStandarModel;
use App\Models\Emi_BobotNilaiModel as EmiBobotNilaiModel;
use App\Models\Emi_RekapStandarModel as EmiRekapStandar;
use Khill\Lavacharts\Lavacharts;

class ExportGrafikEMI implements FromView
{
    protected $profil_id, $tahun_emi;
    public function __construct(String $profil_id, String $tahun_emi)
    {
        $this->profil_id = $profil_id;
        $this->tahun_emi = $tahun_emi;
    }

    public function view(): View
    {
        $total_max_isian_standar = 0;
        $profil = ProfilModel::where([
            'id' => $this->profil_id,
            'tahun_pengisian' => $this->tahun_emi
        ])->first();


        //jumlahkan perstandar
        $capaian = [];
        $nilai_standar_id = [];

        $standar = EmiStandarModel::all();
        $lava = new Lavacharts; // See note below for Laravel
        $popularity = $lava->DataTable();

        foreach ($standar as $cap => $std) { //forrekap
            $total_nilai_per_standar    = 0;
            $total_bobot_nilai          = 0;
            $isian                      = Emi_IsianStandarModel::where([
                'emi_standar_id' => $std->id,
                'emi_jenjang_id' => $profil->jenjang_plot_id

            ])->get();


            if ($isian != null) {
                $popularity->addStringColumn($std->nama_standar);


                foreach ($isian as $key => $isi) { //for isian standar
                    $nilai  = EmiBobotNilaiModel::where([
                        'isian_standar_id' => $isi->id, 'profil_id' => $this->profil_id, 'tahun_emi' => $this->tahun_emi
                    ])->first();

                    if ($nilai != null) {
                        $nilai = $nilai->nilai;
                    } else {
                        $nilai = 0;
                    }

                    //ambil bobot nilai  terus kalikan
                    $total_nilai_per_standar = $total_nilai_per_standar + ($isi->emi_bobot_persen * $nilai);
                    $total_max_isian_standar += $isi->emi_bobot_persen * 7;
                    $total_bobot_nilai += $isi->emi_bobot_persen;
                }
            }

            if ($total_max_isian_standar == 0) {
                $total_max_isian_standar = 1;
            }

            $capaian[$cap] = $total_nilai_per_standar / $total_max_isian_standar * 100;
            $nilai_standar_id[$cap] = $std->nama_standar;

            $bobot[$cap] = $total_bobot_nilai / 100;
        }





        $popularity->addRows(array($capaian));


        $the_chart = $lava->BarChart('Popularity', $popularity);



        return view('exports.emi-grafik', [
            'profildata' => $profil,
            'data_capaian' => $capaian,
            'data_standar' => $nilai_standar_id,
            'data_bobot' => $bobot,
            'lava' => $the_chart,
            'grafik' => ''
        ]);
    }
}
