<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

use App\Models\ProfilModel as ProfilModel;
use App\Models\PenilaianModel as PenilaianModel;
use App\Models\PerencanaanModel as PerencanaanModel;
use App\Models\IndikatorModel as IndikatorModel;
use App\Models\StandarModel as StandarModel;
use App\Models\StandarModelFile as StandarModelFile;
use App\Models\StandarAwalModel as StandarAwalModel;
use App\Models\CategoryModel as CategoryModel;
use App\Models\JenjangModel as JenjangModel;


use App\Models\Emi_StandarModel as EmiStandarModel;
use App\Models\Emi_IsianStandarModel as Emi_IsianStandarModel;
use App\Models\Emi_IsianStandarModel as EmiIsianStandarModel;
use App\Models\Emi_BobotNilaiModel as EmiBobotNilaiModel;
use App\Models\Emi_RekapStandarModel as EmiRekapStandar;
use App\Models\Audit_JadwalModel as AuditJadwalModel;
use App\Models\Audit_KetidaksesuaianModel as AuditKetidaksesuaianModel;
use App\Models\Audit_SaranModel as AuditSaranModel;
use App\Models\Audit_TujuanModel as AuditTujuanModel;
use App\Models\Audit_KesimpulanModel as AuditKesimpulanModel;

use App\Models\Audit_Step_PenentuanJadwalAuditeeModel as AuditPenentuanJadwalAuditeeModel;
use App\Models\Audit_Step_PenentuanJadwalAuditorModel as AuditPenentuanJadwalAuditorModel;
use App\Models\Audit_Step_PenentuanJadwalModel as AuditPenentuanJadwalModel;
use App\Models\Audit_Step_JadwalLingkupModel as AuditJadwalLingkupModel;

use App\Models\Audit_Step_PTKModel as AuditPTKModel;
use App\Models\Audit_JenjangKesimpulanModel as JenjangKesimpulanModel;
use App\Models\Audit_PTKModel as PTKModel;




class ExportLaporanAMI implements FromView
{
    protected $pen_jadwal_id;
    public function __construct(String $pen_jadwal_id)
    {
        $this->pen_jadwal_id = $pen_jadwal_id;
    }

    public function view(): View
    {
        $pen_jadwal_id =  $this->pen_jadwal_id;

        $jadwal = AuditPenentuanJadwalModel::where([
            'pen_jadwal_id' => $pen_jadwal_id
        ])->first();

        $profil = ProfilModel::join('tbl_jenjang', 'tbl_jenjang.jenjang_id', '=', 'tbl_profil.jenjang_plot_id')->where([
            'id' => $jadwal->pen_jadwal_prodi_id
        ])->first();

        $tujuan = AuditTujuanModel::where([
            'audit_jadwal_ref_id' =>  $pen_jadwal_id
        ])->get();

        $jadwal_lingkup = AuditJadwalLingkupModel::where([
            'pen_lingkup_ref_id' =>  $pen_jadwal_id
        ])->get();

        $jadwal_audit  = AuditJadwalModel::where([
            'audit_borang_id' => $pen_jadwal_id
        ])->orderBy('tanggal', 'asc')->distinct('tanggal')->get('tanggal');



        $lauditor = AuditPenentuanJadwalAuditorModel::join('users', 'users.id', '=', 'tbl_audit_borang_penentuan_jadwal_listauditor.pen_jadwal_list_auditor_ref_id')
            ->where([
                'pen_jadwal_list_auditor_user_kesediaan' => 1,
                'pen_jadwal_list_auditor_ref_id' => $pen_jadwal_id,
                'pen_jadwal_list_auditor_user_status' => 1

            ])->first();

        $lauditor_anggota = AuditPenentuanJadwalAuditorModel::join('users', 'users.id', '=', 'tbl_audit_borang_penentuan_jadwal_listauditor.pen_jadwal_list_auditor_ref_id')
            ->where([
                'pen_jadwal_list_auditor_user_kesediaan' => 1,
                'pen_jadwal_list_auditor_ref_id' => $pen_jadwal_id,
                'pen_jadwal_list_auditor_user_status' => 2

            ])->get();

        $lauditee = AuditPenentuanJadwalAuditeeModel::join('users', 'users.id', '=', 'tbl_audit_borang_penentuan_jadwal_listauditee.pen_jadwal_list_auditee_user_id')
            ->where([
                'pen_jadwal_list_auditee_ref_id' => $pen_jadwal_id,
            ])->first();

        $ketidaksesuaian = AuditKetidaksesuaianModel::where([
            'audit_borang_id' => $pen_jadwal_id
        ])->get();

        $saran = AuditSaranModel::where([
            'audit_borang_id' => $pen_jadwal_id
        ])->get();



        return view('exports.ami-laporan', [
            'pen_jadwal_id' => $pen_jadwal_id,
            'lauditor' => $lauditor,
            'lauditor_anggota' => $lauditor_anggota,
            'lauditee' => $lauditee,
            'profildata' => $profil,
            'jadwaldata' => $jadwal,
            'tujuan' => $tujuan,
            'jadwallingkup' => $jadwal_lingkup,
            'jadwalaudit' => $jadwal_audit,
            'ketidaksesuaian' => $ketidaksesuaian,
            'saran' => $saran

        ]);
    }
}
