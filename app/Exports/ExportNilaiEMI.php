<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

use App\Models\ProfilModel as ProfilModel;
use App\Models\PenilaianModel as PenilaianModel;
use App\Models\PerencanaanModel as PerencanaanModel;
use App\Models\IndikatorModel as IndikatorModel;
use App\Models\StandarModel as StandarModel;
use App\Models\StandarModelFile as StandarModelFile;
use App\Models\StandarAwalModel as StandarAwalModel;
use App\Models\CategoryModel as CategoryModel;
use App\Models\JenjangModel as JenjangModel;

use App\Models\Emi_StandarModel as EmiStandarModel;

use App\Models\Emi_IsianStandarModel as EmiIsianStandarModel;
use App\Models\Emi_BobotNilaiModel as EmiBobotNilaiModel;
use App\Models\Emi_RekapStandarModel as EmiRekapStandar;




class ExportNilaiEMI implements FromView
{
    protected $profil_id, $tahun_emi;
    public function __construct(String $profil_id, String $tahun_emi)
    {
        $this->profil_id = $profil_id;
        $this->tahun_emi = $tahun_emi;
    }

    public function view(): View
    {
        $emi_standar = EmiStandarModel::all();

        $emi_standar_name = [];
        $emi_standar_view = [];
        $profil_prodi = [];

        $get_profil_prodi = ProfilModel::join('tbl_jenjang', 'tbl_jenjang.jenjang_id', '=', 'tbl_profil.jenjang_plot_id')->where(['id' => $this->profil_id])->first();


        $jenjang_id = $get_profil_prodi->jenjang_plot_id;

        foreach ($emi_standar as $key => $emk) {

            $emi_standar_list = EmiStandarModel::join('tbl_emi_isian_standar', 'tbl_emi_isian_standar.emi_standar_id', '=', 'tbl_emi_standar.id')


                ->where([
                    'emi_standar_id' => $emk->id,
                    'emi_jenjang_id' => $jenjang_id
                ])
                ->select('*', 'tbl_emi_isian_standar.id as isian_id')
                ->get();

            $emi_standar_view[$key] = $emi_standar_list;
            $emi_standar_name[$key] = $emk->nama_standar;



            // return view('step-kjm.tahap_emi', [
            //     'nama_standar' => $emi, 'standar_list' => $emi_standar_list, 'profil_list' => $profil_prodi, 'profil_id' => $profil_id, 'tahun_emi' => $tahun_emi,
            //     'jenjang_list' => $jenjang, 'jenjang_id' => $jenjang_id, 'tahun_pengukuran_list' => $tahun_pengukuran

            // ]);
        }
        return view('exports.emi-nilai', [
            'standar_list' => $emi_standar_view,
            'standar_name' => $emi_standar_name,
            'profildata' => $get_profil_prodi
        ]);
    }
}
