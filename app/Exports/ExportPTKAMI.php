<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

use App\Models\ProfilModel as ProfilModel;
use App\Models\PenilaianModel as PenilaianModel;
use App\Models\PerencanaanModel as PerencanaanModel;
use App\Models\IndikatorModel as IndikatorModel;
use App\Models\StandarModel as StandarModel;
use App\Models\StandarModelFile as StandarModelFile;
use App\Models\StandarAwalModel as StandarAwalModel;
use App\Models\CategoryModel as CategoryModel;
use App\Models\JenjangModel as JenjangModel;

use App\Models\Emi_StandarModel as EmiStandarModel;
use App\Models\Emi_IsianStandarModel as Emi_IsianStandarModel;
use App\Models\Emi_IsianStandarModel as EmiIsianStandarModel;
use App\Models\Emi_BobotNilaiModel as EmiBobotNilaiModel;
use App\Models\Emi_RekapStandarModel as EmiRekapStandar;

use App\Models\Audit_Step_PenentuanJadwalAuditeeModel as AuditPenentuanJadwalAuditeeModel;
use App\Models\Audit_Step_PenentuanJadwalAuditorModel as AuditPenentuanJadwalAuditorModel;
use App\Models\Audit_Step_PenentuanJadwalModel as AuditPenentuanJadwalModel;
use App\Models\Audit_Step_JadwalLingkupModel as AuditJadwalLingkupModel;

use App\Models\Audit_Step_PTKModel as AuditPTKModel;
use App\Models\Audit_PTKModel as PTKModel;




class ExportPTKAMI implements FromView
{
    protected $pen_jadwal_id;
    public function __construct(String $pen_jadwal_id)
    {
        $this->pen_jadwal_id = $pen_jadwal_id;
    }
    public function view(): View
    { 
        $pen_jadwal_id = $this->pen_jadwal_id;
        
        return view('exports.ami-ptk',['pen_jadwal_id' => $pen_jadwal_id]);
    }
}
