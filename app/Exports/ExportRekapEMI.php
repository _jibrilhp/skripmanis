<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

use App\Models\ProfilModel as ProfilModel;
use App\Models\PenilaianModel as PenilaianModel;
use App\Models\PerencanaanModel as PerencanaanModel;
use App\Models\IndikatorModel as IndikatorModel;
use App\Models\StandarModel as StandarModel;
use App\Models\StandarModelFile as StandarModelFile;
use App\Models\StandarAwalModel as StandarAwalModel;
use App\Models\CategoryModel as CategoryModel;
use App\Models\JenjangModel as JenjangModel;

use App\Models\Emi_StandarModel as EmiStandarModel;
use App\Models\Emi_IsianStandarModel as Emi_IsianStandarModel;
use App\Models\Emi_IsianStandarModel as EmiIsianStandarModel;
use App\Models\Emi_BobotNilaiModel as EmiBobotNilaiModel;
use App\Models\Emi_RekapStandarModel as EmiRekapStandar;




class ExportRekapEMI implements FromView
{
    protected $profil_id, $tahun_emi;
    public function __construct(String $profil_id, String $tahun_emi)
    {
        $this->profil_id = $profil_id;
        $this->tahun_emi = $tahun_emi;
    }

    public function view(): View
    {

        $rekap_temuan = [];
        $rekap_hasil_temuan_sebab = [];
        $rekap_hasil_temuan_akibat = [];
        $rekap_rekomendasi = [];

        $data_standar_id = [];


        $total_max_isian_standar = 0;
        $profil = ProfilModel::where([
            'id' => $this->profil_id,
            'tahun_pengisian' => $this->tahun_emi
        ])->first();


        //jumlahkan perstandar
        $capaian = [];
        $nilai_standar_id = [];

        $standar = EmiStandarModel::all();

        foreach ($standar as $cap => $std) { //forrekap
            $total_nilai_per_standar    = 0;
            $total_bobot_nilai          = 0;
            $isian                      = Emi_IsianStandarModel::where([
                'emi_standar_id' => $std->id,
                'emi_jenjang_id' => $profil->jenjang_plot_id

            ])->get();

            if ($isian != null) {
                foreach ($isian as $key => $isi) { //for isian standar
                    $nilai  = EmiBobotNilaiModel::where([
                        'isian_standar_id' => $isi->id, 'profil_id' => $this->profil_id, 'tahun_emi' => $this->tahun_emi
                    ])->first();

                    if ($nilai != null) {
                        $nilai = $nilai->nilai;
                    } else {
                        $nilai = 0;
                    }

                    //ambil bobot nilai  terus kalikan
                    $total_nilai_per_standar = $total_nilai_per_standar + ($isi->emi_bobot_persen * $nilai);
                    $total_max_isian_standar += $isi->emi_bobot_persen * 7;
                    $total_bobot_nilai += $isi->emi_bobot_persen;
                }
            }

            if ($total_max_isian_standar == 0) {
                $total_max_isian_standar = 1;
            }

            $capaian[$cap] = $total_nilai_per_standar / $total_max_isian_standar * 100;
            $nilai_standar_id[$cap] = $std->nama_standar;
            $bobot[$cap] = $total_bobot_nilai / 100;

            $rekap = EmiRekapStandar::where([
                'rekap_standar_id' => $std->id,
                'rekap_profil_id' =>  $this->profil_id,
                'rekap_tahun_pengukuran' => $this->tahun_emi
            ])->first();

            if ($rekap != null) {
                $rekap_temuan[$cap] = $rekap->rekap_temuan;
                $rekap_hasil_temuan_sebab[$cap] = $rekap->rekap_hasil_temuan_sebab;
                $rekap_hasil_temuan_akibat[$cap] = $rekap->rekap_hasil_temuan_akibat;
                $rekap_rekomendasi[$cap] = $rekap->rekap_rekomendasi;
                $data_standar_id[$cap] = $rekap->rekap_standar_id;
            } else {
                $rekap_temuan[$cap] = '';
                $rekap_hasil_temuan_sebab[$cap] = '';
                $rekap_hasil_temuan_akibat[$cap] = '';
                $rekap_rekomendasi[$cap] = '';
                $data_standar_id[$cap] = $std->id;
            }
        }

        return view('exports.emi-rekap', [
            'profildata' => $profil,
            'data_capaian' => $capaian,
            'data_standar' => $nilai_standar_id,
            'data_bobot' => $bobot,
            'data_rekap_temuan' => $rekap_temuan,
            'data_rekap_sebab' => $rekap_hasil_temuan_sebab,
            'data_rekap_akibat' => $rekap_hasil_temuan_akibat,
            'data_rekap_rekomendasi' => $rekap_rekomendasi,
            'data_standar_id' => $data_standar_id

        ]);
    }
}
