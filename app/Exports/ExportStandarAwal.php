<?php

namespace App\Exports;

use Session;
use App\Models\StandarAwalModel as StandarAwalModel;
use App\Models\StandarModel as StandarModel;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ExportStandarAwal implements FromView
{
    function __construct($category_id,$category_ids) {
        $this->category_id = $category_id;
        $this->dikti_category_ids = $category_ids;
    }

    public function view(): View
    {
       
        $standar_awal = StandarAwalModel::where('standar_dikti_id','=', $this->dikti_category_ids )
        ->where('profil_id','=', Session::get('profil_id'))
        ->where('category_id','=',$this->category_id  )
        ->get();

        $standar_dikti = StandarModel::where('id','=', $this->dikti_category_ids)->first();

        return view('exports.standarawal', [
            'standar_dikti' => $standar_dikti,
            'standar_awal' => $standar_awal
        ]);
    }
}
