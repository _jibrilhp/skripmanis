<?php

namespace App\Http\Controllers;

use Session;
use PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Http\Requests;
use App\Exports\ExportStandarAwal as ExportStandar;
use App\Exports\ExportLaporanAMI as ExportLaporanAMI;
use App\Exports\ExportPTKAMI as ExportPTKAMI;

use App\Imports\ExcelProfilImport as ProfilImport;
use App\Imports\ExcelStandarAwalImport as StandarImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\ProfilModel as ProfilModel;
use App\Models\PenilaianModel as PenilaianModel;
use App\Models\PerencanaanModel as PerencanaanModel;
use App\Models\IndikatorModel as IndikatorModel;
use App\Models\StandarModel as StandarModel;
use App\Models\StandarAwalModel as StandarAwalModel;
use App\Models\CategoryModel as CategoryModel;
use App\Models\ManualMutuModel as ManualMutuModel;
use App\Models\UserModel as UserModel;
use App\Models\Audit_BorangModel as AuditBorangModel;
use App\Models\Audit_AnggotaModel as AuditAnggotaModel;
use App\Models\Audit_TujuanModel as AuditTujuanModel;
use App\Models\Audit_PertanyaanModel as AuditPertanyaanModel;
use App\Models\Audit_LingkupModel as AuditLingkupModel;
use App\Models\Audit_JadwalModel as AuditJadwalModel;
use App\Models\Audit_KetidaksesuaianModel as AuditKetidaksesuaianModel;
use App\Models\Audit_SaranModel as AuditSaranModel;
use App\Models\Audit_KesimpulanModel as AuditKesimpulanModel;

use App\Models\Audit_Step_PermintaanModel as AuditPermintaanModel;
use App\Models\Audit_Step_PermintaanFileModel as AuditPermintaanFileModel;
use App\Models\Audit_Step_AnggotaAuditorModel as AuditPermintaanAnggotaModel;

use App\Models\Audit_Step_PenentuanJadwalAuditeeModel as AuditPenentuanJadwalAuditeeModel;
use App\Models\Audit_Step_PenentuanJadwalAuditorModel as AuditPenentuanJadwalAuditorModel;
use App\Models\Audit_Step_PenentuanJadwalModel as AuditPenentuanJadwalModel;
use App\Models\Audit_Step_JadwalLingkupModel as AuditJadwalLingkupModel;

use App\Models\Audit_Step_PTKModel as AuditPTKModel;
use App\Models\Audit_AdministrasiModel as AdministrasiModel;

use App\Models\Emi_KeadaanModel as EmiKeadaanModel;
use App\Models\Emi_StandarModel as EmiStandarModel;
use App\Models\Emi_IsianStandarModel as EmiIsianStandarModel;
use App\Models\Emi_BobotNilaiModel as EmiBobotNilaiModel;

use App\Models\JenjangModel as JenjangModel;
use App\Models\Audit_JenjangKesimpulanModel as JenjangKesimpulanModel;
use App\Models\Audit_PTKModel as PTKModel;

use Yajra\Datatables\Datatables;

//Jibril Hartri Putra 3 Ramadhan 1440 H

class AuditorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    //new AMI
    function ami_download_laporan($pen_jadwal_id)
    {
        return Excel::store(new ExportLaporanAMI($pen_jadwal_id), 'process_temp2/ami_laporan.xlsx');
    }

    function ami_download_ptk($pen_jadwal_id)
    {
        return Excel::store(new ExportPTKAMI($pen_jadwal_id), 'process_temp2/ami_ptk.xlsx');
    }

    function ami_download_ptk_pdf ($pen_jadwal_id)
    {
        $pdf = PDF::loadview('exports.ami-ptk',['pen_jadwal_id' => $pen_jadwal_id]);
        $output = $pdf->output();
        return file_put_contents(storage_path() . '/app/process_temp2/PTK.pdf', $output);
    }

    function ami_download_laporan_pdf ($pen_jadwal_id)
    {

        $jadwal = AuditPenentuanJadwalModel::where([
            'pen_jadwal_id' => $pen_jadwal_id
        ])->first();

        $profil = ProfilModel::join('tbl_jenjang', 'tbl_jenjang.jenjang_id', '=', 'tbl_profil.jenjang_plot_id')->where([
            'id' => $jadwal->pen_jadwal_prodi_id
        ])->first();

        $tujuan = AuditTujuanModel::where([
            'audit_jadwal_ref_id' =>  $pen_jadwal_id
        ])->get();

        $jadwal_lingkup = AuditJadwalLingkupModel::where([
            'pen_lingkup_ref_id' =>  $pen_jadwal_id
        ])->get();

        $jadwal_audit  = AuditJadwalModel::where([
            'audit_borang_id' => $pen_jadwal_id
        ])->orderBy('tanggal', 'asc')->distinct('tanggal')->get('tanggal');



        $lauditor = AuditPenentuanJadwalAuditorModel::join('users', 'users.id', '=', 'tbl_audit_borang_penentuan_jadwal_listauditor.pen_jadwal_list_auditor_ref_id')
            ->where([
                'pen_jadwal_list_auditor_user_kesediaan' => 1,
                'pen_jadwal_list_auditor_ref_id' => $pen_jadwal_id,
                'pen_jadwal_list_auditor_user_status' => 1

            ])->first();

        $lauditor_anggota = AuditPenentuanJadwalAuditorModel::join('users', 'users.id', '=', 'tbl_audit_borang_penentuan_jadwal_listauditor.pen_jadwal_list_auditor_ref_id')
            ->where([
                'pen_jadwal_list_auditor_user_kesediaan' => 1,
                'pen_jadwal_list_auditor_ref_id' => $pen_jadwal_id,
                'pen_jadwal_list_auditor_user_status' => 2

            ])->get();

        $lauditee = AuditPenentuanJadwalAuditeeModel::join('users', 'users.id', '=', 'tbl_audit_borang_penentuan_jadwal_listauditee.pen_jadwal_list_auditee_user_id')
            ->where([
                'pen_jadwal_list_auditee_ref_id' => $pen_jadwal_id,
            ])->first();

        $ketidaksesuaian = AuditKetidaksesuaianModel::where([
            'audit_borang_id' => $pen_jadwal_id
        ])->get();

        $saran = AuditSaranModel::where([
            'audit_borang_id' => $pen_jadwal_id
        ])->get();



        $pdf = PDF::loadview('exports.ami-laporan', [
            'pen_jadwal_id' => $pen_jadwal_id,
            'lauditor' => $lauditor,
            'lauditor_anggota' => $lauditor_anggota,
            'lauditee' => $lauditee,
            'profildata' => $profil,
            'jadwaldata' => $jadwal,
            'tujuan' => $tujuan,
            'jadwallingkup' => $jadwal_lingkup,
            'jadwalaudit' => $jadwal_audit,
            'ketidaksesuaian' => $ketidaksesuaian,
            'saran' => $saran

        ]);
        $output = $pdf->output();
        return file_put_contents(storage_path() . '/app/process_temp2/Laporan.pdf', $output);
    }

    function ami_download_daftar_hadir_pdf ($pen_jadwal_id)
    {

        $jadwal = AuditPenentuanJadwalModel::where([
            'pen_jadwal_id' => $pen_jadwal_id
        ])->first();

        $profil = ProfilModel::join('tbl_jenjang', 'tbl_jenjang.jenjang_id', '=', 'tbl_profil.jenjang_plot_id')->where([
            'id' => $jadwal->pen_jadwal_prodi_id
        ])->first();

        $tujuan = AuditTujuanModel::where([
            'audit_jadwal_ref_id' =>  $pen_jadwal_id
        ])->get();

        $jadwal_lingkup = AuditJadwalLingkupModel::where([
            'pen_lingkup_ref_id' =>  $pen_jadwal_id
        ])->get();

        $jadwal_audit  = AuditJadwalModel::where([
            'audit_borang_id' => $pen_jadwal_id
        ])->orderBy('tanggal', 'asc')->distinct('tanggal')->get('tanggal');



        $lauditor = AuditPenentuanJadwalAuditorModel::join('users', 'users.id', '=', 'tbl_audit_borang_penentuan_jadwal_listauditor.pen_jadwal_list_auditor_ref_id')
            ->where([
                'pen_jadwal_list_auditor_user_kesediaan' => 1,
                'pen_jadwal_list_auditor_ref_id' => $pen_jadwal_id,
                'pen_jadwal_list_auditor_user_status' => 1

            ])->first();

        $lauditor_anggota = AuditPenentuanJadwalAuditorModel::join('users', 'users.id', '=', 'tbl_audit_borang_penentuan_jadwal_listauditor.pen_jadwal_list_auditor_ref_id')
            ->where([
                'pen_jadwal_list_auditor_user_kesediaan' => 1,
                'pen_jadwal_list_auditor_ref_id' => $pen_jadwal_id,
                'pen_jadwal_list_auditor_user_status' => 2

            ])->get();

        $lauditee = AuditPenentuanJadwalAuditeeModel::join('users', 'users.id', '=', 'tbl_audit_borang_penentuan_jadwal_listauditee.pen_jadwal_list_auditee_user_id')
            ->where([
                'pen_jadwal_list_auditee_ref_id' => $pen_jadwal_id,
            ])->first();

        $ketidaksesuaian = AuditKetidaksesuaianModel::where([
            'audit_borang_id' => $pen_jadwal_id
        ])->get();

        $saran = AuditSaranModel::where([
            'audit_borang_id' => $pen_jadwal_id
        ])->get();



        $pdf = PDF::loadview('exports.ami-daftar_hadir', [
            'pen_jadwal_id' => $pen_jadwal_id,
            'lauditor' => $lauditor,
            'lauditor_anggota' => $lauditor_anggota,
            'lauditee' => $lauditee,
            'profildata' => $profil,
            'jadwaldata' => $jadwal,
            'tujuan' => $tujuan,
            'jadwallingkup' => $jadwal_lingkup,
            'jadwalaudit' => $jadwal_audit,
            'ketidaksesuaian' => $ketidaksesuaian,
            'saran' => $saran

        ]);
        $output = $pdf->output();
        return file_put_contents(storage_path() . '/app/process_temp2/Daftar_Hadir.pdf', $output);
    }

    function ami_download_data_set(Request $request, $jadwal_id)
    {
        if ($jadwal_id != null) {
            $jadwal = AuditPenentuanJadwalModel::where([
                'pen_jadwal_id' => $jadwal_id
            ])->first();

            if ($jadwal !=  null) {
                $download1 = $this->ami_download_laporan_pdf($jadwal_id);
                $download2 = $this->ami_download_ptk_pdf($jadwal_id);
                $download3 = $this->ami_download_daftar_hadir_pdf($jadwal_id);
                
                if ($download1 && $download2 && $download3) {


                    $zip_file = 'AMI_' . date("d-m-y") . '.zip';
                    $zip = new \ZipArchive();
                    $zip->open($zip_file, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

                    $path = storage_path('app/process_temp2');

                    $files = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($path));
                    foreach ($files as $name => $file) {
                        // We're skipping all subfolders
                        if (!$file->isDir()) {
                            $filePath     = $file->getRealPath();

                            // extracting filename with substr/strlen
                            $relativePath = 'Data-AMI/' . substr($filePath, strlen($path) + 1);

                            $zip->addFile($filePath, $relativePath);
                        }
                    }
                    $zip->close();
                    return response()->download($zip_file);
                }
            } else {
                return back()->with('pesan', 'Pilihan tidak tersedia');
            }
        }
    }

    function ami_download_page(Request $request)
    {
        $audit = AuditPenentuanJadwalModel::join('tbl_profil', 'tbl_profil.id', '=', 'tbl_audit_borang_penentuan_jadwal.pen_jadwal_prodi_id')->get();

        return view('download.ami_download', [
            'data_audit' => $audit
        ]);
    }

    static function ami_get_ptk($jadwal_id)
    {
        if ($jadwal_id != null) {
            $sp = AuditPenentuanJadwalModel::where([
                'pen_jadwal_id' => $jadwal_id
            ])->first();

            if ($sp != null) {
                $sp = AuditPenentuanJadwalModel::join('tbl_profil', 'tbl_profil.id', '=', 'tbl_audit_borang_penentuan_jadwal.pen_jadwal_prodi_id')
                    ->join('tbl_jenjang', 'tbl_jenjang.jenjang_id', '=', 'tbl_profil.jenjang_plot_id')
                    ->where([
                        'pen_jadwal_id' => $jadwal_id
                    ])->first()->toArray();

                $lauditor_ketua = AuditPenentuanJadwalAuditorModel::join('users', 'users.id', '=', 'tbl_audit_borang_penentuan_jadwal_listauditor.pen_jadwal_list_auditor_ref_id')
                    ->where([
                        'pen_jadwal_list_auditor_user_kesediaan' => 1,
                        'pen_jadwal_list_auditor_ref_id' => $jadwal_id,
                        'pen_jadwal_list_auditor_user_status' => 1

                    ])->first()->toArray();

                $lauditor = AuditPenentuanJadwalAuditorModel::join('users', 'users.id', '=', 'tbl_audit_borang_penentuan_jadwal_listauditor.pen_jadwal_list_auditor_ref_id')
                    ->where([
                        'pen_jadwal_list_auditor_user_kesediaan' => 1,
                        'pen_jadwal_list_auditor_ref_id' => $jadwal_id,
                        'pen_jadwal_list_auditor_user_status' => 2

                    ])->get()->toArray();

                $lauditee = AuditPenentuanJadwalAuditeeModel::join('users', 'users.id', '=', 'tbl_audit_borang_penentuan_jadwal_listauditee.pen_jadwal_list_auditee_user_id')
                    ->where([
                        'pen_jadwal_list_auditee_ref_id' => $jadwal_id
                    ])->first()->toArray();

                //check PTK No. ini dari kesimpulan yang udah didapet, berarti harus nyari point Mayor Minor ituu
                $check_data_json =  JenjangKesimpulanModel::where([
                    'jk_jenjang_id' => $sp['jenjang_id']
                ])->get();

                $index_number = -1;
                foreach ($check_data_json as $cdj) {
                    $extract_json = json_decode($cdj->jk_json_check, true);
                    foreach ($extract_json as $key => $ej) {
                        if ($ej == 'Major') {
                            $index_number = $cdj->jk_id;
                            break;
                        }
                    }
                }

                if ($index_number == -1) {
                    $data = array('data' => 'tidak ada data sp');
                    return response::json($data, 400);
                }

                $ks = AuditKesimpulanModel::where([
                    'kesimpulan' => $index_number,
                    'audit_borang_id' => $jadwal_id
                ])->first();

                //kalau ketemu maka bisa dilanjutkan untuk ditampilkan data arraynya..
                $check_data_json =  JenjangKesimpulanModel::where([
                    'jk_id' =>  $index_number
                ])->first();

                $jk_json_check = json_decode($check_data_json->jk_json_check, true);
                $jk_json_value = json_decode($check_data_json->jk_json_value, true);
                $isian = '';
				
				if ($ks != null) {
					
					foreach ($jk_json_check as $key => $dc) {
						if ($jk_json_value[$key] == 'V') {
							if (((int) $ks->yes_no_other_check - 1) == $key) {
								$isian = $dc;
							}
						} else {
							if (((int) $ks->yes_no_other_check - 1) == $key) {
								$isian = $dc;
							}
						}
					}
				}
				
                $referensi_mutu = '';

                $pertanyaan = AuditPertanyaanModel::where([
                    'audit_pen_jadwal_id' => $jadwal_id,
                    'yes_no_check'  => 2
                ])->get();

                if ($pertanyaan != null) {
                    foreach ($pertanyaan as $p) {
                        $referensi_mutu .= "> " . $p->referensi_butir_mutu . '<br>';
                    }
                } else {
                    $referensi_mutu = 'none';
                }

                $ptk = PTKModel::join('tbl_audit_borang_penentuan_jadwal', 'tbl_audit_borang_penentuan_jadwal.pen_jadwal_id', '=', 'tbl_audit_ptk_form.ptk_audit_jadwal_ref')->where(['ptk_audit_jadwal_ref' => $jadwal_id])->first();
                if ($ptk == null) {
                    $ptk = 'none';
                }

                $data_keseluruhan = [
                    'sp' => $sp,
                    'ketua_auditor' => $lauditor_ketua,
                    'anggota_auditor' => $lauditor,
                    'auditee' => $lauditee,
                    'isian' => $isian,
                    'referensi_mutu' => $referensi_mutu,
                    'ptk' => $ptk

                ];


                return $data_keseluruhan;
            } else {
                $data = array('data' => 'tidak ada sp');
                return response::json($data, 400);
            }
        } else {
            $data = array('data' => 'tidak ada jadwal id');
            return response::json($data, 400);
        }
    }

    static function ami_get_kesimpulan($jenjang_id, $pen_jadwal_id)
    {
        $kesimpulan = JenjangKesimpulanModel::where([
            'jk_jenjang_id' => $jenjang_id
        ])->get();

        $arr_k = [];
        $isiannya = [];

        foreach ($kesimpulan as $z => $k) {
            $isian = '';
            $data_other = '';
            $arr_k[] = $k;
            $decodeCheck = json_decode($k->jk_json_check, true);
            $decodeValue = json_decode($k->jk_json_value, true);

            $ks = AuditKesimpulanModel::where([
                'kesimpulan' => $k->jk_id,
                'audit_borang_id' => $pen_jadwal_id
            ])->first();

            //dd($ks,$k->jk_id,$request->jadwal_id);
            if ($ks != null) {

                $data_other = $ks->lainnya;
                foreach ($decodeCheck as $key => $dc) {
                    if ($decodeValue[$key] == 'V') {
                        if (((int) $ks->yes_no_other_check - 1) == $key) {
                            // $isian .= $dc . '[V] (' . $ks->lainnya . ') ' . chr(10);
                            $isian .= $dc . ' V ' . chr(10);
                        } else {
                            $isian .= $dc . ' o ' . chr(10);
                        }
                    } else {
                        if (((int) $ks->yes_no_other_check - 1) == $key) {
                            $isian .= $dc . ' V ' . chr(10);
                        } else {
                            $isian .= $dc . ' o ' . chr(10);
                        }
                    }
                }
            }

            if ($ks != null) {
                $isiannya[$z] = array_merge($k->toArray(), ['isian' => $isian, 'other' => $data_other, 'kesimpulan_id' => $ks->id, 'jkk' =>  $ks->kesimpulan, 'yes_no_other_check' => (int) $ks->yes_no_other_check - 1, 'jk_json_check' => $k->jk_json_check]);
            } else {
                $isiannya[$z] = array_merge($k->toArray(), ['isian' => $isian, 'other' => $data_other, 'kesimpulan_id' => '-', 'yes_no_other_check' => '-', 'jkk' => null]);
            }
        }
        return $isiannya;
    }

    function auditor_ami_ptk(Request $request)
    {
        if ($request->last_id != null) {
            $jadwal_id = (int) $request->last_id;
        } else {
            $jadwal_id = 'null';
        }

        $auditPenentuanJadwal = AuditPenentuanJadwalModel::get();

        return view('ami.ptk_audit', [
            'list_jadwal_audit' => $auditPenentuanJadwal,
            'jadwal_id' => $jadwal_id
        ]);
    }
    function auditor_ami_ptk_ajax_view(Request $request)
    {
        if ($request->jadwal_id != null) {
            $sp = AuditPenentuanJadwalModel::where([
                'pen_jadwal_id' => $request->jadwal_id
            ])->first();

            if ($sp != null) {
                $sp = AuditPenentuanJadwalModel::join('tbl_profil', 'tbl_profil.id', '=', 'tbl_audit_borang_penentuan_jadwal.pen_jadwal_prodi_id')
                    ->join('tbl_jenjang', 'tbl_jenjang.jenjang_id', '=', 'tbl_profil.jenjang_plot_id')
                    ->where([
                        'pen_jadwal_id' => $request->jadwal_id
                    ])->first()->toArray();

                $lauditor_ketua = AuditPenentuanJadwalAuditorModel::join('users', 'users.id', '=', 'tbl_audit_borang_penentuan_jadwal_listauditor.pen_jadwal_list_auditor_ref_id')
                    ->where([
                        'pen_jadwal_list_auditor_user_kesediaan' => 1,
                        'pen_jadwal_list_auditor_ref_id' => $request->jadwal_id,
                        'pen_jadwal_list_auditor_user_status' => 1

                    ])->first()->toArray();

                $lauditor = AuditPenentuanJadwalAuditorModel::join('users', 'users.id', '=', 'tbl_audit_borang_penentuan_jadwal_listauditor.pen_jadwal_list_auditor_ref_id')
                    ->where([
                        'pen_jadwal_list_auditor_user_kesediaan' => 1,
                        'pen_jadwal_list_auditor_ref_id' => $request->jadwal_id,
                        'pen_jadwal_list_auditor_user_status' => 2

                    ])->get()->toArray();

                $lauditee = AuditPenentuanJadwalAuditeeModel::join('users', 'users.id', '=', 'tbl_audit_borang_penentuan_jadwal_listauditee.pen_jadwal_list_auditee_user_id')
                    ->where([
                        'pen_jadwal_list_auditee_ref_id' => $request->jadwal_id
                    ])->first()->toArray();

                //check PTK No. ini dari kesimpulan yang udah didapet, berarti harus nyari point Mayor Minor ituu
                $check_data_json =  JenjangKesimpulanModel::where([
                    'jk_jenjang_id' => $sp['jenjang_id']
                ])->get();

                $index_number = -1;
                foreach ($check_data_json as $cdj) {
                    $extract_json = json_decode($cdj->jk_json_check, true);
                    foreach ($extract_json as $key => $ej) {
                        if ($ej == 'Major') {
                            $index_number = $cdj->jk_id;
                            break;
                        }
                    }
                }

                if ($index_number == -1) {
                    $data = array('data' => 'tidak ada data sp');
                    return response::json($data, 400);
                }

                $ks = AuditKesimpulanModel::where([
                    'kesimpulan' => $index_number,
                    'audit_borang_id' => $request->jadwal_id
                ])->first();

                //kalau ketemu maka bisa dilanjutkan untuk ditampilkan data arraynya..
                $check_data_json =  JenjangKesimpulanModel::where([
                    'jk_id' =>  $index_number
                ])->first();

                $jk_json_check = json_decode($check_data_json->jk_json_check, true);
                $jk_json_value = json_decode($check_data_json->jk_json_value, true);
                $isian = '';

                foreach ($jk_json_check as $key => $dc) {
                    if ($jk_json_value[$key] == 'V') {
                        if (((int) $ks->yes_no_other_check - 1) == $key) {
                            $isian = $dc;
                        }
                    } else {
                        if (((int) $ks->yes_no_other_check - 1) == $key) {
                            $isian = $dc;
                        }
                    }
                }
                $referensi_mutu = '';

                $pertanyaan = AuditPertanyaanModel::where([
                    'audit_pen_jadwal_id' => $request->jadwal_id,
                    'yes_no_check'  => 2
                ])->get();

                if ($pertanyaan != null) {
                    foreach ($pertanyaan as $p) {
                        $referensi_mutu .= "> " . $p->referensi_butir_mutu . '<br>';
                    }
                } else {
                    $referensi_mutu = 'none';
                }

                $ptk = PTKModel::where(['ptk_audit_jadwal_ref' => $request->jadwal_id])->first();
                if ($ptk == null) {
                    $ptk = 'none';
                }

                $data_keseluruhan = [
                    'sp' => $sp,
                    'ketua_auditor' => $lauditor_ketua,
                    'anggota_auditor' => $lauditor,
                    'auditee' => $lauditee,
                    'isian' => $isian,
                    'referensi_mutu' => $referensi_mutu,
                    'ptk' => $ptk

                ];


                return $data_keseluruhan;
            } else {
                $data = array('data' => 'tidak ada sp');
                return response::json($data, 400);
            }
        } else {
            $data = array('data' => 'tidak ada jadwal id');
            return response::json($data, 400);
        }
    }

    function auditor_ami_ptk_check(Request $request)
    {
        $audit = PTKModel::where(['ptk_audit_jadwal_ref' => $request->jadwal_id])->first();
        if ($audit == null) {
            return 'none';
        } else {
            return $audit;
        }
    }

    function auditor_ami_ptk_edit(Request $request)
    {
        if ($request->tujuan_ke != null) {
            switch ($request->tujuan_ke) {
                case "uraian_temuan":
                    $audit = PTKModel::updateOrCreate(['ptk_audit_jadwal_ref' => $request->jadwal_id], ['ptk_input_uraian_temuan' => $request->ptk_uraian_temuan_edit_value]);
                    if ($audit) {
                        return redirect()->route('Auditor6', ['last_id' => $request->jadwal_id]);
                    }
                    break;
                case "rtk":

                    $audit = PTKModel::updateOrCreate(['ptk_audit_jadwal_ref' => $request->jadwal_id], ['ptk_input_rtk' => $request->ptk_rtk_edit_value]);

                    if ($audit) {
                        return redirect()->route('Auditor6', ['last_id' => $request->jadwal_id]);
                    }
                    break;
                case "nomor_dokumen":
                    $audit = PTKModel::updateOrCreate(['ptk_audit_jadwal_ref' => $request->jadwal_id], ['ptk_input_no' => $request->ptk_input_nomor_dokumen]);

                    if ($audit) {
                        return redirect()->route('Auditor6', ['last_id' => $request->jadwal_id]);
                    }
                    break;
            }
        }
    }


    function auditor_ami_kesimpulan2_view(Request $request)
    {
        $profil = ProfilModel::where([
            'id' => Session::get('profil_id')
        ])->first();

        $kesimpulan = JenjangKesimpulanModel::where([
            'jk_jenjang_id' => $profil->jenjang_plot_id
        ])->get();

        $arr_k = [];
        $isiannya = [];

        foreach ($kesimpulan as $z => $k) {
            $isian = '';
            $data_other = '';
            $arr_k[] = $k;
            $decodeCheck = json_decode($k->jk_json_check, true);
            $decodeValue = json_decode($k->jk_json_value, true);

            $ks = AuditKesimpulanModel::where([
                'kesimpulan' => $k->jk_id,
                'audit_borang_id' => $request->jadwal_id
            ])->first();

            //dd($ks,$k->jk_id,$request->jadwal_id);
            if ($ks != null) {
				
				if ($ks->yes_no_other_check != null) {
					$data_other = $ks->lainnya;
					foreach ($decodeCheck as $key => $dc) {
						if ($decodeValue[$key] == 'V') {
							if (((int) $ks->yes_no_other_check - 1) == $key) {
								// $isian .= $dc . '[V] (' . $ks->lainnya . ') ' . chr(10);
								$isian .= $dc . '[V] ' . chr(10);
							} else {
								$isian .= $dc . '[ ] ' . chr(10);
							}
						} else {
							if (((int) $ks->yes_no_other_check - 1) == $key) {
								$isian .= $dc . '[V] ' . chr(10);
							} else {
								$isian .= $dc . '[ ] ' . chr(10);
							}
						}
					}
				}
            }

            if ($ks != null) {
                $isiannya[$z] = array_merge($k->toArray(), ['isian' => $isian, 'other' => $data_other, 'kesimpulan_id' => $ks->id, 'jkk' =>  $ks->kesimpulan, 'yes_no_other_check' => (int) $ks->yes_no_other_check - 1, 'jk_json_check' => $k->jk_json_check]);
            } else {
                $isiannya[$z] = array_merge($k->toArray(), ['isian' => $isian, 'other' => $data_other, 'kesimpulan_id' => '-', 'yes_no_other_check' => '-', 'jkk' => null]);
            }
        }
        //dd($isiannya);


        return Datatables::of($isiannya)->make(true);
    }
    function auditor_ami_kesimpulan_ajax_edit(Request $request)
    {
        //for edit request post
        if ($request->kesimpulan_edit_id_value != null && $request->jadwal_id != null) {
            $check_data_json =  JenjangKesimpulanModel::where([
                'jk_id' => $request->jkk_id_value
            ])->first();




            if ($check_data_json != null) {
                $pilihan = 0;
                $json = json_decode($check_data_json->jk_json_check, true);
                foreach ($json as $key => $j) {
                    if ($j ==  $request->pilihan_edit_value) {
                        $pilihan = $key + 1;
                        break;
                    }
                }

                if ($pilihan == 0) {
                    $data = array('data' => 'tidak ada select');
                    return response::json($data, 400);
                }

                $sql = AuditKesimpulanModel::where([
                    'id' => $request->kesimpulan_edit_id_value
                ])->update([
                    'yes_no_other_check' => $pilihan,
                    'lainnya' => $request->lainnya_edit_value,

                ]);

                if ($sql) {

                    return redirect()->route('Auditor0', ['last_id' => $request->jadwal_id])->with('success', 'Berhasil');
                }
            } else {

                $check_data_json =  JenjangKesimpulanModel::where([
                    'jk_id' => (int) $request->pertanyaan_id_value
                ])->first();

                if ($check_data_json != null) {
                    $pilihan = 0;
                    $json = json_decode($check_data_json->jk_json_check, true);
                    foreach ($json as $key => $j) {
                        if ($j ==  $request->pilihan_edit_value) {
                            $pilihan = $key + 1;
                            break;
                        }
                    }

                    if ($pilihan == 0) {
                        $data = array('data' => 'tidak ada select');
                        return response::json($data, 400);
                    }
                    $sql = new AuditKesimpulanModel;
                    $sql->kesimpulan = $request->pertanyaan_id_value;
                    $sql->yes_no_other_check = $pilihan;
                    $sql->lainnya = $request->lainnya_edit_value;
                    $sql->audit_borang_id = $request->jadwal_id;

                    if ($sql->save()) {
                        return redirect()->route('Auditor0', ['last_id' => $request->jadwal_id])->with('success', 'Berhasil');
                    }

                    // $data = array('data' => 'tidak null, jadwal id');
                    // return response::json($data, 400);


                }
            }
        } else {
            $data = array('data' => 'tidak ada');
            return response::json($data, 400);
        }
    }

    function auditor_ami_saran_audit(Request $request)
    {
        $saran = new AuditSaranModel;
        $saran->bidang = $request->op_perbaikan_bidang_audit;
        $saran->kelebihan =  $request->op_perbaikan_kelebihan_audit;
        $saran->peluang_peningkatan = $request->op_perbaikan_peluang_audit;
        $saran->audit_borang_id = $request->jadwal_id;

        if ($saran->save()) {
            return redirect()->route('Auditor0', ['last_id' => $request->jadwal_id]);
        }
    }

    function auditor_ami_saran_ajax_view(Request $request)
    {
        $saran = AuditSaranModel::where([
            'audit_borang_id'   => $request->jadwal_id
        ]);

        return Datatables::of($saran)->make(true);
    }
    function auditor_ami_saran_ajax_edit(Request $request)
    {
        //for edit request post
        if ($request->saran_edit_id_value != null) {

            $sql = AuditSaranModel::where([
                'id' => $request->saran_edit_id_value
            ])->update([
                'bidang' => $request->bidang_edit_value,
                'kelebihan' => $request->kelebihan_edit_value,
                'peluang_peningkatan' => $request->peluang_edit_value,

            ]);

            if ($sql) {
                return back()->with('success', 'Berhasil');
            }
        } else {
            $data = array('data' => 'tidak');
            return response::json($data, 400);
        }
    }

    function auditor_ami_saran_ajax_delete(Request $request)
    {
        if ($request->saran_id != null) {
            $saran = AuditSaranModel::where([
                'id' => $request->saran_id
            ])->delete();

            if ($saran) {
                return back();
            }
        } else { }
    }

    function auditor_ami_temuan_ajax_reload(Request $request)
    {
        $sql2p = AuditKetidaksesuaianModel::where([
            'audit_borang_id' => $request->jadwal_id
        ])->delete();

        return back();
    }

    function auditor_ami_temuan_ajax_view(Request $request)
    {
        if ($request->jadwal_id != null) {
            $sql1 = AuditPertanyaanModel::where([
                'audit_pen_jadwal_id' => $request->jadwal_id,
                'yes_no_check'  => 2
            ])->get();

            if ($sql1 != null) {

                foreach ($sql1 as $p) {

                    $sql2 = AuditKetidaksesuaianModel::firstOrCreate([
                        'audit_pertanyaan_id' => $p->id
                    ], [
                        'audit_borang_id' => $request->jadwal_id,
                        'referensi_butir_mutu' => $p->referensi_butir_mutu,
                        'pernyataan' => $p->hasil_observasi
                    ]);
                }

                $sql2x = AuditKetidaksesuaianModel::where([
                    'audit_borang_id' => $request->jadwal_id
                ]);
            }
        } else {
            $sql2x = array();
        }

        return Datatables::of($sql2x)->make(true);
    }
    function auditor_ami_temuan_ajax_edit(Request $request)
    {
        //for edit request post
        if ($request->ketidaksesuaian_edit_id_value != null) {

            $sql = AuditKetidaksesuaianModel::where([
                'id' => $request->ketidaksesuaian_edit_id_value
            ])->update([
                'kts_ob' => $request->kts_ob_edit_value,
                'referensi_butir_mutu' => $request->temuan_edit_value,
                'pernyataan' => $request->pernyataan_edit_value
            ]);

            if ($sql) {
                return back()->with('success', 'Berhasil');
            }
        } else {
            $data = array('data' => 'tidak');
            return response::json($data, 400);
        }
    }

    function auditor_ami_temuan_ajax_delete(Request $request)
    {
        if ($request->kegiatan_id != null) {
            $kegiatan = AuditJadwalModel::where([
                'id' => $request->kegiatan_id
            ])->delete();

            if ($kegiatan) {
                return back();
            }
        } else { }
    }
    function auditor_ami_kegiatan_audit(Request $request)
    {
        $sql = new AuditJadwalModel;
        $sql->tanggal = $request->op_tanggal_audit;
        $sql->pukul = $request->op_pukul_audit;
        $sql->kegiatan_audit = $request->op_kegiatan_audit;
        $sql->audit_borang_id = $request->jadwal_id;

        if ($sql->save()) {
            $previousUrl = app('url')->previous();

            return redirect()->to($previousUrl . '?' . http_build_query(['last_id' => $request->jadwal_id]));
        }
    }

    static function auditor_ami_get_kegiatan($pen_jadwal_id, $tanggal)
    {
        $sql = AuditJadwalModel::where([
            'audit_borang_id' => $pen_jadwal_id,
            'tanggal' => $tanggal
        ])->get();

        if ($sql != null) {
            return $sql;
        } else {
            return null;
        }
    }
    function auditor_ami_kegiatan_ajax_view(Request $request)
    {
        if ($request->jadwal_id != null) {
            $lingkup = AuditJadwalModel::where([
                'audit_borang_id' => $request->jadwal_id
            ]);
        } else {
            $lingkup = array();
        }

        return Datatables::of($lingkup)->make(true);
    }
    function auditor_ami_kegiatan_ajax_edit(Request $request)
    {
        //for edit request post
        if ($request->lingkup_edit_id_value != null) {

            $sql = AuditJadwalModel::where([
                'id' => $request->jadwal_edit_id_value
            ])->update([
                'pukul' => $request->pukul_edit_value,
                'kegiatan_audit' => $request->kegiatan_edit_value,
                'audit_borang_id' => $request->jadwal_edit_id_value
            ]);

            if ($sql) {
                return back()->with('success', 'Berhasil');
            }
        } else {
            $data = array('data' => 'tidak');
            return response::json($data, 400);
        }
    }

    function auditor_ami_kegiatan_ajax_delete(Request $request)
    {
        if ($request->kegiatan_id != null) {
            $kegiatan = AuditJadwalModel::where([
                'id' => $request->kegiatan_id
            ])->delete();

            if ($kegiatan) {
                return back();
            }
        } else { }
    }

    function auditor_ami_lingkup_audit(Request $request)
    {
        $lingkup = new AuditLingkupModel;
        $lingkup->audit_lingkup = $request->op_lingkup_audit;
        $lingkup->audit_borang_id = $request->jadwal_id;
        if ($lingkup->save()) {
            $previousUrl = app('url')->previous();

            return redirect()->to($previousUrl . '?' . http_build_query(['last_id' => $request->jadwal_id]));
        }
    }
    function auditor_ami_lingkup_ajax_view(Request $request)
    {
        if ($request->jadwal_id != null) {
            $lingkup = AuditLingkupModel::where([
                'audit_borang_id' => $request->jadwal_id
            ]);
        } else {
            $lingkup = array();
        }

        return Datatables::of($lingkup)->make(true);
    }
    function auditor_ami_lingkup_ajax_edit(Request $request)
    {
        //for edit request post
        if ($request->lingkup_edit_id_value != null) {

            $sql = AuditLingkupModel::where([
                'id' => $request->lingkup_edit_id_value
            ])->update([
                'audit_lingkup' => $request->lingkup_edit_value,

            ]);

            if ($sql) {
                return back()->with('success', 'Berhasil');
            }
        } else {
            $data = array('data' => 'tidak');
            return response::json($data, 400);
        }
    }

    function auditor_ami_lingkup_ajax_delete(Request $request)
    {
        if ($request->lingkup_id != null) {
            $lingkup = AuditLingkupModel::where([
                'id' => $request->lingkup_id
            ])->delete();

            if ($lingkup) {
                return back();
            }
        }
    }

    function auditor_ami_tujuan_ajax_view(Request $request)
    {
        if ($request->jadwal_id != null) {
            $tujuan = AuditTujuanModel::where([
                'audit_jadwal_ref_id' => $request->jadwal_id
            ]);
        } else {
            $tujuan = array();
        }

        return Datatables::of($tujuan)->make(true);
    }

    function auditor_ami_tujuan_ajax_view_2(Request $request)
    {
        $tanggalan = AuditPenentuanJadwalModel::where([
            'pen_jadwal_id' => $request->pen_jadwal_id
        ])->first();

        if ($tanggalan != null) {
            $tanggal_awal = $tanggalan->pen_jadwal_tanggal;
            $tanggal_akhir = $tanggalan->pen_jadwal_tanggal_sampai;

            $arr = [
                '0' => $tanggal_awal,
                '1' => $tanggal_akhir
            ];

            return response()->json($arr);
        } else {
            print('hmm');
        }
    }

    function auditor_ami_tujuan_ajax_edit(Request $request)
    {
        //for edit request post
        if ($request->tujuan_id != null) {
            $checks = (($request->tujuan_edit_cek != null) ?  1 : 0);
            $tujuan = AuditTujuanModel::where([
                'id' => $request->tujuan_id
            ])->update([
                'tujuan' => $request->tujuan_edit_isian,
                'yes_no_check' => $checks
            ]);

            if ($tujuan) {
                return back()->with('success', 'Berhasil');
            }
        } else {
            if ($request->jadwal_id != null && $request->tujuan_id != null) {
                $tujuan = AuditTujuanModel::where([
                    'audit_jadwal_ref_id' => $request->jadwal_id,
                    'id' => $request->tujuan_id
                ])->get();

                return $tujuan;
            } else if ($request->jadwal_id != null) {
                $tujuan = AuditTujuanModel::where([
                    'audit_jadwal_ref_id' => $request->jadwal_id
                ])->get();
            } else {
                $data = array('data' => 'tidak');
                return response::json($data, 400);
            }
        }
    }

    function auditor_ami_tujuan_ajax_delete(Request $request)
    {
        if ($request->tujuan_id != null) {
            $tujuan = AuditTujuanModel::where([
                'id' => $request->tujuan_id
            ])->delete();

            return back();
        }
    }

    function auditor_ami_tujuan_audit(Request $request)
    {
        $tujuan                         = new AuditTujuanModel;
        $tujuan->tujuan                 = $request->op_tujuan;
        if ($request->op_check == 'ok') {
            $tujuan->yes_no_check       = 1;
        } else {
            $tujuan->yes_no_check       = 0;
        }
        $tujuan->audit_jadwal_ref_id    = $request->op_tujuan_jadwal_id;

        if ($tujuan->save()) {
            $previousUrl = app('url')->previous();

            return redirect()->to($previousUrl . '?' . http_build_query(['last_id' => $request->op_tujuan_jadwal_id]));
        }
    }


    function auditor_ami_get_info_laporan(Request $request)
    {
        $gabungan = [];

        $prodi_id = AuditPenentuanJadwalModel::where([
            'pen_jadwal_id' => $request->pen_jadwal_id
        ])->first();

        if ($prodi_id != null) {
            $mentan = ProfilModel::where(['id' => $prodi_id->pen_jadwal_prodi_id])->first();



            $ketua_auditor_info = AuditPenentuanJadwalAuditorModel::join('users', 'users.id', '=', 'tbl_audit_borang_penentuan_jadwal_listauditor.pen_jadwal_list_auditor_user_id')
                ->where([
                    'pen_jadwal_list_auditor_ref_id' => $request->pen_jadwal_id,
                    'pen_jadwal_list_auditor_user_status' => 1
                ])->first();

            //dd($ketua_auditor_info);

            $auditor_info = AuditPenentuanJadwalAuditorModel::join('users', 'users.id', '=', 'tbl_audit_borang_penentuan_jadwal_listauditor.pen_jadwal_list_auditor_user_id')
                ->where([
                    'pen_jadwal_list_auditor_ref_id' => $request->pen_jadwal_id,
                    'pen_jadwal_list_auditor_user_status' => 2
                ])->get();

            $gabungan[0] = $prodi_id;
            $gabungan[1] = $mentan;
            $gabungan[2] = $ketua_auditor_info;
            $gabungan[3] = $auditor_info;


            return Response::json($gabungan);
        } else {
            return 'none';
        }
    }

    static function auditor_ami_program_studi_getProfil($profil_id)
    {
        $profil = ProfilModel::where(['id' => $profil_id])->first();

        return $profil;
    }

    function auditor_ami_program_studi(Request $request)
    {
        if ($request->last_id != null) {
            $jadwal_id = (int) $request->last_id;
        } else {
            $jadwal_id = null;
        }

        $auditPenentuanJadwal = AuditPenentuanJadwalModel::get();

        return view('ami.laporan_audit', [
            'list_jadwal_audit' => $auditPenentuanJadwal,
            'jadwal_id' => $jadwal_id
        ]);
    }


    //Auditor saat ini wwkk
    function kjm_step_emi_post_nilai_capaian(Request $request, $profil_id = null, $standar_emi = null, $tahun_emi = null)
    {
        $bobot = EmiBobotNilaiModel::where([
            'isian_standar_id' => $request->isian_standar,
            'profil_id' => $profil_id,
            'tahun_emi' => $tahun_emi
        ])->first();

        if ($bobot == null) {
            //buat daftar nilai baru
            $bobot = new EmiBobotNilaiModel;

            $bobot->isian_standar_id = $request->isian_standar;
            $bobot->nilai = $request->keadaan_nilai_value;
            $bobot->profil_id = $profil_id;
            $bobot->tahun_emi = $tahun_emi;

            if ($bobot->save()) {
                return back();
            } else {
                return 'hmm';
            }
        } else {
            $bobot = EmiBobotNilaiModel::where([
                'isian_standar_id' => $request->isian_standar,
                'profil_id' => $profil_id,
                'tahun_emi' => $tahun_emi
            ])->update([
                'nilai' => $request->keadaan_nilai_value

            ]);

            if ($bobot != null) {
                return back();
            } else {
                return 'hmm';
            }
        }
    }

    function kjm_step_emi_rubrik_save(Request $request, $jenjang_id = null, $standar_emi = null, $isian_standar = null)
    {
        //$profil_jenjang_id = ProfilModel::where(['id' => $profil_id])->first()->jenjang_plot_id;
        $keadaan = EmiKeadaanModel::updateOrCreate([


            'emi_jenjang_id' => $jenjang_id,
            'emi_standar_id' => $standar_emi,
            'emi_isian_standar_id' => $isian_standar,
            'emi_bobot_nilai' => $request->set_emi_bobot_nilai,
        ], [
            'keadaan_prodi_saat_ini' => $request->set_keadaan_prodi_saat_ini
        ]);

        if ($keadaan != null) {
            return back();
        } else {
            return 'hmm';
        }
    }

    function kjm_step_emi_rubrik(Request $request, $jenjang_id = null, $standar_emi = null, $isian_standar = null)
    {
        $jenjang = JenjangModel::all();
        // $profil_prodi = ProfilModel::all();
        // if ($profil_id != null) {
        //     $get_profil_prodi = ProfilModel::join('tbl_jenjang','tbl_jenjang.jenjang_id','=','tbl_profil.jenjang_plot_id')->
        //     where(['id' => $profil_id])->first();
        // } else {
        //     $get_profil_prodi = ProfilModel::join('tbl_jenjang','tbl_jenjang.jenjang_id','=','tbl_profil.jenjang_plot_id')->
        //     where(['id' => Session::get('profil_id')])->first();
        // }

        //$tahun_pengukuran = EmiIsianStandarModel::select("emi_tahun_standar")->distinct("emi_tahun_standar")->get();
        $standar_mutu = EmiStandarModel::all();
        $jenjang_id = $jenjang_id;



        if ($isian_standar != null) {
            $isian_standar_mutu = EmiIsianStandarModel::where([

                'emi_jenjang_id' => $jenjang_id,
                'emi_standar_id'    => $standar_emi,

            ])->get();


            if ($isian_standar_mutu != null) {
                $keadaan = EmiKeadaanModel::where([
                    'emi_jenjang_id' => $jenjang_id,
                    'emi_standar_id'    => $standar_emi,
                    'emi_isian_standar_id' => $isian_standar
                ])->get();


                return view('step-kjm.tahap_emi_rubrik', [
                    //'profil_list' => $profil_prodi,
                    //'profil_id' =>  $profil_id,
                    'jenjang_id' => $jenjang_id,
                    'jenjang_list' => $jenjang,
                    'standar_emi' => $standar_emi,
                    'standar_mutu' => $standar_mutu,
                    'isian_standar_mutu' => $isian_standar_mutu,
                    'keadaan' => $keadaan,
                    'isian_standar' => $isian_standar


                ]);
            } else {
                return view('step-kjm.tahap_emi_rubrik', [
                    //'profil_list' => $profil_prodi,
                    //'profil_id' =>  $profil_id,
                    'jenjang_id' => $jenjang_id,
                    'jenjang_list' => $jenjang,
                    'standar_emi' => $standar_emi,
                    'standar_mutu' => $standar_mutu,
                    'isian_standar_mutu' => $isian_standar,
                    'keadaan' => null,
                    'isian_standar' => $isian_standar


                ]);
            }
        } else {
            $isian_standar_mutu = EmiIsianStandarModel::where([
                'emi_jenjang_id' => $jenjang_id,
                'emi_standar_id'    => $standar_emi,
            ])->get();

            return view('step-kjm.tahap_emi_rubrik', [
                // 'profil_list' => $profil_prodi,
                //'profil_id' =>  $profil_id,
                'jenjang_id' => $jenjang_id,
                'jenjang_list' => $jenjang,
                'standar_emi' => $standar_emi,
                'isian_standar_mutu' => $isian_standar_mutu,
                'keadaan' => null,
                'standar_mutu' => $standar_mutu,
                'isian_standar' => null
            ]);
        }
    }

    function kjm_step_emi_rubrik_redirect(Request $request)
    {
        $profil_id = $request->input_rubrik_profil_id;
        $tahun_pengukuran = $request->set_emi_tahun_standar;
        $standar_mutu = $request->set_emi_standar_mutu;



        return redirect()->route('EmiRubrikSelection0', [
            'profil_id' => $profil_id,

            'standar_emi' => $standar_mutu,

        ]);
    }

    function kjm_step_emi_selection(Request $request, $standar_emi)
    {
        $profil_prodi = ProfilModel::where(['id' => $request->set_emi_program_studi])->first();
        if ($profil_prodi) {
            $tahun_pengukuran = $request->set_emi_tahun_standar;
            session(['profil_id' => $profil_prodi->id]);

            return redirect()->route('EmiSementara', ['profil_id' => $profil_prodi->id, 'standar_emi' => $standar_emi, 'tahun_emi' => $tahun_pengukuran]);
        }
    }

    function kjm_step_emi_save(Request $request, $profil_id = null, $standar_emi = null, $tahun_emi = null)
    {
        if ($profil_id != null) {
            $get_profil_prodi = ProfilModel::join('tbl_jenjang', 'tbl_jenjang.jenjang_id', '=', 'tbl_profil.jenjang_plot_id')->where(['id' => $profil_id])->first();
        }

        $jenjang_id = $get_profil_prodi->jenjang_plot_id;

        $isian = new EmiIsianStandarModel;
        // $isian->emi_profil_id = $request->input_emi_program_studi;
        // $isian->emi_tahun_standar = $request->input_emi_tahun_standar;
        $isian->emi_jenjang_id = $get_profil_prodi->jenjang_plot_id;
        $isian->emi_standar_id = $standar_emi;
        $isian->emi_bobot_persen = $request->input_emi_bobot_terbobot;
        $isian->emi_capaian_terbobot = $request->input_emi_capaian_terbobot;
        $isian->emi_isian_standar = $request->input_emi_tentang_standar;

        if ($isian->save()) {

            return redirect()->route('EmiSementara', ['profil_id' => $profil_id, 'standar_emi' => $standar_emi, 'tahun_emi' => $request->input_emi_tahun_standar]);
            //return back();
        }
    }

    function kjm_step_emi(Request $request, $profil_id, $stadar_emi, $tahun_emi = null)
    {
        $jenjang = JenjangModel::all();
        $profil_prodi = ProfilModel::all();
        if ($profil_id != null) {
            $get_profil_prodi = ProfilModel::join('tbl_jenjang', 'tbl_jenjang.jenjang_id', '=', 'tbl_profil.jenjang_plot_id')->where(['id' => $profil_id])->first();
        } else {
            $get_profil_prodi = ProfilModel::join('tbl_jenjang', 'tbl_jenjang.jenjang_id', '=', 'tbl_profil.jenjang_plot_id')->where(['id' => Session::get('tbl_profil.profil_id')])->first();
        }
        //dd($profil_id);  
        $jenjang_id = $get_profil_prodi->jenjang_plot_id;

        $profil_prodi = ProfilModel::all();
        $tahun_pengukuran = EmiBobotNilaiModel::select("tahun_emi")->distinct("tahun_emi")->get();

        $emi = EmiStandarModel::where(['id' => $stadar_emi])->first();

        if ($tahun_emi != null) {
            $emi_standar_list = EmiStandarModel::join('tbl_emi_isian_standar', 'tbl_emi_isian_standar.emi_standar_id', '=', 'tbl_emi_standar.id')


                ->where([
                    'emi_standar_id' => $stadar_emi,
                    'emi_jenjang_id' => $jenjang_id
                ])
                ->select('*', 'tbl_emi_isian_standar.id as isian_id')
                ->get();
        } else {
            $emi_standar_list = EmiStandarModel::join('tbl_emi_isian_standar', 'tbl_emi_isian_standar.emi_standar_id', '=', 'tbl_emi_standar.id')


                ->where([
                    'emi_standar_id' => $stadar_emi,
                    'emi_jenjang_id' => $jenjang_id

                ])
                ->select('*', 'tbl_emi_isian_standar.id as isian_id')
                ->get();
        }


        return view('step-kjm.tahap_emi', [
            'nama_standar' => $emi, 'standar_list' => $emi_standar_list, 'profil_list' => $profil_prodi, 'profil_id' => $profil_id, 'tahun_emi' => $tahun_emi,
            'jenjang_list' => $jenjang, 'jenjang_id' => $jenjang_id, 'tahun_pengukuran_list' => $tahun_pengukuran

        ]);
    }

    function kjm_step_emi_view(Request $request)
    { }

    static function kjm_step_emi_bobot_nilai($isian_standar_id)
    {
        $bobot = EmiBobotNilaiModel::where(['isian_standar_id' => $isian_standar_id])->first();
        return $bobot;
    }

    static function kjm_step_emi_keadaan_prodi($isian_standar_id, $bobot_nilai)
    {
        $keadaan = EmiKeadaanModel::where(['emi_isian_standar_id' => $isian_standar_id, 'emi_bobot_nilai' => $bobot_nilai])->first();
        return $keadaan;
    }

    function auditor_step_administrasi(Request $request)
    {
        $adm = AdministrasiModel::get();
        return view('step-kjm.tahap_administrasi', ['berkas' => $adm]);
    }

    function auditor_step_administrasi_save(Request $request)
    {
        foreach ($request->file() as $rf) {

            $destinationPath = public_path() . '/unggah/unggah_administrasi/';

            $permintaan_file_name =  md5(md5(md5($rf->getClientOriginalName())) . rand(0, 999)) . "." . $rf->getClientOriginalExtension();
            $rf->move($destinationPath, $permintaan_file_name);

            $ptk_file = new AdministrasiModel;
            $ptk_file->adm_nama = $request->adm_upload_name;
            $ptk_file->adm_file = $permintaan_file_name;
            $ptk_file->adm_tanggal = $request->adm_upload_tgl;
            if ($ptk_file->save()) { } else {
                return 'x';
            }
        }
        return redirect()->route('Adm_index');
    }

    function auditor_step_ptk_index(Request $request)
    {
        // $auditor_ptk = AuditPTKModel::join('tbl_audit_borang_penentuan_jadwal','tbl_audit_borang_penentuan_jadwal.pen_jadwal_id','=','tbl_audit_ptk.audit_ptk_ref_id')
        // ->get();
        //save data penugasan


        $ListJadwalAudit = AuditPenentuanJadwalModel::join('tbl_profil', 'tbl_profil.id', '=', 'tbl_audit_borang_penentuan_jadwal.pen_jadwal_prodi_id')
            ->get();

        return view('step-kjm.tahap_ptk_upload', ['ptkmodel' => $ListJadwalAudit]);
    }

    static function auditor_step_ptk_list_data($jadwal_id)
    {
        // $auditor_ptk = AuditPTKModel::join('tbl_audit_borang_penentuan_jadwal','tbl_audit_borang_penentuan_jadwal.pen_jadwal_id','=','tbl_audit_ptk.audit_ptk_ref_id')
        // ->get();

        $ListBerkasAudit = AuditPTKModel::where(['audit_ptk_ref_id' => $jadwal_id])
            ->get();

        return $ListBerkasAudit;
    }

    function auditor_step_ptk_submit(Request $request)
    {
        foreach ($request->file() as $rf) {

            $destinationPath = public_path() . '/unggah/unggah_ptk/';

            $permintaan_file_name =  md5(md5(md5($rf->getClientOriginalName())) . rand(0, 999)) . "." . $rf->getClientOriginalExtension();
            $rf->move($destinationPath, $permintaan_file_name);

            $ptk_file = new AuditPTKModel;
            $ptk_file->audit_ptk_nama = $request->ptk_upload_name;
            $ptk_file->audit_ptk_file = $permintaan_file_name;
            $ptk_file->audit_ptk_ref_id = $request->ptk_upload_jadwal_id;
            if ($ptk_file->save()) { } else {
                return 'x';
            }
        }
        return redirect()->route('PTK_index');
    }

    function auditor_step_daftar_pertanyaan_view(Request $request)
    {
        if ($request->lingkup_id != null) {
            $pertanyaanAuditor = AuditPertanyaanModel::join('tbl_audit_borang_penentuan_jadwal', 'tbl_audit_borang_penentuan_jadwal.pen_jadwal_id', '=', 'tbl_audit_pertanyaan.audit_pen_jadwal_id')
                ->join('tbl_audit_borang_penentuan_jadwal_lingkup', 'tbl_audit_borang_penentuan_jadwal_lingkup.pen_lingkup_id', '=', 'tbl_audit_pertanyaan.audit_pen_lingkup_id')
                ->where([
                    'pen_jadwal_tanggal' =>  $request->audit_tanggal,
                    'pen_lingkup_id' =>  $request->lingkup_id
                ])
                ->get();
        } else {
            $pertanyaanAuditor = AuditPertanyaanModel::join('tbl_audit_borang_penentuan_jadwal', 'tbl_audit_borang_penentuan_jadwal.pen_jadwal_id', '=', 'tbl_audit_pertanyaan.audit_pen_jadwal_id')
                ->join('tbl_audit_borang_penentuan_jadwal_lingkup', 'tbl_audit_borang_penentuan_jadwal_lingkup.pen_lingkup_ref_id', '=', 'tbl_audit_borang_penentuan_jadwal.pen_jadwal_id')
                ->where(['pen_jadwal_tanggal' =>  $request->audit_tanggal])
                ->get();
        }


        return Datatables::of($pertanyaanAuditor)->make(true);
    }
    //untuk daftar pertanyaan

    function auditor_step_view_tabel_jadwal(request $request)
    {
        $headerAuditor = AuditPenentuanJadwalModel::join('tbl_profil', 'tbl_profil.id', '=', 'tbl_audit_borang_penentuan_jadwal.pen_jadwal_prodi_id')
            ->join('tbl_audit_borang_penentuan_jadwal_lingkup', 'tbl_audit_borang_penentuan_jadwal_lingkup.pen_lingkup_id', '=', 'tbl_audit_borang_penentuan_jadwal.pen_jadwal_lingkup_audit')
            ->where(['pen_jadwal_tanggal' =>  $request->audit_tanggal])
            ->first();

        if ($headerAuditor == null) {
            return abort(404);
        } else {
            return $headerAuditor;
        }
    }

    function auditor_step_view_tabel_auditor(request $request)
    {
        $headerAuditor = AuditPenentuanJadwalModel::join('tbl_audit_borang_penentuan_jadwal_listauditor', 'tbl_audit_borang_penentuan_jadwal_listauditor.pen_jadwal_list_auditor_ref_id', '=', 'tbl_audit_borang_penentuan_jadwal.pen_jadwal_id')
            ->join('users', 'users.id', '=', 'tbl_audit_borang_penentuan_jadwal_listauditor.pen_jadwal_list_auditor_user_id')
            ->where('pen_jadwal_tanggal', '=', $request->audit_tanggal)
            ->get();


        return Datatables::of($headerAuditor)->make(true);
    }

    function auditor_step_view_tabel_auditee(request $request)
    {
        $headerAuditee =  AuditPenentuanJadwalAuditeeModel::join('users', 'users.id', '=', 'tbl_audit_borang_penentuan_jadwal_listauditee.pen_jadwal_list_auditee_user_id')
            ->where('pen_jadwal_list_auditee_ref_id', '=', $request->jadwal_id)
            ->get();
        //join('tbl_audit_borang_penentuan_jadwal_listauditee','tbl_audit_borang_penentuan_jadwal_listauditee.pen_jadwal_list_auditee_ref_id','=','tbl_audit_borang_penentuan_jadwal.pen_jadwal_id')
        //->

        return Datatables::of($headerAuditee)->make(true);
    }

    //selesai untuk daftar pertanyaan

    function auditor_step_daftar_pertanyaan_auditor_view(Request $request)
    {
        $tujuan = AuditTujuanModel::where([
            'audit_jadwal_ref_id' => $request->pen_jadwal_id
        ]);

        $auditLihatAuditor = AuditPenentuanJadwalModel::join('tbl_profil', 'tbl_profil.id', '=', 'tbl_audit_borang_penentuan_jadwal.pen_jadwal_prodi_id')
            ->join('tbl_audit_borang_penentuan_jadwal_listauditor', 'tbl_audit_borang_penentuan_jadwal_listauditor.pen_jadwal_list_auditor_ref_id', '=', 'tbl_audit_borang_penentuan_jadwal.pen_jadwal_id')
            ->join('users', 'users.id', '=', ' tbl_audit_borang_penentuan_jadwal_listauditor.pen_jadwal_list_auditor_user_id')
            ->where('pen_jadwal_tanggal', '=', $request->audit_tanggal)
            ->get();


        return Datatables::of($auditLihatAuditor)->make(true);
    }

    function auditor_step_daftar_pertanyaan_edit(Request $request)
    { }

    function auditor_step_daftar_pertanyaan_update(Request $request)
    { }


    function auditor_step_daftar_pertanyaan_audit(Request $request)
    {
        $auditPenentuanJadwal = AuditPenentuanJadwalModel::distinct('pen_jadwal_tanggal')->get();
        $auditPertanyaan = AuditJadwalLingkupModel::get();


        return view('step-kjm.tahap_auditor_daftar_pertanyaan', ['audit_tanggal' => $auditPenentuanJadwal, 'audit_pertanyaan' => $auditPertanyaan]);
    }

    function auditor_step_data_pelaksana_audit(Request $request)
    {
        $auditPenentuanJadwal = AuditPenentuanJadwalModel::distinct('pen_jadwal_tanggal')->get();

        return view('step-kjm.tahap_data_pelaksana', ['audit_tanggal' => $auditPenentuanJadwal]);
    }

    function auditor_step_data_pelaksana_view(Request $request)
    {
        $auditPenentuanJadwal = AuditPenentuanJadwalModel::join('tbl_profil', 'tbl_profil.id', '=', 'tbl_audit_borang_penentuan_jadwal.pen_jadwal_prodi_id')
            ->join('tbl_audit_borang_penentuan_jadwal_listauditor', 'tbl_audit_borang_penentuan_jadwal_listauditor.pen_jadwal_list_auditor_ref_id', '=', 'tbl_audit_borang_penentuan_jadwal.pen_jadwal_id')
            ->join('users', 'users.id', '=', 'tbl_audit_borang_penentuan_jadwal_listauditor.pen_jadwal_list_auditor_user_id')
            ->where('pen_jadwal_tanggal', '=', $request->audit_tanggal)
            ->get();

        // return $auditPenentuanJadwal;

        return Datatables::of($auditPenentuanJadwal)->make(true);
    }


    function auditor_step_penentuan_jadwal_index(Request $request)
    {
        return view('step-kjm.tahap_penentuan_jadwal');
    }

    function auditor_step_penentuan_jadwal_listing(Request $request)
    {
        $auditPenentuanJadwal = AuditPenentuanJadwalModel::join('tbl_profil', 'tbl_profil.id', '=', 'tbl_audit_borang_penentuan_jadwal.pen_jadwal_prodi_id')
            ->join('tbl_audit_borang_penentuan_jadwal_listauditor', 'tbl_audit_borang_penentuan_jadwal_listauditor.pen_jadwal_list_auditor_ref_id', '=', 'tbl_audit_borang_penentuan_jadwal.pen_jadwal_id')
            ->join('users', 'users.id', '=', 'tbl_audit_borang_penentuan_jadwal_listauditor.pen_jadwal_list_auditor_user_id')
            ->get();

        return Datatables::of($auditPenentuanJadwal)->make(true);
    }

    function auditor_step_penentuan_jadwal_create(Request $request)
    {
        $profil_prodi = ProfilModel::all();

        return view('step-kjm.tahap_penentuan_jadwal_create', ['profil_prodi' => $profil_prodi]);
    }


    function auditor_step_penentuan_jadwal_save(Request $request)
    {
        //simpan bagian si tabel jadwal dulu
        $destinationPath = public_path() . '/unggah/unggah_surat_penugasan';

        $auditPenentuanJadwal                               = new AuditPenentuanJadwalModel;
        $auditPenentuanJadwal->pen_jadwal_prodi_id          = $request->penentuan_jadwal_prodi_audit;
        $auditPenentuanJadwal->pen_jadwal_tanggal           = $request->penentuan_jadwal_tanggal_audit;
        $auditPenentuanJadwal->pen_jadwal_pukul             = $request->penentuan_jadwal_pukul_audit;
        // $auditPenentuanJadwal->pen_jadwal_lingkup_audit     = $request->penentuan_jadwal_lingkup_audit;

        //save data penugasan
        $rf                                                 =   $request->file("penentuan_jadwal_surat_tugas_audit");

        $permintaan_file_name =  md5(md5(md5($rf->getClientOriginalName())) . rand(0, 999)) . "." . $rf->getClientOriginalExtension();
        $rf->move($destinationPath, $permintaan_file_name);

        $auditPenentuanJadwal->pen_jadwal_upload_surat_tugas = $permintaan_file_name;

        if ($auditPenentuanJadwal->save()) {
            $audit_lingkup_jadwal = new AuditJadwalLingkupModel;
            $audit_lingkup_jadwal->pen_lingkup_ref_id   = $auditPenentuanJadwal->id;
            $audit_lingkup_jadwal->pen_lingkup_isian    = $request->penentuan_jadwal_lingkup_audit;

            $auditPenentuanJadwal = $auditPenentuanJadwal->id;

            if ($audit_lingkup_jadwal->save()) {
                AuditPenentuanJadwalModel::where(['pen_jadwal_id' => $auditPenentuanJadwal])->update(['pen_jadwal_lingkup_audit' => $audit_lingkup_jadwal->id]);
            } else {
                return 'stop error lingkup';
            }

            //simpan bagian isi auditor
            $id_penentuanJadwal = $auditPenentuanJadwal->id;

            foreach ($request->penentuan_jadwal_auditor_audit as $key => $pja) {
                $auditPenentuanAuditor = new AuditPenentuanJadwalAuditorModel;
                $auditPenentuanAuditor->pen_jadwal_list_auditor_ref_id      = $id_penentuanJadwal;
                $auditPenentuanAuditor->pen_jadwal_list_auditor_user_id     = $pja;
                $auditPenentuanAuditor->pen_jadwal_list_auditor_user_status = $request->penentuan_jadwal_status_auditor_audit[$key];

                if (!$auditPenentuanAuditor->save()) {
                    return "stop jadwal auditor save";
                }
            }

            foreach ($request->penentuan_jadwal_auditee_audit as $key => $pja) {
                $auditPenentuanAuditee = new AuditPenentuanJadwalAuditeeModel;
                $auditPenentuanAuditee->pen_jadwal_list_auditee_ref_id      = $id_penentuanJadwal;
                $auditPenentuanAuditee->pen_jadwal_list_auditee_user_id     = $pja;

                if (!$auditPenentuanAuditee->save()) {
                    return "stop jadwal auditee save";
                }
            }


            //return "berhasil disimpan";

            return redirect()->route('PenentuanJadwalIndex');
        } else {
            return "stop jadwal save";
        }




        //simpan bagian isi auditee
    }

    function auditor_step_permohonan_index(Request $request)
    {
        return view('step-kjm.tahap_permohonan');
    }

    function auditor_step_permohonan_buka_berkas(Request $request, $permintaan_id)
    {
        //dibagi dua lihat pesan dari kjm-auditor, terus kebalikannya
        $auditPermintaan = AuditPermintaanFileModel::where([
            'per_auditor_permintaan_id' => $permintaan_id
        ])->first();
        $file_permohonan = $auditPermintaan->per_auditor_nama_file;

        $destinationPath = public_path() . '/unggah/unggah_permintaan';


        //update kalau dia udah kebaca
        $auditPermintaan = AuditPermintaanModel::where([
            'permintaan_id' => $permintaan_id
        ])->update([
            'permintaan_status_read' => 1
        ]);



        //ambil si public filenya...

        return response()->file($destinationPath . "/" . $file_permohonan);
    }

    function auditor_step_permohonan_surat_tugas(Request $request, $permintaan_id)
    {
        //dibagi dua lihat pesan dari kjm-auditor, terus kebalikannya
        $auditPermintaan = AuditPenentuanJadwalModel::where([
            'pen_jadwal_id' => $permintaan_id
        ])->first();
        $file_permohonan = $auditPermintaan->pen_jadwal_upload_surat_tugas;

        $destinationPath = public_path() . '/unggah/unggah_surat_penugasan';

        //ambil si public filenya...

        return response()->file($destinationPath . "/" . $file_permohonan);
    }

    function auditor_step_view_read_pemohonan(Request $request)
    {
        $permintaanView = AuditPermintaanModel::join('tbl_profil', 'tbl_profil.id', '=', 'tbl_audit_borang_permintaan.permintaan_profil_id')
            ->join('tbl_audit_borang_permintaan_auditor_file', 'tbl_audit_borang_permintaan_auditor_file.per_auditor_permintaan_id', '=', 'tbl_audit_borang_permintaan.permintaan_id')
            ->where('permintaan_status_read', '=', '0')
            ->select("*", "tbl_audit_borang_permintaan.created_at as surat_dibuat_pada")
            ->get();

        return Datatables::of($permintaanView)->make(true);
    }


    function auditor_step_permintaan_audit_index(Request $request)
    {
        return view('step-kjm.tahap_persiapan_tabel');
    }

    function auditor_step_permintaan_audit_index_add(Request $request)
    {
        $profil_prodi = ProfilModel::all();

        return view('step-kjm.tahap_persiapan', ['profil_prodi' => $profil_prodi]);
    }

    function auditor_step_permintaan_audit_save(Request $request)
    {
        if ($request->file('permintaan_audit_upload_surat') != null) {
            if ($request->has('permintaan_audit_nama')) {
                $permintaan                         = new AuditPermintaanModel;
                $permintaan->permintaan_nama_audit  = $request->permintaan_audit_nama;
                $permintaan->permintaan_profil_id   = $request->permintaan_audit_prodi;

                if ($permintaan->save()) {
                    //upload file..
                    $permintaan_audit_id = $permintaan->id;

                    $destinationPath = public_path() . '/unggah/unggah_permintaan';

                    foreach ($request->file() as $key => $rf) {


                        $permintaan_file_name =  md5(md5(md5($rf->getClientOriginalName()))) . "." . $rf->getClientOriginalExtension();
                        $rf->move($destinationPath, $permintaan_file_name);



                        $permintaan_upload_berkas                               = new AuditPermintaanFileModel;
                        $permintaan_upload_berkas->per_auditor_permintaan_id    = $permintaan_audit_id;
                        $permintaan_upload_berkas->per_auditor_nama_file        = $permintaan_file_name;

                        if ($permintaan_upload_berkas->save()) {

                            return redirect()->route('auditor_permintaan_audit')->with('success', 'Permohonan berhasil');
                            // foreach ($request->auditor_user_id as $aud) {
                            //     $permintaan_auditor = new AuditPermintaanAnggotaModel;
                            //     $permintaan_auditor->per_audit_permintaan_id = $permintaan_audit_id;    
                            //     $permintaan_auditor->per_audit_anggota_id    = $aud;

                            //     if (!$permintaan_auditor->save()) {
                            //         return "save error3";
                            //     } else {
                            //         return redirect()->route('auditor_permintaan_audit');
                            //     }
                            // } //end foreach

                        } else {
                            return 'save error2';
                        }
                    }        //end foreach

                } else {
                    return 'save error1';
                }
            }
        }
    }

    function auditor_step_view_user(Request $request)
    {
        $userView = UserModel::where(['level' => 1])->get();



        return Datatables::of($userView)->make(true);
    }

    function auditor_step_view_permintaan(Request $request)
    {
        $permintaanView = AuditPermintaanModel::join('tbl_profil', 'tbl_profil.id', '=', 'tbl_audit_borang_permintaan.permintaan_profil_id')
            ->join('tbl_audit_borang_permintaan_auditor_file', 'tbl_audit_borang_permintaan_auditor_file.per_auditor_permintaan_id', '=', 'tbl_audit_borang_permintaan.permintaan_id')
            ->select("*", "tbl_audit_borang_permintaan.created_at as surat_dibuat_pada")
            ->get();

        return Datatables::of($permintaanView)->make(true);
    }



    //Auditor dulu

    function auditor_audit_mutu_internal_index(Request $request)
    {
        $auditborang    = AuditBorangModel::all();
        $userListBorang = UserModel::all();

        return view(
            'home/operator_auditor_ami',
            [
                'state'         => 'create',
                'auditborang'   => $auditborang,
                'userList'      => $userListBorang
            ]
        );
    }

    function auditor_mutu_internal_create(Request $request)
    {
        if ($request->has('tabel_anggota_auditor_length')) {
            //$data_anggota = json_decode(json_encode($request->data_anggota,true));


            $auditBorang = new AuditBorangModel;
            $auditBorang->tanggalan = $request->op_tanggalan_audit;

            $explode_ketua_auditor = explode("/", $request->op_ketua_auditor);
            $explode_ketua_user_id = explode("/", $request->op_ketua_unit);

            $auditBorang->auditee_user_id = $this->auditor_audit_mutu_internal_getUserID($explode_ketua_auditor[0]);
            $auditBorang->ketua_user_id = $this->auditor_audit_mutu_internal_getUserID($explode_ketua_user_id[0]);

            $auditBorang->alamat                = $request->op_alamat_data;
            $auditBorang->audit_program_unit    = $request->op_audit_prog_unit;
            $auditBorang->fakultas_direktorat   = $request->op_fakultas_direktorat;
            $auditBorang->program_studi         = $request->op_program_unit;
            $auditBorang->telepon_program       = $request->op_telepon_prog_unit;
            $auditBorang->telepon_audit_program = $request->op_telp_audit_prog_unit;

            if ($auditBorang->save()) {
                //anggota udah ada dari awal jadi nggak usah disimpan lagi sebenarnya, nanti diubah kalau di data auditor
                //balik ke fungsi edit ..
                $pesan_json = array("message" => "ok", "redirect" => route('audit_borang_edit', ['audit_borang_id' => $auditBorang->id]));

                return json_encode($pesan_json);
            }
        }
    }

    function auditor_audit_user_list(Request $request)
    {
        $userListBorang = UserModel::select(['id', 'name'])->get();
        return $userListBorang;
    }


    function auditor_audit_mutu_internal_edit(Request $request, $audit_borang_id)
    {
        $auditborang                = AuditBorangModel::all();
        $userListBorang             = UserModel::all();
        $auditpendahuluanborang     = AuditBorangModel::where('id', '=', $audit_borang_id)->first();

        return view(
            'home/operator_auditor_ami',
            [
                'state'             => 'edit',
                'audit_borang_id'   => $audit_borang_id,
                'auditborang'       => $auditborang,
                'userList'          => $userListBorang,
                'auditPendahuluan'  => $auditpendahuluanborang
            ]
        );
    }

    function auditor_audit_mutu_internal_tujuan_delete(Request $request, $audit_borang_id)
    {
        $auditTujuan = AuditTujuanModel::where('audit_borang_id', '=', $audit_borang_id)
            ->where('id', '=', $request->tujuan_id)->delete();
        return $auditTujuan;
    }

    function auditor_audit_mutu_internal_tujuan_edit(Request $request, $audit_borang_id)
    {
        if ($request->tujuan_edit_cek != null) {
            $sudah_jalan = 1;
        } else {
            $sudah_jalan = 0;
        }

        $auditTujuan = AuditTujuanModel::where('audit_borang_id', '=', $audit_borang_id)
            ->where('id', '=', $request->tujuan_edit_ids)
            ->update([
                'tujuan' => $request->tujuan_edit_isian,
                'yes_no_check' => $sudah_jalan
            ]);

        return $auditTujuan;
    }

    function auditor_ami_tujuan_create(Request $request, $audit_borang_id)
    {
        $auditTujuan = new AuditTujuanModel();
        $auditTujuan->tujuan = $request->op_tujuan;
        if ($request->op_check == 'ok') {
            $auditTujuan->yes_no_check = 1;
        } else {
            $auditTujuan->yes_no_check = 0;
        }

        $auditTujuan->audit_borang_id = $audit_borang_id;

        if ($auditTujuan->save()) {
            return "sip";
        }
    }

    function auditor_ami_tujuan_view(Request $request, $audit_borang_id)
    {
        if ($request->search['value'] != null) {
            $auditTujuan = AuditTujuanModel::where('audit_borang_id', '=', $audit_borang_id)
                ->where('tujuan', 'like', '%' . $request->search['value'] . '%')
                ->get();

            $iFilter = AuditTujuanModel::where('audit_borang_id', '=', $audit_borang_id)
                ->where('tujuan', 'like', '%' . $request->search['value'] . '%')
                ->count();

            $iTotal = AuditTujuanModel::where('audit_borang_id', '=', $audit_borang_id)->count();
        } else {

            $auditTujuan = AuditTujuanModel::where('audit_borang_id', '=', $audit_borang_id)->get();
            $iTotal = AuditTujuanModel::where('audit_borang_id', '=', $audit_borang_id)->count();
            $iFilter = $iTotal;
        }

        $sEcho = intval($request->sEcho);

        $output = array(

            "sEcho" => intval($sEcho),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilter,
            "aaData" => $auditTujuan
        );

        return json_encode($output);
    }

    function auditor_ami_pertanyaan_create(Request $request)
    {
        $auditPertanyaan = new AuditPertanyaanModel();
        $auditPertanyaan->referensi_butir_mutu = $request->op_pertanyaan_referensi;
        $auditPertanyaan->hasil_observasi = $request->op_pertanyaan_hasilobs;

        if ($request->op_pertanyaan_ya == 'ok') {
            $auditPertanyaan->yes_no_check = 1;
        } else if ($request->op_pertanyaan_tidak == 'ok') {
            $auditPertanyaan->yes_no_check = 2;
        } else {
            $auditPertanyaan->yes_no_check = 0;
        }

        $auditPertanyaan->catatan_khusus = $request->op_pertanyaan_catkhusus;
        $auditPertanyaan->audit_pen_jadwal_id = $request->pertanyaan_edit_jadwal_id;
        $auditPertanyaan->audit_pen_lingkup_id = $request->pertanyaan_edit_lingkup;

        if ($auditPertanyaan->save()) {
            return "sip";
        }
    }

    function auditor_ami_pertanyaan_edit(Request $request, $audit_borang_id)
    {
        if ($request->pertanyaan_edit_cek_ya == 1) {
            $hasil_cek = 1;
        } else if ($request->pertanyaan_edit_cek_tidak == 2) {
            $hasil_cek = 2;
        } else {
            $hasil_cek = 0;
        }

        $auditPertanyaan = AuditPertanyaanModel::where([
            'audit_borang_id' => $audit_borang_id,
            'id'    => $request->pertanyaan_edit_ids
        ])->update([
            'referensi_butir_mutu' => $request->pertanyaan_edit_butir_mutu,
            'hasil_observasi'   => $request->pertanyaan_edit_hasilobs,
            'yes_no_check'  => $hasil_cek,
            'catatan_khusus' => $request->pertanyaan_edit_catatan_khusus
        ]);

        return 'ok';
    }

    function auditor_ami_pertanyaan_delete(Request $request, $audit_borang_id)
    {
        $auditPertanyaan = AuditPertanyaanModel::where([
            'audit_borang_id' => $audit_borang_id,
            'id' => $request->pertanyaan_id
        ])->delete();

        return $auditPertanyaan;
    }

    function auditor_ami_pertanyaan_view(Request $request, $audit_borang_id)
    {
        if ($request->search['value'] != null) {
            $auditPertanyaan = AuditPertanyaanModel::where('audit_borang_id', '=', $audit_borang_id)
                ->where('hasil_observasi', 'like', '%' . $request->search['value'] . '%')
                ->get();

            $iFilter = AuditPertanyaanModel::where('audit_borang_id', '=', $audit_borang_id)
                ->where('tujuan', 'like', '%' . $request->search['value'] . '%')
                ->count();

            $iTotal = AuditPertanyaanModel::where('audit_borang_id', '=', $audit_borang_id)->count();
        } else {

            $auditPertanyaan = AuditPertanyaanModel::where('audit_borang_id', '=', $audit_borang_id)->get();
            $iTotal = AuditPertanyaanModel::where('audit_borang_id', '=', $audit_borang_id)->count();
            $iFilter = $iTotal;
        }

        $sEcho = intval($request->sEcho);

        $output = array(

            "sEcho" => intval($sEcho),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilter,
            "aaData" => $auditPertanyaan
        );

        return json_encode($output);
    }

    //Lingkup audit

    function auditor_ami_lingkup_create(Request $request, $audit_borang_id)
    {
        $auditLingkup = new AuditLingkupModel();
        $auditLingkup->audit_lingkup = $request->op_lingkup_audit;


        $auditLingkup->audit_borang_id = $audit_borang_id;

        if ($auditLingkup->save()) {
            return "sip";
        }
    }

    function auditor_ami_lingkup_view(Request $request, $audit_borang_id)
    {
        if ($request->search['value'] != null) {
            $auditLingkup = AuditLingkupModel::where('audit_borang_id', '=', $audit_borang_id)
                ->where('audit_lingkup', 'like', '%' . $request->search['value'] . '%')
                ->get();

            $iFilter = AuditLingkupModel::where('audit_borang_id', '=', $audit_borang_id)
                ->where('audit_lingkup', 'like', '%' . $request->search['value'] . '%')
                ->count();

            $iTotal = AuditLingkupModel::where('audit_borang_id', '=', $audit_borang_id)->count();
        } else {

            $auditLingkup = AuditLingkupModel::where('audit_borang_id', '=', $audit_borang_id)->get();
            $iTotal = AuditLingkupModel::where('audit_borang_id', '=', $audit_borang_id)->count();
            $iFilter = $iTotal;
        }

        $sEcho = intval($request->sEcho);

        $output = array(

            "sEcho" => intval($sEcho),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilter,
            "aaData" => $auditLingkup
        );

        return json_encode($output);
    }

    function auditor_ami_lingkup_edit(Request $request, $audit_borang_id)
    {
        $auditLingkup = AuditLingkupModel::where([
            'audit_borang_id' => $audit_borang_id,
            'id' => $request->lingkup_edit_id_value
        ])->update([
            'audit_lingkup' => $request->lingkup_edit_value
        ]);

        return $auditLingkup;
    }

    function auditor_ami_lingkup_delete(Request $request, $audit_borang_id)
    {
        $auditLingkup = AuditLingkupModel::where([
            'audit_borang_id' => $audit_borang_id,
            'id' => $request->lingkup_id
        ])->delete();

        return $auditLingkup;
    }


    //jadwal audit

    function auditor_ami_jadwal_create(Request $request, $audit_borang_id)
    {
        $auditJadwal = new AuditJadwalModel();
        $auditJadwal->pukul = $request->op_pukul_audit;
        $auditJadwal->kegiatan_audit = $request->op_kegiatan_audit;


        $auditJadwal->audit_borang_id = $audit_borang_id;

        if ($auditJadwal->save()) {
            return "sip";
        }
    }

    function auditor_ami_jadwal_view(Request $request, $audit_borang_id)
    {
        if ($request->search['value'] != null) {
            $auditJadwal = AuditJadwalModel::where('audit_borang_id', '=', $audit_borang_id)
                ->where('kegiatan_audit', 'like', '%' . $request->search['value'] . '%')
                ->get();

            $iFilter = AuditJadwalModel::where('audit_borang_id', '=', $audit_borang_id)
                ->where('kegiatan_audit', 'like', '%' . $request->search['value'] . '%')
                ->count();

            $iTotal = AuditJadwalModel::where('audit_borang_id', '=', $audit_borang_id)->count();
        } else {

            $auditJadwal = AuditJadwalModel::where('audit_borang_id', '=', $audit_borang_id)->get();
            $iTotal = AuditJadwalModel::where('audit_borang_id', '=', $audit_borang_id)->count();
            $iFilter = $iTotal;
        }

        $sEcho = intval($request->sEcho);

        $output = array(

            "sEcho" => intval($sEcho),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilter,
            "aaData" => $auditJadwal
        );

        return json_encode($output);
    }

    function auditor_ami_jadwal_edit(Request $request, $audit_borang_id)
    {
        $auditJadwal = AuditJadwalModel::where([
            'audit_borang_id' => $audit_borang_id,
            'id' => $request->jadwal_edit_id_value
        ])->update([
            'pukul' => $request->pukul_edit_value,
            'kegiatan_audit' => $request->kegiatan_edit_value,
        ]);

        return $auditJadwal;
    }

    function auditor_ami_jadwal_delete(Request $request, $audit_borang_id)
    {
        $auditJadwal = AuditJadwalModel::where([
            'audit_borang_id' => $audit_borang_id,
            'id' => $request->jadwal_id
        ])->delete();

        return $auditJadwal;
    }

    //temuan audit
    function auditor_ami_temuan_create(Request $request, $audit_borang_id)
    {
        $auditStrings = new AuditKetidaksesuaianModel();
        $auditStrings->kts_ob = $request->op_inisial_audit;
        $auditStrings->referensi_butir_mutu = $request->op_referensi_mutu_temuan;
        $auditStrings->pernyataan = $request->op_pernyataan_mutu_temuan;


        $auditStrings->audit_borang_id = $audit_borang_id;

        if ($auditStrings->save()) {
            return "sip";
        }
    }

    function auditor_ami_temuan_view(Request $request, $audit_borang_id)
    {
        if ($request->search['value'] != null) {
            $auditStrings = AuditKetidaksesuaianModel::where('audit_borang_id', '=', $audit_borang_id)
                ->where('referensi_butir_mutu ', 'like', '%' . $request->search['value'] . '%')
                ->get();

            $iFilter = AuditKetidaksesuaianModel::where('audit_borang_id', '=', $audit_borang_id)
                ->where('referensi_butir_mutu ', 'like', '%' . $request->search['value'] . '%')
                ->count();

            $iTotal = AuditKetidaksesuaianModel::where('audit_borang_id', '=', $audit_borang_id)->count();
        } else {

            $auditStrings = AuditKetidaksesuaianModel::where('audit_borang_id', '=', $audit_borang_id)->get();
            $iTotal = AuditKetidaksesuaianModel::where('audit_borang_id', '=', $audit_borang_id)->count();
            $iFilter = $iTotal;
        }

        $sEcho = intval($request->sEcho);

        $output = array(

            "sEcho" => intval($sEcho),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilter,
            "aaData" => $auditStrings
        );

        return json_encode($output);
    }

    function auditor_ami_temuan_edit(Request $request, $audit_borang_id)
    {
        $auditStrings = AuditKetidaksesuaianModel::where([
            'audit_borang_id' => $audit_borang_id,
            'id' => $request->ketidaksesuaian_edit_id_value
        ])->update([
            'kts_ob' => $request->kts_ob_edit_value,
            'referensi_butir_mutu' => $request->temuan_edit_value,
            'pernyataan' => $request->pernyataan_edit_value,
        ]);

        return $auditStrings;
    }

    function auditor_ami_temuan_delete(Request $request, $audit_borang_id)
    {
        $auditStrings = AuditKetidaksesuaianModel::where([
            'audit_borang_id' => $audit_borang_id,
            'id' => $request->temuan_id
        ])->delete();

        return $auditStrings;
    }

    //saran perbaikan audit
    function auditor_ami_saran_create(Request $request, $audit_borang_id)
    {
        $auditStrings = new AuditSaranModel();
        $auditStrings->bidang  = $request->op_perbaikan_bidang_audit;
        $auditStrings->kelebihan = $request->op_perbaikan_kelebihan_audit;
        $auditStrings->peluang_peningkatan = $request->op_perbaikan_peluang_audit;


        $auditStrings->audit_borang_id = $audit_borang_id;

        if ($auditStrings->save()) {
            return "sip";
        }
    }

    function auditor_ami_saran_view(Request $request, $audit_borang_id)
    {
        if ($request->search['value'] != null) {
            $auditStrings = AuditSaranModel::where('audit_borang_id', '=', $audit_borang_id)
                ->where('bidang', 'like', '%' . $request->search['value'] . '%')
                ->get();

            $iFilter = AuditSaranModel::where('audit_borang_id', '=', $audit_borang_id)
                ->where('bidang', 'like', '%' . $request->search['value'] . '%')
                ->count();

            $iTotal = AuditSaranModel::where('audit_borang_id', '=', $audit_borang_id)->count();
        } else {

            $auditStrings = AuditSaranModel::where('audit_borang_id', '=', $audit_borang_id)->get();
            $iTotal = AuditSaranModel::where('audit_borang_id', '=', $audit_borang_id)->count();
            $iFilter = $iTotal;
        }

        $sEcho = intval($request->sEcho);

        $output = array(

            "sEcho" => intval($sEcho),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilter,
            "aaData" => $auditStrings
        );

        return json_encode($output);
    }

    function auditor_ami_saran_edit(Request $request, $audit_borang_id)
    {
        $auditStrings = AuditSaranModel::where([
            'audit_borang_id' => $audit_borang_id,
            'id' => $request->saran_edit_id_value
        ])->update([
            'bidang' => $request->bidang_edit_value,
            'kelebihan' => $request->kelebihan_edit_value,
            'peluang_peningkatan' => $request->peluang_edit_value
        ]);

        return $auditStrings;
    }

    function auditor_ami_saran_delete(Request $request, $audit_borang_id)
    {
        $auditStrings = AuditSaranModel::where([
            'audit_borang_id' => $audit_borang_id,
            'id' => $request->saran_id
        ])->delete();

        return $auditStrings;
    }

    //kesimpulan
    function auditor_ami_kesimpulan_create(Request $request, $audit_borang_id)
    {
        $auditStrings = new AuditKesimpulanModel();
        $auditStrings->kesimpulan = $request->op_kesimpulan_audit;
        $auditStrings->yes_no_other_check = $request->pilihan_kesimpulan;
        $auditStrings->lainnya = $request->op_kesimpulan_lainnya;


        $auditStrings->audit_borang_id = $audit_borang_id;

        if ($auditStrings->save()) {
            return "sip";
        }
    }

    function auditor_ami_kesimpulan_view(Request $request, $audit_borang_id)
    {
        if ($request->search['value'] != null) {
            $auditStrings = AuditKesimpulanModel::where('audit_borang_id', '=', $audit_borang_id)
                ->where('kesimpulan', 'like', '%' . $request->search['value'] . '%')
                ->get();

            $iFilter = AuditKesimpulanModel::where('audit_borang_id', '=', $audit_borang_id)
                ->where('kesimpulan', 'like', '%' . $request->search['value'] . '%')
                ->count();

            $iTotal = AuditKesimpulanModel::where('audit_borang_id', '=', $audit_borang_id)->count();
        } else {

            $auditStrings = AuditKesimpulanModel::where('audit_borang_id', '=', $audit_borang_id)->get();
            $iTotal = AuditKesimpulanModel::where('audit_borang_id', '=', $audit_borang_id)->count();
            $iFilter = $iTotal;
        }

        $sEcho = intval($request->sEcho);

        $output = array(

            "sEcho" => intval($sEcho),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilter,
            "aaData" => $auditStrings
        );

        return json_encode($output);
    }

    function auditor_ami_kesimpulan_edit(Request $request, $audit_borang_id)
    {
        $auditStrings = AuditKesimpulanModel::where([
            'audit_borang_id' => $audit_borang_id,
            'id' => $request->kesimpulan_edit_id_value
        ])->update([
            'kesimpulan' => $request->kesimpulan_edit_value,
            'yes_no_other_check' => $request->pilihan_kesimpulan_edit_value,
            'lainnya' => $request->lainnya_edit_value
        ]);

        return $auditStrings;
    }

    function auditor_ami_kesimpulan_delete(Request $request, $audit_borang_id)
    {
        $auditStrings = AuditKesimpulanModel::where([
            'audit_borang_id' => $audit_borang_id,
            'id' => $request->kesimpulan_id
        ])->delete();

        return $auditStrings;
    }


    static function auditor_audit_mutu_internal_getUserName($user_id)
    {
        $usermodel = UserModel::where('id', '=', $user_id)->first();
        return $usermodel;
    }

    static function auditor_audit_mutu_internal_getUserID($user_name)
    {
        $usermodel = UserModel::where('username', '=', $user_name)->first()->id;
        return $usermodel;
    }
}
