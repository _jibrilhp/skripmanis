<?php

namespace App\Http\Controllers;

use App\Exports\ExportGrafikEMI;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Maatwebsite\Excel\Facades\Excel;



class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function getChart()
    {
      
    }

    function getExcel () {
      
    }
}
