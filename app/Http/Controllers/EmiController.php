<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Exports\ExportStandarAwal as ExportStandar;
use App\Imports\ExcelProfilImport as ProfilImport;
use App\Imports\ExcelStandarAwalImport as StandarImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\ProfilModel as ProfilModel;
use App\Models\PenilaianModel as PenilaianModel;
use App\Models\PerencanaanModel as PerencanaanModel;
use App\Models\IndikatorModel as IndikatorModel;
use App\Models\StandarModel as StandarModel;
use App\Models\StandarAwalModel as StandarAwalModel;
use App\Models\CategoryModel as CategoryModel;
use App\Models\ManualMutuModel as ManualMutuModel;
use App\Models\UserModel as UserModel;
use App\Models\Emi_KeadaanModel as EmiKeadaanModel;
use App\Models\Emi_StandarModel as EmiStandarModel;
use App\Models\Emi_IsianStandarModel as EmiIsianStandarModel;
use App\Models\Emi_BobotNilaiModel as EmiBobotNilaiModel;

//Jibril Hartri Putra 3 Ramadhan 1440 H

class EmiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    function emi_matriks_analisis(Request $request)
    {
        $url_matriks = 'http://localhost:5000/spmi/ami/1';
        return view('emi.matriks', ['url_matriks' => $url_matriks]);
    }

    function emi_matriks_submit(Request $request)
    {
        $data = $request->teks_analysis;

        $ch = curl_init();
        $data = str_ireplace(["\r\n", "\r", "\n","\"","\\","\t","\R","\f","\e"],' ',$data);
        

        curl_setopt($ch, CURLOPT_URL, 'http://localhost:5000/spmi/api/1');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"teks_analysis\":\"" . $data . "\"}");
        curl_setopt($ch, CURLOPT_POST, 1);

        $headers = array();
        $headers[] = 'Content-Type: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            //echo 'Error:' . curl_error($ch);
            $data_result = array('status'=>'failed','teks'=>$data,'analysis' => $result);
            return response()->json($data_result);
        }
        $data_result = array('status'=>'ok','teks'=>$data,'analysis' => $result);
        return response()->json($data_result);
        curl_close($ch);
    }

    function emi_standar_index(Request $request, $emi_standar_id)
    {
        if ($request->isMethod('post')) {
            return redirect()->route('emi_standar_index_pilih', [
                'emi_standar_id' => $emi_standar_id,
                'emi_tahun_standar'  => $request->op_tahun_pengukuran
            ]);
        } else {
            // session(['profil_id'=>1]); //debug

            $profilmodel = ProfilModel::find(Session::get('profil_id'));
            return view('home.operator_emi_standar', [
                'state' => 'pilih',
                'profil' => $profilmodel
            ]);
        }
    }

    function emi_standar_view_index(Request $request, $emi_standar_id, $emi_tahun_standar)
    {
        session(['profil_id' => 1]); //debug

        $profilmodel = ProfilModel::find(Session::get('profil_id'));
        return view('home.operator_emi_standar', [
            'state' => 'view',
            'profil' => $profilmodel,
            'standar_id' => $emi_standar_id,
            'standar_tahun' => $emi_tahun_standar
        ]);
    }

    function emi_standar_table_view(Request $request, $emi_standar_id, $emi_tahun_standar)
    {
        if ($request->search['value'] != null) {
            $auditStrings = EmiIsianStandarModel::where('emi_profil_id', '=', Session::get('profil_id'))
                ->where('emi_tahun_standar', '=', $emi_tahun_standar)
                ->where('emi_standar_id', '=', $emi_standar_id)
                ->where('emi_isian_standar', 'like', '%' . $request->search['value'] . '%')
                ->get();

            $iFilter =   EmiIsianStandarModel::where('emi_profil_id', '=', Session::get('profil_id'))
                ->where('emi_tahun_standar', '=', $emi_tahun_standar)
                ->where('emi_standar_id', '=', $emi_standar_id)
                ->where('emi_isian_standar', 'like', '%' . $request->search['value'] . '%')
                ->count();

            $iTotal =  EmiIsianStandarModel::where('emi_profil_id', '=', Session::get('profil_id'))
                ->where('emi_tahun_standar', '=', $emi_tahun_standar)
                ->where('emi_standar_id', '=', $emi_standar_id)
                ->count();
        } else {

            $auditStrings =   EmiIsianStandarModel::where('emi_profil_id', '=', Session::get('profil_id'))
                ->where('emi_tahun_standar', '=', $emi_tahun_standar)
                ->where('emi_standar_id', '=', $emi_standar_id)
                ->get();

            $iTotal =  EmiIsianStandarModel::where('emi_profil_id', '=', Session::get('profil_id'))
                ->where('emi_tahun_standar', '=', $emi_tahun_standar)
                ->where('emi_standar_id', '=', $emi_standar_id)
                ->count();

            $iFilter = $iTotal;
        }

        $sEcho = intval($request->sEcho);

        $output = array(

            "sEcho" => intval($sEcho),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilter,
            "aaData" => $auditStrings
        );

        return json_encode($output);
    }

    function emi_standar_table_create(Request $request, $emi_standar_id, $emi_tahun_standar)
    {
        $isistandar = new EmiIsianStandarModel;
        $isistandar->emi_profil_id = Session::get('profil_id');
        $isistandar->emi_tahun_standar = $emi_tahun_standar;
        $isistandar->emi_standar_id = $emi_standar_id;
        $isistandar->emi_isian_standar = $request->op_emi_isian_standar;

        if ($isistandar->save()) {
            return 'sip';
        }
    }

    function emi_standar_table_edit(Request $request, $emi_standar_id, $emi_tahun_standar)
    {
        $auditStrings = EmiIsianStandarModel::where([
            'emi_profil_id' => Session::get('profil_id'),
            'emi_tahun_standar'    => $emi_tahun_standar,
            'emi_standar_id'    => $emi_standar_id,
            'id'    => $request->isian_standar_edit_id_value
        ])->update([
            'emi_isian_standar' => $request->isian_standar_edit_value,
        ]);

        return $auditStrings;
    }

    function emi_standar_table_delete(Request $request, $emi_standar_id, $emi_tahun_standar)
    {
        $auditStrings = EmiIsianStandarModel::where([
            'emi_profil_id' => Session::get('profil_id'),
            'emi_tahun_standar'    => $emi_tahun_standar,
            'emi_standar_id'    => $emi_standar_id,
            'id'    => $request->isian_id
        ])->delete();

        return $auditStrings;
    }

    function emi_standar_option_pilihan_standar(Request $request, $emi_standar_id, $emi_tahun_standar, $emi_pilihan = null)
    {
        $auditStrings =   EmiIsianStandarModel::where('emi_profil_id', '=', Session::get('profil_id'))
            ->where('emi_tahun_standar', '=', $emi_tahun_standar)
            ->where('emi_standar_id', '=', $emi_standar_id)
            ->get();

        $arr = [];

        foreach ($auditStrings as $r) {
            if ($emi_pilihan == $r->id) {
                array_push($arr, "<option selected=\"selected\" value=\"" . $r->id . "\">" . $r->emi_isian_standar . "</option>");
            } else {
                array_push($arr, "<option value=\"" . $r->id . "\">" . $r->emi_isian_standar . "</option>");
            }
        }

        return $arr;
    }


    function emi_keadaan_table_view(Request $request, $emi_standar_id, $emi_tahun_standar)
    {
        if ($request->search['value'] != null) {
            $auditStrings = EmiKeadaanModel::leftJoin('tbl_emi_isian_standar', 'emi_isian_standar_id', '=', 'tbl_emi_isian_standar.id')
                ->where('tbl_emi_keadaan_prodi.emi_profil_id', '=', Session::get('profil_id'))
                ->where('tbl_emi_keadaan_prodi.emi_tahun_standar', '=', $emi_tahun_standar)
                ->where('tbl_emi_keadaan_prodi.emi_standar_id', '=', $emi_standar_id)

                ->where('keadaan_prodi_saat_ini', 'like', '%' . $request->search['value'] . '%')
                ->get();

            $iFilter = EmiKeadaanModel::leftJoin('tbl_emi_isian_standar', 'emi_isian_standar_id', '=', 'tbl_emi_isian_standar.id')
                ->where('tbl_emi_keadaan_prodi.emi_profil_id', '=', Session::get('profil_id'))
                ->where('tbl_emi_keadaan_prodi.emi_tahun_standar', '=', $emi_tahun_standar)
                ->where('tbl_emi_keadaan_prodi.emi_standar_id', '=', $emi_standar_id)

                ->where('keadaan_prodi_saat_ini', 'like', '%' . $request->search['value'] . '%')
                ->count();

            $iTotal = EmiKeadaanModel::leftJoin('tbl_emi_isian_standar', 'emi_isian_standar_id', '=', 'tbl_emi_isian_standar.id')
                ->where('tbl_emi_keadaan_prodi.emi_profil_id', '=', Session::get('profil_id'))
                ->where('tbl_emi_keadaan_prodi.emi_tahun_standar', '=', $emi_tahun_standar)
                ->where('tbl_emi_keadaan_prodi.emi_standar_id', '=', $emi_standar_id)

                ->count();
        } else {

            $auditStrings = EmiKeadaanModel::leftJoin('tbl_emi_isian_standar', 'emi_isian_standar_id', '=', 'tbl_emi_isian_standar.id')
                ->where('tbl_emi_keadaan_prodi.emi_profil_id', '=', Session::get('profil_id'))
                ->where('tbl_emi_keadaan_prodi.emi_tahun_standar', '=', $emi_tahun_standar)
                ->where('tbl_emi_keadaan_prodi.emi_standar_id', '=', $emi_standar_id)

                ->where('keadaan_prodi_saat_ini', 'like', '%' . $request->search['value'] . '%')
                ->get();

            $iTotal = EmiKeadaanModel::leftJoin('tbl_emi_isian_standar', 'emi_isian_standar_id', '=', 'tbl_emi_isian_standar.id')
                ->where('tbl_emi_keadaan_prodi.emi_profil_id', '=', Session::get('profil_id'))
                ->where('tbl_emi_keadaan_prodi.emi_tahun_standar', '=', $emi_tahun_standar)
                ->where('tbl_emi_keadaan_prodi.emi_standar_id', '=', $emi_standar_id)


                ->count();

            $iFilter = $iTotal;
        }

        $sEcho = intval($request->sEcho);

        $output = array(

            "sEcho" => intval($sEcho),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilter,
            "aaData" => $auditStrings
        );

        return json_encode($output);
    }

    function emi_keadaan_table_create(Request $request, $emi_standar_id, $emi_tahun_standar)
    {
        $isistandar = new EmiKeadaanModel;
        $isistandar->emi_profil_id = Session::get('profil_id');
        $isistandar->emi_tahun_standar = $emi_tahun_standar;
        $isistandar->emi_standar_id = $emi_standar_id;
        $isistandar->emi_isian_standar_id = $request->op_emi_pilihan_standar;
        $isistandar->emi_bobot_nilai = $request->op_emi_bobot_nilai_standar;
        $isistandar->keadaan_prodi_saat_ini = $request->op_emi_keadaan_prodi_standar;

        if ($isistandar->save()) {
            return 'sip';
        }
    }

    function emi_keadaan_table_edit(Request $request, $emi_standar_id, $emi_tahun_standar)
    {
        $auditStrings = EmiKeadaanModel::where([
            'emi_profil_id' => Session::get('profil_id'),
            'emi_tahun_standar'    => $emi_tahun_standar,
            'emi_standar_id'    => $emi_standar_id,
            'keadaan_id'    => $request->keadaan_prodi_edit_id_value
        ])->update([
            'keadaan_prodi_saat_ini' => $request->keadaan_prodi_edit_value,
            'emi_bobot_nilai' => $request->keadaan_nilai_value,
        ]);

        return $auditStrings;
    }

    function emi_keadaan_table_delete(Request $request, $emi_standar_id, $emi_tahun_standar)
    {
        $auditStrings = EmiKeadaanModel::where([
            'emi_profil_id' => Session::get('profil_id'),
            'emi_tahun_standar'    => $emi_tahun_standar,
            'emi_standar_id'    => $emi_standar_id,
            'keadaan_id'    => $request->keadaan_id
        ])->delete();

        return $auditStrings;
    }

    function emi_capaian_table_view(Request $request, $emi_standar_id, $emi_tahun_standar)
    {
        $hasil = EmiKeadaanModel::getProcessNilaiCapaian($emi_tahun_standar, $emi_standar_id);

        $sEcho = intval($request->sEcho);

        $output = array(

            "sEcho" => intval($sEcho),
            "iTotalRecords" => max($hasil),
            "iTotalDisplayRecords" => max($hasil),
            "aaData" => $hasil
        );

        return json_encode($output);
    }

    function emi_capaian_table_edit(Request $request, $emi_standar_id, $emi_tahun_standar)
    {
        if ($request->capaian_edit_id_value == '-') {
            //artinya belum ada dan buat..
            $bobotnilai = new EmiBobotNilaiModel;
            $bobotnilai->isian_standar_id = $request->capaian_isian_value;
            $bobotnilai->nilai = $request->capaian_nilai_value;


            if ($bobotnilai->save()) {
                return 'ok';
            }
        } else {
            $bobotnilai = EmiBobotNilaiModel::where([
                'bobot_id' => $request->capaian_edit_id_value
            ])->update([
                'nilai' => $request->capaian_nilai_value
            ]);
            return  $bobotnilai;
        }
    }
}
