<?php

namespace App\Http\Controllers;

use PDF;
use Session;
use Illuminate\Http\Request;
use App\Exports\ExportStandarAwal as ExportStandar;
use App\Imports\ExcelProfilImport as ProfilImport;
use App\Exports\ExportGrafikEMI;
use App\Exports\ExportRekapEMI;
use App\Exports\ExportNilaiEMI;

use App\Imports\ExcelStandarAwalImport as StandarImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\ProfilModel as ProfilModel;
use App\Models\PenilaianModel as PenilaianModel;
use App\Models\PerencanaanModel as PerencanaanModel;
use App\Models\IndikatorModel as IndikatorModel;
use App\Models\StandarModel as StandarModel;
use App\Models\StandarModelFile as StandarModelFile;
use App\Models\StandarAwalModel as StandarAwalModel;
use App\Models\CategoryModel as CategoryModel;
use App\Models\JenjangModel as JenjangModel;

use App\Models\Emi_StandarModel as EmiStandarModel;
use App\Models\Emi_IsianStandarModel as Emi_IsianStandarModel;
use App\Models\Emi_IsianStandarModel as EmiIsianStandarModel;
use App\Models\Emi_BobotNilaiModel as EmiBobotNilaiModel;
use App\Models\Emi_RekapStandarModel as EmiRekapStandar;
use Khill\Lavacharts\Lavacharts;

use CpChart\Data;
use CpChart\Image;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    //baru
    function emi_download_nilai_pdf($profil_id, $tahun_emi)
    {
        $emi_standar = EmiStandarModel::all();

        $emi_standar_name = [];
        $emi_standar_view = [];
        $profil_prodi = [];

        $get_profil_prodi = ProfilModel::join('tbl_jenjang', 'tbl_jenjang.jenjang_id', '=', 'tbl_profil.jenjang_plot_id')->where(['id' => $profil_id])->first();


        $jenjang_id = $get_profil_prodi->jenjang_plot_id;

        foreach ($emi_standar as $key => $emk) {

            $emi_standar_list = EmiStandarModel::join('tbl_emi_isian_standar', 'tbl_emi_isian_standar.emi_standar_id', '=', 'tbl_emi_standar.id')


                ->where([
                    'emi_standar_id' => $emk->id,
                    'emi_jenjang_id' => $jenjang_id
                ])
                ->select('*', 'tbl_emi_isian_standar.id as isian_id')
                ->get();

            $emi_standar_view[$key] = $emi_standar_list;
            $emi_standar_name[$key] = $emk->nama_standar;

        }
        $pdf = PDF::loadview('exports.emi-nilai', [
            'standar_list' => $emi_standar_view,
            'standar_name' => $emi_standar_name,
            'profildata' => $get_profil_prodi
        ]);
        $output = $pdf->output();
        return file_put_contents(storage_path() . '/app/process_temp/Nilai_Standar.pdf', $output);

    }

    function emi_download_rekap_analisis_pdf($profil_id, $tahun_emi)
    {
        $rekap_temuan = [];
        $rekap_hasil_temuan_sebab = [];
        $rekap_hasil_temuan_akibat = [];
        $rekap_rekomendasi = [];

        $data_standar_id = [];


        $total_max_isian_standar = 0;
        $profil = ProfilModel::where([
            'id' => $profil_id,
            'tahun_pengisian' => $tahun_emi
        ])->first();


        //jumlahkan perstandar
        $capaian = [];
        $nilai_standar_id = [];

        $standar = EmiStandarModel::all();

        foreach ($standar as $cap => $std) { //forrekap
            $total_nilai_per_standar    = 0;
            $total_bobot_nilai          = 0;
            $isian                      = Emi_IsianStandarModel::where([
                'emi_standar_id' => $std->id,
                'emi_jenjang_id' => $profil->jenjang_plot_id

            ])->get();

            if ($isian != null) {
                foreach ($isian as $key => $isi) { //for isian standar
                    $nilai  = EmiBobotNilaiModel::where([
                        'isian_standar_id' => $isi->id, 'profil_id' => $profil_id, 'tahun_emi' => $tahun_emi
                    ])->first();

                    if ($nilai != null) {
                        $nilai = $nilai->nilai;
                    } else {
                        $nilai = 0;
                    }

                    //ambil bobot nilai  terus kalikan
                    $total_nilai_per_standar = $total_nilai_per_standar + ($isi->emi_bobot_persen * $nilai);
                    $total_max_isian_standar += $isi->emi_bobot_persen * 7;
                    $total_bobot_nilai += $isi->emi_bobot_persen;
                }
            }

            if ($total_max_isian_standar == 0) {
                $total_max_isian_standar = 1;
            }

            $capaian[$cap] = $total_nilai_per_standar / $total_max_isian_standar * 100;
            $nilai_standar_id[$cap] = $std->nama_standar;
            $bobot[$cap] = $total_bobot_nilai / 100;

            $rekap = EmiRekapStandar::where([
                'rekap_standar_id' => $std->id,
                'rekap_profil_id' =>  $profil_id,
                'rekap_tahun_pengukuran' => $tahun_emi
            ])->first();

            if ($rekap != null) {
                $rekap_temuan[$cap] = $rekap->rekap_temuan;
                $rekap_hasil_temuan_sebab[$cap] = $rekap->rekap_hasil_temuan_sebab;
                $rekap_hasil_temuan_akibat[$cap] = $rekap->rekap_hasil_temuan_akibat;
                $rekap_rekomendasi[$cap] = $rekap->rekap_rekomendasi;
                $data_standar_id[$cap] = $rekap->rekap_standar_id;
            } else {
                $rekap_temuan[$cap] = '';
                $rekap_hasil_temuan_sebab[$cap] = '';
                $rekap_hasil_temuan_akibat[$cap] = '';
                $rekap_rekomendasi[$cap] = '';
                $data_standar_id[$cap] = $std->id;
            }
        }

        $pdf = PDF::loadview('exports.emi-rekap', [
            'profildata' => $profil,
            'data_capaian' => $capaian,
            'data_standar' => $nilai_standar_id,
            'data_bobot' => $bobot,
            'data_rekap_temuan' => $rekap_temuan,
            'data_rekap_sebab' => $rekap_hasil_temuan_sebab,
            'data_rekap_akibat' => $rekap_hasil_temuan_akibat,
            'data_rekap_rekomendasi' => $rekap_rekomendasi,
            'data_standar_id' => $data_standar_id

        ]);
        $output = $pdf->output();
        return file_put_contents(storage_path() . '/app/process_temp/Rekap_Analisis_Standar.pdf', $output);
    }

    function emi_download_grafik_pdf($profil_id, $tahun_emi)
    {
        $total_max_isian_standar = 0;
        $profil = ProfilModel::where([
            'id' => $profil_id,
            'tahun_pengisian' => $tahun_emi
        ])->first();


        //jumlahkan perstandar
        $capaian = [];
        $nilai_standar_id = [];

        $standar = EmiStandarModel::all();
        /* Create and populate the Data object */
        $data = new Data();

        foreach ($standar as $cap => $std) { //forrekap
            $total_nilai_per_standar    = 0;
            $total_bobot_nilai          = 0;
            $isian                      = Emi_IsianStandarModel::where([
                'emi_standar_id' => $std->id,
                'emi_jenjang_id' => $profil->jenjang_plot_id

            ])->get();


            if ($isian != null) {

                foreach ($isian as $key => $isi) { //for isian standar
                    $nilai  = EmiBobotNilaiModel::where([
                        'isian_standar_id' => $isi->id, 'profil_id' => $profil_id, 'tahun_emi' => $tahun_emi
                    ])->first();

                    if ($nilai != null) {
                        $nilai = $nilai->nilai;
                    } else {
                        $nilai = 0;
                    }

                    //ambil bobot nilai  terus kalikan
                    $total_nilai_per_standar = $total_nilai_per_standar + ($isi->emi_bobot_persen * $nilai);
                    $total_max_isian_standar += $isi->emi_bobot_persen * 7;
                    $total_bobot_nilai += $isi->emi_bobot_persen;
                }
            }

            if ($total_max_isian_standar == 0) {
                $total_max_isian_standar = 1;
            }

            $capaian[$cap] = $total_nilai_per_standar / $total_max_isian_standar * 100;
            $nilai_standar_id[$cap] = $std->nama_standar;

            $bobot[$cap] = $total_bobot_nilai / 100;
        }
        $rata = 0;
        $jml = 0;
        foreach ($bobot as $bt) {
            $rata += $bt;
            $jml++;
            $data->addPoints($bt, "Nilai");
        }

        $data->addPoints(($rata / $jml), "Nilai");


        $data->setAxisName(0, "Nilai");
        foreach ($nilai_standar_id as $nsi) {
            $data->addPoints($nsi, "persen");
            //  dd($nsi);
        }
        $data->addPoints("Rata-rata", "persen");

        $data->setSerieDescription("persen", "persen");
        $data->setAbscissa("persen");

        /* Create the Image object */
        $image = new Image(1200, 550, $data);
        // $image->drawGradientArea(0, 0, 500, 500, DIRECTION_VERTICAL, [
        //     "StartR" => 240,
        //     "StartG" => 240,
        //     "StartB" => 240,
        //     "EndR" => 240,
        //     "EndG" => 240,
        //     "EndB" => 240,
        //     "Alpha" => 100
        // ]);
        // $image->drawGradientArea(0, 0, 500, 500, DIRECTION_HORIZONTAL, [
        //     "StartR" => 44,
        //     "StartG" => 44,
        //     "StartB" => 228,
        //     "EndR" => 240,
        //     "EndG" => 240,
        //     "EndB" => 240,
        //     "Alpha" => 20
        // ]);
        $image->setFontProperties(["FontName" => "calibri.ttf", "FontSize" => 9]);

        /* Draw the chart scale */
        $image->setGraphArea(300, 30, 980, 480);
        $image->drawScale([
            "CycleBackground" => true,
            "DrawSubTicks" => true,
            "GridR" => 0,
            "GridG" => 0,
            "GridB" => 0,
            "GridAlpha" => 10,
            "Pos" => SCALE_POS_TOPBOTTOM
        ]);

        /* Turn on shadow computing */
        $image->setShadow(true, ["X" => 1, "Y" => 1, "R" => 0, "G" => 0, "B" => 0, "Alpha" => 10]);

        /* Draw the chart */
        $image->drawBarChart(["DisplayPos" => LABEL_POS_INSIDE, "DisplayValues" => true, "Rounded" => true, "Surrounding" => 30]);

        /* Write the legend */
        $image->drawLegend(1070, 215, ["Style" => LEGEND_NOBORDER, "Mode" => LEGEND_HORIZONTAL]);

        /* Render the picture (choose the best way) */
        $grafik =  $image->render("render_grafik.png");


        $pdf = PDF::loadview('exports.emi-grafik', [
            'profildata' => $profil,
            'data_capaian' => $capaian,
            'data_standar' => $nilai_standar_id,
            'data_bobot' => $bobot,
            'grafik' => $grafik
        ]);
        //return $pdf->download('grafik_standar');
        $output = $pdf->output();
        return file_put_contents(storage_path() . '/app/process_temp/Grafik_Standar.pdf', $output);
    }



    function emi_download_nilai($profil_id, $tahun_emi)
    {
        $profil = ProfilModel::where([
            'id' => $profil_id,
            'tahun_pengisian' => $tahun_emi
        ])->first();

        if ($profil != null) {
            return Excel::store(new ExportNilaiEMI($profil->id, $profil->tahun_pengisian), 'process_temp/nilai_standar.xlsx');
        } else {
            return null;
        }
    }

    function emi_download_rekap_analisis($profil_id, $tahun_emi)
    {
        $profil = ProfilModel::where([
            'id' => $profil_id,
            'tahun_pengisian' => $tahun_emi
        ])->first();

        if ($profil != null) {
            return Excel::store(new ExportRekapEMI($profil->id, $profil->tahun_pengisian), 'process_temp/rekap_analisis_standar.xlsx');
        } else {
            return null;
        }
    }

    function emi_download_grafik($profil_id, $tahun_emi)
    {
        $profil = ProfilModel::where([
            'id' => $profil_id,
            'tahun_pengisian' => $tahun_emi
        ])->first();

        if ($profil != null) {
            return Excel::store(new ExportGrafikEMI($profil->id, $profil->tahun_pengisian), 'process_temp/grafik_standar.xlsx');
        } else {
            return null;
        }
    }

    function emi_download_data_set(Request $request, $profil_id, $tahun_emi)
    {
        if ($request->profil_id != null) {

            //proses Rekap Analisis per Standar
            $download1 = $this->emi_download_grafik_pdf($profil_id, $tahun_emi);
            $download2 = $this->emi_download_rekap_analisis_pdf($profil_id, $tahun_emi);
            $download3 = $this->emi_download_nilai_pdf($profil_id, $tahun_emi);
            
            //$download1 =  $this->emi_download_rekap_analisis($profil_id, $tahun_emi);
            //$download2 = $this->emi_download_grafik($profil_id, $tahun_emi);
            //$download3 = $this->emi_download_nilai($profil_id, $tahun_emi);

            if ($download1 && $download2 && $download3) {


                $zip_file = 'EMI_' . date("d-m-y") . '.zip';
                $zip = new \ZipArchive();
                $zip->open($zip_file, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

                $path = storage_path('app/process_temp');

                $files = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($path));
                foreach ($files as $name => $file) {
                    // We're skipping all subfolders
                    if (!$file->isDir()) {
                        $filePath     = $file->getRealPath();

                        // extracting filename with substr/strlen
                        $relativePath = 'Data-EMI/' . substr($filePath, strlen($path) + 1);

                        $zip->addFile($filePath, $relativePath);
                    }
                }
                $zip->close();
                return response()->download($zip_file);
            }



            $retProfil = ProfilModel::where([
                'id' => $profil_id,
                'tahun_pengisian' => $tahun_emi
            ])->first();

            if ($retProfil != null) { } else {
                return back()->with('pesan', 'Pilihan tidak tersedia');
            }
        }
    }

    function emi_download_page(Request $request)
    {
        $profil = ProfilModel::get();
        $profil_tahun = ProfilModel::distinct('tahun_pengisian')->get('tahun_pengisian');

        return view('download.emi_download', ['profil' => $profil, 'tahun' => $profil_tahun]);
    }

    function emi_rekap_set_data(Request $request)
    {
        if ($request->rekap_id == null) {
            //berarti create
            $rekapstandar = new EmiRekapStandar;
            $rekapstandar->rekap_standar_id         = $request->standar_id;
            $rekapstandar->rekap_profil_id          = $request->profil_id;
            $rekapstandar->rekap_tahun_pengukuran   = $request->tahun;
            $rekapstandar->rekap_temuan             = $request->rekap_temuan;
            $rekapstandar->rekap_hasil_temuan_sebab = $request->rekap_hasil_temuan_disebabkan;
            $rekapstandar->rekap_hasil_temuan_akibat = $request->rekap_hasil_temuan_mengakibatkan;
            $rekapstandar->rekap_rekomendasi        = $request->rekap_rekomendasi;

            if ($rekapstandar->save()) {
                return back()->with('success', 'Berhasil diubah');
            }
        } else {
            //berarti update
            $rekapstandar = EmiRekapStandar::find($request->rekap_id);
            $rekapstandar->rekap_standar_id         = $request->standar_id;
            $rekapstandar->rekap_profil_id          = $request->profil_id;
            $rekapstandar->rekap_tahun_pengukuran   = $request->tahun;
            $rekapstandar->rekap_temuan             = $request->rekap_temuan;
            $rekapstandar->rekap_hasil_temuan_sebab = $request->rekap_hasil_temuan_disebabkan;
            $rekapstandar->rekap_hasil_temuan_akibat = $request->rekap_hasil_temuan_mengakibatkan;
            $rekapstandar->rekap_rekomendasi        = $request->rekap_rekomendasi;

            if ($rekapstandar->save()) {
                return back()->with('success', 'Berhasil diubah');
            }
        }
    }

    function emi_rekap_get_data(Request $request)
    {

        $rekap = EmiRekapStandar::where([
            'rekap_standar_id' => $request->standar_id,
            'rekap_profil_id' => $request->profil_id,
            'rekap_tahun_pengukuran' => $request->tahun
        ])->first();

        if ($rekap != null) {
            return $rekap;
        } else {
            return "none";
        }
    }

    function emi_rekap_standar(Request $request, $profil_id = null, $tahun_emi = null)
    {
        $rekap_temuan = [];
        $rekap_hasil_temuan_sebab = [];
        $rekap_hasil_temuan_akibat = [];
        $rekap_rekomendasi = [];

        $data_standar_id = [];

        $profil = ProfilModel::where(['id' => $profil_id, 'tahun_pengisian' => $tahun_emi])->first();
        $total_max_isian_standar = 0;


        //jumlahkan perstandar
        $capaian = [];
        $nilai_standar_id = [];

        $standar = EmiStandarModel::all();

        foreach ($standar as $cap => $std) { //forrekap
            $total_nilai_per_standar    = 0;
            $total_bobot_nilai          = 0;
            $isian                      = Emi_IsianStandarModel::where([
                'emi_standar_id' => $std->id,
                'emi_jenjang_id' => $profil->jenjang_plot_id

            ])->get();

            if ($isian != null) {
                foreach ($isian as $key => $isi) { //for isian standar
                    $nilai  = EmiBobotNilaiModel::where([
                        'isian_standar_id' => $isi->id, 'profil_id' => $profil_id, 'tahun_emi' => $tahun_emi
                    ])->first();

                    if ($nilai != null) {
                        $nilai = $nilai->nilai;
                    } else {
                        $nilai = 0;
                    }

                    //ambil bobot nilai  terus kalikan
                    $total_nilai_per_standar = $total_nilai_per_standar + ($isi->emi_bobot_persen * $nilai);
                    $total_max_isian_standar += $isi->emi_bobot_persen * 7;
                    $total_bobot_nilai += $isi->emi_bobot_persen;
                }
            }

            if ($total_max_isian_standar == 0) {
                $total_max_isian_standar = 1;
            }

            $capaian[$cap] = $total_nilai_per_standar / $total_max_isian_standar * 100;
            $nilai_standar_id[$cap] = $std->nama_standar;
            $bobot[$cap] = $total_bobot_nilai / 100;

            $rekap = EmiRekapStandar::where([
                'rekap_standar_id' => $std->id,
                'rekap_profil_id' => $profil_id,
                'rekap_tahun_pengukuran' => $tahun_emi
            ])->first();

            if ($rekap != null) {
                $rekap_temuan[$cap] = $rekap->rekap_temuan;
                $rekap_hasil_temuan_sebab[$cap] = $rekap->rekap_hasil_temuan_sebab;
                $rekap_hasil_temuan_akibat[$cap] = $rekap->rekap_hasil_temuan_akibat;
                $rekap_rekomendasi[$cap] = $rekap->rekap_rekomendasi;
                $data_standar_id[$cap] = $rekap->rekap_standar_id;
            } else {
                $rekap_temuan[$cap] = '';
                $rekap_hasil_temuan_sebab[$cap] = '';
                $rekap_hasil_temuan_akibat[$cap] = '';
                $rekap_rekomendasi[$cap] = '';
                $data_standar_id[$cap] = $std->id;
            }
        }

        //baru digabung semua

        return view('emi.rekapstandar', [
            'profildata' => $profil,
            'data_capaian' => $capaian,
            'data_standar' => $nilai_standar_id,
            'data_bobot' => $bobot,
            'data_rekap_temuan' => $rekap_temuan,
            'data_rekap_sebab' => $rekap_hasil_temuan_sebab,
            'data_rekap_akibat' => $rekap_hasil_temuan_akibat,
            'data_rekap_rekomendasi' => $rekap_rekomendasi,
            'data_standar_id' => $data_standar_id

        ]);
    }

    function  emi_grafik_standar(Request $request, $profil_id = null, $tahun_emi = null)
    {
        $profil = ProfilModel::where(['id' => $profil_id, 'tahun_pengisian' => $tahun_emi])->first();
        $total_max_isian_standar = 0;


        //jumlahkan perstandar
        $capaian = [];
        $nilai_standar_id = [];
        $bobot = [];

        $standar = EmiStandarModel::all();

        foreach ($standar as $cap => $std) { //forstandar
            $total_nilai_per_standar    = 0;

            $isian                      = Emi_IsianStandarModel::where(['emi_standar_id' => $std->id])->get();

            if ($isian != null) {
                foreach ($isian as $key => $isi) { //for isian standar
                    $nilai  = EmiBobotNilaiModel::where([
                        'isian_standar_id' => $isi->id, 'profil_id' => $profil_id, 'tahun_emi' => $tahun_emi
                    ])->first();

                    if ($nilai != null) {
                        $nilai = $nilai->nilai;
                    } else {
                        $nilai = 0;
                    }

                    //ambil bobot nilai  terus kalikan
                    $total_nilai_per_standar = $total_nilai_per_standar + ($isi->emi_bobot_persen * $nilai);
                    $total_max_isian_standar += $isi->emi_bobot_persen * 7;
                }
            }

            $capaian[$cap] = $total_nilai_per_standar / $total_max_isian_standar * 100;
            $nilai_standar_id[$cap] = $std->nama_standar;
        }

        //baru digabung semua
        //return var_dump($capaian,$nilai_standar_id);

        return view('emi.grafikstandar', [
            'profildata' => $profil,
            'data_capaian' => $capaian,
            'data_standar' => $nilai_standar_id,

        ]);
    }


    //yang lama
    public function home_index()
    {
        return view("home/index");
    }

    public function operator_borang()
    {
        $profilmodel = ProfilModel::all();

        return view("home/operator_borang", ['profil' => $profilmodel]);
    }

    public function operator_borang_lihat(Request $request)
    {
        $profilmodel = ProfilModel::find($request->op_borang_select1);

        if ($profilmodel != null) {

            //deteksi ini jenjang apa.. masukkan ke database
            if (stripos($profilmodel->jenjang, "D3") !== false) {
                $hasil_jenjang = JenjangModel::where(['jenjang_nama' => 'D3'])->first();
                ProfilModel::where(['id' => $profilmodel->id])->update(['jenjang_plot_id' => $hasil_jenjang->jenjang_id]);
            } else if (stripos($profilmodel->jenjang, "S1") !== false) {
                $hasil_jenjang = JenjangModel::where(['jenjang_nama' => 'S1'])->first();
                ProfilModel::where(['id' => $profilmodel->id])->update(['jenjang_plot_id' => $hasil_jenjang->jenjang_id]);
            } else {
                //dianggap S1 kalau bukan siapa2 
                $hasil_jenjang = JenjangModel::where(['jenjang_nama' => 'S1'])->first();
                ProfilModel::where(['id' => $profilmodel->id])->update(['jenjang_plot_id' => $hasil_jenjang->jenjang_id]);
            }

            session([
                'profil_id' => $profilmodel->id,
                'tahun_pengisian' => $profilmodel->tahun_pengisian,
                'jenjang_id'    => $profilmodel->jenjang_plot_id


            ]);


            return view("home/operator_profil_tambah_data", ['profil' => $profilmodel, 'borang' => 'ubah']);
        } else {
            return 'i dont know this document is missing';
        }
    }

    function operator_borang_create(Request $request)
    {
        return view("home/operator_profil_tambah_data", ['borang' => 'input']);
    }

    function operator_borang_create_data(Request $request)
    {
        $profilmodel = new ProfilModel;
        $profilmodel->kode_pt = $request->op_tambah_t25;
        $profilmodel->nama_pt = $request->op_tambah_t26;
        $profilmodel->alamat_pt = $request->op_tambah_t27;
        $profilmodel->status_pt = $request->op_tambah_t28;
        $profilmodel->nama_fakultas = $request->op_tambah_t29;
        $profilmodel->telepon_pt = $request->op_tambah_t30;
        $profilmodel->jenjang = $request->op_tambah_t31;
        $profilmodel->nama_prodi = $request->op_tambah_t26b;
        $profilmodel->tgl_prog = $request->op_tambah_t1;
        $profilmodel->no_sk = $request->op_tambah_t2;
        $profilmodel->no_sk_operasional = $request->op_tambah_t3;
        $profilmodel->tgl_pengisian = $request->op_tambah_t4;
        $profilmodel->akreditasi_prog = $request->op_tambah_t5;
        $profilmodel->alamat_website = $request->op_tambah_t6;
        $profilmodel->alamat_email = $request->op_tambah_t7;
        $profilmodel->telepon_prog = $request->op_tambah_t8;
        $profilmodel->visi_prog = $request->op_tambah_t9;
        $profilmodel->misi_prog = $request->op_tambah_t10;
        $profilmodel->tujuan_prog = $request->op_tambah_t11;
        $profilmodel->kompetensi_prog = $request->op_tambah_t12;
        $profilmodel->peta_prog = $request->op_tambah_t13;
        $profilmodel->kriteria_mhs = $request->op_tambah_t14;
        $profilmodel->jumlah_sks_min = $request->op_tambah_t15;
        $profilmodel->jumlah_nilai_d = $request->op_tambah_t16;
        $profilmodel->ipk_minimal = $request->op_tambah_t17;
        $profilmodel->syarat_lainnya = $request->op_tambah_t17a;
        $profilmodel->jumlah_mhs_tahun = $request->op_tambah_t18a;
        $profilmodel->jumlah_mhs = $request->op_tambah_t18b;
        $profilmodel->jumlah_dosen_tetap_ps = $request->op_tambah_t19;
        $profilmodel->jumlah_dosen_luar_ps = $request->op_tambah_t20;
        $profilmodel->jumlah_tenaga_kependidikan = $request->op_tambah_t21;
        $profilmodel->identitas_nama_ketua_prod = $request->op_tambah_t22;
        $profilmodel->identitas_nidn = $request->op_tambah_t24;
        $profilmodel->tahun_pengisian = $request->op_tambah_t32;

        if ($profilmodel->save()) {
            return back()->with('success', 'Berhasil disimpan');
        } else {
            return back()->with('pesan', 'Ada kesalahan dalam menyimpan data');
        }
    }

    public function operator_borang_update_data(Request $request)
    {
        if ($request->has('op_tambah_t25')) {

            $profilmodel = ProfilModel::find($request->op_tambah_hidden_t1);

            $profilmodel->kode_pt = $request->op_tambah_t25;
            $profilmodel->nama_pt = $request->op_tambah_t26;
            $profilmodel->alamat_pt = $request->op_tambah_t27;
            $profilmodel->status_pt = $request->op_tambah_t28;
            $profilmodel->nama_fakultas = $request->op_tambah_t29;
            $profilmodel->telepon_pt = $request->op_tambah_t30;
            $profilmodel->jenjang = $request->op_tambah_t31;
            $profilmodel->nama_prodi = $request->op_tambah_t26b;
            $profilmodel->tgl_prog = $request->op_tambah_t1;
            $profilmodel->no_sk = $request->op_tambah_t2;
            $profilmodel->no_sk_operasional = $request->op_tambah_t3;
            $profilmodel->tgl_pengisian = $request->op_tambah_t4;
            $profilmodel->akreditasi_prog = $request->op_tambah_t5;
            $profilmodel->alamat_website = $request->op_tambah_t6;
            $profilmodel->alamat_email = $request->op_tambah_t7;
            $profilmodel->telepon_prog = $request->op_tambah_t8;
            $profilmodel->visi_prog = $request->op_tambah_t9;
            $profilmodel->misi_prog = $request->op_tambah_t10;
            $profilmodel->tujuan_prog = $request->op_tambah_t11;
            $profilmodel->kompetensi_prog = $request->op_tambah_t12;
            $profilmodel->peta_prog = $request->op_tambah_t13;
            $profilmodel->kriteria_mhs = $request->op_tambah_t14;
            $profilmodel->jumlah_sks_min = $request->op_tambah_t15;
            $profilmodel->jumlah_nilai_d = $request->op_tambah_t16;
            $profilmodel->ipk_minimal = $request->op_tambah_t17;
            $profilmodel->syarat_lainnya = $request->op_tambah_t17a;
            $profilmodel->jumlah_mhs_tahun = $request->op_tambah_t18a;
            $profilmodel->jumlah_mhs = $request->op_tambah_t18b;
            $profilmodel->jumlah_dosen_tetap_ps = $request->op_tambah_t19;
            $profilmodel->jumlah_dosen_luar_ps = $request->op_tambah_t20;
            $profilmodel->jumlah_tenaga_kependidikan = $request->op_tambah_t21;
            $profilmodel->identitas_nama_ketua_prod = $request->op_tambah_t22;
            $profilmodel->identitas_nidn = $request->op_tambah_t24;
            $profilmodel->tahun_pengisian = $request->op_tambah_t32;

            if ($profilmodel->save()) {
                return back()->with('success', 'Berhasil disimpan');
            } else {
                return back()->with('pesan', 'Ada kesalahan dalam menyimpan data');
            }
        }
    }

    public function operator_borang_download_template()
    {
        $template_file_path = storage_path('app/template_profil_file_import.xlsx');
        $headers = [
            'Content-Type' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        ];

        return response()->download($template_file_path, 'templat_profil.xlsx', $headers);
    }

    public function operator_profil_upload_data(Request $request)
    {
        if ($request->has('operator_upload_t2')) {
            Excel::import(new ProfilImport, $request->file('operator_upload_t1'));
            return view("home/operator_profil_upload_data");
        } else {
            return view("home/operator_profil_upload_data");
        }
    }

    public function operator_penilaian_pilih(Request $request)
    {
        if ($request->isMethod('post')) {
            return redirect()->route('operator_rekap_analisis_home', ['id' => $request->op_penilaian_select1, 'bagian' => 'perencanaan']);
        } else {
            $profilmodel = ProfilModel::all();
            return view("home/operator_penilaian_pilihprodi", ['profil' => $profilmodel]);
        }
    }

    public function operator_penilaian_tampil(Request $request)
    {
        //ada ?bagian  artinya untuk mengetahui apakah
        //sedang berada di perencanaan,pengendalian,evaluasi,...,peningkatan

        //id adalah penghubung ke profil sebagai patokan aja




        if ($request->has('bagian') && $request->has('id')) {
            switch ($request->bagian) {
                case "perencanaan":
                    $profilmodel = new PerencanaanModel;
                    $profilmodel = $profilmodel->getPerencanaan($request->id);
                    if ($profilmodel != null) {
                        return view("home/operator_penilaian_perencanaan", ['profil' => $profilmodel[0], 'bagian' => $request->bagian, 'profil_id' => $request->id]);
                    } else {
                        return view("home/operator_penilaian_perencanaan", ['profil' => $profilmodel, 'bagian' => $request->bagian, 'profil_id' => $request->id]);
                    }

                    break;

                case "penetapan":
                    $profilmodel = new PerencanaanModel;
                    $profilmodel = $profilmodel->getPerencanaan($request->id);
                    if ($profilmodel != null) {
                        return view("home/operator_penilaian_penetapan", ['profil' => $profilmodel[0], 'bagian' => $request->bagian, 'profil_id' => $request->id]);
                    } else {
                        return view("home/operator_penilaian_penetapan", ['profil' => $profilmodel, 'bagian' => $request->bagian, 'profil_id' => $request->id]);
                    }
                    break;

                case "pelaksanaan":
                    break;

                case "evaluasi":
                    break;

                case "pengendalian":
                    break;


                default:
                    echo "Alternatif tidak ditemukan";
            }
        } else {
            $profilmodel = ProfilModel::all();
            return view("home/operator_standar_pilih", ['profil' => $profilmodel, 'pilih' => $request->category_ids]);
        }
    }

    public function operator_penilaian_tampil_save(Request $request)
    {
        //ada ?bagian  artinya untuk mengetahui apakah
        //sedang berada di perencanaan,pengendalian,evaluasi,...,peningkatan

        //id adalah penghubung ke profil sebagai patokan aja

        if ($request->has('op_perencanaan_hidden1') && $request->has('op_perencanaan_hidden2')) {
            switch ($request->op_perencanaan_hidden2) {
                case "perencanaan":
                    $profilmodel = PerencanaanModel::updateOrCreate(
                        ['profil_id' => $request->op_perencanaan_hidden1],
                        [
                            'tujuan' => $request->op_perencanaan_t1,
                            'sasaran' =>  $request->op_perencanaan_t2
                        ]
                    );

                    if ($profilmodel != null) {
                        return back()->with('success', 'Simpan berhasil');
                    } else {
                        return back()->with('pesan', 'Gagal di simpan');
                    }

                    break;

                case "penetapan":
                    break;

                case "pelaksanaan":
                    break;

                case "evaluasi":
                    break;

                case "pengendalian":
                    break;

                case "penetapan":
                    break;

                default:
                    echo "Alternatif tidak ditemukan";
            }
        }
    }

    public function operator_standar_mutu(Request $request, $category_ids)
    {
        if ($category_ids !== null) {
            // session([
            //     'user_id'=>1,
            //     'profil_id'=>1,
            //     'level'=>0,
            // ]);

            $categorymodel = CategoryModel::find($category_ids);
            $standarmodel = StandarModel::where([
                'category_id' => $category_ids,
            ])->get();
            $standarawalmodel = StandarAwalModel::find(Session::get('profil_id'));
            $profilmodel = ProfilModel::find(Session::get('profil_id'));

            return view("home/operator_standar", ['profilmodel' => $profilmodel, 'standarmodel' => $standarmodel, 'standarawal' => $standarawalmodel, 'category' => $categorymodel, 'category_ids_id' => $category_ids]);
        } else {
            return 'hmm';
        }
    }

    static function operator_standar_mutu_check_file($dikti_id)
    {
        $standar = StandarModelFile::where([
            'dikti_id' => $dikti_id,
            'profil_id' => Session::get('profil_id')
        ])->first();

        return $standar;
    }

    function operator_standar_mutu_borang(Request $request, $category_ids, $standar_dikti_ids)
    {
        if ($category_ids !== null) {


            $categorymodel = CategoryModel::find($category_ids);
            $standarmodel = StandarModel::where('category_id', '=', $category_ids)
                ->where('id', '=', $standar_dikti_ids)
                ->first();
            $standarawalmodel = StandarAwalModel::where('category_id', '=', $category_ids)
                ->where('profil_id', '=', Session::get('profil_id'))
                ->where('category_id', '=', $category_ids)
                ->where('standar_dikti_id', '=', $standar_dikti_ids)
                ->get();


            return view("home/operator_spm_input_data", ['standaredit' => null, 'standarmodel' => $standarmodel, 'standarawal' => $standarawalmodel, 'category' => $categorymodel, 'category_ids' => $category_ids, 'standar_dikti_ids' => $standar_dikti_ids, 'status' => 'sip2']);
        } else {
            return 'hmm';
        }
    }

    function operator_standar_mutu_borang_save(Request $request, $category_ids, $standar_dikti_ids)
    {
        $standarawalmodel = new StandarAwalModel;
        $standarawalmodel->category_id      = $category_ids;
        $standarawalmodel->profil_id        = Session::get('profil_id');
        $standarawalmodel->standar_dikti_id = $standar_dikti_ids;
        $standarawalmodel->s1               = $request->op_standar_input;
        $standarawalmodel->s2               = $request->op_indikator_input;
        $standarawalmodel->s3               = $request->op_strategi_input;

        if ($standarawalmodel->save()) {
            return back()->with('success', 'Data berhasil disimpan');
        } else {
            return back()->with('pesan', 'Data tidak berhasil disimpan');
        }
    }

    function operator_standar_mutu_borang_berkas_save(Request $request, $category_ids, $standar_dikti_ids = null)
    {
        if ($request->has('sip')) {

            $destinationPath = public_path() . '/unggah/unggah_spm';

            foreach ($request->file() as $key => $rf) {
                $key_on_berkas_standar = explode("_", $key)[4];

                $file_name =  md5(md5(md5($rf->getClientOriginalName()))) . "." . $rf->getClientOriginalExtension();
                $rf->move($destinationPath, $file_name);


                // StandarModel::where([
                //     'id' => $key_on_berkas_standar,
                //     'category_id' => $category_ids
                // ])->update([
                //     'file_standar'=> $file_name
                // ]);



                // $newfile = new StandarModelFile;
                // $newfile->dikti_id = $key_on_berkas_standar;
                // $newfile->profil_id = Session::get('profil_id');
                // $newfile->file_standar = $file_name;

                $newfile = StandarModelFile::updateOrCreate([
                    'dikti_id' => $key_on_berkas_standar,
                    'profil_id' => Session::get('profil_id')
                ], [
                    'file_standar' => $file_name
                ]);

                //$newfile->save();
            }


            return redirect()->route('operator_standar_mutu', ['/' => $category_ids])->with('success', 'File anda telah berhasil di upload');
        } else {
            return redirect()->route('operator_standar_mutu', ['/' => $category_ids])->with('pesan', 'File anda tidak berhasil di upload');
        }
    }

    function operator_standar_mutu_borang_edit_save(Request $request, $category_ids, $standar_dikti_ids)
    {
        if ($request->has('id')) {
            $standar_edit_awalmodel = StandarAwalModel::where([
                'id'         => $request->id,
                'category_id' => $category_ids,
                'profil_id' => Session::get('profil_id'),
                'standar_dikti_id' => $standar_dikti_ids
            ])
                ->update([
                    's1'            => $request->op_standar_input,
                    's2'            => $request->op_indikator_input,
                    's3'            => $request->op_strategi_input,
                    'updated_at'    =>  date('Y-m-d G:i:s')
                ]);

            if ($standar_edit_awalmodel == true) {
                return redirect()->route('operator_borang_spm_view', ['category_ids' => $category_ids, 'standar_dikti_ids' => $standar_dikti_ids])->with('success', 'Data berhasil disimpan');;
            }
        }
    }

    function operator_standar_mutu_borang_edit(Request $request, $category_ids, $standar_dikti_ids)
    {
        $standar_edit_awalmodel = StandarAwalModel::where([
            'id'         => $request->id,
            'category_id' => $category_ids,
            'profil_id' => Session::get('profil_id'),
            'standar_dikti_id' => $standar_dikti_ids
        ])->first();



        if ($category_ids !== null) {
            // session([
            //     'user_id'=>1,
            //     'profil_id'=>7,
            //     'level'=>0,
            // ]);

            $profilmodel = ProfilModel::where('id', '=', Session::get('profil_id'));
            $categorymodel = CategoryModel::find($category_ids);
            $standarmodel = StandarModel::where('category_id', '=', $category_ids)

                ->where('id', '=', $standar_dikti_ids)
                ->first();
            $standarawalmodel = StandarAwalModel::where('category_id', '=', $category_ids)
                ->where('profil_id', '=', Session::get('profil_id'))
                ->where('category_id', '=', $category_ids)
                ->where('standar_dikti_id', '=', $standar_dikti_ids)
                ->get();


            return view("home/operator_spm_input_data", ['profilmodel' => $profilmodel, 'standaredit' => $standar_edit_awalmodel, 'standarmodel' => $standarmodel, 'standarawal' => $standarawalmodel, 'category' => $categorymodel, 'category_ids' => $category_ids, 'standar_dikti_ids' => $standar_dikti_ids, 'status' => 'sip2']);
        } else {
            return 'hmm';
        }
    }

    function operator_standar_mutu_borang_delete(Request $request, $category_ids, $standar_dikti_ids)
    {
        $standarawalmodel = StandarAwalModel::where([
            'id'         => $request->id,
            'category_id' => $category_ids,
            'profil_id' => Session::get('profil_id'),
            'standar_dikti_id' => $standar_dikti_ids
        ])->delete();

        if ($standarawalmodel) {
            return back()->with('success', 'Data berhasil dihapus');
        } else {
            return back()->with('pesan', 'Data tidak berhasil dihapus');
        }
    }

    function operator_standar_mutu_save(Request $request, $category_ids)
    {
        if ($request->has('sip')) {
            //exit(var_dump($request->file()));
            session(['category_ids' => $category_ids]);

            foreach ($request->file() as $key => $rf) {
                $key_on_standar_dikti = explode("_", $key)[4];
                session(['standar_dikti_ids' => $key_on_standar_dikti]);
                Excel::import(new StandarImport, $request->file($key));
            }


            return redirect()->route('operator_standar_mutu', ['/' => $category_ids])->with('success', 'File anda telah berhasil di upload');
        } else {
            return redirect()->route('operator_standar_mutu', ['/' => $category_ids])->with('pesan', 'File anda tidak berhasil di upload');
        }
    }

    function operator_standar_mutu_download(Request $request, $category_ids)
    {
        if ($category_ids !== null) {
            $standarmodel = StandarModel::find($category_ids);
            return Excel::download(new ExportStandar($request->id, $category_ids), 'Hasil-' . $standarmodel->nama_standar . '-' . date("dmy") . ".xlsx");
        }
    }

    public function operator_manual_mutu(Request $request)
    {
        //untuk manual mutu
        if ($request->has('standard_category_ids')) {
            $indikatormodel = IndikatorModel::find($request->standard_category_ids);
        }
    }

    public function operator_profil_add_data()
    {
        $profilmodel = new ProfilModel;

        return view("home/operator_profil_tambah_data", ['borang' => 'input']);
    }

    public function operator_profil_add_data_save()
    {
        return view("home/operator_profil_tambah_data", ['borang' => 'input']);
    }

    public function operator_prodi_add_data()
    {
        return view("home/operator_prodi_tambah_data");
    }
}
