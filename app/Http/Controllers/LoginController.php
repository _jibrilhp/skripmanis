<?php

namespace App\Http\Controllers;

use Session;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use App\Models\ProfilModel as ProfilModel;
use App\Models\UserModel as UserModel;


//Jibril Hartri Putra 3 Ramadhan 1440 H

class LoginController extends Controller
{

    use AuthenticatesUsers;

    protected $maxLoginAttempts = 5;
    protected $lockoutTime = 60;


    function login_view_index(Request $request)
    {
        if (Session::get('username') !== null) {
            return redirect()->route('home_index_view');
        } else {
            return view('home.login_spmi_view');
        }
        
    }

    function logout_action(Request $request)
    {
        Auth::logout();
        $request->session()->flush();        
       
        return redirect()->route('login_index_view');
    }

    function login_authenticate (Request $request)
    {
        if ($this->hasTooManyLoginAttempts($request)) 
        {
            $this->fireLockoutEvent($request);
    
            return back()->with(['warning' => 'Anda berkali-kali melakukan kesalahan dalam login, Anda tidak diizinkan login untuk beberapa menit..']);
        }

        if (Auth::attempt(['username' =>$request->spmi_username , 'password' => $request->spmi_password ]))
        {

            if ($request->spmi_username !== null && $request->spmi_password !== null)
            {
                $userMod = UserModel::where([
                    'username' => $request->spmi_username
                ])
                ->orWhere([
                    'email' => $request->spmi_username
                ]);

                $userReq = UserModel::where([
                    'username' => $request->spmi_username
                ])
                ->orWhere([
                    'email' => $request->spmi_username
                ])->first();

                if ($userReq !== null) {
                    if (password_verify($request->spmi_password,$userReq->password)) {
                        $this->clearLoginAttempts($request);
                        $userMod->update(['updated_at' => date("Y-m-d H:i:s")]);
                        session([
                            'user_id'       => $userReq->id,
                            'view_user_id'       => $userReq->id,
                            'name'          => $userReq->name,
                            'username'      => $userReq->username,
                            'level'         => $userReq->level,
                            'profil_id'     => $userReq->profil_id, //default
                            'tahun_pengisian' => 2019, // default diganti pas klik profil
                            'fakultas_id'   => $userReq->fakultas_id,
                            'jabatan'       => $userReq->jabatan,
                            'email'         => $userReq->email,
                            'foto'          => $userReq->foto,
                            'telepon'       => $userReq->telepon,
                            
                        ]);

                        return redirect()->route('home_index_view');

                    } else{
                        $this->incrementLoginAttempts($request);
                        return back()->with(['warning' => 'Maaf username atau password Anda salah']);    
                    }
                } else {
                    $this->incrementLoginAttempts($request);
                    return back()->with(['warning' => 'Maaf username tidak ditemukan']);
                }
            } else {
                $this->incrementLoginAttempts($request);
                return back()->with(['warning' => 'Pengujian sistem terdeteksi']);
            }
        }  else {
            $this->incrementLoginAttempts($request);
            return back()->with(['warning' => 'Maaf username atau password Anda salah']);
        }
    }
}