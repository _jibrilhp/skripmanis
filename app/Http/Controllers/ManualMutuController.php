<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Exports\ExportStandarAwal as ExportStandar;
use App\Imports\ExcelProfilImport as ProfilImport;
use App\Imports\ExcelStandarAwalImport as StandarImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\ProfilModel as ProfilModel;
use App\Models\PenilaianModel as PenilaianModel;
use App\Models\PerencanaanModel as PerencanaanModel; 
use App\Models\IndikatorModel as IndikatorModel; 
use App\Models\StandarModel as StandarModel;
use App\Models\StandarAwalModel as StandarAwalModel; 
use App\Models\CategoryModel as CategoryModel; 
use App\Models\ManualMutuModel as ManualMutuModel;

//Jibril Hartri Putra 3 Ramadhan 1440 H

class ManualMutuController extends Controller
{
    function operator_manual_mutu_index(Request $request, $category_ids)
    {
        if ($category_ids !== null) {
            // session([
            //     'user_id'=>1,
            //     'profil_id'=>1,
            //     'level'=>0,
            // ]);
            $profilmodel = ProfilModel::where(['id' => Session::get('profil_id')])->first();    
            $categorymodel = CategoryModel::find($category_ids);
            $standarmodel = StandarModel::where('category_id','=',$category_ids)->get();
            $standarawalmodel = StandarAwalModel::find(Session::get('profil_id'));
            $indikatormodel = IndikatorModel::where('category_id','=',$category_ids)->get();
            

            return view("home/operator_manual_mutu",['profilmodel'=>$profilmodel,'standarmodel'=>$standarmodel,'standarawal'=>$standarawalmodel,'category'=>$categorymodel,'indikatormodel'=>$indikatormodel,'category_now'=>$category_ids]);


        } else {
            return 'hmm';
        }
    }

    function operator_manual_mutu_save(Request $request,$category_ids)
    {   
        if ($category_ids !== null) {
            $destinationPath = public_path() .'/unggah/unggah_mutu';
           

            foreach ($request->file() as $key=> $rf) {
                $key_on_category_id = explode("_",$key)[4];
                $key_on_standar_dikti_id = explode("_",$key)[5];
                
                $file_name =  md5(md5(md5($rf->getClientOriginalName()))) . "." . $rf->getClientOriginalExtension();
                $rf->move($destinationPath, $file_name);

                $manualmutu = ManualMutuModel::updateOrCreate(
                    [
                        'standar_category_id' => $key_on_category_id,
                        'standar_dikti_id' => $key_on_standar_dikti_id,
                        'profil_id'             => Session::get('profil_id')
                    ],
                    [
                        'category_id' => $category_ids, 
                        'file_location' =>   $file_name
                    ]);
            }

            return back()->with('success','File berhasil diupload');
            
        } else {
            return 'error..';
        }
    }

    function operator_manual_download(Request $request,$category_ids,$category_standar_ids,$standar_dikti_ids)
    {
        if ($category_ids !== null && $category_standar_ids !== null && $standar_dikti_ids !== null) {
            $manualmutu = ManualMutuModel::where([
                'category_id'           =>  $category_ids,
                'standar_category_id'   =>  $category_standar_ids,
                'standar_dikti_id'      => $standar_dikti_ids,
                'profil_id'             => Session::get('profil_id')
            ])->first();
            
            if ($manualmutu !== null) {
                $destinationPath = public_path() .'/unggah/unggah_mutu';
                return response()->file($destinationPath . "/" . $manualmutu->file_location);
            } else {
                return back()->with('success','File belum diupload..');
            }
        }
    }
}