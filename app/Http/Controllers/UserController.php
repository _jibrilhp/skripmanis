<?php

namespace App\Http\Controllers;

use Session;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use App\Models\ProfilModel as ProfilModel;
use App\Models\UserModel as UserModel;
use App\Models\FakultasModel as FakultasModel;

//Jibril Hartri Putra 3 Ramadhan 1440 H

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    function user_list_view_index (Request $request)
    {
        $profil_saya = UserModel::find(Session::get('view_user_id'));
        $fakultas   = FakultasModel::all();
        $profil_model = ProfilModel::all();

        return view('home.user_spmi_view',
        [
            'profil' => $profil_saya,
            'profil_prodi' => $profil_model,
            'fakultas' => $fakultas,
            'judul'     => "User Management",
            
            
        ]);
    }

    function user_list_view_save (Request $request,$user_id)
    {
        if ($user_id == null) {
            $profil_saya = UserModel::find(Session::get('view_user_id'));
        } else {
            $profil_saya = UserModel::find($user_id);
        }
        
        
        if ($request->sip == 'ok')
        {
            $profil_saya->name      = $request->user_fullname_value;
            $profil_saya->username  = $request->user_username_value;

            if ($request->user_password_value != null) 
            {
                $profil_saya->password = password_hash($request->user_password_value,PASSWORD_DEFAULT);
            }

            $profil_saya->email     = $request->user_email_value;
            $profil_saya->telepon   = $request->user_telepon_value;
            $profil_saya->jabatan   = $request->user_jabatan_value;

            if ($request->hasFile('user_foto_value')) {

                $nama_file_ext  = $request->file('user_foto_value')->getClientOriginalExtension();
                $nama_file_foto = md5(md5($request->file('user_foto_value')->getClientOriginalName() . rand (0,100)) ) . "." . $nama_file_ext;
                
                if (false !== mb_strpos($request->file('user_foto_value')->getMimeType(), "image")) {
                    $request->file('user_foto_value')->move(public_path('unggah/unggah_foto/'),$nama_file_foto );
                } else {
                    return 'file tidak sesuai';
                }
                
                
                $profil_saya->foto = $nama_file_foto;
            }

            if (Session::get('level') == 0) {
                $profil_saya->level = $request->user_level_value;
                $profil_saya->fakultas_id = $request->user_fakultas_value;
                $profil_saya->profil_id = $request->user_prodi_value;
            }

            

            if ($profil_saya->save()) {
                return back()->with('success','Berhasil disimpan');
            }
        } else {
            return 'hmm';
        }
    }

    function user_list_view_maintain(Request $request)
    {
        $profil_saya = UserModel::all();
        $fakultas   = FakultasModel::all();
        $profil_model = ProfilModel::all();

        return view('admin.view-user',
        [
            'profil' => $profil_saya,
            'profil_prodi' => $profil_model,
            'fakultas' => $fakultas,
            
            
        ]);
    }

    public function AuthRouteAPI(Request $request){
        return $request->user();
     }

     public function OraIso(Request $request){
        return "mohon maaf, silahkan pilih standar yang dituju.";
     }

    function user_admin_new_user_save(Request $request)
    {
        $auth_user  = Auth::user();
        $level_user = $auth_user->level;

        //jika level user selain superadmin, maka nggak bisa buat user samsek

        if ($level_user == 0) {
            $profil_saya            = new UserModel;
            $profil_saya->name      = $request->user_fullname_value;
            $profil_saya->username  = $request->user_username_value;
            $profil_saya->password  = password_hash($request->user_password_value,PASSWORD_DEFAULT);
            $profil_saya->level     = $request->user_level_value;
            
            if ($request->user_prodi_value != '-') {
                $profil_saya->profil_id     = (int) $request->user_prodi_value;
            } else {
                $profil_saya->profil_id     = -1;
            }

            if ($request->user_fakultas_value != '-') {
                $profil_saya->fakultas_id     = (int) $request->user_fakultas_value;
            } else {
                $profil_saya->fakultas_id     = -1;
            }

            if ($request->hasFile('user_foto_value')) {

                $nama_file_ext  = $request->file('user_foto_value')->getClientOriginalExtension();
                $nama_file_foto = md5(md5($request->file('user_foto_value')->getClientOriginalName() . rand (0,100)) ) . "." . $nama_file_ext;
                
                if (false !== mb_strpos($request->file('user_foto_value')->getMimeType(), "image")) {
                    $request->file('user_foto_value')->move(public_path('unggah/unggah_foto/'),$nama_file_foto );
                } else {
                    return 'file tidak sesuai';
                }
                
                
                $profil_saya->foto = $nama_file_foto;
            }

            $profil_saya->jabatan   = $request->user_jabatan_value;
            $profil_saya->email     = $request->user_email_value;
            $profil_saya->telepon   = $request->user_telepon_value;

            if ($profil_saya->save()) {
                return back()->with('success','Berhasil membuat user ' . $request->user_fullname_value);
            } else {
                return back()->with('pesan','Gagal membuat user ' . $request->user_fullname_value . ', periksa kembali nama user');
            }

        } else {
            return "tidak dapat membuat user";
        }


    }

    function user_admin_edit_view (Request $request, $user_id)
    {
        $profil_saya = UserModel::find($user_id);
        $fakultas   = FakultasModel::all();
        $profil_model = ProfilModel::all();

        return view('home.user_spmi_view',
        [
            'profil' => $profil_saya,
            'profil_prodi' => $profil_model,
            'fakultas' => $fakultas,
            'judul'     => "Edit User",
            
            
        ]);
    }

    

    function user_admin_delete_confirm (Request $request, $user_id)
    {
        $user_data = UserModel::find($user_id);
        if ($user_data->delete()) {
            return back()->with('success','Berhasil menghapus user..');
        } else {
            return back()->with('pesan','Gagal menghapus user..');
        }
    }

    static function getFakultasBagian ($bagian_id) {
        $fakultas = FakultasModel::find($bagian_id);

        if ($fakultas != null) {
            return $fakultas;
        }
    }

    static function getProfilProdi ($profil_id) {
        $profil = ProfilModel::find($profil_id);

        if ($profil != null) {
            return $profil;
        }
    }

}