<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * Indicates whether the XSRF-TOKEN cookie should be set on the response.
     *
     * @var bool
     */
    protected $addHttpCookie = true;

    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //
        'auditor/ami/userlist',
        '/auditor/ami/borang/tujuan/*',
        '/auditor/ami/borang/pertanyaan/*',
        '/auditor/ami/borang/lingkup/*',
        '/auditor/ami/borang/jadwal/*',
        '/auditor/ami/borang/temuan/*',
        '/auditor/ami/borang/saran/*',
        '/auditor/ami/borang/kesimpulan/*',
        '/emi/*/table/*',
        '/auditor/ami/tahap/auditor/form/daftar_pertanyaan/*',
        '/ami/tujuan/ajax/*',
        '/ami/lingkup/ajax/*',
        '/ami/kegiatan/ajax/*',
        '/ami/temuan/ajax/*',
        '/ami/saran/ajax/*',
        '/ami/kesimpulan/ajax/*',
        '/ami/ptk/ajax/*',
        '/th/emi/matriks_analisis/okesip',

        

    ];
}

