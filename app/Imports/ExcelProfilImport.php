<?php

namespace App\Imports;

use App\Models\ProfilModel as ProfilImport;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class ExcelProfilImport implements ToCollection
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */

    public function collection(Collection $rows)
    {
        
       if ($rows[0][1] == 'Profil Diri') {
        // Cek udah row 0-1 profil dirinya ada..
            $result_profil =  ProfilImport::updateOrCreate([
                'nama_prodi'    => $rows[9][3],
                'jenjang'       => $rows[8][3],
                'nama_fakultas' => $rows[6][3],
                'telepon_pt'    => $rows[7][3],
                'kode_pt'       => $rows[1][3],
                'nama_pt'       => $rows[2][3],
                'status_pt'     => $rows[4][3],
                'alamat_pt'     => $rows[3][3],
            ]);

            $last_result_id = $result_profil->id;

            $result_update_profil = ProfilImport::find($last_result_id);

            $result_update_profil->tgl_prog = $this->transformDate($rows[10][3]);
            $result_update_profil->no_sk = $rows[11][3];
            $result_update_profil->no_sk_operasional = $rows[12][3];
            $result_update_profil->tgl_pengisian  = $this->transformDate($rows[13][3]);
            $result_update_profil->akreditasi_prog = $rows[14][3];
            $result_update_profil->alamat_website  = $rows[15][3];
            $result_update_profil->alamat_email = $rows[16][3];
            $result_update_profil->telepon_prog = $rows[17][3];
            $result_update_profil->visi_prog  = $rows[18][3];
            $result_update_profil->misi_prog  = $rows[19][3];
            $result_update_profil->tujuan_prog  = $rows[20][3];
            $result_update_profil->kompetensi_prog  = $rows[21][3];
            $result_update_profil->peta_prog  = $rows[22][3];
            $result_update_profil->kriteria_mhs  = $rows[23][3];
            $result_update_profil->jumlah_sks_min  = $rows[25][3];
            $result_update_profil->jumlah_nilai_d  = $rows[26][3];
            $result_update_profil->ipk_minimal  = $rows[27][3];
            $result_update_profil->syarat_lainnya = $rows[28][3];
            $result_update_profil->jumlah_mhs_tahun = $rows[29][3];
            $result_update_profil->jumlah_mhs  = $rows[30][3];
            $result_update_profil->jumlah_dosen_tetap_ps = $rows[31][3];            
            $result_update_profil->jumlah_dosen_luar_ps = $rows[32][3];
            $result_update_profil->jumlah_tenaga_kependidikan  = $rows[33][3];
            $result_update_profil->identitas_nama_ketua_prod = $rows[35][3];
            $result_update_profil->identitas_nidn = $rows[36][3];
            $result_update_profil->identitas_nohp = $rows[37][3];


            if ($result_update_profil->save()) {
                return back()->with('pesan','Berhasil mengimport data..');
            } else{
                return back()->with('pesan','Terjadi kesalahan dalam mengimport data..');
            }

            
       }   else {
           return back()->with('pesan','Format yang anda masukkan tidak sesuai..');
       }   
    }

    public function transformDate($value, $format = 'Y-m-d')
    {
        try {
            return \Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($value));
        } catch (\ErrorException $e) {
            return \Carbon\Carbon::createFromFormat($format, $value);
        }
    }
}
