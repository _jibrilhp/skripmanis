<?php

namespace App\Imports;

use Session;
use App\Models\StandarAwalModel as StandarImport;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class ExcelStandarAwalImport implements ToCollection
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */

    public function collection(Collection $rows)
    {
       
       
       if ($rows[0][0] == 'Poin Standar' && $rows[0][1] == 'Isi Standar') {
        // Foreach data collectionnya

        foreach ($rows as $key => $r) {

            if ($key != 0) {

            $result_standar =  StandarImport::updateOrCreate([
                's1'            => $rows[$key][0],    
            ],[
                'category_id'        => Session::get('category_ids'),
                'profil_id'          => Session::get('profil_id'),
                'standar_dikti_id'   => Session::get('standar_dikti_ids'),
                's2'                 => $rows[$key][1],

            ]);

          }
        }  
   
       }   else {
           return back()->with('pesan','Format yang anda masukkan tidak sesuai..');
       }   
    }

    public function transformDate($value, $format = 'Y-m-d')
    {
        try {
            return \Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($value));
        } catch (\ErrorException $e) {
            return \Carbon\Carbon::createFromFormat($format, $value);
        }
    }
}
