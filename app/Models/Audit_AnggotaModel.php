<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Audit_AnggotaModel extends Model
{
    protected $table = 'tbl_audit_anggota';
    protected $fillable = ['audit_borang_id','audit_user_id'];
}
