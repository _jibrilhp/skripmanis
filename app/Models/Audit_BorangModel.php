<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Audit_BorangModel extends Model
{ 
    protected $table = 'tbl_audit_borang';
    protected $fillable = ['tanggalan','auditee_user_id','ketua_user_id','alamat','fakultas_direktorat','program_studi','telepon_program','audit_program_unit'];
}
