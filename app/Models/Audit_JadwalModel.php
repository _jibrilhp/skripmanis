<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Audit_JadwalModel extends Model
{
    protected $table = 'tbl_audit_jadwal';
    protected $fillable = ['tanggal','pukul','kegiatan_audit','audit_borang_id'];
}
