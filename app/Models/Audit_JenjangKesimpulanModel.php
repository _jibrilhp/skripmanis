<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Audit_JenjangKesimpulanModel extends Model
{
    protected $primaryKey  = 'jk_id';
    protected $table = 'tbl_audit_jenjang_kesimpulan';
    protected $fillable = ['jk_id','jk_jenjang_id','jk_isi_kesimpulan','jk_json_check','jk_json_value'];
}
