<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Audit_KesimpulanModel extends Model
{
    protected $table = 'tbl_audit_kesimpulan';
    protected $fillable = ['kesimpulan','yes_no_other_check','lainnya','audit_borang_id'];
}
