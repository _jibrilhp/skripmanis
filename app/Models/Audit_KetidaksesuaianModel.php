<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Audit_KetidaksesuaianModel extends Model
{
    protected $table = 'tbl_audit_temuan';
    protected $fillable = ['kts_ob','referensi_butir_mutu','pernyataan','audit_borang_id','audit_pertanyaan_id'];
}
