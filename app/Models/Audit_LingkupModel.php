<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Audit_LingkupModel extends Model
{
    protected $table = 'tbl_audit_lingkup';
    protected $fillable = ['audit_lingkup','audit_borang_id'];
}
