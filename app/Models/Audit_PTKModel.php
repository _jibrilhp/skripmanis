<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Audit_PTKModel extends Model
{
    protected $primaryKey  = 'ptk_id';
    protected $table = 'tbl_audit_ptk_form';
    protected $fillable = ['ptk_audit_jadwal_ref', 'ptk_input_no', 'ptk_input_uraian_temuan', 'ptk_input_rtk'];
}
