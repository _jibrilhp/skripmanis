<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Audit_PertanyaanModel extends Model
{
    protected $table = 'tbl_audit_pertanyaan';
    protected $fillable = ['referensi_butir_mutu','hasil_observasi','yes_no_check','catatan_khusus','audit_borang_id'];
}
