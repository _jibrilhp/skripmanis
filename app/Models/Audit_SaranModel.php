<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Audit_SaranModel extends Model
{
    protected $table = 'tbl_audit_saran_perbaikan';
    protected $fillable = ['bidang','kelebihan','peluang_peningkatan','audit_borang_id'];
}
