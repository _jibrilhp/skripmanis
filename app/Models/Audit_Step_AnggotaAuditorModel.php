<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Audit_Step_AnggotaAuditorModel extends Model
{
    protected $table = 'tbl_audit_borang_permintaan_auditor';
    protected $fillable = ['per_audit_id','per_audit_permintaan_id','per_audit_anggota_id'];
}
