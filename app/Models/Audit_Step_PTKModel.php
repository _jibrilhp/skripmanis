<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Audit_Step_PTKModel extends Model
{
    protected $table = 'tbl_audit_ptk';
}
