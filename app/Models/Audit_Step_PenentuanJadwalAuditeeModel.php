<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Audit_Step_PenentuanJadwalAuditeeModel extends Model
{
    protected $table = 'tbl_audit_borang_penentuan_jadwal_listauditee';
    protected $fillable = ['pen_jadwal_list_auditee_id','pen_jadwal_list_auditee_ref_id','pen_jadwal_list_auditee_user_id'];
}
