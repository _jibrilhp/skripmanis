<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Audit_Step_PenentuanJadwalAuditorModel extends Model
{
    protected $table = 'tbl_audit_borang_penentuan_jadwal_listauditor';
    protected $fillable = ['pen_jadwal_list_auditor_id','pen_jadwal_list_auditor_ref_id','pen_jadwal_list_auditor_user_id','pen_jadwal_list_auditor_user_status'];
}
