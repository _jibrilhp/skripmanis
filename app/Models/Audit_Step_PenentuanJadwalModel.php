<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Audit_Step_PenentuanJadwalModel extends Model
{
    protected $table = 'tbl_audit_borang_penentuan_jadwal';
    protected $fillable = ['pen_jadwal_id','pen_jadwal_prodi_id','pen_jadwal_tanggal','pen_jadwal_pukul','pen_jadwal_lingkup_audit'];
}
