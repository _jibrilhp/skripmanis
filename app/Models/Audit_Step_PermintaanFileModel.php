<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Audit_Step_PermintaanFileModel extends Model
{
    protected $table = 'tbl_audit_borang_permintaan_auditor_file';
    protected $fillable = ['per_auditor_id','per_auditor_permintaan_id','per_auditor_nama_file'];
}
