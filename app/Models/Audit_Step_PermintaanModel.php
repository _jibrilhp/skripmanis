<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Audit_Step_PermintaanModel extends Model
{
    protected $table = 'tbl_audit_borang_permintaan';
    protected $fillable = ['permintaan_id','permintaan_nama_audit','permintaan_profil_id'];
}
