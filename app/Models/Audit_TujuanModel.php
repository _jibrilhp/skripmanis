<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Audit_TujuanModel extends Model
{
    protected $table = 'tbl_audit_tujuan';
    protected $fillable = ['tujuan','yes_no_check','audit_jadwal_ref_id'];
}
