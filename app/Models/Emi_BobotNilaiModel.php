<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Emi_BobotNilaiModel extends Model
{
    protected $table = 'tbl_emi_bobot_nilai';
    protected $fillable = ['isian_standar_id','nilai'];
}
