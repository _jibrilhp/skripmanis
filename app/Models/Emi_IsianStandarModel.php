<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Emi_IsianStandarModel extends Model
{
    protected $table = 'tbl_emi_isian_standar';
    // protected $fillable = ['emi_profil_id','emi_tahun_standar','emi_standar_id','emi_isian_standar'];
}
