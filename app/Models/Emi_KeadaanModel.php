<?php

namespace App\Models;

use App\Models\Emi_KeadaanModel as EmiKeadaanModel;
use App\Models\Emi_StandarModel as EmiStandarModel;
use App\Models\Emi_IsianStandarModel as EmiIsianStandarModel; 
use App\Models\Emi_BobotNilaiModel as EmiBobotNilaiModel; 

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Emi_KeadaanModel extends Model
{
    protected $primaryKey  = 'keadaan_id';
    protected $table = 'tbl_emi_keadaan_prodi';
    protected $fillable = ['emi_jenjang_id','emi_standar_id','emi_isian_standar_id','emi_bobot_nilai','keadaan_prodi_saat_ini'];

    public static function getProcessNilaiCapaian($emi_tahun_standar,$emi_standar_id)
    {
        $standarlist =  Emi_KeadaanModel::getStandarList($emi_tahun_standar,$emi_standar_id);

        $arr = [];

        foreach ($standarlist as $key => $s)
        {
            
            $nilailist = Emi_KeadaanModel::getBobotNilai($s->id);
            
            if ($nilailist !== '-') {
                $keadaanlist = Emi_KeadaanModel::getKeadaanSaatIni($emi_tahun_standar,$emi_standar_id,$nilailist->nilai);

                $meong = array([
                    'isian_standar' => $s->emi_isian_standar,
                    'nilai' => $nilailist->nilai,
                    'keadaan' => $keadaanlist->keadaan_prodi_saat_ini,
                    'bobot_id' => $nilailist->bobot_id,
                    'isian_id'=> $s->id,
                    'bobot'=> Emi_KeadaanModel::getBobotNilaiPercentage(1),
                    'sebutan' => Emi_KeadaanModel::getBobotNilaiSebutan($nilailist->nilai)
                ]);
            } else {
                $meong = array([
                    'isian_standar' => $s->emi_isian_standar,
                    'nilai' => '-',
                    'keadaan' => '-',
                    'bobot_id' => '-',
                    'isian_id'=> $s->id,
                    'bobot'=> '-',
                    'sebutan' => '-'
                ]);
            }

            $arr = array_merge($arr,$meong);

        }
        return $arr;
    }

    public static function getBobotNilaiPercentage($bagian_standar) {
        switch ($bagian_standar)
        {
            case 1:
                return '0.83';
            break;
        }
    }

    public static function getBobotNilaiSebutan($angka) {
        switch ($angka)
        {
            case 1:
                return 'Perbaikan menyeluruh dan  mendesak';
            break;

            case 2:
                return 'Perbaikan mayor';
            break;

            case 3:
                return 'Perbaikan minor';
            break;

            case 4:
                return 'Cukup';
            break;

            case 5:
                return 'Lebih dari Cukup';
            break;

            case 6:
                return 'Baik';
            break;

            case 7:
                return 'Sangat Baik';
            break;
        }
    }

    public static function getStandarList($emi_tahun_standar,$emi_standar_id) 
    {
        $result = EmiIsianStandarModel::where([
            'emi_tahun_standar'  =>  $emi_tahun_standar,
            'emi_standar_id'     =>  $emi_standar_id
        ])->get();

        return $result;
    }

    public static function getKeadaanSaatIni($emi_tahun_standar,$emi_standar_id,$emi_bobot_nilai)
    {
        $result = EmiKeadaanModel::where([
            'emi_tahun_standar'  =>  $emi_tahun_standar,
            'emi_standar_id'     =>  $emi_standar_id,
            'emi_bobot_nilai'    =>  $emi_bobot_nilai
        ])->first();
 
        if ($result !== null) {
            return $result;
        } else {
            return "-";
        }
        
    }

    public static function getBobotNilai($emi_isian_standar)
    {
        $result = EmiBobotNilaiModel::where([
            'isian_standar_id'  =>  $emi_isian_standar,
            
        ])->first();

        if ($result !== null) {
            return $result;
        } else {
            return "-";
        }

        
    }

}
