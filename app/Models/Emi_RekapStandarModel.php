<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Emi_RekapStandarModel extends Model
{
    protected $primaryKey  = 'rekap_id';
    protected $table = 'tbl_emi_rekap_standar';
}
