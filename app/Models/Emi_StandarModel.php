<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Emi_StandarModel extends Model
{
    protected $table = 'tbl_emi_standar';
    protected $fillable = ['nama_standar'];
}
