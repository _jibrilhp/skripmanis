<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class FakultasModel extends Model
{
    protected $table = 'tbl_fakultas_dep_prodi';
    protected $fillable = ['nama_bagian'];
}
