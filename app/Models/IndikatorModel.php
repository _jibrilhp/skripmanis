<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class IndikatorModel extends Model
{
    protected $table = 'tbl_standard_category';
    protected $fillable = ['category_id','category_standard_name'];
}
