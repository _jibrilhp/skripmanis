<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class JenjangModel extends Model
{
    protected $primaryKey  = 'jenjang_id';
    protected $table = 'tbl_jenjang';
}
