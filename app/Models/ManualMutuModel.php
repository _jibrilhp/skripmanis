<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ManualMutuModel extends Model
{
    protected $table = 'tbl_manual_mutu';
    protected $fillable = ['profil_id','category_id','standar_category_id','standar_dikti_id','file_location'];
}
