<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MenuModel extends Model
{
    protected $table = 'tbl_menu';
    protected $fillable = ['parent_id','level','menu_name','menu_icon','route_name'];
}
