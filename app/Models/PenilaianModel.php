<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PenilaianModel extends Model
{
    protected $table = 'tbl_penilaian';
    protected $fillable = ['profil_id','bagian','rekap_nilai',
    'bobot_standar','nilai_bobot_standar','nilai_capaian_standar',
    'sebutan','temuan','hasil_temuan','hasil_temuan_disebabkan',
    'rekomendasi','format_standar','format_indikator','format_pencapaian',
    'format_midline','format_baseline'];

    public function getPenilaian($profilid)
    {
        $result = DB::table('tbl_penilaian')
                    ->rightjoin('tbl_profil','tbl_penilaian.profil_id','=','tbl_profil.id')
                    ->select('*','tbl_penilaian.id as penilaian_id')
                    ->where('tbl_profil.id','=',$profilid)
                    ->get();
        return $result;
    }

  
}
