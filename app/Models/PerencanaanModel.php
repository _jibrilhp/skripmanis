<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PerencanaanModel extends Model
{
    protected $table = 'tbl_perencanaan';
    protected $fillable = ['profil_id','tujuan','sasaran'];


    public function createPerencanaan($profilid) 
    {

    }

    public function getPerencanaan($profilid)
    {
        $result = DB::table('tbl_perencanaan')
                    ->rightjoin('tbl_profil','tbl_perencanaan.profil_id','=','tbl_profil.id')
                    ->select('*','tbl_perencanaan.id as perencanaan_id')
                    ->where('tbl_profil.id','=',$profilid)
                    ->get();
        return $result;
    }

}
