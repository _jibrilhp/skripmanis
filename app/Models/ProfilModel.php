<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ProfilModel extends Model
{
    protected $table = 'tbl_profil';
    protected $fillable = [
        'nama_prodi',
        'jenjang',
        'nama_fakultas',
        'telepon_pt',
        'kode_pt',
        'nama_pt',
        'status_pt',
        'alamat_pt',
        'tgl_prog',
        'no_sk',
        'no_sk_operasional',
        'tgl_pengisian',
        'akreditasi_prog',
        'alamat_website',
        'alamat_email',
        'telepon_prog',
        'visi_prog',
        'misi_prog',
        'kompetensi_prog',
        'peta_prog',
        'kriteria_mhs',
        'jumlah_sks_min',
        'jumlah_nilai_d',
        'ipk_minimal',
        'jumlah_mhs_tahun',
        'jumlah_mhs',
        'jumlah_dosen_tetap_ps',
        'jumlah_dosen_luar_ps',
        'jumlah_tenaga_kependidikan',
        'identitas_nama_ketua_prod',
        'identitas_nidn',
        'identitas_nohp',
    ];

}
