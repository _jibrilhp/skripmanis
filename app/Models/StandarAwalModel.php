<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class StandarAwalModel extends Model
{
    protected $table = 'tbl_standar_awal';
    protected $fillable = ['profil_id','category_id','category_standar_id','standar_dikti_id','s1','s2'];
}
