<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class StandarModel extends Model
{
    protected $table = 'tbl_standar_dikti';
    protected $fillable = ['profil_id','standar_awal_id','nama_standar'];
}
