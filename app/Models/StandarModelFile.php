<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class StandarModelFile extends Model
{
    protected $primaryKey  = 'dikti_file_id';
    protected $table = 'tbl_standar_dikti_file';
    protected $fillable = ['file_standar'];
    
}
