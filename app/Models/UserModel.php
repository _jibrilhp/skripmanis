<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UserModel extends Model
{
    protected $table = 'users';
    protected $fillable = ['name','email','password','level','fakultas_id','jabatan','email','foto','telepon'];
}
