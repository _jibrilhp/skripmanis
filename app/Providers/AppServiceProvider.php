<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Events\Dispatcher;
use JeroenNoten\LaravelAdminLte\Events\BuildingMenu;
use App\Models\MenuModel as MenuModel;
use Session;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Dispatcher $events)
    {
        $events->listen(BuildingMenu::class, function (BuildingMenu $event) {
            $event->menu->add('MENU UTAMA');
            
            $menumodel = new MenuModel();
            $menumodel = $menumodel::orderBy('parent_id', 'ASC')->get();
     

            foreach ($menumodel as $m) {
             
                if ($m->parent_id == -1) {
                    $event->menu->add([
                        'text'        => $m->menu_name,
                        'icon'        => $m->menu_icon,
                        'label_color' => 'warning',
                        'submenu' => $this->getSubMenu($m->id,$m->level+1),
                    ]);
                }
            }
        });
    }

    function getDeepMenu($menu_id,$level) {
        $menumodel = MenuModel::where([
            'parent_id' => $menu_id,
            'level' => $level
        ])->get();

        if ($menumodel->count()) {
            return true;
        } else {
            return false;
        }
    }

    function getSubMenu($menu_id,$level) {
        
        //check has a deeper level hmm?
        
        $menumodel = MenuModel::where([
            'parent_id' => $menu_id,
            'level' => $level,
            'menu_enabled' => 1
        ])->get();

        $menuarray = array();

        foreach ($menumodel as $m) {
            if ($this->getDeepMenu($m->id,$m->level+1)) {
                array_push($menuarray,array(
                    'text'  => $m->menu_name,
                    'icon'  => $m->menu_icon,
                    'url'   => str_ireplace("{tahun_pengisian}",Session::get('tahun_pengisian'),str_ireplace("{profil_id}",Session::get('profil_id'),$m->route_name)),        
                    'submenu' => $this->getSubMenu($m->id,$m->level+1),
                ));  
            } else {
                array_push($menuarray,array(
                    'text'  => $m->menu_name,
                    'icon'  => $m->menu_icon,
                    'url'   => str_ireplace("{tahun_pengisian}",Session::get('tahun_pengisian'),str_ireplace("{profil_id}",Session::get('profil_id'),$m->route_name)),                    
                ));  
            }
        }

        return $menuarray;
    }
}
