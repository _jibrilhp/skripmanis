<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_Profil', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama_prodi')->unique();
            $table->string('jenjang');
            $table->string('nama_fakultas');
            $table->string('telepon_pt'); 
            $table->string('kode_pt'); 
            $table->string('nama_pt'); 
            $table->string('status_pt'); 
            $table->string('alamat_pt');
            $table->string('tgl_prog')->nullable(); 
            $table->string('no_sk')->nullable(); 
            $table->string('no_sk_operasional')->nullable(); 
            $table->string('tgl_pengisian')->nullable(); 
            $table->string('akreditasi_prog')->nullable(); 
            $table->string('alamat_website')->nullable(); 
            $table->string('alamat_email')->nullable(); 
            $table->string('telepon_prog')->nullable(); 
            $table->string('visi_prog')->nullable(); 
            $table->text('misi_prog')->nullable(); 
            $table->text('tujuan_prog')->nullable(); 
            $table->text('kompetensi_prog')->nullable(); 
            $table->text('peta_prog')->nullable(); 
            $table->string('kriteria_mhs')->nullable(); 
            $table->string('jumlah_sks_min')->nullable(); 
            $table->string('jumlah_nilai_d')->nullable(); 
            $table->string('ipk_minimal')->nullable();
            $table->string('syarat_lainnya')->nullable(); 
            $table->string('jumlah_mhs_tahun')->nullable(); 
            $table->string('jumlah_mhs')->nullable(); 
            $table->string('jumlah_dosen_tetap_ps')->nullable(); 
            $table->string('jumlah_dosen_luar_ps')->nullable(); 
            $table->string('jumlah_tenaga_kependidikan')->nullable(); 
            $table->string('identitas_nama_ketua_prod')->nullable(); 
            $table->string('identitas_nidn')->nullable(); 
            $table->string('identitas_nohp')->nullable(); 

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_Profil');
    }
}
