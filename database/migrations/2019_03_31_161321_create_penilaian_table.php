<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenilaianTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_Penilaian', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('profil_id');
            $table->integer('user_id');
            $table->string('bagian');
            $table->text('rekap_nilai'); //dibuat filter untuk WHERE
            $table->double('bobot_standar')->nullable();
            $table->double('nilai_bobot_standar')->nullable();
            $table->double('nilai_capaian_standar')->nullable();
            $table->text('sebutan')->nullable();
            $table->text('temuan')->nullable();
            $table->text('hasil_temuan')->nullable();
            $table->text('hasil_temuan_disebabkan')->nullable();
            $table->text('rekomendasi')->nullable();

            

            //untuk bagian upload file
            $table->text('format_standar')->nullable();
            $table->text('format_indikator')->nullable();
            $table->text('format_pencapaian')->nullable();
            $table->text('format_midline')->nullable();
            $table->text('format_baseline')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_Penilaian');
    }
}
