<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStandarAwal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_standar_awal', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('category_id');
           // $table->integer('category_standar_id'); // ini buat dalemannya
            $table->integer('profil_id');
            $table->integer('user_id');
            $table->integer('standar_dikti_id'); //buat list standarnya apaan aja
            $table->longtext('s1');
            $table->longtext('s2');
            $table->longtext('s3')->nullable();
            $table->timestamps();
      
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_standar_awal');
    }
}
