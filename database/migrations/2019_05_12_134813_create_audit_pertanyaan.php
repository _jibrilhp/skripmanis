<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuditPertanyaan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_audit_pertanyaan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('referensi_butir_mutu')->nullable();
            $table->longtext('hasil_observasi');
            $table->integer('yes_no_check')->comment('kalau 1 ya, 0 tidak');
            $table->text('catatan_khusus')->nullable();
            $table->integer('audit_borang_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_audit_pertanyaan');
    }
}
