<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuditBorang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_audit_borang', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('tanggalan')->comment('hari, tanggal, pukul..');
            $table->integer('auditee_user_id');
            $table->integer('ketua_user_id');
            $table->text('alamat');
            $table->string('fakultas_direktorat');
            $table->string('program_studi');
            $table->string('telepon_program');
            $table->string('telepon_audit_program');
            $table->string('audit_program_unit');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_audit_borang');
    }
}
