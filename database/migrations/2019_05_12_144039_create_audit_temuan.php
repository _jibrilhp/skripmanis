<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuditTemuan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_audit_temuan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kts_ob');
            $table->string('referensi_butir_mutu');
            $table->longtext('pernyataan');
            $table->integer('audit_borang_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_audit_temuan');
    }
}
