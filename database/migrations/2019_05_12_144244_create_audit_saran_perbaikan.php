<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuditSaranPerbaikan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_audit_saran_perbaikan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('bidang');
            $table->text('kelebihan')->nullable();
            $table->text('peluang_peningkatan')->nullable();
            $table->integer('audit_borang_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_audit_saran_perbaikan');
    }
}
