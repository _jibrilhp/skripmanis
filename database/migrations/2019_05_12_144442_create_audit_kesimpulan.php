<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuditKesimpulan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_audit_kesimpulan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('kesimpulan');
            $table->integer('yes_no_other_check');
            $table->longtext('lainnya')->nullable();
            $table->integer('audit_borang_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_audit_kesimpulan');
    }
}
