<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblEmiStandar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_emi_standar', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama_standar');      
            //$table->integer('tampilkan_standar')->default('1')->comment('kalau 1 berarti tampilkan, kalau 0 standar itu disembunyikan');
            //$table->integer('urutan')->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_emi_standar');
    }
}
