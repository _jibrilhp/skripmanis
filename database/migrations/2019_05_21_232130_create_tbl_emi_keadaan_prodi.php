<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblEmiKeadaanProdi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_emi_keadaan_prodi', function (Blueprint $table) {
            $table->bigIncrements('keadaan_id');
            $table->integer('emi_profil_id');
            $table->integer('emi_tahun_standar');   
            $table->integer('emi_standar_id');
            $table->integer('emi_isian_standar_id');
            $table->integer('emi_bobot_nilai'); // 1 isinya apa, 7 .. 
            $table->longtext('keadaan_prodi_saat_ini');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_emi_keadaan_prodi');
    }
}
