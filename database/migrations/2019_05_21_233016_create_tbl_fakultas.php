<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblFakultas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_fakultas_dep_prodi', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('id');
            $table->string('nama_bagian')->comment('bisa fakultas, departemen, prodi, lembaga..');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_fakultas');
    }
}
