<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblEmiIsianStandar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_emi_isian_standar', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('emi_profil_id');
            $table->integer('emi_tahun_standar');   
            $table->integer('emi_standar_id');
            $table->longtext('emi_isian_standar');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_emi_isian_standar');
    }
}
