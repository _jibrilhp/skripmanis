<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewTabelAuditBorangPermintaan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_audit_borang_permintaan', function (Blueprint $table) {
            $table->bigIncrements('permintaan_id');
            $table->string('permintaan_nama_audit');
            $table->integer('permintaan_profil_id');
            //$table->longtext('permintaan_json_upload_surat')
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_audit_borang_permintaan');
    }
}
