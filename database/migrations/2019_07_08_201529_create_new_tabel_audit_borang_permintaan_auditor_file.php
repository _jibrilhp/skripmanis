<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewTabelAuditBorangPermintaanAuditorFile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_audit_borang_permintaan_auditor_file', function (Blueprint $table) {
            $table->bigIncrements('per_auditor_id');
            $table->integer('per_auditor_permintaan_id');
            $table->longtext('per_auditor_nama_file');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_audit_borang_permintaan_auditor');
    }
}
