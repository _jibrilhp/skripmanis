<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewTabelAuditBorangPermintaanAuditor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_audit_borang_permintaan_auditor', function (Blueprint $table) {
            $table->bigIncrements('per_audit_id');
            $table->integer('per_audit_permintaan_id');
            $table->integer('per_audit_anggota_id')->comment('id user dari si anggota ini siapa aja');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_audit_borang_permintaan_auditor');
    }
}
