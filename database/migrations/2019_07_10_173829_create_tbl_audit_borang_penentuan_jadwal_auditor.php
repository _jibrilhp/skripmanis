<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblAuditBorangPenentuanJadwalAuditor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_audit_borang_penentuan_jadwal', function (Blueprint $table) {
            $table->bigIncrements('pen_jadwal_id');
            $table->integer("pen_jadwal_prodi_id")->comment("diambil dari profil prodi");
            $table->string("pen_jadwal_tanggal");
            $table->string("pen_jadwal_pukul");
            $table->string("pen_jadwal_lingkup_audit");
            $table->string("pen_jadwal_upload_surat_tugas");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_audit_borang_penentuan_jadwal');
    }
}
