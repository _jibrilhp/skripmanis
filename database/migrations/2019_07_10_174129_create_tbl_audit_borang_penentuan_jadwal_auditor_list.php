<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblAuditBorangPenentuanJadwalAuditorList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_audit_borang_penentuan_jadwal_listauditor', function (Blueprint $table) {
            $table->bigIncrements('pen_jadwal_list_auditor_id');
            $table->string('pen_jadwal_list_auditor_ref_id');
            $table->integer('pen_jadwal_list_auditor_user_id');
            $table->integer('pen_jadwal_list_auditor_user_status');
            $table->integer('pen_jadwal_list_auditor_user_kesediaan')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_audit_borang_penentuan_jadwal_listauditor');
    }
}
