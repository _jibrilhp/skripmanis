<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblAuditBorangPenentuanJadwalAuditeeList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_audit_borang_penentuan_jadwal_listauditee', function (Blueprint $table) {
            $table->bigIncrements('pen_jadwal_list_auditee_id');
            $table->string('pen_jadwal_list_auditee_ref_id');
            $table->integer('pen_jadwal_list_auditee_user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_audit_borang_penentuan_jadwal_listauditee');
    }
}
