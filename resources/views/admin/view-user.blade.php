@php use \App\Http\Controllers\UserController; @endphp
@extends('adminlte::page')


@section('title', 'SPMI dan SPME')


@section('content_header')

    <h1>Pengelolaan User</h1>

@stop


@section('content')


@if (Session::has('success'))
    <br>
        <div class="alert alert-success">
           
            {!! \Session::get('success') !!}
            
        </div>
@endif

@if (Session::has('pesan'))
    <br>
        <div class="alert alert-warning">
           
            {!! \Session::get('pesan') !!}
            
        </div>
@endif

<div class='row'>
        <div class='col-md-12'>
            <!-- Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Manajemen user</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
              
                <div class="box-body">
                    <table id="tabel_manajemen_user" class="table table-bordered table-hover" width="100%">
                        <thead>
                            <th>Nama Lengkap</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Telepon</th>
                            <th>Jabatan</th>
                            <th>Foto</th>
                            <th>Level</th>
                            <th>Jabatan</th>
                            <th>Bagian</th>
                            <th>Prodi</th>
                            <th>Aksi</th>
                        </thead>
                        <tbody>
                        @foreach ($profil as $p)
                            <tr>
                                <td>{{$p->name}}</td>
                                <td>{{$p->username}}</td>
                                <td>{{$p->email}}</td>
                                <td>{{$p->telepon}}</td>
                                <td>{{$p->jabatan}}</td>
                                <td>@if ($p->foto != '') <img src="{{asset('/unggah/unggah_foto/' .$p->foto)}}" width="100px" height="100px">@endif</td>
                                <td>@if ($p->level == 0) SuperAdmin (KJM) @elseif ($p->level == 1) Admin (Auditor) @else Auditee (Kajur) @endif</td>
                                <td>{{$p->jabatan}}</td>
                                <td>{{UserController::getFakultasBagian($p->fakultas_id)->nama_bagian}}</td>
                                <td>{{UserController::getProfilProdi($p->profil_id)->nama_prodi}}</td>
                                <td><a href="{{route('user_admin_edit_view',['user_id'=>$p->id])}}"><i class="fa fa-edit"></i></a> | <a href="{{route('user_admin_delete_confirm',['user_id'=>$p->id])}}" class="confirmation-question" data-confirmation="Apakah anda ingin MENGHAPUS user {{ $p->name }} ?"><i class="fa fa-trash"></i></a></td>
                            </tr>
                        </tbody>
                        @endforeach
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->

    </div><!-- /.row -->

    



<div class='row'>
        <div class='col-md-12'>
            <!-- Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Tambah user</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
              
                <div class="box-body">
                    <form method="post" enctype="multipart/form-data" class="form-horizontal" action="{{route('user_admin_newuser_save') }}">
                    <input type="hidden" value="ok" name="sip">

                        @csrf
                    <!-- <hr style="display: block; height: 1px; border: 0; border-top: 1px solid #ccc; margin: 1em 0;padding: 0; "> -->
                  
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="inputUsername4" class="col-sm-2 control-label">Username</label>
                                <div class="col-sm-10">
                                    <input name="user_username_value" class="form-control" id="inputUsername4" placeholder="Silahkan masukan username" value="">
                                </div>
                            </div>  

                            <div class="form-group">
                                <label for="inputFullname4" class="col-sm-2 control-label">Nama Lengkap</label>
                                <div class="col-sm-10">
                                    <input name="user_fullname_value" class="form-control" id="inputFullname4" placeholder="Silahkan masukan nama lengkap" value="">
                                </div>
                            </div>
                      
                            <div class="form-group">
                                <label for="inputPasswordname4" class="col-sm-2 control-label">Password</label>
                                <div class="col-sm-10">
                                    <input type="password" name="user_password_value" class="form-control" id="inputPasswordname4" placeholder="Masukkan password/Kata sandi untuk user ini" value="">
                                    <h6>Masukkan password/Kata sandi untuk user ini</h6>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail4" class="col-sm-2 control-label">Email</label>
                                <div class="col-sm-10">
                                    <input name="user_email_value" class="form-control" id="inputEmail4" placeholder="Silahkan masukan email anda" value="">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputTelepon4" class="col-sm-2 control-label">Telepon</label>
                                <div class="col-sm-10">
                                    <input name="user_telepon_value" class="form-control" id="inputTelepon4" placeholder="Silahkan masukan telepon anda" value="">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputJabatan4" class="col-sm-2 control-label">Jabatan</label>
                                <div class="col-sm-10">
                                    <input name="user_jabatan_value" class="form-control" id="inputJabatan4" placeholder="Silahkan masukan jabatan" value="">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputFoto4" class="col-sm-2 control-label">Foto</label>
                                <div class="col-sm-10">
                                    
                                    <input type="file" name="user_foto_value" id="inputFoto4" placeholder="" value="">
                                    
                                    
                                    <h6> Foto untuk user ini </h6>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-10">
                                    <h5> Pengaturan </h5>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputLevel4" class="col-sm-2 control-label">Level User</label>
                                <div class="col-sm-10">
                                    <select name="user_level_value" class="form-control" id="inputLevel4" >
                                        <option  value="0"> Super Admin (KJM) </option>
                                        <option  value="1"> Admin (Auditor) </option>
                                        <option  value="2"> User (Auditee/Kajur) </option>
                                    </select>
                                </div>
                            </div>  
                        
                            <div class="form-group">
                                <label for="inputFakultas4" class="col-sm-2 control-label">Bagian</label>
                                <div class="col-sm-10">
                                    <select name="user_fakultas_value" class="form-control" id="inputFakultas4">
                                        <option value="-">Pilih Fakultas/Departemen/Prodi Anda</option>
                                        @foreach ($fakultas as $f)
                                        <option  value="{{$f->id}}">{{$f->nama_bagian}}</option>
                                        @endforeach
                                    </select>
                                    <h6>Superadmin/Admin tidak memerlukan pilihan ini</h6>
                                </div>
                                
                            </div>

                            <div class="form-group">
                                <label for="inputProfilModel4" class="col-sm-2 control-label">Profil Prodi</label>
                                <div class="col-sm-10">
                                    <select name="user_prodi_value" class="form-control" id="inputProfilModel4">
                                        <option value="-">Pilih Profil Prodi Anda</option>
                                        @foreach ($profil_prodi as $p)
                                        <option value="{{$p->id}}">{{$p->nama_prodi}}</option>
                                        @endforeach
                                    </select>
                                    <h6>Superadmin/Admin tidak memerlukan pilihan ini</h6>
                                </div>
                            </div>
                    <button type="submit" class="btn btn-primary"> Simpan </button>
                    <!-- end of div -->
                        </div>
                    </div>
                    <!-- end of div -->
                       
                 

                    </form>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->

    </div><!-- /.row -->

    

@stop


@section('css')

   

@stop


@section('js')

<script>
  
    $(document).ready(function() {
        $('#tabel_manajemen_user').DataTable({
            "scrollX": true
        });
    });

    
    //  click event listener to links that requires custom confirm
    $('.confirmation-question').click(function(e){

        //  get link and its confirmation message
        var link                = $(this);
        var confirmationmessage = link.data('confirmation');
        var address             = link.attr('href');

        e.preventDefault();

        bootbox.confirm({
            title   : '<i class="fas fa-exclamation-triangle"></i> Konfirmasi',
            message : confirmationmessage,
            buttons : {
                confirm : {
                    label : 'Yes',
                    className: 'btn-success'
                },
                cancel : {
                    label : 'No',
                    className : 'btn-danger'
                }
            },
            callback : function (result) {
                if (result)
                {
                    //  simulate click the link
                    window.location.href = address;
                }
            }
            });
        });
    

</script>

@stop

