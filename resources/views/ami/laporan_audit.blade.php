@php use App\Http\Controllers\AuditorController; @endphp
@extends('adminlte::page')


@section('title', 'SPMI dan SPME')


@section('content_header')

<h1>Audit Mutu Internal</h1>

@stop


@section('content')


@if (Session::has('success'))
<br>
<div class="alert alert-success">

    {!! \Session::get('success') !!}

</div>
@endif

@if (Session::has('pesan'))
<br>
<div class="alert alert-warning">

    {!! \Session::get('pesan') !!}

</div>
@endif

<div class='row'>
    <div class='col-md-12'>
        <!-- Box -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Permintaan Tindakan Koreksi (PTK)</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i
                            class="fa fa-minus"></i></button>
                </div>
            </div>

            <div class="box-body">
                <form id="ptk_ami_post" method="post" enctype="multipart/form-data" class="form-horizontal">
                    <input type="hidden" value="ok" name="sip">

                    @csrf

                    <div class="row">
                        <div class="col-md-12">

                            <div class="form-group">
                                <label for="inputUsername4" class="col-sm-2 control-label">Pilih Jadwal Audit</label>
                                <div class="col-sm-10">
                                    <select name="laporan_audit_jadwal" class="form-control">
                                        <option value="0">-- Pilih jadwal --</option>
                                        @if ($list_jadwal_audit != null)
                                        @foreach ($list_jadwal_audit as $lja)
                                        <option @php ($lja->pen_jadwal_id == (int) $jadwal_id ?
                                            print('selected="selected"') : null)
                                            @endphp value="{{ $lja->pen_jadwal_id }}">{{ $lja->pen_jadwal_tanggal }} -
                                            {{ AuditorController::auditor_ami_program_studi_getProfil($lja->pen_jadwal_prodi_id)->nama_prodi }}
                                        </option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUsername4" class="col-sm-2 control-label">Auditee</label>
                                <div class="col-sm-10">
                                    <h6><b>Program Studi : </b>
                                        <h6 id="laporan_audit_program_studi"></h6>
                                    </h6>

                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUsername4" class="col-sm-2 control-label">Ketua Program Studi</label>
                                <div class="col-sm-10">
                                    <p id="laporan_audit_ketua_prodi"></p>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUsername4" class="col-sm-2 control-label">Tanggal Audit</label>
                                <div class="col-sm-10">
                                    <p id="laporan_audit_tanggal"></p>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUsername4" class="col-sm-2 control-label">Ketua Auditor</label>
                                <div class="col-sm-10">
                                    <p id="laporan_audit_ketua_auditor"></p>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUsername4" class="col-sm-2 control-label">Anggota Auditor</label>
                                <div class="col-sm-10">
                                    <p id="laporan_audit_anggota"></p>
                                </div>
                            </div>

                            <!-- end of div -->
                        </div>
                    </div>
                    <!-- end of div -->

                </form>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->

    <!-- tujuan -->
    <div class='col-md-12'>
        <!-- Box -->
        <div class="box primary-box">
            <!-- <div class="box collapsed-box"> -->
            <div class="box-header with-border">
                <h3 class="box-title">Tujuan Audit</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i
                            class="fa fa-minus"></i></button>
                </div>
            </div>

            <div class="box-body">

                <div class="row">
                    <div class="col-md-12">
                        <!-- <hr style="display: block; height: 1px; border: 0; border-top: 1px solid #ccc; margin: 1em 0;padding: 0; "> -->
                        <form class="form-horizontal" id="tujuan-audit-form-0" method="post"
                            action="{{ route('Auditor2') }}">
                            @csrf
                            <input type="hidden" name="op_tujuan_jadwal_id">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="inputTujuan3" class="col-sm-2 control-label">Tujuan</label>

                                    <div class="col-sm-10">
                                        <textarea name="op_tujuan" class="form-control" id="inputTujuan3"
                                            placeholder="Masukkan tujuan yang akan dicapai"></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputCeklist3" class="col-sm-2 control-label">Ceklis</label>


                                    <div class="col-sm-10">
                                        <input type="checkbox" name="op_check" id="inputCeklist3" value="ok"> Berikan
                                        tanda ceklis sesuai yang dikerjakan
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="submitTujuan" class="col-sm-2 control-label"></label>

                                    <div class="col-sm-10">
                                        <button type="submit" name="op_submit_tujuan" id="submitTujuan"
                                            class="btn btn-primary"> Simpan </button>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputAnggotaAudit3" class="col-sm-2 control-label">Isian Tujuan</label>


                                    <div class="col-sm-10">

                                        <table id="tabel_tujuan_auditor" class="table table-bordered display">
                                            <thead>
                                                <tr>
                                                    <th>Tujuan</th>
                                                    <th>Cek</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>

                                        </table>

                                    </div> <!-- /.col-sm-10 -->

                                </div>



                            </div>
                            <!-- /.box-body -->

                        </form>
                    </div>
                </div>

            </div><!-- /.box-body -->
        </div><!-- /.box -->

    </div><!-- /.col -->

    <!-- lingkup audit -->
    <div class='col-md-12'>
        <!-- Box -->
        <div class="box primary-box">
            <!-- <div class="box collapsed-box"> -->
            <div class="box-header with-border">
                <h3 class="box-title">Lingkup Audit</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i
                            class="fa fa-minus"></i></button>
                </div>
            </div>

            <div class="box-body">

                <div class="row">
                    <div class="col-md-12">
                        <!-- <hr style="display: block; height: 1px; border: 0; border-top: 1px solid #ccc; margin: 1em 0;padding: 0; "> -->
                        <form class="form-horizontal" id="lingkup-audit-form-0" method="post"
                            action="{{ route('Auditor3') }}">
                            <input type="hidden" id="theJadwalid" name="jadwal_id">
                            @csrf
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="inputLingkup3" class="col-sm-2 control-label">Lingkup Audit</label>

                                    <div class="col-sm-10">
                                        <textarea name="op_lingkup_audit" class="form-control" id="inputLingkup3"
                                            placeholder="Masukkan lingkup audit yang ingin dicapai"></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="submitLingkup" class="col-sm-2 control-label"></label>

                                    <div class="col-sm-10">
                                        <button type="submit" name="op_submit_lingkup" id="submitLingkup"
                                            class="btn btn-primary"> Simpan </button>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputAnggotaAudit3" class="col-sm-2 control-label">Isian Lingkup</label>


                                    <div class="col-sm-10">

                                        <table id="tabel_lingkup_auditor" class="table table-bordered display">
                                            <thead>
                                                <tr>

                                                    <th>#</th>
                                                    <th>Lingkup</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>

                                        </table>

                                    </div> <!-- /.col-sm-10 -->

                                </div>

                            </div>
                            <!-- /.box-body -->

                        </form>
                    </div>
                </div>

            </div><!-- /.box-body -->
        </div><!-- /.box -->

    </div><!-- /.col -->

    <!-- Jadwal audit-->
    <div class='col-md-12'>
        <!-- Box -->
        <div class="box primary-box">
            <!-- <div class="box collapsed-box"> -->
            <div class="box-header with-border">
                <h3 class="box-title">Jadwal Audit</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i
                            class="fa fa-minus"></i></button>
                </div>
            </div>

            <div class="box-body">

                <div class="row">
                    <div class="col-md-12">
                        <!-- <hr style="display: block; height: 1px; border: 0; border-top: 1px solid #ccc; margin: 1em 0;padding: 0; "> -->
                        <form class="form-horizontal" id="jadwal-audit-form-0" method="POST"
                            action="{{ route('Auditor4') }}">
                            @csrf
                            <input type="hidden" id="theJadwalid2" name="jadwal_id">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="inputTanggalAudit3" class="col-sm-2 control-label">Tanggal</label>

                                    <div class="col-sm-10">
                                        <select name="op_tanggal_audit" class="form-control"
                                            id="inputTanggalAudit3"></select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputPukulAudit3" class="col-sm-2 control-label">Pukul</label>

                                    <div class="col-sm-10">
                                        <textarea name="op_pukul_audit" class="form-control" id="inputPukulAudit3"
                                            placeholder="Masukkan pukul audit sesuai jadwal"></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputKegiatanAudit3" class="col-sm-2 control-label">Kegiatan
                                        Audit</label>

                                    <div class="col-sm-10">
                                        <textarea name="op_kegiatan_audit" class="form-control" id="inputKegiatanAudit3"
                                            placeholder="Masukkan kegiatan sesuai jadwal tersebut"></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="submitKegiatan" class="col-sm-2 control-label"></label>

                                    <div class="col-sm-10">
                                        <button type="submit" name="op_submit_kegiatan" id="submitKegiatan"
                                            class="btn btn-primary"> Simpan </button>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputAnggotaAudit3" class="col-sm-2 control-label">Tabel Jadwal
                                        Audit</label>


                                    <div class="col-sm-10">

                                        <table id="tabel_kegiatan_auditor" class="table table-bordered display">
                                            <thead>
                                                <tr>

                                                    <th>#</th>
                                                    <th>Tanggal</th>
                                                    <th>Pukul</th>
                                                    <th>Kegiatan Audit</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>

                                        </table>

                                    </div> <!-- /.col-sm-10 -->

                                </div>

                            </div>
                            <!-- /.box-body -->

                        </form>
                    </div>
                </div>

            </div><!-- /.box-body -->
        </div><!-- /.box -->

    </div><!-- /.col -->

    <!-- Temuan audit -->
    <div class='col-md-12'>
        <!-- Box -->
        <div class="box primary-box">
            <!-- <div class="box collapsed-box"> -->
            <div class="box-header with-border">
                <h3 class="box-title">Temuan Audit</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i
                            class="fa fa-minus"></i></button>
                </div>
            </div>

            <div class="box-body">

                <div class="row">
                    <div class="col-md-12">
                        <!-- <hr style="display: block; height: 1px; border: 0; border-top: 1px solid #ccc; margin: 1em 0;padding: 0; "> -->

                        <div class="box-body">
                            <form class="form-horizontal" id="temuan-audit-form-0">
                                <h5>1. Ketidaksesuaian</h5>
                                {{-- <div class="form-group">
                                        <label for="inputInisialAudit3" class="col-sm-2 control-label">KTS/OB (Inisial Auditor)</label>

                                        <div class="col-sm-10">
                                            <input type="text" name="op_inisial_audit" class="form-control" id="inputInisialAudit3" placeholder="Masukkan inisial auditor">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputReferensiTemuanAudit3" class="col-sm-2 control-label">Temuan Audit</label>

                                        <div class="col-sm-10">
                                            <textarea name="op_referensi_mutu_temuan" class="form-control" id="inputReferensiTemuanAudit3" placeholder="Masukkan temuan audit"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputPeryataanTemuanAudit3" class="col-sm-2 control-label">Pernyataan Audit</label>

                                        <div class="col-sm-10">
                                            <textarea name="op_pernyataan_mutu_temuan" class="form-control" id="inputPeryataanTemuanAudit3" placeholder="Masukkan pernyataan audit"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="submitTemuan" class="col-sm-2 control-label"></label>

                                        <div class="col-sm-10">
                                            <button type="submit" name="op_submit_temuan" id="submitTemuan" class="btn btn-primary"> Simpan </button>
                                        </div>
                                    </div> --}}

                                <div class="form-group">
                                    <label for="inputKetidaksesuaianAudit3" class="col-sm-2 control-label">Tabel
                                        Ketidaksesuaian</label>


                                    <div class="col-sm-10">

                                        <table id="tabel_ketidaksesuaian" class="table table-bordered display">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>KTS/OB</th>
                                                    <th>Referensi (butir mutu)</th>
                                                    <th>Pernyataan</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>

                                        </table>

                                    </div> <!-- /.col-sm-10 -->

                                </div>
                            </form>
                            <form class="form-horizontal" method="post" id="saran-audit-form-0"
                                action="{{ route('Auditor5') }}">
                                <input type="hidden" id="theJadwalid3" name="jadwal_id">
                                @csrf
                                <h5>2. Saran Perbaikan</h5>

                                <div class="form-group">
                                    <label for="inputSaranPerbaikanAudit3" class="col-sm-2 control-label">Bidang</label>

                                    <div class="col-sm-10">
                                        <textarea name="op_perbaikan_bidang_audit" class="form-control"
                                            id="inputSaranPerbaikanAudit3"
                                            placeholder="Masukkan bidang saran perbaikan"></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputKelebihanPerbaikanAudit3"
                                        class="col-sm-2 control-label">Kelebihan</label>

                                    <div class="col-sm-10">
                                        <textarea name="op_perbaikan_kelebihan_audit" class="form-control"
                                            id="inputKelebihanPerbaikanAudit3"
                                            placeholder="Masukkan kelebihan saran perbaikan audit"></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputPeluangAudit3" class="col-sm-2 control-label">Peluang
                                        Peningkatan</label>

                                    <div class="col-sm-10">
                                        <textarea name="op_perbaikan_peluang_audit" class="form-control"
                                            id="inputPeluangAudit3" placeholder="Masukkan temuan audit"></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="submitPerbaikan" class="col-sm-2 control-label"></label>

                                    <div class="col-sm-10">
                                        <button type="submit" name="op_submit_perbaikan" id="submitPerbaikan"
                                            class="btn btn-primary"> Simpan </button>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputTabelPerbaikanAudit3" class="col-sm-2 control-label">Tabel Saran
                                        Perbaikan</label>


                                    <div class="col-sm-10">

                                        <table id="tabel_perbaikan_auditor" class="table table-bordered display">
                                            <thead>
                                                <tr>

                                                    <th>No</th>
                                                    <th>Bidang</th>
                                                    <th>Kelebihan</th>
                                                    <th>Peluang Peningkatan</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>

                                        </table>

                                    </div> <!-- /.col-sm-10 -->

                                </div>
                            </form>
                        </div>
                        <!-- /.box-body -->


                    </div>
                </div>

            </div><!-- /.box-body -->
        </div><!-- /.box -->

    </div><!-- /.col -->

    <!-- kesimpulan audit -->
    <div class='col-md-12'>
        <!-- Box -->
        <div class="box primary-box">
            <!-- <div class="box collapsed-box"> -->
            <div class="box-header with-border">
                <h3 class="box-title">Kesimpulan</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i
                            class="fa fa-minus"></i></button>
                </div>
            </div>

            <div class="box-body">

                <div class="row">
                    <div class="col-md-12">
                        <!-- <hr style="display: block; height: 1px; border: 0; border-top: 1px solid #ccc; margin: 1em 0;padding: 0; "> -->
                        <form class="form-horizontal" id="kesimpulan-audit-form-0">
                            <div class="box-body">
                                {{-- <div class="form-group">
                                    <label for="inputPoinKesimpulanAudit3" class="col-sm-2 control-label">Poin
                                        Kesimpulan</label>

                                    <div class="col-sm-10">
                                        <textarea name="op_kesimpulan_audit" class="form-control"
                                            id="inputPoinKesimpulanAudit3"
                                            placeholder="Masukkan bulir kesimpulan audit"></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputYesNoOtherAudit3" class="col-sm-2 control-label">Pilihan Ya, Tidak,
                                        Lainnya</label>

                                    <div class="col-sm-10">
                                        <select name="pilihan_kesimpulan" id="pilihan-kesimpulan-id"
                                            class="form-control">
                                            <option value="1">Ya</option>
                                            <option value="2">Tidak</option>
                                            <option value="3">Lainnya</option>
                                        </select>
                                        <hr>

                                        <textarea name="op_kesimpulan_lainnya" class="form-control"
                                            id="inputYesNoOtherAudit3"
                                            placeholder="Jika memilih lainnya, silahkan tuliskan disini"></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="submitKegiatan" class="col-sm-2 control-label"></label>

                                    <div class="col-sm-10">
                                        <button type="submit" name="op_submit_kegiatan" id="submitKegiatan"
                                            class="btn btn-primary"> Simpan </button>
                                    </div>
                                </div> --}}

                                <div class="form-group">
                                    <label for="inputAnggotaAudit3" class="col-sm-2 control-label">Kesimpulan</label>


                                    <div class="col-sm-10">

                                        <table id="tabel_kesimpulan_auditor" class="table table-bordered display">
                                            <thead>
                                                <tr>

                                                    <th>#</th>
                                                    <th>Poin Kesimpulan</th>
                                                    <th>V / X</th>
                                                    <th></th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>

                                        </table>

                                    </div> <!-- /.col-sm-10 -->

                                </div>

                            </div>
                            <!-- /.box-body -->

                        </form>
                    </div>
                </div>

            </div><!-- /.box-body -->
        </div><!-- /.box -->

    </div><!-- /.col -->


</div><!-- /.row -->

<!-- modal form jadwal audit -->
<div class="modal" id="modal_edit_audit">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only">Close</span></button>
                <h4 class="modal-title">Mengubah data Jadwal Audit</h4>
            </div>
            <form method="post" id="jadwal-edit-form-0" action="{{ route('audit_ajax_kegiatan_edit') }}">
                <div class="modal-body">
                    <input type="hidden" id="jadwal_edit_id_ref" name="jadwal_edit_id_value">
                    <p>Tanggal</p>
                    <select class="form-control" id="tanggal_edit_ref" name="tanggal_edit_value">

                    </select>

                    <hr>

                    <p>Pukul</p>
                    <input type="text" class="form-control" id="pukul_edit_ref" name="pukul_edit_value">

                    <hr>

                    <p>Kegiatan Audit</p>
                    <textarea id="kegiatan_edit_ref" name="kegiatan_edit_value" class="form-control"></textarea>

                </div>

                <div class="modal-footer">

                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- end-- --}}

<!-- modal form ketidaksesuaian -->
<div class="modal" id="modal_edit_ketidaksesuaian">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only">Close</span></button>
                <h4 class="modal-title">Mengubah data Ketidaksesuaian</h4>
            </div>
            <form method="post" id="ketidaksesuaian-edit-form-0" action="{{ route('audit_ajax_temuan_edit') }}">
                <div class="modal-body">
                    <input type="hidden" id="ketidaksesuaian_edit_id_ref" name="ketidaksesuaian_edit_id_value">

                    <p>KTS/OB (Inisial Auditor)</p>
                    <input type="text" class="form-control" id="kts_ob_edit_ref" name="kts_ob_edit_value">

                    <hr>

                    <p>Temuan Audit</p>
                    <textarea id="temuan_edit_ref" name="temuan_edit_value" class="form-control"></textarea>

                    <hr>

                    <p>Peryataan Audit</p>
                    <textarea id="pernyataan_edit_ref" name="pernyataan_edit_value" class="form-control"></textarea>


                </div>

                <div class="modal-footer">

                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- modal form kesimpulan -->
<div class="modal" id="modal_edit_kesimpulan">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only">Close</span></button>
                <h4 class="modal-title">Mengubah data kesimpulan</h4>
            </div>
            <form method="post" id="kesimpulan-edit-form-0" action="{{ route('audit_ajax_kesimpulan_edit') }}">
                <div class="modal-body">
                    <input type="hidden" id="kesimpulan_edit_id_ref" name="kesimpulan_edit_id_value">
                    <input type="hidden" id="jkk_id_ref" name="jkk_id_value">
                    <input type="hidden" id="pertanyaan_id_ref" name="pertanyaan_id_value">
                    <input type="hidden" id="jadwal_id_ref" name="jadwal_id">

                    <p>Poin Kesimpulan</p>
                    <textarea disabled id="poin_kesimpulan_edit_ref" name="poin_kesimpulan_edit_value"
                        class="form-control"></textarea>

                    <hr>

                    <p>Pilihan</p>
                    <select id="pilihan_edit_ref" name="pilihan_edit_value" class="form-control">
                        <option value="1"> Ya </option>
                        <option value="2"> Tidak </option>
                        <option value="3"> Lainnya </option>
                    </select>

                    <hr>
                    <p> Teks yang diisi sesuai pilihan, silahkan isi..</p>
                    <textarea id="lainnya_edit_ref" name="lainnya_edit_value" class="form-control"></textarea>

                </div>

                <div class="modal-footer">

                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- modal form saran perbaikan -->
<div class="modal" id="modal_edit_saran_perbaikan">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only">Close</span></button>
                <h4 class="modal-title">Mengubah data Saran Perbaikan</h4>
            </div>
            <form method="post" id="saran-edit-form-0" action="{{ route('audit_ajax_saran_edit') }}">
                <div class="modal-body">

                    <input type="hidden" id="saran_edit_id_ref" name="saran_edit_id_value">

                    <p>Bidang</p>
                    <textarea id="bidang_edit_ref" name="bidang_edit_value" class="form-control"></textarea>

                    <hr>

                    <p>Kelebihan</p>
                    <textarea id="kelebihan_edit_ref" name="kelebihan_edit_value" class="form-control"></textarea>

                    <hr>

                    <p>Peluang Peningkatan</p>
                    <textarea id="peluang_edit_ref" name="peluang_edit_value" class="form-control"></textarea>


                </div>

                <div class="modal-footer">

                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>



<!-- modal edit tujuan -->
<div class="modal" id="modal_edit_tujuan">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only">Close</span></button>
                <h4 class="modal-title">Mengubah data tujuan</h4>
            </div>
            <form method="post" action="{{ route('audit_ajax_tujuan_edit') }}">
                <div class="modal-body">
                    <input type="hidden" id="modal_tujuan_id" name="tujuan_id">
                    <p>Isi tujuan</p>
                    <textarea type="text" id="tujuan_edit_id" name="tujuan_edit_isian" class="form-control"></textarea>

                    <hr>

                    <p>Ceklis</p>
                    <input type="checkbox" name="tujuan_edit_cek" id="tujuan_edit_ceklist"> Berikan tanda ceklis sesuai
                    yang dikerjakan

                </div>

                <div class="modal-footer">

                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end modal edit tujuan -->

<!-- modal form edit lingkup -->
<div class="modal" id="modal_edit_lingkup">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only">Close</span></button>
                <h4 class="modal-title">Mengubah data lingkup</h4>
            </div>
            <form method="post" id="lingkup-edit-form-0" action="{{ route('audit_ajax_lingkup_edit') }}">
                <div class="modal-body">
                    <input type="hidden" id="lingkup_edit_id_ref" name="lingkup_edit_id_value">
                    <p>Lingkup Audit</p>
                    <textarea id="lingkup_edit_ref" name="lingkup_edit_value" class="form-control"></textarea>

                </div>

                <div class="modal-footer">

                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- end --}}




@stop


@section('css')

@stop


@section('js')

<script>
    function hapus_tujuan(e) {
        var tanya = confirm('Apakah anda yakin untuk menghapus tujuan ini?');
        if (tanya == true) {
            window.location = "{{ route('audit_ajax_tujuan_delete') }}?tujuan_id=" + e;
        }
    }

    function hapus_lingkup(e) {
        var tanya = confirm('Apakah anda yakin untuk menghapus lingkup ini?');
        if (tanya == true) {
            window.location = "{{ route('audit_ajax_lingkup_delete') }}?lingkup_id=" + e;
        }
    }

    function hapus_jadwal (e) 
    {
        var tanya = confirm('Apakah anda yakin untuk menghapus lingkup ini?');
        if (tanya == true) {
            window.location = "{{ route('audit_ajax_kegiatan_delete') }}?kegiatan_id=" + e;
        }
    }

    function hapus_ketidaksesuaian (e) 
    {
        var tanya = confirm('Apakah anda yakin untuk menghapus lingkup ini?');
        if (tanya == true) {
            window.location = "{{ route('audit_ajax_kegiatan_delete') }}?kegiatan_id=" + e;
        }
    }

    function hapus_saran_perbaikan(e)
    {
        var tanya = confirm('Apakah anda yakin untuk menghapus saran ini?');
        if (tanya == true) {
            window.location = "{{ route('audit_ajax_saran_delete') }}?saran_id=" + e;
        }
    }
    

    $(document).ready(function(){
        var data_ref_id @php ($jadwal_id != null) ? print(' ="' . (int) $jadwal_id . '";') : null  @endphp
        
       
      
        //select menu
        var tabel_kesimpulan = $('#tabel_kesimpulan_auditor').DataTable({
            "bProcessing": true,
            "bServerSide": true,
            "ajax": {
                "url": "{{route('audit_ajax_kesimpulan_view')}}?jadwal_id=" + data_ref_id,
                "type": "POST"    
            },
            "columns": [
                {   "render" : function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1; }
                },
                { "data": "jk_isi_kesimpulan" },
                { 
                    "data" : "isian"
                },
                { "data": "other" },
                { 
                    "mData": "0", 
                    "mRender": function ( data, type, full ) {
                        return '<a href="#modal_edit_kesimpulan" data-toggle="modal" data-edit-kesimpulan="'+ full.jk_isi_kesimpulan +'" data-edit-ya-tidak="'+ full.yes_no_other_check +'" data-edit-lainnya="'+ full.other +'" data-edit-pilihan-kesimpulan="'+ full.jk_json_check +'" data-edit-kesimpulanid="'+ full.kesimpulan_id +'" data-edit-pertanyaan-id="'+   full.jk_id  +'  " data-edit-jk-id="'+ full.jkk +'"><i class="fa fa-edit"></i></a> ';
                    }
                }
            ]

        });

        var tabel_saran_perbaikan = $('#tabel_perbaikan_auditor').DataTable({
            "bProcessing": true,
            "bServerSide": true,
            "ajax": {
                "url": "{{route('audit_ajax_saran_view') }}?jadwal_id=" + data_ref_id,
                "type": "POST"    
            },
            "columns": [
                {   "render" : function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1; }
                },
                { "data": "bidang" },
                { "data": "kelebihan" },
                { "data": "peluang_peningkatan" },
                { 
                    "mData": "0", 
                    "mRender": function ( data, type, full ) {
                        return '<a href="#modal_edit_saran_perbaikan" data-toggle="modal" data-edit-bidang="'+ full.bidang +'" data-edit-kelebihan="'+ full.kelebihan +'" data-edit-peluang-peningkatan="'+ full.peluang_peningkatan +'" data-edit-saranid="'+ full.id +'"><i class="fa fa-edit"></i></a> | <a class="delete-jadwal-data" onclick="hapus_saran_perbaikan(\''+ full.id + '\')"  data-delete-id="'+ full.id +'"><i class="fa fa-trash" aria-hidden="true"></i></a>';
                    }
                }
            ]

        });

        var tabel_temuan = $('#tabel_ketidaksesuaian').DataTable({
            "bProcessing": true,
            "bServerSide": true,
            "ajax": {
                "url": "{{route('audit_ajax_temuan_view')}}?jadwal_id=" + data_ref_id,
                "type": "POST"    
            },
            "columns": [
                {   "render" : function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1; }
                },
                { "data": "kts_ob" },
                { "data": "referensi_butir_mutu" },
                { "data": "pernyataan" },
                { 
                    "mData": "0", 
                    "mRender": function ( data, type, full ) {
                        return '<a href="#modal_edit_ketidaksesuaian" data-toggle="modal" data-edit-ktsob="'+ full.kts_ob +'" data-edit-temuan="'+ full.referensi_butir_mutu +'" data-edit-pernyataan="'+ full.pernyataan +'" data-edit-temuanid="'+ full.id +'"><i class="fa fa-edit"></i></a>';
                    }
                }
            ]

        });

        

        var tabel_jadwal = $('#tabel_kegiatan_auditor').DataTable({
            "bProcessing": true,
            "bServerSide": true,
            "ajax": {
                "url": "{{route('audit_ajax_kegiatan_view')}}?jadwal_id=" + data_ref_id,
                "type": "POST"    
            },
            "columns": [
                {   "render" : function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1; }
                },
                { "data": "tanggal" },
                { "data": "pukul" },
                { "data": "kegiatan_audit" },
                { 
                    "mData": "0", 
                    "mRender": function ( data, type, full ) {
                        return '<a href="#modal_edit_audit" data-toggle="modal" data-edit-tanggal="'+ full.tanggal +'" data-edit-pukul="'+ full.pukul +'" data-edit-kegiatan="'+ full.kegiatan_audit +'" data-edit-jadwalid="'+ full.id +'"><i class="fa fa-edit"></i></a> | <a class="delete-jadwal-data" onclick="hapus_jadwal(\''+ full.id + '\')"  data-delete-id="'+ full.id +'"><i class="fa fa-trash" aria-hidden="true"></i></a>';
                    }
                }
            ]

        });

        var tabel_lingkup = $('#tabel_lingkup_auditor').DataTable({
            "bProcessing": true,
            "bServerSide": true,
            "ajax": {
                "url": "{{ route('audit_ajax_lingkup_view') }}?jadwal_id=" + data_ref_id,
                "type": "POST"    
            },
            "columns": [
                {   "render" : function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1; }
                },
                { "data": "audit_lingkup" },
                { 
                    "mData": "0", 
                    "mRender": function ( data, type, full ) {
                        return '<a href="#modal_edit_lingkup" data-toggle="modal" data-edit-lingkup="'+ full.audit_lingkup +'" data-edit-lingkupid="'+ full.id +'"><i class="fa fa-edit"></i></a> | <a class="delete-lingkup-data" onclick="hapus_lingkup(\''+ full.id + '\')" data-confirmation="Apakah anda ingin menghapus tujuan ini?" data-delete-id="'+ full.id +'"><i class="fa fa-trash" aria-hidden="true"></i></a>';
                    }
                }
            ]

        });
        
        
        var tabel_tujuan = $('#tabel_tujuan_auditor').DataTable({
            
            "bProcessing": true,
            "bServerSide": true,
            "ajax": {          
                "url": "{{ route('audit_ajax_tujuan_view') }}?jadwal_id=" + data_ref_id,
                "type": "POST"    
            },
            "columns": [
                { "data": "tujuan" },
                { 
                    "mData": "0", 
                    "mRender": function ( data, type, full ) {
                        if (full.yes_no_check == '1') {
                            return '<i class="fa fa-check" aria-hidden="true"></i>';
                        } else {
                            return '-';
                        }
                    }
                },
                { 
                    "mData": "0", 
                    "mRender": function ( data, type, full ) {
                        return '<a href="#modal_edit_tujuan" data-toggle="modal" data-edit-tujuan="'+ full.tujuan +'" data-edit-ceklist="'+ full.yes_no_check +'" data-edit-tujuanid="'+ full.id +'"><i class="fa fa-edit"></i></a> | <a class="delete-tujuan-data" onclick="hapus_tujuan(\''+ full.id + '\')" data-confirmation="Apakah anda ingin menghapus tujuan ini?" data-delete-id="'+ full.id +'"><i class="fa fa-trash" aria-hidden="true"></i></a>';
                    }
                }
            ]

        });


        //miscellaneous

        $('select[name="laporan_audit_jadwal"]').change(function() {
            
            var selectedJadwal = $('select[name="laporan_audit_jadwal"]').children("option:selected").val();

            $.ajax({
                url : "{{ route('Auditor1') }}?pen_jadwal_id=" + selectedJadwal,
                success: function(result) {
                    $("input[name='op_tujuan_jadwal_id']").val(selectedJadwal);
                    console.log(result);

                    if (result != 'none') {
                        $('#laporan_audit_program_studi').text(result[1].nama_prodi);
                        $('#laporan_audit_alamat').text(result[1].alamat_pt);
                        $('#laporan_audit_ketua_prodi').text(result[1].identitas_nama_ketua_prod);
                        $('#laporan_audit_tanggal').text(result[0].pen_jadwal_tanggal + ' - ' + result[0].pen_jadwal_tanggal_sampai);
                        $('#laporan_audit_ketua_auditor').text(result[2].name);


                        //Result data
                        $("input[name='op_tujuan_jadwal_id']").val(result[0].pen_jadwal_id);
                        data_ref_id = result[0].pen_jadwal_id;
                        $('#theJadwalid').val(data_ref_id);
                        $('#theJadwalid2').val(data_ref_id);
                        $('#theJadwalid3').val(data_ref_id);
                        $('#jadwal_id_ref').val(data_ref_id);
                        

                        

                        tabel_tujuan.ajax.url('{{ route('audit_ajax_tujuan_view') }}?jadwal_id=' + data_ref_id  ).load();
                        tabel_lingkup.ajax.url('{{ route('audit_ajax_lingkup_view') }}?jadwal_id=' + data_ref_id  ).load();
                        tabel_jadwal.ajax.url("{{route('audit_ajax_kegiatan_view')}}?jadwal_id=" + data_ref_id).load();
                        tabel_temuan.ajax.url('{{ route('audit_ajax_temuan_view')}}?jadwal_id=' + data_ref_id).load();
                        tabel_saran_perbaikan.ajax.url('{{ route('audit_ajax_saran_view') }}?jadwal_id=' + data_ref_id).load();
                        tabel_kesimpulan.ajax.url('{{route('audit_ajax_kesimpulan_view')}}?jadwal_id=' + data_ref_id).load();
                        
                        data_auditor = result[3];
                        isian = '';
                        if (data_auditor != null) {
                            jQuery.each(result[3], function(i, val) {
                                isian = isian + val.name + ' <br>';
                            });

                            $('#laporan_audit_anggota').html(isian);

                        }





                    }
                    
                }
            });

            $.ajax({
                url : "{{ route('Auditor1d') }}?pen_jadwal_id=" + $('select[name="laporan_audit_jadwal"]').children("option:selected").val(),
                success: function(result) {
                    var html = '';
                    var i;
                    for (i=0; i<result.length;i++) {
                        html += '<option value ="' + result[i] + '">' + result[i] + '</option>';
                    }
                    $('#inputTanggalAudit3').html(html);
                }
            });
        });

        


        // loader
        $('#modal_edit_kesimpulan').on('show.bs.modal', function(e) {
            
            
            var kesimpulan_edit_id_ref = $(e.relatedTarget).data('edit-kesimpulanid');
            var kesimpulanjenjangpertanyaanid = $(e.relatedTarget).data('edit-jk-id');
            var jk_id = $(e.relatedTarget).data('edit-pertanyaan-id');
            var kesimpulan_edit_value =  $(e.relatedTarget).data('edit-kesimpulan');
            var pilihan_edit_value =  $(e.relatedTarget).data('edit-ya-tidak');
            var lainnya_edit_value =  $(e.relatedTarget).data('edit-lainnya');
            var data_option = $(e.relatedTarget).data('edit-pilihan-kesimpulan');
            $('#pilihan_edit_ref').find('option').remove();
            $.each(data_option, function(i, value) {
                $('#pilihan_edit_ref').append($('<option>').text(value).attr('value', value));
            });

            $(e.currentTarget).find('#kesimpulan_edit_id_ref').val(kesimpulan_edit_id_ref);
            $(e.currentTarget).find('#poin_kesimpulan_edit_ref').val(kesimpulan_edit_value);
            $(e.currentTarget).find('#pilihan_edit_ref').val(pilihan_edit_value);
            $(e.currentTarget).find('#lainnya_edit_ref').val(lainnya_edit_value);
            $(e.currentTarget).find('#jkk_id_ref').val(kesimpulanjenjangpertanyaanid);
            $(e.currentTarget).find('#pertanyaan_id_ref').val(jk_id);
           
        });

        $('#modal_edit_saran_perbaikan').on('show.bs.modal', function(e) {
            
            var saran_id_edit_value = $(e.relatedTarget).data('edit-saranid');
            var bidang_edit_value =  $(e.relatedTarget).data('edit-bidang');
            var kelebihan_edit_value =  $(e.relatedTarget).data('edit-kelebihan');
            var peluang_edit_value =  $(e.relatedTarget).data('edit-peluang-peningkatan');

            $(e.currentTarget).find('#saran_edit_id_ref').val(saran_id_edit_value);
            $(e.currentTarget).find('#bidang_edit_ref').val(bidang_edit_value);
            $(e.currentTarget).find('#kelebihan_edit_ref').val(kelebihan_edit_value);
            $(e.currentTarget).find('#peluang_edit_ref').val(peluang_edit_value);
           
        });

        $('#modal_edit_tujuan').on('shown.bs.modal', function(e) {
           var tujuan_id = $(e.relatedTarget).data('edit-tujuanid');

           $('#modal_tujuan_id').val(tujuan_id);
           $('#tujuan_edit_id').val($(e.relatedTarget).data('edit-tujuan'));
           $('#tujuan_edit_cek').val($(e.relatedTarget).data('edit-ceklist'));
           
        });

        $('#modal_edit_lingkup').on('shown.bs.modal', function(e) {
            var lingkup_edit_value = $(e.relatedTarget).data('edit-lingkup');
            var lingkupid_edit_value =  $(e.relatedTarget).data('edit-lingkupid');

            $(e.currentTarget).find('#lingkup_edit_id_ref').val(lingkupid_edit_value);
            $(e.currentTarget).find('#lingkup_edit_ref').val(lingkup_edit_value);

        });


        $('#modal_edit_audit').on('show.bs.modal', function(e) {
            
            var jadwal_id_edit_value = $(e.relatedTarget).data('edit-jadwalid');
            var pukul_edit_value =  $(e.relatedTarget).data('edit-pukul');
            var kegiatan_edit_value =  $(e.relatedTarget).data('edit-kegiatan');
            var tanggal_edit_value = $(e.relatedTarget).data('edit-tanggal');

            $.ajax({
                url : "{{ route('Auditor1d') }}?pen_jadwal_id=" + $('select[name="laporan_audit_jadwal"]').children("option:selected").val(),
                success: function(result) {
                    var html = '';
                    var i;
                    for (i=0; i<result.length;i++) {
                        html += '<option value ="' + result[i] + '">' + result[i] + '</option>';
                    }
                    $('#tanggal_edit_ref').html(html);
                }
            });
            
            $(e.currentTarget).find('#jadwal_edit_id_ref').val(jadwal_id_edit_value);
            $(e.currentTarget).find('#pukul_edit_ref').val(pukul_edit_value);
            $(e.currentTarget).find('#kegiatan_edit_ref').val(kegiatan_edit_value);
            $(e.currentTarget).find('#tanggal_edit_ref').val(tanggal_edit_value);
           
        });

        $('#modal_edit_ketidaksesuaian').on('show.bs.modal', function(e) {
            
            var temuan_id_edit_value = $(e.relatedTarget).data('edit-temuanid');
            var ktsob_edit_value =  $(e.relatedTarget).data('edit-ktsob');
            var temuan_edit_value =  $(e.relatedTarget).data('edit-temuan');
            var pernyataan_edit_value =  $(e.relatedTarget).data('edit-pernyataan');

            $(e.currentTarget).find('#ketidaksesuaian_edit_id_ref').val(temuan_id_edit_value);
            $(e.currentTarget).find('#kts_ob_edit_ref').val(ktsob_edit_value);
            $(e.currentTarget).find('#temuan_edit_ref').val(temuan_edit_value);
            $(e.currentTarget).find('#pernyataan_edit_ref').val(pernyataan_edit_value);
           
        });

        @php 
            ($jadwal_id != null) ?  print("$('select[name=\"laporan_audit_jadwal\"]').trigger('change');") : null 
        @endphp

    });
</script>

@stop