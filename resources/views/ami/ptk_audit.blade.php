@php use App\Http\Controllers\AuditorController; @endphp
@extends('adminlte::page')


@section('title', 'SPMI dan SPME')


@section('content_header')

<h1>Audit Mutu Internal</h1>

@stop


@section('content')


@if (Session::has('success'))
<br>
<div class="alert alert-success">

    {!! \Session::get('success') !!}

</div>
@endif

@if (Session::has('pesan'))
<br>
<div class="alert alert-warning">

    {!! \Session::get('pesan') !!}

</div>
@endif

<div class='row'>
    <div class='col-md-12'>
        <!-- Box -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Permintaan Tindakan Koreksi (PTK)</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i
                            class="fa fa-minus"></i></button>
                </div>
            </div>

            <div class="box-body form-horizontal">
                {{-- <form id="ptk_ami_post" method="post" enctype="multipart/form-data" class="form-horizontal"> --}}
                    <input type="hidden" value="ok" name="sip">

                    @csrf

                    <div class="row">
                        <div class="col-md-12">

                            <div class="form-group">
                                <label for="inputUsername4" class="col-sm-2 control-label">Pilih Jadwal Audit</label>
                                <div class="col-sm-10">
                                    <select name="laporan_audit_jadwal" class="form-control">
                                        <option value="0">-- Pilih jadwal --</option>
                                        @if ($list_jadwal_audit != null)
                                        @foreach ($list_jadwal_audit as $lja)
                                        <option onclick="audit_jadwal ({{ $lja->pen_jadwal_id }})" @php ($lja->
                                            pen_jadwal_id == (int) $jadwal_id ?
                                            print('selected="selected"') : null)
                                            @endphp value="{{ $lja->pen_jadwal_id }}">{{ $lja->pen_jadwal_tanggal }} -
                                            {{ AuditorController::auditor_ami_program_studi_getProfil($lja->pen_jadwal_prodi_id)->nama_prodi }}
                                        </option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUsername4" class="col-sm-2 control-label">No. Dokumen</label>
                                <div class="col-sm-10">
                                    <form method="post" action="{{ route('Auditor6a') }}">
                                        @csrf
                                        <input type="hidden" id="modal_jadwal_id3" name="jadwal_id">
                                        <input type="hidden" name="tujuan_ke" value="nomor_dokumen">
                                        <div class="row">
                                            <div class="col-sm-7">
                                                <input type="text" id="ptk_nomor_dokumen" name="ptk_input_nomor_dokumen" class="form-control"  placeholder="Contoh input : Bajamtu-UG/AMI-PTK/PS/01.01/2018">
                                            </div>
                                            <div class="col-sm-3">
                                                <button type="submit" name="ptk_input_click_ok" class="btn btn-default">Simpan</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUsername4" class="col-sm-2 control-label">PTK</label>
                                <div class="col-sm-10">
                                    <table class="table table-bordered">
                                        <tbody>
                                            <tr>
                                                <td width="248">
                                                    <p>Program Studi</p>
                                                </td>
                                                <td colspan="3" width="380">
                                                    <p id="ptk_prog_studi"></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="248">
                                                    <p>Ketua Program Studi</p>
                                                </td>
                                                <td colspan="3" width="380">
                                                    <p id="ptk_auditee_1"></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="248">
                                                    <p>Auditor</p>
                                                </td>
                                                <td width="139">
                                                    <p id="ptk_auditor1"></p>
                                                </td>
                                                <td width="104">
                                                    <p>Tanggal Audit</p>
                                                </td>
                                                <td width="137">
                                                    <p id="ptk_tanggal">Agustus 2018</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="248">
                                                    <p><strong>PTK No</strong>:</p>
                                                </td>
                                                <td colspan="3" width="380">
                                                    <p><strong>Kategori :
                                                            <p id="ptk_major">[ ] Major</p>
                                                        </strong><strong></strong><strong>
                                                            <p id="ptk_minor">[ ] Minor</p>
                                                        </strong><strong>
                                                            <p id="ptk_observasi">[ ] Observasi</p>
                                                        </strong></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="248">
                                                    <p>Referensi (Butir Mutu)</p>
                                                </td>
                                                <td colspan="3" width="380">
                                                    <p id="ptk_referensi"></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" width="628">
                                                    <p>Uraian Temuan (<em>diisi oleh auditor &amp; ditandatangani)</em>:
                                                    </p>
                                                    <p>&nbsp;<a href="#modal_edit_uraian_temuan" data-toggle="modal"><i
                                                                class="fa fa-edit"></i></a></p>
                                                    <textarea class="form-control" id="ptk_uraian_temuan"></textarea>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="248">
                                                    <p>Tanda Tangan dan Nama Auditor</p>
                                                    <p>&nbsp;</p>
                                                    <p>&nbsp;</p>
                                                    <p>&nbsp;</p>
                                                    <p id="ptk_tanggal_tutup_1">Tanggal : </p>
                                                    <p>&nbsp;</p>
                                                    <p>&nbsp;</p>
                                                    <p>&nbsp;</p>
                                                    <p>&nbsp;</p>
                                                    <p>&nbsp;</p>
                                                    <p>&nbsp;</p>
                                                </td>
                                                <td colspan="3" width="380">
                                                    <p>&nbsp;</p>
                                                    <p>&nbsp;</p>
                                                    <p>&nbsp;</p>
                                                    <p id="ptk_auditor2">()</p>
                                                    <p>&nbsp;</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" width="628">
                                                    <p>Rencana Tindakan Koreksi (<em>diisi oleh teraudit &amp;
                                                            ditandatangani</em>):</p>
                                                    <p>&nbsp;<a href="#modal_edit_rtk" data-toggle="modal"><i
                                                                class="fa fa-edit"></i></a> </p>
                                                    <textarea class="form-control" id="ptk_rencana"></textarea>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="248">
                                                    <p>Tanda Tangan dan Nama Teraudit</p>
                                                    <p>&nbsp;</p>
                                                    <p>&nbsp;</p>
                                                    <p>&nbsp;</p>
                                                    <p id="ptk_tanggal_tutup_2">Tanggal : </p>
                                                    <p>&nbsp;</p>
                                                </td>
                                                <td colspan="3" width="380">
                                                    <p>&nbsp;&nbsp;</p>
                                                    <p>&nbsp;</p>
                                                    <p>&nbsp;</p>
                                                    <p id="ptk_auditee_2">()</p>
                                                    <p>&nbsp;</p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>

                                </div>
                            </div>



                            <!-- end of div -->
                        </div>
                    </div>
                    <!-- end of div -->

                {{-- </form> --}}
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->

    <!-- modal edit uraian temuan -->
    <div class="modal fade" id="modal_edit_uraian_temuan">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                            aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">Mengubah data uraian temuan</h4>
                </div>
                <form method="post" action="{{ route('Auditor6a') }}">
                    <div class="modal-body">
                        <input type="hidden" id="modal_jadwal_id" name="jadwal_id">
                        <input type="hidden" name="tujuan_ke" value="uraian_temuan">

                        <p>Uraian Temuan</p>
                        <textarea type="text" id="ptk_uraian_temuan_edit_id" name="ptk_uraian_temuan_edit_value"
                            class="form-control"></textarea>

                        <hr>


                    </div>

                    <div class="modal-footer">

                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- end modal uraian temuan -->

    <!-- modal edit rencana tindakan koreksi -->
    <div class="modal fade" id="modal_edit_rtk">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                            aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">Mengubah data Rencana Tindakan Koreksi</h4>
                </div>
                <form method="post" action="{{ route('Auditor6a') }}">
                    <div class="modal-body">
                        <input type="hidden" id="modal_jadwal_id2" name="jadwal_id">
                        <input type="hidden" name="tujuan_ke" value="rtk">

                        <p>Rencana Tindakan Koreksi</p>
                        <textarea type="text" id="ptk_rtk_edit_id" name="ptk_rtk_edit_value"
                            class="form-control"></textarea>

                        <hr>


                    </div>

                    <div class="modal-footer">

                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- end modal rencana tindakan koreksi -->



    @stop


    @section('css')

    @stop


    @section('js')

    <script src="{{ url('/') . '/js/moment.js' }}"></script>
    <script>
        var data_jadwal = {{$jadwal_id }};

            function audit_jadwal (pen_jadwal_id) {
                window.location = "{{ route('Auditor6') }}?last_id=" + pen_jadwal_id;
            }

            $(document).ready(function() {
                if (data_jadwal != null) {
                    $.ajax({
                        url : "{{ route('audit_ajax_ptk_view') }}?jadwal_id=" + data_jadwal,
                        success: function(result) {
                        
                            console.log(result); 
                            $('#ptk_prog_studi').text(result.sp.nama_prodi);
                            $('#ptk_auditee_1').text(result.auditee.name);
                            $('#ptk_auditor').text(result.ketua_auditor.name);
                            
                            var tanggal_semua = moment(result.sp.pen_jadwal_tanggal,"YYYY-MM-DD").format("DD-MM-YYYY") + ' -- ' + moment(result.sp.pen_jadwal_tanggal_sampai,"YYYY-MM-DD").format("DD-MM-YYYY");
                            $('#ptk_tanggal').text(tanggal_semua);

                            if (result.isian == 'Major') {
                                $('#ptk_major').text('[V] Major');
                                $('#ptk_minor').text('[ ] Minor');
                                $('#ptk_observasi').text('[ ] Observasi');
                            } else if (result.isian == 'Minor') {
                                $('#ptk_major').text('[ ] Major');
                                $('#ptk_minor').text('[V] Minor');
                                $('#ptk_observasi').text('[ ] Observasi');
                            } else if (result.isian == 'Observasi') {
                                $('#ptk_major').text('[ ] Major');
                                $('#ptk_minor').text('[ ] Minor');
                                $('#ptk_observasi').text('[V] Observasi');
                            } else {
                                alert('n not found');
                            }

                            //Dirgahayu Indonesia 74th
                            //Semoga menjadi negeri yang diberkahi

                            if (result.referensi_mutu == 'none') {
                                $('#ptk_referensi').html('');
                            } else {
                                $('#ptk_referensi').html(result.referensi_mutu);
                            }

                            if (result.ptk != 'none') {
                                var uraian = result.ptk.ptk_input_uraian_temuan;
                                var rtk = result.ptk.ptk_input_rtk;
                                if (uraian != null) {
                                    uraian.replace(/\n/g, "<br />");
                                }

                                if (rtk != null) {
                                    rtk.replace(new RegExp('\r?\n','g'), '<br>');
                                }
                                
                                $('#ptk_uraian_temuan').html(uraian);
                                $('#ptk_rencana').html(rtk);
                            } 

                            $('#ptk_tanggal_tutup_1').text(moment(result.sp.pen_jadwal_tanggal_sampai,"YYYY-MM-DD").format("DD-MM-YYYY"));
                            $('#ptk_auditor1').text("(" + result.ketua_auditor.name + ")");
                            $('#ptk_auditor2').text("(" + result.ketua_auditor.name + ")");
                            $('#ptk_tanggal_tutup_2').text(moment(result.sp.pen_jadwal_tanggal_sampai,"YYYY-MM-DD").format("DD-MM-YYYY"));
                            $('#ptk_auditee_2').text("(" + result.auditee.name + ")");
                            $('#ptk_nomor_dokumen').val(result.ptk.ptk_input_no);
                            $('#modal_jadwal_id3').val(data_jadwal);

                                

                            
                    }}); 
                }

                $('#modal_edit_uraian_temuan').on('show.bs.modal', function(e) {
                    $.ajax({
                        url : "{{ route('Auditor6b') }}?jadwal_id=" + data_jadwal,
                        type : "POST",
                        success: function(result) {
                            if (result != "none") {
                                $('#modal_jadwal_id').val(data_jadwal);
                                $('#ptk_uraian_temuan_edit_id').val(result.ptk_input_uraian_temuan);
                            } else {
                                $('#modal_jadwal_id').val(data_jadwal);
                            }
                        }
                    });
                });

                $('#modal_edit_rtk').on('show.bs.modal', function(e) {
                    $.ajax({
                        url : "{{ route('Auditor6b') }}?jadwal_id=" + data_jadwal,
                        type : "POST",
                        success: function(result) {
                            if (result != "none") {
                                $('#modal_jadwal_id2').val(data_jadwal);
                                $('#ptk_rtk_edit_id').val(result.ptk.ptk_input_rtk);
                            } else {
                                $('#modal_jadwal_id2').val(data_jadwal);
                            }
                        }
                    });
                });

                
            });
    </script>

    @stop