@extends('adminlte::page')


@section('title', 'SPMI dan SPME')


@section('content_header')

<h1>Download Berkas AMI</h1>

@stop


@section('content')


@if (Session::has('success'))
<br>
<div class="alert alert-success">

    {!! \Session::get('success') !!}

</div>
@endif

@if (Session::has('pesan'))
<br>
<div class="alert alert-warning">

    {!! \Session::get('pesan') !!}

</div>
@endif

<div class='row'>
    <div class='col-md-12'>
        <!-- Box -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Download Laporan AMI</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i
                            class="fa fa-minus"></i></button>
                </div>
            </div>

            <div class="box-body">
                <form id="auditor_anggota_post" method="post" enctype="multipart/form-data" class="form-horizontal">
                    <input type="hidden" value="ok" name="sip">

                    @csrf
                    <!-- <hr style="display: block; height: 1px; border: 0; border-top: 1px solid #ccc; margin: 1em 0;padding: 0; "> -->

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="inputUsername4" class="col-sm-2 control-label">Jadwal Kegiatan Audit</label>
                                <div class="col-sm-10">
                                    <select onchange="ami_select_jadwal()" name="pen_jadwal_id" class="form-control">
                                        <option>-- Pilih --</option>
                                        @foreach ($data_audit as $da)
                                        <option value="{{$da->pen_jadwal_id}}">{{$da->jenjang}} /
                                            {{$da->pen_jadwal_tanggal}} - {{$da->pen_jadwal_tanggal_sampai}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <!-- end of div -->
                        </div>
                    </div>
                    <!-- end of div -->
                </form>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->

</div><!-- /.row -->



@stop


@section('css')

<!-- <link rel="stylesheet" href="/css/admin_custom.css"> -->

@stop


@section('js')

<script>
    function ami_select_jadwal () {
            if ($('select[name="pen_jadwal_id"]').val() == '-- Pilih --') {
                alert('maaf pilih jadwal terlebih dahulu');
            } else {
                window.location = "{{ url('/') }}/dw/ami/download_laporan/" + $('select[name="pen_jadwal_id"]').val() ;
            }
    }

    $(document).ready(function(){
     
    });
</script>

@stop