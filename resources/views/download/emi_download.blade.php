@extends('adminlte::page')


@section('title', 'SPMI dan SPME')


@section('content_header')

<h1>Download Berkas EMI</h1>

@stop


@section('content')


@if (Session::has('success'))
<br>
<div class="alert alert-success">

    {!! \Session::get('success') !!}

</div>
@endif

@if (Session::has('pesan'))
<br>
<div class="alert alert-warning">

    {!! \Session::get('pesan') !!}

</div>
@endif

<div class='row'>
    <div class='col-md-12'>
        <!-- Box -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Download Laporan EMI</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i
                            class="fa fa-minus"></i></button>
                </div>
            </div>

            <div class="box-body">
                <form id="auditor_anggota_post" method="post" enctype="multipart/form-data" class="form-horizontal">
                    <input type="hidden" value="ok" name="sip">

                    @csrf
                    <!-- <hr style="display: block; height: 1px; border: 0; border-top: 1px solid #ccc; margin: 1em 0;padding: 0; "> -->

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="inputUsername4" class="col-sm-2 control-label">Profil</label>
                                <div class="col-sm-10">
                                    <select onclick="emi_select_profil()" name="profil_model_id" class="form-control">
                                        <option>-- Pilih --</option>
                                        @foreach ($profil as $p)
                                        <option value="{{ $p->id }}">{{ $p->jenjang }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <!-- end of div -->
                            <div class="form-group">
                                <label for="inputUsername4" class="col-sm-2 control-label"> </label>
                                <div class="col-sm-10">
                                    <select onchange="emi_select_tahun()" name="profil_tahun" class="form-control">
                                        <option>-- Pilih --</option>
                                        @foreach ($tahun as $p)
                                        <option value="{{ $p->tahun_pengisian }}">{{ $p->tahun_pengisian }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <!-- end of div -->
                        </div>
                    </div>
                    <!-- end of div -->
                </form>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->

</div><!-- /.row -->



@stop


@section('css')

<!-- <link rel="stylesheet" href="/css/admin_custom.css"> -->

@stop


@section('js')

<script>
    function emi_select_profil () {
        //alert($('select[name="profil_model_id"]').val());
    }

    function emi_select_tahun () {
        if ($('select[name="profil_model_id"]').val() == '-- Pilih --') {
            alert('maaf pilih profil terlebih dahulu');
        } else {
            window.location = "{{ url('/') }}/dw/emi/download_laporan/" + $('select[name="profil_model_id"]').val() + '/' + $('select[name="profil_tahun"]').val() ;
        }
    }

    $(document).ready(function(){
        

    });
</script>

@stop