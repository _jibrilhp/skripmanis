@extends('adminlte::page')


@section('title', 'SPMI dan SPME')


@section('content_header')

<h1>Audit Mutu Internal</h1>

@stop


@section('content')


@if (Session::has('success'))
    <br>
        <div class="alert alert-success">
           
            {!! \Session::get('success') !!}
            
        </div>
@endif

@if (Session::has('pesan'))
    <br>
        <div class="alert alert-warning">
           
            {!! \Session::get('pesan') !!}
            
        </div>
@endif

<div class='row'>
        <div class='col-md-12'>
            <!-- Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Grafik Nilai Capaian Per-Standar</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
              
                <div class="box-body">
                    <form id="grafik_nilai_capaian_standar_post" method="post" enctype="multipart/form-data" class="form-horizontal">
                    <input type="hidden" value="ok" name="sip">

                        @csrf
                  
                    <div class="row">
                        <div class="col-md-12">

                            <div class="form-group">
                                <label for="inputUsername4" class="col-sm-2 control-label">Nama Program Studi</label>
                                <div class="col-sm-10">
                                    <input type="text" name="grafik_nama_emi" class="form-control" id="inputUsername4" placeholder="Nama program studi" value="{{$profildata->nama_prodi}}" required>
                                </div>
                            </div>  

                            <div class="form-group">
                                <label for="inputUsername4" class="col-sm-2 control-label">Tahun Pengukuran Mutu</label>
                                <div class="col-sm-10">
                                    <input type="text" name="grafik_tahun_emi" class="form-control" id="inputUsername4" placeholder="Tahun pengukuran mutu" value="{{$profildata->tahun_pengisian}}" required>
                                </div>
                            </div>  

                            <div class="form-group">
                                <label for="inputUsername4" class="col-sm-2 control-label">Rekap Nilai</label>
                                <div class="col-sm-10">
                                    <table style="overflow-x:auto;" id="grafik_tabel_rekap_nilai" class="table table-bordered display">
                                        <thead>
                                            <tr>
                                                <th>Rekap nilai</th>
                                                <th>Nilai Capaian per standar</th>
                                                <th>Sebutan</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                        @php 
                                            $total_data = 0;
                                            $total_persen = 0;
                                        @endphp
                                        @foreach ($data_capaian as $key => $dc)
                                        @php
                                            $total_persen += $data_capaian[$key];
                                            $total_data++;
                                        @endphp
                                            <tr>
                                                <td>{{ $data_standar[$key] }}</td>
                                                <td>{{ $data_capaian[$key] }}%</td>
                                                <td>@if ($data_capaian[$key] >= 86) 
                                                    Sangat Baik
                                                @elseif ($data_capaian[$key] >= 72)
                                                    Baik
                                                @elseif ($data_capaian[$key] >= 58)
                                                    Lebih dari Cukup
                                                @elseif ($data_capaian[$key] >= 43)
                                                    Cukup
                                                @elseif ($data_capaian[$key] >= 29)
                                                    Perbaikan minor
                                                @elseif ($data_capaian[$key] >= 15)
                                                    Perbaikan mayor
                                                @elseif ($data_capaian[$key] >= 0)
                                                    Perbaikan menyeluruh dan mendesak
                                                @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Rata-rata pencapaian</th>
                                                <th>{{ $total_persen/$total_data }}%</th>
                                                <th>@if (($total_persen/$total_data) >= 86) 
                                                    Sangat Baik
                                                @elseif (($total_persen/$total_data) >= 72)
                                                    Baik
                                                @elseif (($total_persen/$total_data) >= 58)
                                                    Lebih dari Cukup
                                                @elseif (($total_persen/$total_data) >= 43)
                                                    Cukup
                                                @elseif (($total_persen/$total_data) >= 29)
                                                    Perbaikan minor
                                                @elseif (($total_persen/$total_data) >= 15)
                                                    Perbaikan mayor
                                                @elseif (($total_persen/$total_data) >= 0)
                                                    Perbaikan menyeluruh dan mendesak
                                                @endif
                                                </th>
                                            </tr>
                                        </tfoot>

                                        
                                    </table>
                                </div>
                            </div>  

                          

                   
                    <!-- end of div -->
                        </div>
                    </div>
                    <!-- end of div -->

                  
                       
                 

                    </form>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->

    </div><!-- /.row -->

    

@stop


@section('css')

@stop


@section('js')

<script>
    $(document).ready(function(){
    });
</script>

@stop



