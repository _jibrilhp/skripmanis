@extends('adminlte::page')


@section('title', 'SPMI dan SPME')


@section('content_header')

<h1>Matriks Penilaian Akreditasi Sarjana</h1>

@stop


@section('content')


@if (Session::has('success'))
<br>
<div class="alert alert-success">

    {!! \Session::get('success') !!}

</div>
@endif

@if (Session::has('pesan'))
<br>
<div class="alert alert-warning">

    {!! \Session::get('pesan') !!}

</div>
@endif

<div class='row'>
    <div class='col-md-12'>
        <!-- Box -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Analisis Nilai Matriks</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i
                            class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <form class="form-horizontal" method="post" id="post_text_analysis">
                    <div class="form-group">
                        <label for="inputJabatan4" class="col-sm-2 control-label">Teks</label>
                        <div class="col-sm-10">
                            <textarea name="teks_analysis" class="form-control" 
                                placeholder="Input analisis teks" value=""></textarea>
                        </div>
                    </div>
                    
                    <div class="form-group">
                            
                        <div class="col-sm-2"></div>
                        <div class="col-sm-10">
                            <button type="submit" class="btn btn-primary">Periksa</button>
                        </div>
                    </div>

                    <div class="form-group">
                            <label for="inputJabatan4" class="col-sm-2 control-label">Penilaian</label>
                            <div class="col-sm-10">
                                <h5 id="hasil">Belum ada hasil</h5>
                            </div>
                        </div>
                        
                </form>

            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->

</div><!-- /.row -->




@stop


@section('css')



@stop


@section('js')

<script>
    $(document).ready(function() {
        $('#post_text_analysis').submit(function (e) {
            e.preventDefault();

            $("#hasil").html("Memproses ke server ..");

            var post_data = $('#post_text_analysis').serializeArray();
            $.ajax({            
                type: "POST",
                url: '{{route('post_data_matriks')}}',  
                data: post_data,               
                success: function(json){
                    if (json.status == 'ok') {
                        $("#hasil").html("Hasil analisis, kata ini termasuk kelompok Harkat dan Peringkat <br>" + json.analysis)
                    } else {
                        $("#hasil").html("Maaf server sedang sibuk atau tidak dapat dikoneksi");
                    }
                    
                }
            });


      
        });
    });
    

</script>

@stop