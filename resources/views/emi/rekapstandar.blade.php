@extends('adminlte::page')


@section('title', 'SPMI dan SPME')


@section('content_header')

<h1>Audit Mutu Internal</h1>

@stop


@section('content')


@if (Session::has('success'))
    <br>
        <div class="alert alert-success">
           
            {!! \Session::get('success') !!}
            
        </div>
@endif

@if (Session::has('pesan'))
    <br>
        <div class="alert alert-warning">
           
            {!! \Session::get('pesan') !!}
            
        </div>
@endif

<div class='row'>
        <div class='col-md-12'>
            <!-- Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Rekapitulasi dan Analisis Per-Standar</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
              
                <div class="box-body">
                    <form id="rekap_standar_post" method="post" enctype="multipart/form-data" class="form-horizontal">
                    <input type="hidden" value="ok" name="sip">

                        @csrf
                  
                    <div class="row">
                        <div class="col-md-12">

                            <div class="form-group">
                                <label for="inputUsername4" class="col-sm-2 control-label">Nama Program Studi</label>
                                <div class="col-sm-10">
                                    <input type="text" name="rekap_nama_emi" class="form-control" id="inputUsername4" placeholder="Nama program studi" value="{{$profildata->nama_prodi}}" required>
                                </div>
                            </div>  

                            <div class="form-group">
                                <label for="inputUsername4" class="col-sm-2 control-label">Tahun Pengukuran Mutu</label>
                                <div class="col-sm-10">
                                    <input type="text" name="rekap_tahun_emi" class="form-control" id="inputUsername4" placeholder="Tahun pengukuran mutu" value="{{$profildata->tahun_pengisian}}" required>
                                </div>
                            </div>  

                            <div class="form-group">
                                <label for="inputUsername4" class="col-sm-2 control-label"></label>
                                <div class="col-sm-10">
                                    <table style="overflow-x:auto;" id="rekap_tabel_rekap_nilai" class="table table-bordered display">
                                        <thead>
                                            <tr>
                                                <th>Rekap nilai</th>
                                                <th>Bobot standar</th>
                                                <th>Nilai Capaian terbobot per standar</th>
                                                <th>Nilai Capaian per standar</th>
                                                <th>Sebutan</th>
                                                <th>Temuan</th>
                                                <th>Hasil temuan disebabkan</th>
                                                <th>Hasil temuan mengakibatkan</th>
                                                <th>Rekomendasi</th>
                                                <th></th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                        @php 
                                            $total_data = 0;
                                            $total_persen = 0;
                                        @endphp
                                        @foreach ($data_capaian as $key => $dc)
                                        @php
                                            $total_persen += $data_capaian[$key];
                                            $total_data++;
                                        @endphp
                                            <tr>
                                                <td>{{ $data_standar[$key] }}</td>
                                                <td>{{ $data_bobot[$key] }}</td>
                                                <td>{{ $data_bobot[$key] * $data_capaian[$key] }}</td>
                                                <td>{{ $data_capaian[$key] }}</td>   
                                                <td>@if ($data_capaian[$key] >= 86) 
                                                    Sangat Baik
                                                @elseif ($data_capaian[$key] >= 72)
                                                    Baik
                                                @elseif ($data_capaian[$key] >= 58)
                                                    Lebih dari Cukup
                                                @elseif ($data_capaian[$key] >= 43)
                                                    Cukup
                                                @elseif ($data_capaian[$key] >= 29)
                                                    Perbaikan minor
                                                @elseif ($data_capaian[$key] >= 15)
                                                    Perbaikan mayor
                                                @elseif ($data_capaian[$key] >= 0)
                                                    Perbaikan menyeluruh dan mendesak
                                                @endif
                                                </td>
                                                
                                                <td>{{ $data_rekap_temuan[$key] }}</td>
                                                <td>{{ $data_rekap_sebab[$key] }}</td>
                                                <td>{{ $data_rekap_akibat[$key] }}</td>
                                                <td>{{ $data_rekap_rekomendasi[$key] }}</td>
                                                <td><a href="#" class="rekap-edit-button" data-standar="{{ $data_standar_id[$key] }}" data-profil="{{ $profildata->id }}" data-tahun="{{ $profildata->tahun_pengisian }}" data-toggle="modal" data-target="#myModalRekap"><i class="fa fa-edit"></i></a></td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Rata-rata pencapaian</th>
                                                <th></th>
                                                <th></th> 
                                                <th>{{ ($total_persen/$total_data) }} </th> 
                                                <th>@if (($total_persen/$total_data) >= 86) 
                                                    Sangat Baik
                                                @elseif (($total_persen/$total_data) >= 72)
                                                    Baik
                                                @elseif (($total_persen/$total_data) >= 58)
                                                    Lebih dari Cukup
                                                @elseif (($total_persen/$total_data) >= 43)
                                                    Cukup
                                                @elseif (($total_persen/$total_data) >= 29)
                                                    Perbaikan minor
                                                @elseif (($total_persen/$total_data) >= 15)
                                                    Perbaikan mayor
                                                @elseif (($total_persen/$total_data) >= 0)
                                                    Perbaikan menyeluruh dan mendesak
                                                @endif
                                                </th>
                                                <th></th>
                                            </tr>
                                        </tfoot>

                                        
                                    </table>
                                </div>
                            </div>  

                          

                   
                    <!-- end of div -->
                        </div>
                    </div>
                    <!-- end of div -->

                  
                       
                 

                    </form>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->

    </div><!-- /.row -->

    <!-- Modal Rekap Standar-->
    <div id="myModalRekap" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <form id="post_data_model" method="post" action="{{ route('RekapStandar2') }}">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Ubah data Rekap Standar</h4>
                    </div>
                    <div class="modal-body">  
                    <input type="hidden" name="rekap_id">
                            @csrf                        
                        <div class="row">
                            <div class="col-md-12">

                                <div class="form-group">
                                    <label for="inputUsername4" class="col-sm-2 control-label">Temuan</label>
                                    <div class="col-sm-10">
                                        <textarea name="rekap_temuan" class="form-control" id="inputUsername4" placeholder="Isian temuan"></textarea>
                                    </div>
                                </div>  
                                
                                <div class="form-group">
                                    <label for="inputUsername4" class="col-sm-2 control-label">Hasil temuan disebabkan</label>
                                    <div class="col-sm-10">
                                        <textarea name="rekap_hasil_temuan_disebabkan" class="form-control" id="inputUsername4" placeholder=""></textarea>
                                    </div>
                                </div>  
                                
                                <div class="form-group">
                                    <label for="inputUsername4" class="col-sm-2 control-label">Hasil temuan mengakibatkan</label>
                                    <div class="col-sm-10">
                                        <textarea name="rekap_hasil_temuan_mengakibatkan" class="form-control" id="inputUsername4" placeholder="""></textarea>
                                    </div>
                                </div>  

                                <div class="form-group">
                                    <label for="inputUsername4" class="col-sm-2 control-label">Rekomendasi</label>
                                    <div class="col-sm-10">
                                        <textarea name="rekap_rekomendasi" class="form-control" id="inputUsername4" placeholder=""></textarea>
                                    </div>
                                </div>  

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" >Simpan</button>
                    </div>
                </div>
            </form>

        </div>
    </div>

    

@stop


@section('css')

@stop


@section('js')

<script>
    $(document).ready(function(){
        var standar_id;
        var profil_id;
        var tahun ;

        $('#rekap_tabel_rekap_nilai').DataTable({
            "scrollX": true
        });

        $('.rekap-edit-button').click(function() {
            
            standar_id = $(this).data('standar');
            profil_id = $(this).data('profil');
            tahun = $(this).data('tahun');

            $('#post_data_model').attr('action','{{ route('RekapStandar2') }}');

            

            $.ajax({
                url: "{{ route('RekapStandar1') }}?standar_id=" + standar_id + "&profil_id=" + profil_id + "&tahun=" + tahun,
                success: function(result){

                    $('#post_data_model').attr('action','{{ route('RekapStandar2') }}?standar_id=' + standar_id + '&profil_id=' + profil_id + '&tahun=' + tahun  );

                    if (result == 'none') {
                        console.log('tidak ada data');
                    } else {
                        //var obj_data = JSON.parse(result);

                        $("textarea[name='rekap_temuan']").text(result.rekap_temuan);
                        $("textarea[name='rekap_hasil_temuan_disebabkan']").text(result.rekap_hasil_temuan_sebab);
                        $("textarea[name='rekap_hasil_temuan_mengakibatkan']").text(result.rekap_hasil_temuan_akibat);
                        $("textarea[name='rekap_rekomendasi']").text(result.rekap_rekomendasi);
                        $("input[name='rekap_id']").val(result.rekap_id);
                        
                    }
                }
            });
        });
    });
</script>

@stop



