@php
use App\Http\Controllers\AuditorController;

$ami = AuditorController::ami_get_ptk($pen_jadwal_id);

@endphp
<!DOCTYPE html>
<html>
<head>
    <title>Daftar Hadir</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<style>
      @page { size: 21cm 29.7cm landscape; }
</style>
<body>
<p><center><strong>DAFTAR HADIR</strong></center></p>
<p><center><strong>AUDIT MUTU INTERNAL (AMI)</strong></center></p>
<p><center>{{$ami['ptk']->ptk_input_no}}</center></p>
<p><strong>&nbsp;</strong></p>
<p>&nbsp;</p>
<p>Program Studi&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : <strong>{{$ami['sp']['nama_prodi']}}</strong></p>


<p>Auditor&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : 1.
    {{$ami['ketua_auditor']['name']}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    (Ketua)</p>
<ol start="2">
    @foreach ($ami['anggota_auditor'] as $data_item)
    <li>&nbsp; {{$data_item['name']}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (Anggota)</li>
    @endforeach
</ol>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;</p>
<table class="table table-bordered">
    <tbody>
        <tr>
            <td rowspan="2" width="48">
                <p><strong>No.</strong></p>
            </td>
            <td rowspan="2" width="186">
                <p><strong>Nama</strong></p>
            </td>
            <td width="170">
                <p><strong>&nbsp;</strong></p>
            </td>
            <td colspan="2" width="400">
                <p><strong>Tanda Tangan</strong></p>
            </td>
        </tr>
        <tr>
            <td width="170">
                <p><strong>Jabatan</strong></p>
            </td>
            <td width="200">
                <p><strong>{{date("d-m-Y",strtotime($ami['sp']['pen_jadwal_tanggal']))}}
                        </strong></p>
            </td>
            <td width="200">
                <p><strong>{{date("d-m-Y",strtotime($ami['sp']['pen_jadwal_tanggal_sampai']))}}</strong></p>
            </td>
        </tr>
        <tr>
            <td width="48">
                <p>1.</p>
                <p>&nbsp;</p>
            </td>
            <td width="186">
                <p> {{$ami['ketua_auditor']['name']}}</p>
            </td>
            <td width="170">
                <p>Ketua Auditor</p>
            </td>
            <td width="236">&nbsp;</td>
            <td width="217">&nbsp;</td>
        </tr>
        
        @foreach ($ami['anggota_auditor'] as $key => $data_anggota)
        <tr>
            <td width="48">
                <p>{{2+ $key}}.</p>
                <p>&nbsp;</p>
            </td>
            <td width="186">
                <p>{{$data_anggota['name']}}</p>
            </td>
            <td width="170">
                <p>Anggota</p>
            </td>
            <td width="236">
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
            </td>
            <td width="217">&nbsp;</td>
        </tr>
        @endforeach
    </tbody>
</table>
<p>&nbsp;</p>
</body>
</html>