@php
use App\Http\Controllers\AuditorController;
@endphp
<!DOCTYPE html>
<html>

<head>
    <title>Laporan Audit Mutu Internal</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<style>
    @page {
        size: 21cm 35cm landscape;
    }

    li.ndak {
        list-style-type: none;
    }

    li.ndak2 {
        list-style-type: square;
    }
</style>

<body>
    <p>
        <center><strong>LAPORAN AUDIT MUTU INTERNAL</strong></center>
    </p>
    <p>
        <center><strong>PROGRAM STUDI</strong></center>
    </p>
    <p>&nbsp;</p>
    <ol>
        <li class="ndak"><strong>I. PENDAHULUAN</strong></li>
    </ol>
    <table class="table table-bordered">
        <tbody>
            <tr>
                <td width="150">
                    <p>Auditee</p>
                </td>
                <td colspan="3" width="452">
                    <p>Program Studi {{ $profildata->nama_prodi }}</p>
                    <p>Ketua Program Studi : {{$lauditee->name}}</p>
                    <p>&nbsp;</p>
                </td>
            </tr>
            <tr>
                <td width="150">
                    <p>Alamat</p>
                </td>
                <td colspan="3" width="452">
                    <p>
                        {{$profildata->alamat_pt}}</p>
                    <p>&nbsp;</p>
                </td>
            </tr>
            <tr>
                <td width="150">
                    <p>Ketua Program Studi</p>
                </td>
                <td width="214">
                    <p>{{$lauditee->name}}</p>
                </td>
                <td colspan="2" width="238">
                    <p>Telp. : {{$profildata->telepon_prog}}</p>
                </td>
            </tr>
            <tr>
                <td width="150">
                    <p>Tanggal Audit</p>
                </td>
                <td colspan="3" width="452">
                    <p>{{date("d-m-Y",strtotime($jadwaldata->pen_jadwal_tanggal)) . ' - ' . date("d-m-Y",strtotime($jadwaldata->pen_jadwal_tanggal_sampai)) }}
                    </p>
                </td>
            </tr>
            <tr>
                <td width="150">
                    <p>Ketua Auditor</p>
                </td>
                <td colspan="3" width="452">
                    <p>{{$lauditor->name}}</p>
                </td>
            </tr>
            <tr>
                <td width="150">
                    <p>Anggota Auditor</p>
                </td>
                <td colspan="3" width="452">
                    @foreach ($lauditor_anggota as $lag)
                    <p>{{$lag->name}}</p>
                    @endforeach

                </td>
            </tr>
            <tr>
                <td width="150">
                    <p>Tanda Tangan</p>
                    <p>Ketua Auditor</p>
                </td>
                <td colspan="2" width="217">
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                </td>
                <td width="234">
                    <p>Tanda Tangan</p>
                    <p>Auditee :&nbsp;&nbsp;</p>
                </td>
            </tr>
            <tr>
                <td width="150">
                    {{$lauditor->name}}
                </td>
                <td colspan="2" width="217">
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                </td>
                <td width="234">
                    {{$lauditee->name}}
                </td>
            </tr>
        </tbody>
    </table>
    <p>&nbsp;</p>
    <ol>
        <li class="ndak"><strong>II. TUJUAN AUDIT:</strong></li>
    </ol>
    <p><em>Beri tanda &radic; sesuai yang dikerjakan.</em></p>
    <table width="601" class="table table-bordered">
        <tbody>
            @foreach ($tujuan as $key => $tj)


            <tr>
                <td width="555">
                    <p>{{chr(97 + $key)}}.&nbsp;&nbsp; {{$tj->tujuan }}</p>
                </td>
                <td width="46">
                    <p>@if ($tj->yes_no_check == '1') V @endif&nbsp;</p>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <p>&nbsp;</p>
    <ul>
        <li class="ndak"><strong>III. LINGKUP AUDIT:</strong></li>
    </ul>
    <p>Butir-butir Standar :</p>
    <ul>
        @foreach ($jadwallingkup as $jl)
        <li class="ndak2">{{$jl->pen_lingkup_isian}}</li>
        @endforeach
    </ul>
    <p>&nbsp;</p>
    <ol>
        <li class="ndak"><strong>IV. JADWAL AUDIT:</strong></li>
    </ol>
    <table width="446" class="table table-bordered">
        <tbody>
            <tr>
                <td width="38">
                    <p><strong>No</strong></p>
                </td>
                <td width="142">
                    <p><strong>Jam</strong></p>
                </td>
                <td width="267">
                    <p><strong>Kegiatan Audit</strong></p>
                </td>
            </tr>
            @foreach ($jadwalaudit as $jl)


            <tr>
                <td colspan="3" width="446">
                    <p>{{date("d-m-Y",strtotime($jl->tanggal))}}</p>
                </td>
            </tr>
            @php
            $ami_get = AuditorController::auditor_ami_get_kegiatan($pen_jadwal_id,$jl->tanggal);
            @endphp
            @if ($ami_get != null)
            @php $nom =1 ; @endphp
            @foreach ($ami_get as $key => $am)

            <tr>
                <td width="38">
                    <p>{{$nom++}}</p>
                </td>
                <td width="142">
                    <p>{{$am->pukul}}</p>
                </td>
                <td width="267">
                    <p>{{$am->kegiatan_audit}}</p>
                </td>
            </tr>
            @endforeach


            @endif
            @endforeach
        </tbody>
    </table>
    <p><strong>&nbsp;</strong></p>
    <ol>
        <li class="ndak"><strong>V. TEMUAN AUDIT:</strong>
            <ol>
                <li class="ndak"><strong>1. Ketidak-sesuaian</strong></li>
            </ol>
        </li>
    </ol>
    <table width="603" class="table table-bordered">
        <tbody>
            <tr>
                <td width="73">
                    <p><strong>KTS/OB</strong></p>
                    <p><strong>(Initial Auditor)</strong></p>
                </td>
                <td width="134">
                    <p><strong>Referensi</strong></p>
                    <p><strong>(butir mutu)</strong></p>
                </td>
                <td width="395">
                    <p><strong>Pernyataan</strong><strong></strong></p>
                </td>
            </tr>
            @foreach ($ketidaksesuaian as $kn)
            <tr>
                <td width="73">
                    <p><strong>{{$kn->kts_ob}}</strong></p>
                    <p><strong></strong></p>
                    <p>&nbsp;</p>
                </td>
                <td width="134">
                    <p>{{$kn->referensi_butir_mutu}}</p>
                </td>
                <td width="395">
                    <p>{{$kn->pernyataan}}</p>
                </td>
            </tr>
            @endforeach

        </tbody>
    </table>
    <p>&nbsp;</p>
    <ol start="2">
        <li class="ndak"><strong>2. Saran perbaikan :</strong></li>
    </ol>
    <table width="603" class="table table-bordered">
        <tbody>
            <tr>
                <td width="38">
                    <p><strong>No</strong></p>
                </td>
                <td width="121">
                    <p><strong>Bidang</strong></p>
                </td>
                <td width="163">
                    <p><strong>Kelebihan </strong></p>
                </td>
                <td width="282">
                    <p><strong>Peluang Peningkatan </strong></p>
                </td>
            </tr>
            @foreach ($saran as $key => $sn)


            <tr>
                <td width="38">
                    <p><strong>{{1 + $key}}</strong></p>
                </td>
                <td width="121">
                    <p><strong>{{$sn->bidang}}</strong></p>
                </td>
                <td width="163">
                    <p><strong>{{$sn->kelebihan}}&nbsp;</strong></p>
                </td>
                <td width="282">
                    <p>-&nbsp;&nbsp;&nbsp; <strong>{{$sn->peluang_peningkatan}}</strong></p>
                </td>
            </tr>
            @endforeach

        </tbody>
    </table>
    <p>&nbsp;</p>
    <ol>
        <li class="ndak"><strong>VI. KESIMPULAN AUDIT</strong></li>
    </ol>
    <p><strong>&nbsp;</strong></p>
    <p>Tim audit menyimpulkan :</p>
    @php
    $jkesimpulan = AuditorController::ami_get_kesimpulan($profildata->jenjang_id,$pen_jadwal_id);

    @endphp
    @foreach($jkesimpulan as $key => $jk)
    <ol start="{{$jk['urutan']}}">
        <li>{{$jk['jk_isi_kesimpulan']}}
            <strong> {{$jk['isian']}}
            </strong>

        </li>
        <li class="ndak">
            {{$jk['other']}}
        </li>
    </ol>
    <p>&nbsp;</p>
    @endforeach