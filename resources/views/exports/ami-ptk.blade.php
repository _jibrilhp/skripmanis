@php
use App\Http\Controllers\AuditorController;

$ami = AuditorController::ami_get_ptk($pen_jadwal_id);

@endphp
<!DOCTYPE html>
<html>
<head>
	<title>Permintaan Tindakan Koreksi</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<style>
      @page { size: 21cm 29.7cm landscape; }
</style>
<body>
<p><center><strong>PERMINTAAN TINDAKAN KOREKSI (PTK)</strong></center></p>
<p><center>{{$ami['ptk']->ptk_input_no}}</center></p>
<p>&nbsp;</p>
<table width="628" class="table table-bordered">
    <tbody>
        <tr>
            <td width="248">
                <p>Program Studi</p>
            </td>
            <td colspan="3" width="380">
                <p>{{$ami['sp']['nama_prodi']}}</p>
            </td>
        </tr>
        <tr>
            <td width="248">
                <p>Ketua Program Studi</p>
            </td>
            <td colspan="3" width="380">
                <p>{{$ami['auditee']['name']}}</p>
            </td>
        </tr>
        <tr>
            <td width="248">
                <p>Auditor</p>
            </td>
            <td width="139">
                <p>{{$ami['ketua_auditor']['name']}}</p>
            </td>
            <td width="104">
                <p>Tanggal Audit</p>
            </td>
            <td width="137">
                <p>{{date("d-m-Y",strtotime($ami['sp']['pen_jadwal_tanggal']))}} -
                    {{date("d-m-Y",strtotime($ami['sp']['pen_jadwal_tanggal_sampai']))}}</p>
            </td>
        </tr>
        <tr>
            <td width="248">
                <p><strong>PTK No</strong>:</p>
            </td>
            <td colspan="3" width="380">
                @if ($ami['isian'] == 'Mayor')
                <p><strong>Kategori :&nbsp;&nbsp;&nbsp; </strong><strong>&nbsp;Mayor&nbsp;&nbsp;&nbsp;&nbsp;
                    </strong><strong>V</strong><strong>&nbsp; Minor&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </strong><strong>&nbsp;Observasi</strong></p>
                @elseif ($ami['isian'] == 'Minor')
                <p><strong>Kategori :&nbsp;&nbsp;&nbsp; </strong><strong>&nbsp;Mayor&nbsp;&nbsp;&nbsp;&nbsp;
                    </strong><strong></strong><strong>V &nbsp; Minor&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </strong><strong>&nbsp;Observasi</strong></p>
                @elseif ($ami['isian'] == 'Observasi')
                <p><strong>Kategori :&nbsp;&nbsp;&nbsp; </strong><strong>&nbsp;Mayor&nbsp;&nbsp;&nbsp;&nbsp;
                    </strong><strong></strong><strong> &nbsp; Minor&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong><strong>V
                        &nbsp;Observasi</strong></p>
                @endif

            </td>
        </tr>
        <tr>
            <td width="248">
                <p>Referensi (Butir Mutu)</p>
            </td>
            <td colspan="3" width="380">
                <p>{!! $ami['referensi_mutu'] !!}</p>

            </td>
        </tr>
        <tr>
            <td colspan="4" width="628">
                <p>{{$ami['ptk']->ptk_input_uraian_temuan }}</p>
                <p>&nbsp;</p>
            </td>
        </tr>
        <tr>
            <td width="248">
                <p>Tanda Tangan dan Nama Auditor</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>Tanggal : {{date("d-m-Y",strtotime($ami['sp']['pen_jadwal_tanggal_sampai']))}}</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
            </td>
            <td colspan="3" width="200">
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p></p>
                <p>({{$ami['ketua_auditor']['name']}})&nbsp;</p>
            </td>
        </tr>
        <tr>
            <td colspan="4" width="628">
                <p>Rencana Tindakan Koreksi (<em>diisi oleh teraudit &amp; ditandatangani</em>):</p>
                <p>{{$ami['ptk']->ptk_input_rtk }}</p>

                <p>&nbsp;&nbsp;</p>
            </td>
        </tr>
        <tr>
            <td width="248">
                <p>Tanda Tangan dan Nama Teraudit</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>Tanggal : {{date("d-m-Y",strtotime($ami['sp']['pen_jadwal_tanggal_sampai']))}}</p>
                <p>&nbsp;</p>
            </td>
            <td colspan="3" width="380">
                <p>&nbsp;&nbsp;</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p></p>
                <p>({{$ami['auditee']['name']}})&nbsp;</p>
            </td>
        </tr>
    </tbody>
</table>
<p><strong><u><br /> </u></strong></p>
<p><strong><u>&nbsp;</u></strong></p>
</body>
</html>