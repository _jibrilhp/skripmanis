<!DOCTYPE html>
<html>
<head>
	<title>Grafik Nilai Capaian Per-Standar</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<style>
      @page { size: 21cm 29.7cm landscape; }
</style>
<body>
<table width="654"  class='table table-bordered'>
    <tbody>
    
        <tr>
            <td colspan="2">GRAFIK NILAI CAPAIAN PER-STANDAR</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">Nama Program Studi:&nbsp;</td>
            <td>{{ $profildata->jenjang }}</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">Tahun Pengukuran Mutu:&nbsp;</td>
            <td>{{ $profildata->tahun_pengisian }}</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td width="5">&nbsp;</td>
            <td><b>Rekap nilai</b></td>
            <td width="90">Nilai capaian per standar</td>
            <td width="120">Sebutan</td>
            <td>&nbsp;</td>
        </tr>

        @php
            $total_data = 0;
            $total_persen = 0;
        @endphp

        @foreach ($data_capaian as $key => $dc)
        @php
            $total_persen += $data_capaian[$key];
            $total_data++;
        @endphp
        <tr>
            <td width="5">&nbsp;</td>
            <td width="316">{{ $data_standar[$key] }}</td>
            <td>{{ $data_capaian[$key] }}</td>
            <td>@if ($data_capaian[$key] >= 86) 
                Sangat Baik
            @elseif ($data_capaian[$key] >= 72)
                Baik
            @elseif ($data_capaian[$key] >= 58)
                Lebih dari Cukup
            @elseif ($data_capaian[$key] >= 43)
                Cukup
            @elseif ($data_capaian[$key] >= 29)
                Perbaikan minor
            @elseif ($data_capaian[$key] >= 15)
                Perbaikan mayor
            @elseif ($data_capaian[$key] >= 0)
                Perbaikan menyeluruh dan mendesak
            @endif</td>
            <td>&nbsp;</td>
        </tr>
        @endforeach

        <tr>
            <td>&nbsp;</td>
            <td>Rata-rata Capaian</td>
            <td>{{ $total_persen / 100 }}</td>
            <td>@if ($total_persen /100  >= 86) 
                Sangat Baik
            @elseif ($total_persen /100 >= 72)
                Baik
            @elseif ($total_persen /100 >= 58)
                Lebih dari Cukup
            @elseif ($total_persen /100 >= 43)
                Cukup
            @elseif ($total_persen /100 >= 29)
                Perbaikan minor
            @elseif ($total_persen /100 >= 15)
                Perbaikan mayor
            @elseif ($total_persen /100 >= 0)
                Perbaikan menyeluruh dan mendesak
            @endif</td>
            <td>&nbsp;</td>
        </tr>
        
    </tbody>
</table>
<img src="{{ public_path() . '/render_grafik.png' }}" alt="Red dot" />
<p>&nbsp;</p>


