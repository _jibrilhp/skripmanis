@php
use App\Http\Controllers\AuditorController;
@endphp
<!DOCTYPE html>
<html>
<head>
	<title>Nilai Capaian & Target</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<style>
      @page { size: 21cm 60cm landscape; }
</style>
<body>
<table width="1300"  >
    <tbody>
        <tr>
            <td colspan="3" width="208">NILAI CAPAIAN</td>
            <td width="222">&nbsp;</td>
            <td width="79">&nbsp;</td>
            <td width="205">&nbsp;</td>
            <td width="64">&nbsp;</td>
            <td width="92">&nbsp;</td>
            <td width="78">&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td width="148">&nbsp;</td>
            <td width="222">&nbsp;</td>
            <td width="79">&nbsp;</td>
            <td width="205">&nbsp;</td>
            <td>&nbsp;</td>
            <td width="92">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3">Nama Program Studi:&nbsp;</td>
            <td colspan="4">{{ $profildata->jenjang }}</td>
            <td width="92">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3">Tahun Pengukuran Mutu:&nbsp;</td>
            <td colspan="4">{{ $profildata->tahun_pengisian }}</td>
            <td width="92">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td width="148">&nbsp;</td>
            <td width="222">&nbsp;</td>
            <td width="79">&nbsp;</td>
            <td width="205">&nbsp;</td>
            <td>&nbsp;</td>
            <td width="92">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
    <table class="table table-bordered">
        @php
        $nomor = 0;
        $bobot_nilai_total = 0;
        $capaian_nilai_total = 0;
        @endphp
        @foreach ($standar_name as $key => $im)
        @php
        $nomor++
        @endphp



        <tr>
            <td colspan="3">{{$nomor }}. {{ $im }}</td>
            <td width="222">&nbsp;</td>
            <td width="79">&nbsp;</td>
            <td width="205">&nbsp;</td>
            <td>&nbsp;</td>
            <td width="92">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    
    
        <tr>
            <td width = "1">&nbsp;</td>
            <td width = "1">&nbsp;</td>
            <td width="148">&nbsp;</td>
            <td width="222">&nbsp;</td>
            <td width="79">Bobot (%)</td>
            <td width="205">Keadaan Prodi saat ini</td>
            <td width="64">Nilai capaian</td>
            <td width="92">Sebutan</td>
            <td width="78">&nbsp;Capaian Terbobot&nbsp;</td>
        </tr>
        @php
        $total_nilai = 0;
        $bobot_nilai = 0;

        $capaian_nilai = 0;
        $urutan = 0;
        @endphp
        @foreach ($standar_list[$key] as $sl)
        @php
        $urutan++;
        $au = AuditorController::kjm_step_emi_bobot_nilai($sl->isian_id);
        if ($au != null)
        {
        $ak = AuditorController::kjm_step_emi_keadaan_prodi($sl->isian_id,$au->nilai);
        } else {
        $ak = null;
        }
        @endphp
        <tr>
            <td width = "1">&nbsp;</td>
            <td width = "1"></td>
            <td colspan="2" width="370"><b>{{$sl->emi_isian_standar}}</b></td>
            <td width="79">{{$sl->emi_bobot_persen}}%</td>
            <td width="390">@if ($ak != null) {{$ak->keadaan_prodi_saat_ini}} @endif </td>
            <td width="64">@if ($au != null){{$au->nilai}} @endif</td>
            <td width="120">@if ($au != null)
                @if ($au->nilai > 6) Sangat Baik
                @elseif ($au->nilai > 5) Baik
                @elseif ($au->nilai > 4) Lebih dari cukup
                @elseif ($au->nilai > 3) Cukup
                @elseif ($au->nilai > 2) Perbaikan minor
                @elseif ($au->nilai > 1) Perbaikan mayor
                @elseif ($au->nilai > 0) Perbaikan menyeluruh dan mendesak
                @endif
                @php $total_nilai += $au->nilai; @endphp

                @php $capaian_nilai += (float) $au->nilai * (float) $sl->emi_bobot_persen / 100; @endphp
                @endif </td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; @if ($au !=
                null){{(float) $au->nilai * (float) $sl->emi_bobot_persen  / 100 }} @endif</td>
        </tr>
        @php $bobot_nilai += (float) $sl->emi_bobot_persen / 100;
        $bobot_nilai_total += (float) $sl->emi_bobot_persen / 100;
        @endphp
        @endforeach
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td width="148">Rata-rata</td>
            <td width="222">&nbsp;</td>
            <td width="79">&nbsp;</td>
            <td width="205">&nbsp;</td>
            <td width="64">@if ($urutan > 0) {{ $total_nilai / $urutan }} @endif</td>
            <td width="92">@if ($urutan > 0)
                @if (($total_nilai / $urutan) > 0)
                @if (($total_nilai / $urutan) > 6) Sangat Baik
                @elseif (($total_nilai / $urutan) > 5) Baik
                @elseif (($total_nilai / $urutan) > 4) Lebih dari cukup
                @elseif (($total_nilai / $urutan) > 3) Cukup
                @elseif (($total_nilai / $urutan) > 2) Perbaikan minor
                @elseif (($total_nilai / $urutan) > 1) Perbaikan mayor
                @elseif (($total_nilai / $urutan) > 0) Perbaikan menyeluruh dan mendesak
                @endif
                @endif
                @endif</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td width="148">Total</td>
            <td width="222">&nbsp;</td>
            <td width="79">{{ (float) $bobot_nilai }}%</td>
            <td width="205">&nbsp;</td>
            <td>&nbsp;</td>
            <td width="92">&nbsp;</td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                {{ $capaian_nilai }}</td>
        </tr>
        @php $total_capaian_max = 7 * $bobot_nilai; $capaian_nilai_total += (float) $capaian_nilai; @endphp
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td width="148">Nilai standar</td>
            <td width="222">&nbsp;</td>
            <td width="79">&nbsp;</td>
            <td width="205">&nbsp;</td>
            <td>&nbsp;</td>
            <td width="92">&nbsp;</td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; @if ($total_capaian_max > 0)
                {{$capaian_nilai / $total_capaian_max}} @endif</td>
        </tr>
        @endforeach

        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td width="148">&nbsp;</td>
            <td width="222">&nbsp;</td>
            <td width="79">&nbsp;</td>
            <td width="205">&nbsp;</td>
            <td>&nbsp;</td>
            <td width="92">&nbsp;</td>
            <td>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
            </td>
        </tr>
    </tbody>
</table>


<table width="948">
    <tbody>
        <tr>
            <td width="29">
                <p>&nbsp;</p>
                <p>&nbsp;</p>
            </td>
            <td width="31">&nbsp;</td>
            <td width="148">TOTAL terbobot</td>
            <td width="222">&nbsp;</td>
            <td width="79">{{$bobot_nilai_total}}%</td>
            <td width="205">&nbsp;</td>
            <td colspan="2" width="156">Jumlah bobot =</td>
            <td width="78">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{$capaian_nilai_total}}</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td width="148">&nbsp;</td>
            <td width="222">&nbsp;</td>
            <td width="79">&nbsp;</td>
            <td width="205">&nbsp;</td>
            <td>&nbsp;</td>
            <td width="92">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td width="148">&nbsp;</td>
            <td width="222">&nbsp;</td>
            <td width="79">&nbsp;</td>
            <td width="205">&nbsp;</td>
            <td>&nbsp;</td>
            <td width="92">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td width="222">&nbsp;</td>
            <td width="79">&nbsp;</td>
            <td width="205">&nbsp;</td>
            <td>&nbsp;</td>
            <td>Depok, 30 Juli 2018</td>
            <td width="78">&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td width="222">&nbsp;</td>
            <td width="79">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td width="78">&nbsp;</td>
        </tr>
        <tr>
            <td>
                <table>
                    <tbody>
                        <tr>
                            <td width="29">&nbsp;</td>
                        </tr>
                    </tbody>
                </table>
            </td>
            <td>&nbsp;</td>
            <td>Koordinator&nbsp; UPMA&nbsp;</td>
            <td width="222">&nbsp;</td>
            <td width="79">&nbsp;</td>
            <td width="205">&nbsp;</td>
            <td colspan="3">Ketua Program Studi, {{$profildata->jenjang}}</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td width="148">&nbsp;</td>
            <td width="222">&nbsp;</td>
            <td width="79">&nbsp;</td>
            <td width="205">&nbsp;</td>
            <td>&nbsp;</td>
            <td>
                <table>
                    <tbody>
                        <tr>
                            <td width="92">&nbsp;</td>
                        </tr>
                    </tbody>
                </table>
            </td>
            <td width="78">&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td width="148">&nbsp;</td>
            <td width="222">&nbsp;</td>
            <td width="79">&nbsp;</td>
            <td width="205">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td width="78">&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td width="148">&nbsp;</td>
            <td width="222">&nbsp;</td>
            <td>&nbsp;</td>
            <td width="205">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td width="78">&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>Dr. Karmilasari</td>
            <td width="222">&nbsp;</td>
            <td>&nbsp;</td>
            <td width="205">&nbsp;</td>
            <td>&nbsp;</td>
            <td colspan="2">{{$profildata->identitas_nama_ketua_prod}}</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>NIDN. 0328117101</td>
            <td width="222">&nbsp;</td>
            <td width="79">&nbsp;</td>
            <td width="205">&nbsp;</td>
            <td>&nbsp;</td>
            <td colspan="2">NIDN: {{$profildata->identitas_nidn}}</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td width="148">&nbsp;</td>
            <td width="222">&nbsp;</td>
            <td width="79">&nbsp;</td>
            <td width="205">&nbsp;</td>
            <td>&nbsp;</td>
            <td width="92">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td width="148">&nbsp;</td>
            <td width="222">&nbsp;</td>
            <td>Mengetahui Dekan</td>
            <td width="205">&nbsp;</td>
            <td>&nbsp;</td>
            <td width="92">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td width="148">&nbsp;</td>
            <td colspan="3">Fakultas Teknologi Industri</td>
            <td>&nbsp;</td>
            <td width="92">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td width="148">&nbsp;</td>
            <td width="222">&nbsp;</td>
            <td width="79">&nbsp;</td>
            <td width="205">&nbsp;</td>
            <td>&nbsp;</td>
            <td width="92">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td width="148">&nbsp;</td>
            <td width="222">&nbsp;</td>
            <td width="79">&nbsp;</td>
            <td width="205">&nbsp;</td>
            <td>&nbsp;</td>
            <td width="92">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td width="148">&nbsp;</td>
            <td width="222">&nbsp;</td>
            <td>Prof. Dr. Bambang Suryawan</td>
            <td width="205">&nbsp;</td>
            <td>&nbsp;</td>
            <td width="92">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td width="148">&nbsp;</td>
            <td width="222">&nbsp;</td>
            <td>NIDN: 0022044502</td>
            <td width="205">&nbsp;</td>
            <td>&nbsp;</td>
            <td width="92">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </tbody>
</table>
<p>&nbsp;</p>
</body>
</html>