<!DOCTYPE html>
<html>
<head>
	<title>Rekap Analisis Standar Per-Standar</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<style>
      @page { size: 21cm 50cm landscape; }
</style>
<body>
<table width="1130" >
    <tbody>
        <tr>
            <td colspan="3" width="348"><b>Rekapitulasi dan Analisis Per-Standar</b></td>
            <td width="67">&nbsp;</td>
            <td width="83">&nbsp;</td>
            <td width="146">&nbsp;</td>
            <td width="146">&nbsp;</td>
            <td width="156">&nbsp;</td>
            <td width="184">&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>Nama Program Studi :&nbsp;</td>
            <td colspan="3">{{ $profildata->jenjang }}</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>Tahun Pengukuran Mutu:&nbsp;</td>
            <td>{{ $profildata->tahun_pengisian }}</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
    <table  class='table table-bordered'>
        <tr>
            <td>Rekap nilai</td>
            <td width="72">Bobot standar</td>
            <td width="75">Nilai Capaian&nbsp; terbobot per standar</td>
            <td width="67">Nilai Capaian per standar</td>
            <td>Sebutan</td>
            <td width="146">Temuan</td>
            <td width="146">Hasil Temuan Disebabkan&nbsp;</td>
            <td width="156">Hasil Temuan Mengakibatkan</td>
            <td width="184">Rekomendasi</td>
        </tr>
        @php
            $total_data = 0;
            $total_persen = 0;
        @endphp
        @foreach ($data_capaian as $key => $dc)
        @php
            $total_persen += $data_capaian[$key];
            $total_data++;
        @endphp


        <tr>
            <td width="201">{{ $data_standar[$key] }}</td>
            <td width="72">{{ $data_bobot[$key] }}</td>
            <td width="75">{{ $data_bobot[$key] * $data_capaian[$key] }}</td>
            <td width="67">{{ $data_capaian[$key] }}</td>
            <td width="83">@if ($data_capaian[$key] >= 86) 
                Sangat Baik
            @elseif ($data_capaian[$key] >= 72)
                Baik
            @elseif ($data_capaian[$key] >= 58)
                Lebih dari Cukup
            @elseif ($data_capaian[$key] >= 43)
                Cukup
            @elseif ($data_capaian[$key] >= 29)
                Perbaikan minor
            @elseif ($data_capaian[$key] >= 15)
                Perbaikan mayor
            @elseif ($data_capaian[$key] >= 0)
                Perbaikan menyeluruh dan mendesak
            @endif</td>
            <td width="146">{{ $data_rekap_temuan[$key] }}</td>
            <td width="146">{{ $data_rekap_sebab[$key] }}</td>
            <td width="156">{{ $data_rekap_akibat[$key] }}</td>
            <td width="184">{{ $data_rekap_rekomendasi[$key] }}</td>
        </tr>

        @endforeach

    </tbody>
</table>
<p>&nbsp;</p>
<table width="644" >
    <tbody>
        <tr>
            <td width="201">Konversi Angka Mutu ke Sebutan</td>
            <td width="72">&nbsp;</td>
            <td width="75">&nbsp;</td>
            <td width="67">0,00</td>
            <td width="83">14</td>
            <td width="146">&nbsp;Perbaikan menyeluruh dan&nbsp; mendesak&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>15,00</td>
            <td>28</td>
            <td>&nbsp;Perbaikan mayor&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>29,00</td>
            <td>42</td>
            <td>&nbsp;Perbaikan minor&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>43,00</td>
            <td>57</td>
            <td>&nbsp;Cukup&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>58,00</td>
            <td>71</td>
            <td>&nbsp;Lebih dari Cukup&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>72,00</td>
            <td>85</td>
            <td>&nbsp;Baik&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>86,00</td>
            <td>100</td>
            <td>&nbsp;Sangat baik&nbsp;</td>
        </tr>
    </tbody>
</table>
<p>&nbsp;</p>
<table width="946">
    <tbody>
        <tr>
            <td width="201">&nbsp;</td>
            <td width="72">&nbsp;</td>
            <td width="75">&nbsp;</td>
            <td width="67">&nbsp;</td>
            <td width="83">&nbsp;</td>
            <td width="146">&nbsp;</td>
            <td width="146">&nbsp;</td>
            <td width="156">&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <table>
                    <tbody>
                        <tr>
                            <td width="72">&nbsp;</td>
                        </tr>
                    </tbody>
                </table>
            </td>
            <td>&nbsp;</td>
            <td colspan="2">Koordinator&nbsp; UPMA&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>
                <table>
                    <tbody>
                        <tr>
                            <td width="156">Ketua Program Studi, {{$profildata->jenjang}}</td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td width="67">&nbsp;</td>
            <td width="83">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td width="67">&nbsp;</td>
            <td width="83">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td width="67">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td colspan="2">Dr. Karmilasari</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>Prof. Dr. Ing. Adang Suhendra</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td colspan="2">NIDN : 0328117101</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>NIDN: {{ $profildata->identitas_nidn }}</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>Mengetahui Dekan</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>Fakultas Teknologi Industri</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>Prof. Dr. Bambang Suryawan</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>NIDN: 0022044502</td>
            <td>&nbsp;</td>
        </tr>
    </tbody>
</table>
<p>&nbsp;</p>
</body>
</html>