<h4>{{$standar_dikti->nama_standar}}</h4>
<br><br>
<table>
    <thead>
    <tr>
        <th>#</th>
        <th>Poin Standar</th>
        <th>Isi Standar</th>
    </tr>
    </thead>
    <tbody>
    @php $angka = 0; @endphp
    @foreach($standar_awal as $st)
    @php $angka = $angka + 1; @endphp
        <tr>
            <td>{{ $angka }}</td>
            <td>{{ $st->s1 }}</td>
            <td>{{ $st->s2 }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
