<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Login to SPMI - Universitas Gunadarma</title>

<link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/bootstrap/dist/css/bootstrap.min.css') }}">
<script src="{{ asset('vendor/adminlte/vendor/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('vendor/adminlte/vendor/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<style type="text/css">
	.login-form {
		width: 340px;
    	margin: 50px auto;
	}
    .login-form form {
    	margin-bottom: 15px;
        background: #f7f7f7;
        box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
        padding: 30px;
    }
    .login-form h2 {
        margin: 0 0 15px;
    }
    .form-control, .btn {
        min-height: 38px;
        border-radius: 2px;
    }
    .btn {        
        font-size: 15px;
        font-weight: bold;
    }

    body {
        background: rgba(183,222,237,1);
    }
</style>
</head>
<body style="background-image: url('{{ asset('/assets/image/') }}/gunadarma-landscape.png');">
<div class="login-form">
    <form method="post">
    @csrf
        <h2 class="text-center"><img src="{{asset('/assets/image/logo-gundar-large.png')}}" width="100px" height="100px"></h2>  
        <h2 class="text-center"><i>S P M I UG</i></h2>  

        @if (Session::has('warning'))
        <br>
        <div class="alert alert-warning">
           
            {!! \Session::get('warning') !!}
            
        </div>
        @endif

        <div class="form-group">
            <input type="text" name="spmi_username" class="form-control" placeholder="Username" required="required">
        </div>
        <div class="form-group">
            <input name="spmi_password" type="password" class="form-control" placeholder="Password" required="required">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary btn-block">Log in</button>
        </div>
        <div class="clearfix">
           
        </div>        
    </form>
    
</div>
</body>
</html>                                		                            