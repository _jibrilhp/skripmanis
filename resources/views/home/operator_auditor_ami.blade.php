@php use \App\Http\Controllers\AuditorController; @endphp
@extends('adminlte::page')


@section('title', 'SPMI dan SPME')


@section('content_header')

    <h1>SPMI dan SPME</h1>

@stop


@section('content')


@if (Session::has('success'))
    <br>
        <div class="alert alert-success">
           
            {!! \Session::get('success') !!}
            
        </div>
@endif

@if (Session::has('pesan'))
    <br>
        <div class="alert alert-warning">
           
            {!! \Session::get('pesan') !!}
            
        </div>
@endif

    <div class='row'>
        <div class='col-md-12'>
            <!-- Box -->
            @if ($state == 'edit')
                <div class="box collapsed-box"> 
            @else
                <div class="box primary-box"> 
            @endif

            <!-- <div class="box collapsed-box"> -->
                <div class="box-header with-border">
                    <h3 class="box-title">Daftar AMI</h3> | <a href="" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah laporan audit mutu internal  </a>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                
                <div class="box-body">
                <!-- <hr style="display: block; height: 1px; border: 0; border-top: 1px solid #ccc; margin: 1em 0;padding: 0; "> -->
                    <div class="row">
                        <div class="col-md-12">
                        <table style="overflow-x:auto;" id="tabel_standar" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Auditee</th>
                                <th>Ketua Auditor</th>
                                <th>Hari, tanggal</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php $angka = 0; @endphp
                            @foreach ($auditborang as $key => $ab)
                            @php $angka = $angka + 1; @endphp
                            
                            <tr class="baris-diklik" data-href="{{route('audit_borang_edit',['audit_borang_id'=>$ab->id])}}">
                                
                                <td>{{$angka}}</td>
                                <td>
                                    {{AuditorController::auditor_audit_mutu_internal_getUserName($ab->auditee_user_id)->name }}
                                </td>
                                <td>{{AuditorController::auditor_audit_mutu_internal_getUserName($ab->ketua_user_id)->name }}
                                </td>
                                <td>{{$ab->tanggalan}}</td>
                                
                                <td>
                                    <a href="{{route('audit_borang_edit',['audit_borang_id'=>$ab->id])}}"><i class="fa fa-pencil-square-o"></i></a> | <a href="{{route('audit_borang_delete',['audit_borang_id'=>$ab->id])}}"><i class="fa fa-trash"></i></a>
                                </td>
                                <!-- <td><textarea></textarea></td> -->
                            </tr>
                            
                            @endforeach
                            
                            </tbody>
                            
                        </table>
                        </div>
                    </div>

                    
                
                </div><!-- /.box-body -->
            </div><!-- /.box -->

        </div><!-- /.col -->

        <div class='col-md-12'>
            <!-- Box -->
                    @if ($state == 'edit')
                        <div class="box collapsed-box"> 
                    @else
                        <div class="box primary-box"> 
                    @endif
                    <div class="box-header with-border">
                        <h3 class="box-title">Pendahuluan</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    
                    <div class="box-body">

                    <div class="row">
                        <div class="col-md-12">
                            <!-- <hr style="display: block; height: 1px; border: 0; border-top: 1px solid #ccc; margin: 1em 0;padding: 0; "> -->
                            <form class="form-horizontal" id="pendahuluan-form-0">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="inputFakultas3" class="col-sm-2 control-label">Fakultas / Direktorat / Biro</label>

                                        <div class="col-sm-10">
                                            <input type="text" name="op_fakultas_direktorat" class="form-control" id="inputFakultas3" placeholder="Masukkan nama fakultas, direktorat, atau biro" @if ($state == 'edit') value="{{$auditPendahuluan->fakultas_direktorat}}" @endif>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputProgStud3" class="col-sm-2 control-label">Program Studi / Unit Kerja</label>

                                        <div class="col-sm-10">
                                            <input type="text" name="op_program_unit" class="form-control" id="inputProgStud3" placeholder="Masukkan program studi atau unit kerja" @if ($state == 'edit') value="{{$auditPendahuluan->program_studi}}" @endif>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputAlamat3" class="col-sm-2 control-label">Alamat</label>

                                        <div class="col-sm-10">
                                            <input type="text" name="op_alamat_data" class="form-control" id="inputAlamat3" placeholder="Masukkan alamat "  @if ($state == 'edit') value="{{$auditPendahuluan->alamat}}" @endif>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputKetua3" class="col-sm-2 control-label">Ketua Program Studi / Unit Kerja</label>

                                        <div class="col-sm-10">
                                            <input type="text" name="op_ketua_unit" class="form-control" id="inputKetua3" placeholder="Masukkan nama dari ketua program studi / unit kerja"  @if ($state == 'edit') value="{{AuditorController::auditor_audit_mutu_internal_getUserName($auditPendahuluan->ketua_user_id)->username}}/{{AuditorController::auditor_audit_mutu_internal_getUserName($auditPendahuluan->ketua_user_id)->name}}" @endif>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputTelp3" class="col-sm-2 control-label">Telepon Program Studi / Unit Kerja</label>

                                        <div class="col-sm-10">
                                            <input type="text" name="op_telepon_prog_unit" class="form-control" id="inputTelp3" placeholder="Telepon data.." @if ($state == 'edit') value="{{$auditPendahuluan->telepon_program}}" @endif>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputTanggal3" class="col-sm-2 control-label">Tanggal Audit</label>

                                        <div class="col-sm-10">
                                            <input type="date" name="op_tanggalan_audit" class="form-control" id="inputTanggal3" @if ($state == 'edit') value="{{date("c", strtotime($auditPendahuluan->tanggalan))}}" @endif>
                                        </div>
                                    </div>

                                </div>
                                <!-- /.box-body -->
                              
                            </form>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <!-- <hr style="display: block; height: 1px; border: 0; border-top: 1px solid #ccc; margin: 1em 0;padding: 0; "> -->
                            <form id="pendahuluan-form-1" class="form-horizontal" method="post">
                            @csrf
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="inputKetuaAudit3" class="col-sm-2 control-label">Ketua Auditor</label>

                                        <div class="col-sm-10">
                                            <input type="text" name="op_ketua_auditor" class="form-control" id="inputKetuaAudit3" placeholder="Masukkan nama ketua auditor" @if ($state == 'edit') value="{{AuditorController::auditor_audit_mutu_internal_getUserName($auditPendahuluan->auditee_user_id)->username}}/{{AuditorController::auditor_audit_mutu_internal_getUserName($auditPendahuluan->auditee_user_id)->name}}" @endif>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputTelpBagianAudit3" class="col-sm-2 control-label">Telepon Audit Program Studi/ Unit Kerja</label>

                                        <div class="col-sm-10">
                                            <input type="text" name="op_telp_audit_prog_unit" class="form-control" id="inputTelpBagianAudit3" placeholder="Telepon dari audit.." @if ($state == 'edit') value="{{$auditPendahuluan->telepon_audit_program}}" @endif>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label for="inputBagianAudit3" class="col-sm-2 control-label">Bagian Program Studi / Unit Kerja</label>

                                        <div class="col-sm-10">
                                            <input type="text" name="op_audit_prog_unit" class="form-control" id="inputBagianAudit3" placeholder="Masukkan bagian yang diaudit" @if ($state == 'edit') value="{{$auditPendahuluan->audit_program_unit}}" @endif>
                                        </div>
                                    </div>

                                   
                                    

                                    <div class="form-group">
                                        <label for="inputAnggotaAudit3" class="col-sm-2 control-label">Anggota Auditor</label>

                                       
                                        <div class="col-sm-10">
                                        
                                            <table  id="tabel_anggota_auditor" class="table table-bordered display">
                                                <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Nama</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                
                                            </table>
                                            
                                        </div> <!-- /.col-sm-10 -->
                                        
                                    </div>

                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    <button type="submit" class="btn btn-info pull-right">Simpan</button>
                                </div>
                                <!-- /.box-footer -->
                            </form>
                            <!-- <button id="oke" class="btn btn-default">test..</button> -->
                        </div>
                    </div>
                  

                        
                    
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            
        </div><!-- /.col -->
@if ($state == 'edit')
        <!-- daftar pertanyaan -->
        <div class='col-md-12'>
            <!-- Box -->
                <div class="box primary-box"> 
                <!-- <div class="box collapsed-box"> -->
                    <div class="box-header with-border">
                        <h3 class="box-title">Daftar Pertanyaan</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    
                    <div class="box-body">

                    <div class="row">
                        <div class="col-md-12">
                            <!-- <hr style="display: block; height: 1px; border: 0; border-top: 1px solid #ccc; margin: 1em 0;padding: 0; "> -->
                            <form class="form-horizontal" id="pertanyaan-audit-form-0">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="inputReferensi3" class="col-sm-2 control-label">Referensi (Butir Mutu)</label>

                                        <div class="col-sm-10">
                                            <textarea name="op_pertanyaan_referensi" class="form-control" id="inputReferensi3" placeholder="Masukkan referensi (butir mutu) yang diinginkan"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputHasilObservasi3" class="col-sm-2 control-label">Hasil Observasi</label>

                                        <div class="col-sm-10">
                                            <textarea  name="op_pertanyaan_hasilobs" class="form-control" id="inputHasilObservasi3" ></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputHasilObsCeklist3" class="col-sm-2 control-label">Ceklis</label>
                                        

                                        <div class="col-sm-10">
                                            <input type="checkbox" name="op_pertanyaan_ya" id="inputHasilObsCeklist3_ya" value="ok"> Ya <br>
                                            <input type="checkbox" name="op_pertanyaan_tidak" id="inputHasilObsCeklist3_tidak" value="ok"> Tidak
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputCatKhusus3" class="col-sm-2 control-label">Catatan khusus</label>

                                        <div class="col-sm-10">
                                            <textarea  name="op_pertanyaan_catkhusus" class="form-control" id="inputCatKhusus3" ></textarea>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label for="submitHasilObservasi" class="col-sm-2 control-label"></label>

                                        <div class="col-sm-10">
                                            <button type="submit" name="op_submit_pertanyaan" id="submitHasilObservasi" class="btn btn-primary"> Simpan </button>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputIsianReferensi3" class="col-sm-2 control-label">Isian Pertanyaan</label>

                                       
                                        <div class="col-sm-10">
                                        
                                            <table style="overflow-x:auto;" id="tabel_pertanyaan_referensi" class="table table-bordered display">
                                                <thead>
                                                <tr>
                                                    <th>Referensi (butir mutu)</th>
                                                    <th>Hasil Observasi</th>
                                                    <th>Y</th>
                                                    <th>T</th>
                                                    <th>Catatan Khusus</th>
                                                    <th>Aksi</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                
                                            </table>
                                            
                                        </div> <!-- /.col-sm-10 -->
                                        
                                    </div>

                                    

                                </div>
                                <!-- /.box-body -->
                              
                            </form>
                        </div>
                    </div>

                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            
        </div><!-- /.col -->

        <!-- tujuan -->
        <div class='col-md-12'>
            <!-- Box -->
                <div class="box primary-box"> 
                <!-- <div class="box collapsed-box"> -->
                    <div class="box-header with-border">
                        <h3 class="box-title">Tujuan Audit</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    
                    <div class="box-body">

                    <div class="row">
                        <div class="col-md-12">
                            <!-- <hr style="display: block; height: 1px; border: 0; border-top: 1px solid #ccc; margin: 1em 0;padding: 0; "> -->
                            <form class="form-horizontal" id="tujuan-audit-form-0">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="inputTujuan3" class="col-sm-2 control-label">Tujuan</label>

                                        <div class="col-sm-10">
                                            <textarea name="op_tujuan" class="form-control" id="inputTujuan3" placeholder="Masukkan tujuan yang akan dicapai"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputCeklist3" class="col-sm-2 control-label">Ceklis</label>
                                        

                                        <div class="col-sm-10">
                                            <input type="checkbox" name="op_check" id="inputCeklist3" value="ok"> Berikan tanda ceklis sesuai yang dikerjakan
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="submitTujuan" class="col-sm-2 control-label"></label>

                                        <div class="col-sm-10">
                                            <button type="submit" name="op_submit_tujuan" id="submitTujuan" class="btn btn-primary"> Simpan </button>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputAnggotaAudit3" class="col-sm-2 control-label">Isian Tujuan</label>

                                       
                                        <div class="col-sm-10">
                                        
                                            <table  id="tabel_tujuan_auditor" class="table table-bordered display">
                                                <thead>
                                                <tr>
                                                   
                                                    <th>Tujuan</th>
                                                    <th>Cek</th>
                                                    <th>Aksi</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                
                                            </table>
                                            
                                        </div> <!-- /.col-sm-10 -->
                                        
                                    </div>

                                    

                                </div>
                                <!-- /.box-body -->
                              
                            </form>
                        </div>
                    </div>

                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            
        </div><!-- /.col -->
        
        <!-- lingkup audit -->
        <div class='col-md-12'>
            <!-- Box -->
                <div class="box primary-box"> 
                <!-- <div class="box collapsed-box"> -->
                    <div class="box-header with-border">
                        <h3 class="box-title">Lingkup Audit</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    
                    <div class="box-body">

                    <div class="row">
                        <div class="col-md-12">
                            <!-- <hr style="display: block; height: 1px; border: 0; border-top: 1px solid #ccc; margin: 1em 0;padding: 0; "> -->
                            <form class="form-horizontal" id="lingkup-audit-form-0">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="inputLingkup3" class="col-sm-2 control-label">Lingkup Audit</label>

                                        <div class="col-sm-10">
                                            <textarea name="op_lingkup_audit" class="form-control" id="inputLingkup3" placeholder="Masukkan lingkup audit yang ingin dicapai"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="submitLingkup" class="col-sm-2 control-label"></label>

                                        <div class="col-sm-10">
                                            <button type="submit" name="op_submit_lingkup" id="submitLingkup" class="btn btn-primary"> Simpan </button>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputAnggotaAudit3" class="col-sm-2 control-label">Isian Lingkup</label>

                                       
                                        <div class="col-sm-10">
                                        
                                            <table  id="tabel_lingkup_auditor" class="table table-bordered display">
                                                <thead>
                                                <tr>
                                                   
                                                    <th>#</th>
                                                    <th>Lingkup</th>
                                                    <th>Aksi</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                
                                            </table>
                                            
                                        </div> <!-- /.col-sm-10 -->
                                        
                                    </div>

                                </div>
                                <!-- /.box-body -->
                              
                            </form>
                        </div>
                    </div>

                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            
        </div><!-- /.col -->

        <!-- Jadwal audit-->
        <div class='col-md-12'>
            <!-- Box -->
                <div class="box primary-box"> 
                <!-- <div class="box collapsed-box"> -->
                    <div class="box-header with-border">
                        <h3 class="box-title">Jadwal Audit</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    
                    <div class="box-body">

                    <div class="row">
                        <div class="col-md-12">
                            <!-- <hr style="display: block; height: 1px; border: 0; border-top: 1px solid #ccc; margin: 1em 0;padding: 0; "> -->
                            <form class="form-horizontal" id="jadwal-audit-form-0">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="inputPukulAudit3" class="col-sm-2 control-label">Pukul</label>

                                        <div class="col-sm-10">
                                            <textarea name="op_pukul_audit" class="form-control" id="inputPukulAudit3" placeholder="Masukkan pukul audit sesuai jadwal"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputKegiatanAudit3" class="col-sm-2 control-label">Kegiatan Audit</label>

                                        <div class="col-sm-10">
                                            <textarea name="op_kegiatan_audit" class="form-control" id="inputKegiatanAudit3" placeholder="Masukkan kegiatan sesuai jadwal tersebut"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="submitKegiatan" class="col-sm-2 control-label"></label>

                                        <div class="col-sm-10">
                                            <button type="submit" name="op_submit_kegiatan" id="submitKegiatan" class="btn btn-primary"> Simpan </button>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputAnggotaAudit3" class="col-sm-2 control-label">Tabel Jadwal Audit</label>

                                       
                                        <div class="col-sm-10">
                                        
                                            <table  id="tabel_kegiatan_auditor" class="table table-bordered display">
                                                <thead>
                                                <tr>
                                                   
                                                    <th>#</th>
                                                    <th>Pukul</th>
                                                    <th>Kegiatan Audit</th>
                                                    <th>Aksi</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                
                                            </table>
                                            
                                        </div> <!-- /.col-sm-10 -->
                                        
                                    </div>

                                </div>
                                <!-- /.box-body -->
                              
                            </form>
                        </div>
                    </div>

                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            
        </div><!-- /.col -->

        <!-- Temuan audit -->
        <div class='col-md-12'>
            <!-- Box -->
                <div class="box primary-box"> 
                <!-- <div class="box collapsed-box"> -->
                    <div class="box-header with-border">
                        <h3 class="box-title">Temuan Audit</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    
                    <div class="box-body">

                    <div class="row">
                        <div class="col-md-12">
                            <!-- <hr style="display: block; height: 1px; border: 0; border-top: 1px solid #ccc; margin: 1em 0;padding: 0; "> -->
                           
                                <div class="box-body">
                                <form class="form-horizontal" id="temuan-audit-form-0">
                                    <h5>1. Ketidaksesuaian</h5>
                                    <div class="form-group">
                                        <label for="inputInisialAudit3" class="col-sm-2 control-label">KTS/OB (Inisial Auditor)</label>

                                        <div class="col-sm-10">
                                            <input type="text" name="op_inisial_audit" class="form-control" id="inputInisialAudit3" placeholder="Masukkan inisial auditor">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputReferensiTemuanAudit3" class="col-sm-2 control-label">Temuan Audit</label>

                                        <div class="col-sm-10">
                                            <textarea name="op_referensi_mutu_temuan" class="form-control" id="inputReferensiTemuanAudit3" placeholder="Masukkan temuan audit"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputPeryataanTemuanAudit3" class="col-sm-2 control-label">Pernyataan Audit</label>

                                        <div class="col-sm-10">
                                            <textarea name="op_pernyataan_mutu_temuan" class="form-control" id="inputPeryataanTemuanAudit3" placeholder="Masukkan pernyataan audit"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="submitTemuan" class="col-sm-2 control-label"></label>

                                        <div class="col-sm-10">
                                            <button type="submit" name="op_submit_temuan" id="submitTemuan" class="btn btn-primary"> Simpan </button>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputKetidaksesuaianAudit3" class="col-sm-2 control-label">Tabel Ketidaksesuaian</label>

                                       
                                        <div class="col-sm-10">
                                        
                                            <table  id="tabel_ketidaksesuaian_auditor" class="table table-bordered display">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>KTS/OB</th>
                                                    <th>Referensi (butir mutu)</th>
                                                    <th>Pernyataan</th>
                                                    <th>Aksi</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                
                                            </table>
                                            
                                        </div> <!-- /.col-sm-10 -->
                                        
                                    </div>
                                    </form>
                                    <form class="form-horizontal" id="saran-audit-form-0">
                                    <h5>2. Saran Perbaikan</h5>

                                    <div class="form-group">
                                        <label for="inputSaranPerbaikanAudit3" class="col-sm-2 control-label">Bidang</label>

                                        <div class="col-sm-10">
                                            <textarea name="op_perbaikan_bidang_audit" class="form-control" id="inputSaranPerbaikanAudit3" placeholder="Masukkan bidang saran perbaikan"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputKelebihanPerbaikanAudit3" class="col-sm-2 control-label">Kelebihan</label>

                                        <div class="col-sm-10">
                                            <textarea name="op_perbaikan_kelebihan_audit" class="form-control" id="inputKelebihanPerbaikanAudit3" placeholder="Masukkan kelebihan saran perbaikan audit"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputPeluangAudit3" class="col-sm-2 control-label">Peluang Peningkatan</label>

                                        <div class="col-sm-10">
                                            <textarea name="op_perbaikan_peluang_audit" class="form-control" id="inputPeluangAudit3" placeholder="Masukkan temuan audit"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="submitPerbaikan" class="col-sm-2 control-label"></label>

                                        <div class="col-sm-10">
                                            <button type="submit" name="op_submit_perbaikan" id="submitPerbaikan" class="btn btn-primary"> Simpan </button>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputTabelPerbaikanAudit3" class="col-sm-2 control-label">Tabel Saran Perbaikan</label>

                                       
                                        <div class="col-sm-10">
                                        
                                            <table  id="tabel_perbaikan_auditor" class="table table-bordered display">
                                                <thead>
                                                <tr>
                                                   
                                                    <th>No</th>
                                                    <th>Bidang</th>
                                                    <th>Kelebihan</th>
                                                    <th>Peluang Peningkatan</th>
                                                    <th>Aksi</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                
                                            </table>
                                            
                                        </div> <!-- /.col-sm-10 -->
                                        
                                    </div>
                                </form>
                                </div>
                                <!-- /.box-body -->
                              
                            
                        </div>
                    </div>

                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            
        </div><!-- /.col -->

        <!-- kesimpulan audit -->
        <div class='col-md-12'>
            <!-- Box -->
                <div class="box primary-box"> 
                <!-- <div class="box collapsed-box"> -->
                    <div class="box-header with-border">
                        <h3 class="box-title">Kesimpulan</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    
                    <div class="box-body">

                    <div class="row">
                        <div class="col-md-12">
                            <!-- <hr style="display: block; height: 1px; border: 0; border-top: 1px solid #ccc; margin: 1em 0;padding: 0; "> -->
                            <form class="form-horizontal" id="kesimpulan-audit-form-0">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="inputPoinKesimpulanAudit3" class="col-sm-2 control-label">Poin Kesimpulan</label>

                                        <div class="col-sm-10">
                                            <textarea name="op_kesimpulan_audit" class="form-control" id="inputPoinKesimpulanAudit3" placeholder="Masukkan bulir kesimpulan audit"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputYesNoOtherAudit3" class="col-sm-2 control-label">Pilihan Ya, Tidak, Lainnya</label>

                                        <div class="col-sm-10">
                                            <select name="pilihan_kesimpulan" id="pilihan-kesimpulan-id" class="form-control">
                                                <option value="1">Ya</option>
                                                <option value="2">Tidak</option>
                                                <option value="3">Lainnya</option>
                                            </select>
                                            <hr>

                                            <textarea name="op_kesimpulan_lainnya" class="form-control" id="inputYesNoOtherAudit3" placeholder="Jika memilih lainnya, silahkan tuliskan disini"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="submitKegiatan" class="col-sm-2 control-label"></label>

                                        <div class="col-sm-10">
                                            <button type="submit" name="op_submit_kegiatan" id="submitKegiatan" class="btn btn-primary"> Simpan </button>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputAnggotaAudit3" class="col-sm-2 control-label">Tabel Jadwal Audit</label>

                                       
                                        <div class="col-sm-10">
                                        
                                            <table  id="tabel_kesimpulan_auditor" class="table table-bordered display">
                                                <thead>
                                                <tr>
                                                   
                                                    <th>#</th>
                                                    <th>Poin Kesimpulan</th>
                                                    <th>Pilihan</th>
                                                    <th>Lainnya</th>
                                                    <th>Aksi</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                
                                            </table>
                                            
                                        </div> <!-- /.col-sm-10 -->
                                        
                                    </div>

                                </div>
                                <!-- /.box-body -->
                              
                            </form>
                        </div>
                    </div>

                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            
        </div><!-- /.col -->





@endif
    </div><!-- /.row -->

        <!-- modal form kesimpulan -->
        <div class="modal" id="modal_edit_kesimpulan">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title">Mengubah data kesimpulan</h4>
                    </div>
                    <form method="post" id="kesimpulan-edit-form-0">
                        <div class="modal-body">
                            <input type="hidden" id="kesimpulan_edit_id_ref" name="kesimpulan_edit_id_value">

                            <p>Poin Kesimpulan</p>
                            <textarea id="poin_kesimpulan_edit_ref" name="poin_kesimpulan_edit_value" class="form-control"></textarea>

                            <hr>

                            <p>Pilihan</p>
                            <select id="pilihan_edit_ref" name="pilihan_edit_value" class="form-control">
                                <option value="1"> Ya </option>
                                <option value="2"> Tidak </option>
                                <option value="3"> Lainnya </option>
                            </select>

                            <hr>
                            <p> Jika memilih lainnya, silahkan isi..</p>
                            <textarea id="lainnya_edit_ref" name="lainnya_edit_value" class="form-control"></textarea>

                        </div>
                    
                    <div class="modal-footer">
                        
                        <button type="submit" class="btn btn-primary" >Simpan</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>

    <!-- modal form saran perbaikan -->
    <div class="modal" id="modal_edit_saran_perbaikan">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title">Mengubah data Saran Perbaikan</h4>
                    </div>
                    <form method="post" id="saran-edit-form-0">
                        <div class="modal-body">
                            <input type="hidden" id="saran_edit_id_ref" name="saran_edit_id_value">

                            <p>Bidang</p>
                            <textarea id="bidang_edit_ref" name="bidang_edit_value" class="form-control"></textarea>

                            <hr>

                            <p>Kelebihan</p>
                            <textarea id="kelebihan_edit_ref" name="kelebihan_edit_value" class="form-control"></textarea>

                            <hr>

                            <p>Peluang Peningkatan</p>
                            <textarea id="peluang_edit_ref" name="peluang_edit_value" class="form-control"></textarea>

 
                        </div>
                    
                    <div class="modal-footer">
                        
                        <button type="submit" class="btn btn-primary" >Simpan</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>

    <!-- modal form ketidaksesuaian -->
        <div class="modal" id="modal_edit_ketidaksesuaian">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title">Mengubah data Ketidaksesuaian</h4>
                    </div>
                    <form method="post" id="ketidaksesuaian-edit-form-0">
                        <div class="modal-body">
                            <input type="hidden" id="ketidaksesuaian_edit_id_ref" name="ketidaksesuaian_edit_id_value">

                            <p>KTS/OB (Inisial Auditor)</p>
                            <input type="text" class="form-control" id="kts_ob_edit_ref" name="kts_ob_edit_value">

                            <hr>

                            <p>Temuan Audit</p>
                            <textarea id="temuan_edit_ref" name="temuan_edit_value" class="form-control"></textarea>

                            <hr>

                            <p>Peryataan Audit</p>
                            <textarea id="pernyataan_edit_ref" name="pernyataan_edit_value" class="form-control"></textarea>

 
                        </div>
                    
                    <div class="modal-footer">
                        
                        <button type="submit" class="btn btn-primary" >Simpan</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>

     <!-- modal form jadwal audit -->
        <div class="modal" id="modal_edit_audit">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title">Mengubah data Jadwal Audit</h4>
                    </div>
                    <form method="post" id="jadwal-edit-form-0">
                        <div class="modal-body">
                            <input type="hidden" id="jadwal_edit_id_ref" name="jadwal_edit_id_value">

                            <p>Pukul</p>
                            <input type="text" class="form-control" id="pukul_edit_ref" name="pukul_edit_value">

                            <hr>

                            <p>Kegiatan Audit</p>
                            <textarea id="kegiatan_edit_ref" name="kegiatan_edit_value" class="form-control"></textarea>
 
                        </div>
                    
                    <div class="modal-footer">
                        
                        <button type="submit" class="btn btn-primary" >Simpan</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>


     <!-- modal form edit lingkup -->
        <div class="modal" id="modal_edit_lingkup">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title">Mengubah data lingkup</h4>
                    </div>
                    <form method="post" id="lingkup-edit-form-0">
                        <div class="modal-body">
                            <input type="hidden" id="lingkup_edit_id_ref" name="lingkup_edit_id_value">
                            <p>Lingkup Audit</p>
                            <textarea id="lingkup_edit_ref" name="lingkup_edit_value" class="form-control"></textarea>
 
                        </div>
                    
                    <div class="modal-footer">
                        
                        <button type="submit" class="btn btn-primary" >Simpan</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>



    <!-- modal form edit pertanyaan -->
        <div class="modal" id="modal_edit_pertanyaan">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title">Mengubah data pertanyaan</h4>
                    </div>
                    <form method="post" id="pertanyaan-edit-form-0">
                        <div class="modal-body">
                            <input type="hidden" id="pertanyaan_edit_idx" name="pertanyaan_edit_ids">
                            <p>Referensi (butir mutu)</p>
                            <textarea id="pertanyaan_edit_ref" name="pertanyaan_edit_butir_mutu" class="form-control"></textarea>

                            <hr>
                            <p>Hasil observasi</p>
                            <textarea id="pertanyaan_edit_hasilobs" name="pertanyaan_edit_hasilobs" class="form-control"></textarea>

                            <hr>

                            <p>Ceklis</p>
                            <input type="checkbox" name="pertanyaan_edit_cek_ya" id="pertanyaan_edit_ceklist_ya" value="1" > Ya
                            <input type="checkbox" name="pertanyaan_edit_cek_tidak" id="pertanyaan_edit_ceklist_tidak" value="2" > Tidak

                            <hr>

                            <p>Catatan khusus</p>
                            <textarea id="pertanyaan_edit_catatan" name="pertanyaan_edit_catatan_khusus" class="form-control"></textarea>

                            
                        </div>
                    
                    <div class="modal-footer">
                        
                        <button type="submit" class="btn btn-primary" >Simpan</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>


    <!-- modal form data -->

        <div class="modal" id="modal_edit_tujuan">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title">Mengubah data tujuan</h4>
                    </div>
                    <form method="post" id="tujuan-edit-form-0">
                        <div class="modal-body">
                            <input type="hidden" id="tujuan_edit_idx" name="tujuan_edit_ids">
                            <p>Isi tujuan</p>
                            <textarea type="text" id="tujuan_edit_id" name="tujuan_edit_isian" class="form-control"></textarea>

                            <hr>

                            <p>Ceklis</p>
                            <input type="checkbox" name="tujuan_edit_cek" id="tujuan_edit_ceklist" > Berikan tanda ceklis sesuai yang dikerjakan
                            
                        </div>
                    
                    <div class="modal-footer">
                        
                        <button type="submit" class="btn btn-primary" >Simpan</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>




@stop


@section('css')

    <!-- <link rel="stylesheet" href="/css/admin_custom.css"> -->

@stop


@section('js')



<script>

@if ($state == 'edit')
    //kesimpulan
    function hapus_kesimpulan(kesimpulan_id)
    {
        var tabel_kesimpulan = $('#tabel_kesimpulan_auditor').DataTable();
        bootbox.confirm({
            title   : '<i class="fa fa-exclamation-triangle"></i> Konfirmasi',
            message : "Apakah anda ingin menghapus poin tujuan ini?",
            buttons : {
                confirm : {
                    label : 'Ya',
                    className: 'btn-primary'
                },
                cancel : {
                    label : 'Tidak',
                    className : 'btn-danger'
                }
            },
            callback : function (result) {
                if (result)
                {
                    $.ajax({
                        type: "POST",
                        url: "{{route('audit_borang_kesimpulan_delete',['audit_borang_id'=>$audit_borang_id])}}",   
                        data: {"kesimpulan_id":kesimpulan_id},
                        success: function (result) {
                            tabel_kesimpulan.ajax.reload();
                        }
                    });
                }
            }
        });
    }

    $(document).ready(function() {

        var tabel_kesimpulan = $('#tabel_kesimpulan_auditor').DataTable({
            "bProcessing": true,
            "bServerSide": true,
            "ajax": {
                "url": "{{route('audit_borang_kesimpulan_view',['audit_borang_id'=>$audit_borang_id])}}",
                "type": "POST"    
            },
            "columns": [
                {   "render" : function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1; }
                },
                { "data": "kesimpulan" },
                { 
                    "mData": "0", 
                    "mRender": function ( data, type, full ) {
                        if (full.yes_no_other_check == '1') {
                            return 'Ya';
                        } else if (full.yes_no_other_check == '2') {
                            return 'Tidak';
                        } else {
                            return 'Lainnya';
                        }
                    }
                },
                { "data": "lainnya" },
                { 
                    "mData": "0", 
                    "mRender": function ( data, type, full ) {
                        return '<a href="#modal_edit_kesimpulan" data-toggle="modal" data-edit-kesimpulan="'+ full.kesimpulan +'" data-edit-ya-tidak="'+ full.yes_no_other_check +'" data-edit-lainnya="'+ full.lainnya +'" data-edit-kesimpulanid="'+ full.id +'"><i class="fa fa-edit"></i></a> | <a class="delete-jadwal-data" onclick="hapus_kesimpulan(\''+ full.id + '\')"  data-delete-id="'+ full.id +'"><i class="fa fa-trash" aria-hidden="true"></i></a>';
                    }
                }
            ]

        });

        $('#kesimpulan-audit-form-0').submit(function(e) {
            e.preventDefault();
            var data_kesimpulan_audit_form = $('#kesimpulan-audit-form-0').serializeArray();

            $.ajax({
                type: "POST",
                url: "{{route('audit_borang_kesimpulan_create',['audit_borang_id'=>$audit_borang_id])}}",   
                data: data_kesimpulan_audit_form,
                success: function (result) {
                    $('#inputPoinKesimpulanAudit3').val('');
                    $('#inputYesNoOtherAudit3').val('');
                  
                    tabel_kesimpulan.ajax.reload();
                }
            });

        });

        $('#kesimpulan-edit-form-0').submit(function(e) {
            e.preventDefault();

            var data_form_kesimpulan = $('#kesimpulan-edit-form-0').serializeArray();

            $.ajax({
                type: "POST",
                url: "{{route('audit_borang_kesimpulan_edit',['audit_borang_id'=>$audit_borang_id])}}",   
                data: data_form_kesimpulan,
                success: function (result) {
                    $('#modal_edit_kesimpulan').modal('toggle'); 
                    tabel_kesimpulan.ajax.reload();
                }
            });

        });

        $('#modal_edit_kesimpulan').on('show.bs.modal', function(e) {
            
            var kesimpulan_edit_id_ref = $(e.relatedTarget).data('edit-kesimpulanid');
            var kesimpulan_edit_value =  $(e.relatedTarget).data('edit-kesimpulan');
            var pilihan_edit_value =  $(e.relatedTarget).data('edit-ya-tidak');
            var lainnya_edit_value =  $(e.relatedTarget).data('edit-lainnya');

            $(e.currentTarget).find('#kesimpulan_edit_id_ref').val(kesimpulan_edit_id_ref);
            $(e.currentTarget).find('#poin_kesimpulan_edit_ref').val(kesimpulan_edit_value);
            $(e.currentTarget).find('#pilihan_edit_ref').val(pilihan_edit_value);
            $(e.currentTarget).find('#lainnya_edit_ref').val(lainnya_edit_value);
           
        });

    });

    //temuan audit saran dan perbaikan
    function hapus_saran_perbaikan(saran_id)
    {
        var tabel_saran_perbaikan = $('#tabel_perbaikan_auditor').DataTable();
        bootbox.confirm({
            title   : '<i class="fa fa-exclamation-triangle"></i> Konfirmasi',
            message : "Apakah anda ingin menghapus poin tujuan ini?",
            buttons : {
                confirm : {
                    label : 'Ya',
                    className: 'btn-primary'
                },
                cancel : {
                    label : 'Tidak',
                    className : 'btn-danger'
                }
            },
            callback : function (result) {
                if (result)
                {
                    //  simulate click the link
                    $.ajax({
                        type: "POST",
                        url: "{{route('audit_borang_saran_delete',['audit_borang_id'=>$audit_borang_id])}}",   
                        data: {"saran_id":saran_id},
                        success: function (result) {
                            tabel_saran_perbaikan.ajax.reload();
                        }
                    });
                }
            }
        });
    }

    $(document).ready(function() {

        var tabel_saran_perbaikan = $('#tabel_perbaikan_auditor').DataTable({
            "bProcessing": true,
            "bServerSide": true,
            "ajax": {
                "url": "{{route('audit_borang_saran_view',['audit_borang_id'=>$audit_borang_id])}}",
                "type": "POST"    
            },
            "columns": [
                {   "render" : function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1; }
                },
                { "data": "bidang" },
                { "data": "kelebihan" },
                { "data": "peluang_peningkatan" },
                { 
                    "mData": "0", 
                    "mRender": function ( data, type, full ) {
                        return '<a href="#modal_edit_saran_perbaikan" data-toggle="modal" data-edit-bidang="'+ full.bidang +'" data-edit-kelebihan="'+ full.kelebihan +'" data-edit-peluang-peningkatan="'+ full.peluang_peningkatan +'" data-edit-saranid="'+ full.id +'"><i class="fa fa-edit"></i></a> | <a class="delete-jadwal-data" onclick="hapus_saran_perbaikan(\''+ full.id + '\')"  data-delete-id="'+ full.id +'"><i class="fa fa-trash" aria-hidden="true"></i></a>';
                    }
                }
            ]

        });

        $('#saran-audit-form-0').submit(function(e) {
            e.preventDefault();
            var data_saran_audit_form = $('#saran-audit-form-0').serializeArray();

            $.ajax({
                type: "POST",
                url: "{{route('audit_borang_saran_create',['audit_borang_id'=>$audit_borang_id])}}",   
                data: data_saran_audit_form,
                success: function (result) {
                    $('#inputSaranPerbaikanAudit3').val('');
                    $('#inputKelebihanPerbaikanAudit3').val('');
                    $('#inputPeluangAudit3').val('');
                  
                    tabel_saran_perbaikan.ajax.reload();
                }
            });

        });

        $('#saran-edit-form-0').submit(function(e) {
            e.preventDefault();

            var data_form_saran = $('#saran-edit-form-0').serializeArray();

            $.ajax({
                type: "POST",
                url: "{{route('audit_borang_saran_edit',['audit_borang_id'=>$audit_borang_id])}}",   
                data: data_form_saran,
                success: function (result) {
                    $('#modal_edit_saran_perbaikan').modal('toggle'); 
                    tabel_saran_perbaikan.ajax.reload();
                }
            });

        });

        $('#modal_edit_saran_perbaikan').on('show.bs.modal', function(e) {
            
            var saran_id_edit_value = $(e.relatedTarget).data('edit-saranid');
            var bidang_edit_value =  $(e.relatedTarget).data('edit-bidang');
            var kelebihan_edit_value =  $(e.relatedTarget).data('edit-kelebihan');
            var peluang_edit_value =  $(e.relatedTarget).data('edit-peluang-peningkatan');

            $(e.currentTarget).find('#saran_edit_id_ref').val(saran_id_edit_value);
            $(e.currentTarget).find('#bidang_edit_ref').val(bidang_edit_value);
            $(e.currentTarget).find('#kelebihan_edit_ref').val(kelebihan_edit_value);
            $(e.currentTarget).find('#peluang_edit_ref').val(peluang_edit_value);
           
        });

    });

    //temuan audit ketidaksesuaian
    function hapus_ketidaksesuaian(temuan_id)
    {
        var tabel_ketidaksesuaian = $('#tabel_ketidaksesuaian_auditor').DataTable();
        bootbox.confirm({
            title   : '<i class="fa fa-exclamation-triangle"></i> Konfirmasi',
            message : "Apakah anda ingin menghapus poin tujuan ini?",
            buttons : {
                confirm : {
                    label : 'Ya',
                    className: 'btn-primary'
                },
                cancel : {
                    label : 'Tidak',
                    className : 'btn-danger'
                }
            },
            callback : function (result) {
                if (result)
                {
                    //  simulate click the link
                    $.ajax({
                        type: "POST",
                        url: "{{route('audit_borang_temuan_delete',['audit_borang_id'=>$audit_borang_id])}}",   
                        data: {"temuan_id":temuan_id},
                        success: function (result) {
                            tabel_ketidaksesuaian.ajax.reload();
                        }
                    });
                }
            }
        });
    }

    $(document).ready(function() {

        var tabel_ketidaksesuaian = $('#tabel_ketidaksesuaian_auditor').DataTable({
            "bProcessing": true,
            "bServerSide": true,
            "ajax": {
                "url": "{{route('audit_borang_temuan_view',['audit_borang_id'=>$audit_borang_id])}}",
                "type": "POST"    
            },
            "columns": [
                {   "render" : function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1; }
                },
                { "data": "kts_ob" },
                { "data": "referensi_butir_mutu" },
                { "data": "pernyataan" },
                { 
                    "mData": "0", 
                    "mRender": function ( data, type, full ) {
                        return '<a href="#modal_edit_ketidaksesuaian" data-toggle="modal" data-edit-ktsob="'+ full.kts_ob +'" data-edit-temuan="'+ full.referensi_butir_mutu +'" data-edit-pernyataan="'+ full.pernyataan +'" data-edit-temuanid="'+ full.id +'"><i class="fa fa-edit"></i></a> | <a class="delete-jadwal-data" onclick="hapus_ketidaksesuaian(\''+ full.id + '\')"  data-delete-id="'+ full.id +'"><i class="fa fa-trash" aria-hidden="true"></i></a>';
                    }
                }
            ]

        });

        $('#temuan-audit-form-0').submit(function(e) {
            e.preventDefault();
            var data_temuan_audit_form = $('#temuan-audit-form-0').serializeArray();

            $.ajax({
                type: "POST",
                url: "{{route('audit_borang_temuan_create',['audit_borang_id'=>$audit_borang_id])}}",   
                data: data_temuan_audit_form,
                success: function (result) {
                    $('#inputInisialAudit3').val('');
                    $('#inputReferensiTemuanAudit3').val('');
                    $('#inputPeryataanTemuanAudit3').val('');
                  
                    tabel_ketidaksesuaian.ajax.reload();
                }
            });

        });

        $('#ketidaksesuaian-edit-form-0').submit(function(e) {
            e.preventDefault();

            var data_form_ketidaksesuaian = $('#ketidaksesuaian-edit-form-0').serializeArray();

            $.ajax({
                type: "POST",
                url: "{{route('audit_borang_temuan_edit',['audit_borang_id'=>$audit_borang_id])}}",   
                data: data_form_ketidaksesuaian,
                success: function (result) {
                    $('#modal_edit_ketidaksesuaian').modal('toggle'); 
                    tabel_ketidaksesuaian.ajax.reload();
                }
            });

        });

        $('#modal_edit_ketidaksesuaian').on('show.bs.modal', function(e) {
            
            var temuan_id_edit_value = $(e.relatedTarget).data('edit-temuanid');
            var ktsob_edit_value =  $(e.relatedTarget).data('edit-ktsob');
            var temuan_edit_value =  $(e.relatedTarget).data('edit-temuan');
            var pernyataan_edit_value =  $(e.relatedTarget).data('edit-pernyataan');

            $(e.currentTarget).find('#ketidaksesuaian_edit_id_ref').val(temuan_id_edit_value);
            $(e.currentTarget).find('#kts_ob_edit_ref').val(ktsob_edit_value);
            $(e.currentTarget).find('#temuan_edit_ref').val(temuan_edit_value);
            $(e.currentTarget).find('#pernyataan_edit_ref').val(pernyataan_edit_value);
           
        });

    });


    //untuk jadwal audit
    function hapus_jadwal(jadwal_id)
    {
        var tabel_jadwal = $('#tabel_kegiatan_auditor').DataTable();
        bootbox.confirm({
            title   : '<i class="fa fa-exclamation-triangle"></i> Konfirmasi',
            message : "Apakah anda ingin menghapus poin tujuan ini?",
            buttons : {
                confirm : {
                    label : 'Ya',
                    className: 'btn-primary'
                },
                cancel : {
                    label : 'Tidak',
                    className : 'btn-danger'
                }
            },
            callback : function (result) {
                if (result)
                {
                    //  simulate click the link
                    $.ajax({
                        type: "POST",
                        url: "{{route('audit_borang_jadwal_delete',['audit_borang_id'=>$audit_borang_id])}}",   
                        data: {"jadwal_id":jadwal_id},
                        success: function (result) {
                            tabel_jadwal.ajax.reload();
                        }
                    });
                }
            }
        });
    }

    $(document).ready(function() {

        var tabel_jadwal = $('#tabel_kegiatan_auditor').DataTable({
            "bProcessing": true,
            "bServerSide": true,
            "ajax": {
                "url": "{{route('audit_borang_jadwal_view',['audit_borang_id'=>$audit_borang_id])}}",
                "type": "POST"    
            },
            "columns": [
                {   "render" : function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1; }
                },
                { "data": "pukul" },
                { "data": "kegiatan_audit" },
                { 
                    "mData": "0", 
                    "mRender": function ( data, type, full ) {
                        return '<a href="#modal_edit_audit" data-toggle="modal" data-edit-pukul="'+ full.pukul +'" data-edit-kegiatan="'+ full.kegiatan_audit +'" data-edit-jadwalid="'+ full.id +'"><i class="fa fa-edit"></i></a> | <a class="delete-jadwal-data" onclick="hapus_jadwal(\''+ full.id + '\')"  data-delete-id="'+ full.id +'"><i class="fa fa-trash" aria-hidden="true"></i></a>';
                    }
                }
            ]

        });

        $('#jadwal-audit-form-0').submit(function(e) {
            e.preventDefault();
            var data_jadwal_audit_form = $('#jadwal-audit-form-0').serializeArray();

            $.ajax({
                type: "POST",
                url: "{{route('audit_borang_jadwal_create',['audit_borang_id'=>$audit_borang_id])}}",   
                data: data_jadwal_audit_form,
                success: function (result) {
                    $('#inputPukulAudit3').val('');
                    $('#inputKegiatanAudit3').val('');
                  
                    tabel_jadwal.ajax.reload();
                }
            });

        });

        $('#jadwal-edit-form-0').submit(function(e) {
            e.preventDefault();

            var data_form_jadwal = $('#jadwal-edit-form-0').serializeArray();

            $.ajax({
                type: "POST",
                url: "{{route('audit_borang_jadwal_edit',['audit_borang_id'=>$audit_borang_id])}}",   
                data: data_form_jadwal,
                success: function (result) {
                    $('#modal_edit_audit').modal('toggle'); 
                    tabel_jadwal.ajax.reload();
                }
            });

        });

        $('#modal_edit_audit').on('show.bs.modal', function(e) {
            
            var jadwal_id_edit_value = $(e.relatedTarget).data('edit-jadwalid');
            var pukul_edit_value =  $(e.relatedTarget).data('edit-pukul');
            var kegiatan_edit_value =  $(e.relatedTarget).data('edit-kegiatan');

            $(e.currentTarget).find('#jadwal_edit_id_ref').val(jadwal_id_edit_value);
            $(e.currentTarget).find('#pukul_edit_ref').val(pukul_edit_value);
            $(e.currentTarget).find('#kegiatan_edit_ref').val(kegiatan_edit_value);
           
        });

    });

    //untuk bulir lingkup
    function hapus_lingkup(lingkup_id)
    {
        var tabel_lingkup = $('#tabel_lingkup_auditor').DataTable();
        bootbox.confirm({
            title   : '<i class="fa fa-exclamation-triangle"></i> Konfirmasi',
            message : "Apakah anda ingin menghapus poin tujuan ini?",
            buttons : {
                confirm : {
                    label : 'Ya',
                    className: 'btn-primary'
                },
                cancel : {
                    label : 'Tidak',
                    className : 'btn-danger'
                }
            },
            callback : function (result) {
                if (result)
                {
                    //  simulate click the link
                    $.ajax({
                        type: "POST",
                        url: "{{route('audit_borang_lingkup_delete',['audit_borang_id'=>$audit_borang_id])}}",   
                        data: {"lingkup_id":lingkup_id},
                        success: function (result) {
                            tabel_lingkup.ajax.reload();
                        }
                    });
                }
            }
        });
    }

    $(document).ready(function() {

        var tabel_lingkup = $('#tabel_lingkup_auditor').DataTable({
            "bProcessing": true,
            "bServerSide": true,
            "ajax": {
                "url": "{{route('audit_borang_lingkup_view',['audit_borang_id'=>$audit_borang_id])}}",
                "type": "POST"    
            },
            "columns": [
                {   "render" : function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1; }
                },
                { "data": "audit_lingkup" },
                { 
                    "mData": "0", 
                    "mRender": function ( data, type, full ) {
                        return '<a href="#modal_edit_lingkup" data-toggle="modal" data-edit-lingkup="'+ full.audit_lingkup +'" data-edit-lingkupid="'+ full.id +'"><i class="fa fa-edit"></i></a> | <a class="delete-lingkup-data" onclick="hapus_lingkup(\''+ full.id + '\')" data-confirmation="Apakah anda ingin menghapus tujuan ini?" data-delete-id="'+ full.id +'"><i class="fa fa-trash" aria-hidden="true"></i></a>';
                    }
                }
            ]

        });

        $('#lingkup-audit-form-0').submit(function(e) {
            e.preventDefault();
            var data_lingkup_audit_form = $('#lingkup-audit-form-0').serializeArray();

            $.ajax({
                type: "POST",
                url: "{{route('audit_borang_lingkup_create',['audit_borang_id'=>$audit_borang_id])}}",   
                data: data_lingkup_audit_form,
                success: function (result) {
                    $('#inputLingkup3').val('');
                  
                    tabel_lingkup.ajax.reload();
                }
            });

        });

        $('#lingkup-edit-form-0').submit(function(e) {
            e.preventDefault();

            var data_form_lingkup = $('#lingkup-edit-form-0').serializeArray();

            $.ajax({
                type: "POST",
                url: "{{route('audit_borang_lingkup_edit',['audit_borang_id'=>$audit_borang_id])}}",   
                data: data_form_lingkup,
                success: function (result) {
                    $('#modal_edit_lingkup').modal('toggle'); 
                    tabel_lingkup.ajax.reload();
                }
            });

        });

        $('#modal_edit_lingkup').on('show.bs.modal', function(e) {
            
            var lingkup_edit_value = $(e.relatedTarget).data('edit-lingkup');
            var lingkupid_edit_value =  $(e.relatedTarget).data('edit-lingkupid');

            $(e.currentTarget).find('#lingkup_edit_id_ref').val(lingkupid_edit_value);
            $(e.currentTarget).find('#lingkup_edit_ref').val(lingkup_edit_value);
           
        });

    });


    //untuk bulir pertanyaan
    function hapus_pertanyaan(pertanyaan_id)
    {
        var tabel_pertanyaan = $('#tabel_pertanyaan_referensi').DataTable();
        bootbox.confirm({
            title   : '<i class="fa fa-exclamation-triangle"></i> Konfirmasi',
            message : "Apakah anda ingin menghapus poin tujuan ini?",
            buttons : {
                confirm : {
                    label : 'Ya',
                    className: 'btn-primary'
                },
                cancel : {
                    label : 'Tidak',
                    className : 'btn-danger'
                }
            },
            callback : function (result) {
                if (result)
                {
                    //  simulate click the link
                    $.ajax({
                        type: "POST",
                        url: "{{route('audit_borang_pertanyaan_delete',['audit_borang_id'=>$audit_borang_id])}}",   
                        data: {"pertanyaan_id":pertanyaan_id},
                        success: function (result) {
                            tabel_pertanyaan.ajax.reload();
                        }
                    });
                }
            }
        });
    }

    $(document).ready(function() {

        var tabel_pertanyaan = $('#tabel_pertanyaan_referensi').DataTable({
            "bProcessing": true,
            "bServerSide": true,
            "ajax": {
                "url": "{{route('audit_borang_pertanyaan_view',['audit_borang_id'=>$audit_borang_id])}}",
                "type": "POST"    
            },
            "columns": [
                { "data": "referensi_butir_mutu" },
                { "data": "hasil_observasi" },
                { 
                    "mData": "0", 
                    "mRender": function ( data, type, full ) {
                        if (full.yes_no_check == '1') {
                            return '<i class="fa fa-check" aria-hidden="true"></i>';
                        } else {
                            return '-';
                        }
                    }
                },
                { 
                    "mData": "0", 
                    "mRender": function ( data, type, full ) {
                        if (full.yes_no_check == '2') {
                            return '<i class="fa fa-check" aria-hidden="true"></i>';
                        } else {
                            return '-';
                        }
                    }
                },
                { "data": "catatan_khusus" },
                { 
                    "mData": "0", 
                    "mRender": function ( data, type, full ) {
                        return '<a href="#modal_edit_pertanyaan" data-toggle="modal" data-edit-pertanyaan-referensi="'+ full.referensi_butir_mutu +'" data-edit-pertanyaan-hasilobs="'+ full.hasil_observasi +'" data-edit-pertanyaan-yesnocheck="'+ full.yes_no_check + '" data-edit-pertanyaan-catatan-khusus="'+ full.catatan_khusus +'" data-edit-pertanyaanid="'+ full.id +'"><i class="fa fa-edit"></i></a> | <a class="delete-pertanyaan-data" onclick="hapus_pertanyaan(\''+ full.id + '\')" data-confirmation="Apakah anda ingin menghapus tujuan ini?" data-delete-id="'+ full.id +'"><i class="fa fa-trash" aria-hidden="true"></i></a>';
                    }
                }
            ]

        });

        $('#pertanyaan-audit-form-0').submit(function(e) {
            e.preventDefault();
            var data_pertanyaan_audit_form = $('#pertanyaan-audit-form-0').serializeArray();

            $.ajax({
                type: "POST",
                url: "{{route('audit_borang_pertanyaan_create',['audit_borang_id'=>$audit_borang_id])}}",   
                data: data_pertanyaan_audit_form,
                success: function (result) {
                    $('#inputReferensi3').val('');
                    $('#inputHasilObservasi3').val('');
                    $('#inputCatKhusus3').val('');
                    $('#inputHasilObsCeklist3_ya').prop('checked',false);
                    $('#inputHasilObsCeklist3_tidak').prop('checked',false);

                    tabel_pertanyaan.ajax.reload();
                }
            });

        });

        $('#pertanyaan-edit-form-0').submit(function(e) {
            e.preventDefault();

            var data_form_pertanyaan = $('#pertanyaan-edit-form-0').serializeArray();

            $.ajax({
                type: "POST",
                url: "{{route('audit_borang_pertanyaan_edit',['audit_borang_id'=>$audit_borang_id])}}",   
                data: data_form_pertanyaan,
                success: function (result) {
                    $('#modal_edit_pertanyaan').modal('toggle'); 
                    tabel_pertanyaan.ajax.reload();
                }
            });

        });

        $('#modal_edit_pertanyaan').on('show.bs.modal', function(e) {
            
            var referensi_edit_value = $(e.relatedTarget).data('edit-pertanyaan-referensi');
            var hasilobs_edit_value =  $(e.relatedTarget).data('edit-pertanyaan-hasilobs');
            var yesnocheck_edit_value =  $(e.relatedTarget).data('edit-pertanyaan-yesnocheck');
            var catkhusus_edit_value =  $(e.relatedTarget).data('edit-pertanyaan-catatan-khusus');
            var pertanyaan_edit_id = $(e.relatedTarget).data('edit-pertanyaanid');

            $(e.currentTarget).find('#pertanyaan_edit_idx').val(pertanyaan_edit_id);
            $(e.currentTarget).find('#pertanyaan_edit_ref').val(referensi_edit_value);
            $(e.currentTarget).find('#pertanyaan_edit_hasilobs').val(hasilobs_edit_value);
            $(e.currentTarget).find('#pertanyaan_edit_catatan').val(catkhusus_edit_value);

                if (yesnocheck_edit_value == 1) {
                    $(e.currentTarget).find('#pertanyaan_edit_ceklist_ya').prop('checked',true);
                } else if (yesnocheck_edit_value == 2) {
                    $(e.currentTarget).find('#pertanyaan_edit_ceklist_tidak').prop('checked',true);
                }
          

        });

    });


    // untuk tujuan 
    function hapus_tujuan(delete_id)
    {
        var tabel_tujuan = $('#tabel_tujuan_auditor').DataTable();
        bootbox.confirm({
            title   : '<i class="fa fa-exclamation-triangle"></i> Konfirmasi',
            message : "Apakah anda ingin menghapus poin tujuan ini?",
            buttons : {
                confirm : {
                    label : 'Ya',
                    className: 'btn-primary'
                },
                cancel : {
                    label : 'Tidak',
                    className : 'btn-danger'
                }
            },
            callback : function (result) {
                if (result)
                {
                    //  simulate click the link
                    $.ajax({
                        type: "POST",
                        url: "{{route('audit_borang_tujuan_delete',['audit_borang_id'=>$audit_borang_id])}}",   
                        data: {"tujuan_id":delete_id},
                        success: function (result) {
                            tabel_tujuan.ajax.reload();
                        }
                    });
                }
            }
        });
    }

    $(document).ready(function() {

        var tabel_tujuan = $('#tabel_tujuan_auditor').DataTable({
            "bProcessing": true,
            "bServerSide": true,
            "ajax": {          
                "url": "{{route('audit_borang_tujuan_view',['audit_borang_id'=>$audit_borang_id])}}",
                "type": "POST"    
            },
            "columns": [
                { "data": "tujuan" },
                { 
                    "mData": "0", 
                    "mRender": function ( data, type, full ) {
                        if (full.yes_no_check == '1') {
                            return '<i class="fa fa-check" aria-hidden="true"></i>';
                        } else {
                            return '-';
                        }
                    }
                },
                { 
                    "mData": "0", 
                    "mRender": function ( data, type, full ) {
                        return '<a href="#modal_edit_tujuan" data-toggle="modal" data-edit-tujuan="'+ full.tujuan +'" data-edit-ceklist="'+ full.yes_no_check +'" data-edit-tujuanid="'+ full.id +'"><i class="fa fa-edit"></i></a> | <a class="delete-tujuan-data" onclick="hapus_tujuan(\''+ full.id + '\')" data-confirmation="Apakah anda ingin menghapus tujuan ini?" data-delete-id="'+ full.id +'"><i class="fa fa-trash" aria-hidden="true"></i></a>';
                    }
                }
            ]

        });

        $('#tujuan-audit-form-0').submit(function(e) {
            e.preventDefault();

            var data_form_tujuan = $('#tujuan-audit-form-0').serializeArray();

            $.ajax({
                type: "POST",
                url: "{{route('audit_borang_tujuan_create',['audit_borang_id'=>$audit_borang_id])}}",   
                data: data_form_tujuan,
                success: function (result) {
                    $('#inputTujuan3').val('');
                    $('#inputCeklist3').prop('checked',false);

                    tabel_tujuan.ajax.reload();
                }
            });

        });

        $('#modal_edit_tujuan').on('show.bs.modal', function(e) {
            
            var tujuan_edit_value = $(e.relatedTarget).data('edit-tujuan');
            var tujuan_edit_cek =  $(e.relatedTarget).data('edit-ceklist');
            var tujuan_edit_tujuanid =  $(e.relatedTarget).data('edit-tujuanid');
            $(e.currentTarget).find('#tujuan_edit_id').val(tujuan_edit_value);
                if (tujuan_edit_cek == 1) {
                    $(e.currentTarget).find('#tujuan_edit_ceklist').prop('checked',true);
                } else {
                    $(e.currentTarget).find('#tujuan_edit_ceklist').prop('checked',false);
                }
            $(e.currentTarget).find('#tujuan_edit_idx').val(tujuan_edit_tujuanid);
            
        });

        $('.delete-tujuan-data').click(function(e){

        //  get link and its confirmation message
        var link                = $(this);
        var confirmationmessage = link.data('confirmation');
        var delete_id           = link.attr('delete-id');

        e.preventDefault();

        bootbox.confirm({
            title   : '<i class="fas fa-exclamation-triangle"></i> Konfirmasi',
            message : confirmationmessage,
            buttons : {
                confirm : {
                    label : 'Ya',
                    className: 'btn-primary'
                },
                cancel : {
                    label : 'Tidak',
                    className : 'btn-danger'
                }
            },
            callback : function (result) {
                if (result)
                {
                    //  simulate click the link
                    $.ajax({
                        type: "POST",
                        url: "{{route('audit_borang_tujuan_create',['audit_borang_id'=>$audit_borang_id])}}",   
                        data: {"tujuan_id":delete_id},
                        success: function (result) {
                            tabel_tujuan.ajax.reload();
                        }
                    });
                }
            }
            });
        });

        $('#tujuan-edit-form-0').submit(function(e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: "{{route('audit_borang_tujuan_edit',['audit_borang_id'=>$audit_borang_id])}}",   
                data: $('#tujuan-edit-form-0').serializeArray(),
                success: function (result) {
                    $('#modal_edit_tujuan').modal('toggle'); 
                    tabel_tujuan.ajax.reload();
                }
            });
            
        });

    });

@endif
    $(document).ready(function() {
        $('#tabel_standar').DataTable();
    });

    $(document).ready(function() {
        $(".baris-diklik").click(function() {
            window.location = $(this).data("href");
        });
    });

   

    $(document).ready(function (){
        var tabelnya = $('#tabel_anggota_auditor').DataTable({   
            select: {
                style: 'multi'
            },       
        });

        $('#tabel_anggota_auditor tbody').on( 'click', 'tr', function () {
        
        });

        $("#pendahuluan-form-1").submit(function(e){
            e.preventDefault();

            datanya = tabelnya.rows({selected:  true}).data();
            arr = [];
            for (index = 0 ; index < datanya.length ; ++index) {
                arr.push(datanya[index]);
            }

            console.log(JSON.parse(JSON.stringify(arr)));

            var data_form = $('#pendahuluan-form-0,#pendahuluan-form-1').serializeArray();
            data_form.push({name: 'data_anggota', value: JSON.stringify(arr)});
            

            $.ajax({
                type: "POST",
                url: "{{route('audit_borang_create')}}",   
                data: data_form,
                success: function (result) {
                    var hasil;
                    if (result !== null) {
                        hasil = JSON.parse(result);

                        if (hasil.message == "ok") {
                            window.location = hasil.redirect;
                        } else {
                            alert('terjadi kesalahan');
                        }
                    }
                }
            });
        });


        $("#oke").click(function(){
            //for debugging purposes
            datanya = tabelnya.rows({selected:  true}).data();
            arr = [];
            for (index = 0 ; index < datanya.length ; ++index) {
                arr.push(datanya[index]);
            }

            console.log(arr);
            alert("button");
        }); 

    });


  
</script>



@stop

