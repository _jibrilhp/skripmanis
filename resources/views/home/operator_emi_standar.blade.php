@extends('adminlte::page')


@section('title', 'SPMI dan SPME')


@section('content_header')

    <h1>SPMI dan SPME</h1>

@stop


@section('content')


@if (Session::has('success'))
    <br>
        <div class="alert alert-success">
           
            {!! \Session::get('success') !!}
            
        </div>
@endif

@if (Session::has('pesan'))
    <br>
        <div class="alert alert-warning">
           
            {!! \Session::get('pesan') !!}
            
        </div>
@endif

<div class='row'>
        <div class='col-md-12'>
            <!-- Box -->
            @if ($state == 'pilih')
                <div class="box primary-box"> 
            @else
                <div class="box collapsed-box"> 
            @endif
                <div class="box-header with-border">
                    <h3 class="box-title">Profil Program Studi </h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        <!-- <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button> -->
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <form method="post">
                        @csrf
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <h3>Tahun Pengukuran Mutu</h3>
                                </div>
                                <div class="form-group">
                                    <select name="op_tahun_pengukuran" class="form-control">
                                            <option value="kosong">--- Pilih ---</option>
                                            <option value="2017">2017</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                        <button type="submit" class="btn btn-lg btn-primary"> Lihat </button>
                                </div>
                            </div>
                            <div class="col-md-2"></div>
                        </form>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
@if ($state == 'view')
       <!-- list standar untuk ngisi, edit, hapus -->
       <div class='col-md-12'>

            <!-- Box -->
                <div class="box primary-box"> 
                <!-- <div class="box collapsed-box"> -->
                    <div class="box-header with-border">
                        <h3 class="box-title">Nama Standar</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    
                    <div class="box-body">

                    <div class="row">
                        <div class="col-md-12">
                            <!-- <hr style="display: block; height: 1px; border: 0; border-top: 1px solid #ccc; margin: 1em 0;padding: 0; "> -->
                            <form class="form-horizontal" id="emi-standar-form-0">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="inputEmiStandar3" class="col-sm-2 control-label">Isi Standar</label>

                                        <div class="col-sm-10">
                                            <textarea name="op_emi_isian_standar" class="form-control" id="inputEmiStandar3" placeholder="Masukkan Isian Standar"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="submitEmiIsianStandar" class="col-sm-2 control-label"></label>

                                        <div class="col-sm-10">
                                            <button type="submit" name="op_submit_emi_isian" id="submitEmiIsianStandar" class="btn btn-primary"> Simpan </button>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputIsianReferensi3" class="col-sm-2 control-label">Isian Standar</label>

                                       
                                        <div class="col-sm-10">
                                        
                                            <table  id="tabel_emi_isian_standar" class="table table-bordered display">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Isian Standar</th>
                                                    <th>Aksi</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                
                                            </table>
                                            
                                        </div> <!-- /.col-sm-10 -->
                                        
                                    </div>

                                    

                                </div>
                                <!-- /.box-body -->
                              
                            </form>
                        </div>
                    </div>

                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            
        </div><!-- /.col -->


        <!-- keadaan prodi untuk ngisi, edit, hapus -->
         <div class='col-md-12'>
            <!-- Box -->
                <div class="box primary-box"> 
                <!-- <div class="box collapsed-box"> -->
                    <div class="box-header with-border">
                        <h3 class="box-title">Isian Keadaan Prodi saat ini</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    
                    <div class="box-body">

                    <div class="row">
                        <div class="col-md-12">
                            <!-- <hr style="display: block; height: 1px; border: 0; border-top: 1px solid #ccc; margin: 1em 0;padding: 0; "> -->
                            <form class="form-horizontal" id="emi-keadaan-ini-form-0">
                                
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="inputEmiPilihanStandar3" class="col-sm-2 control-label">Pilihan Standar</label>

                                        <div class="col-sm-10">
                                            <select id ="op_emi_pilihan_standar_ref" name="op_emi_pilihan_standar" class="form-control" id="inputEmiPilihanStandar3" placeholder="Masukkan Isian Standar">
                                                <option value="--">Pilih Standar</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputEmiKeadaanProdi3" class="col-sm-2 control-label">Isi Keadaan Prodi</label>

                                        <div class="col-sm-10">
                                            <textarea name="op_emi_keadaan_prodi_standar" class="form-control" id="inputEmiKeadaanProdi3" placeholder="Masukkan Keadaan Prodi saat ini"></textarea>
                                        </div>
                                    </div>

                                    
                                    <div class="form-group">
                                        <label for="inputEmiBobotNilai3" class="col-sm-2 control-label">Bobot Nilai</label>

                                        <div class="col-sm-10">
                                            <select name="op_emi_bobot_nilai_standar" class="form-control" id="inputEmiBobotNilai3">
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                                <option>6</option>
                                                <option>7</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="submitEmiSubmitKeadaan" class="col-sm-2 control-label"></label>

                                        <div class="col-sm-10">
                                            <button type="submit" name="op_submit_emi_isian" id="submitEmiSubmitKeadaan" class="btn btn-primary"> Simpan </button>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputIsianReferensi3" class="col-sm-2 control-label">Isian Keadaan Prodi saat ini</label>

                                       
                                        <div class="col-sm-10">
                                        
                                            <table  id="tabel_emi_keadaan_prodi" class="table table-bordered display">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Tentang Standar</th>
                                                    <th>Keadaan Prodi Saat ini</th>
                                                    <th>Untuk bobot nilai</th>
                                                    <th>Aksi</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                
                                            </table>
                                            
                                        </div> <!-- /.col-sm-10 -->
                                        
                                    </div>

                                    

                                </div>
                                <!-- /.box-body -->
                              
                            </form>
                        </div>
                    </div>

                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            
        </div><!-- /.col -->

        <!-- nilai capaian kek di excel, cuma bisa ngedit nilainya aja -->
        <div class='col-md-12'>
            <!-- Box -->
                <div class="box primary-box"> 
                <!-- <div class="box collapsed-box"> -->
                    <div class="box-header with-border">
                        <h3 class="box-title">Nilai Capaian</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    
                    <div class="box-body">

                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-bordered display">
                                <thead>
                                    <td>Nama Program Studi</td>
                                    <th>{{$profil->nama_prodi}}</th>
                                </thead>
                                <tbody>
                                    <td>Tahun Pengukuran Mutu</td>
                                    <th>{{$standar_tahun}}</th>
                                </tbody>
                            </table>
                            <form class="form-horizontal" id="emi-capaian-form-0">
                                
                                <div class="box-body">

                                    <div class="form-group">
                                        <label for="inputIsianReferensi3" class="col-sm-2 control-label">Isian Nilai Capaian</label>

                                       
                                        <div class="col-sm-10">
                                        
                                            <table  id="tabel_emi_capaian_prodi" class="table table-bordered display">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Tentang Standar</th>
                                                    <th>Bobot (%)</th>
                                                    <th>Keadaan Prodi Saat Ini</th>
                                                    <th>Nilai Capaian</th>
                                                    <th>Sebutan</th>
                                                    <th>Aksi</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                
                                            </table>
                                            
                                        </div> <!-- /.col-sm-10 -->
                                        
                                    </div>

                                    

                                </div>
                                <!-- /.box-body -->
                              
                            </form>
                        </div>
                    </div>

                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            
        </div><!-- /.col -->


@endif
    

    </div><!-- /.row -->
@if ($state == 'view')
         <!-- modal form capaian -->
         <div class="modal" id="modal_edit_capaian">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title">Mengubah nilai capaian</h4>
                    </div>
                    <form method="post" id="capaian-edit-form-0">
                        <div class="modal-body">
                            <input type="hidden" id="capaian_edit_id_ref" name="capaian_edit_id_value">
                            <input type="hidden" id="capaian_isian_ref" name="capaian_isian_value">
                            <p>Bobot Nilai</p>
                            <select id="capaian_nilai_ref" name="capaian_nilai_value" class="form-control" id="inputEmiBobotNilai3">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                            </select>
 
                        </div>
                    
                    <div class="modal-footer">
                        
                        <button type="submit" class="btn btn-primary" >Simpan</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- modal form edit keadaan prodi -->
        <div class="modal" id="modal_edit_keadaan_prodi">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title">Mengubah data keadaan prodi</h4>
                    </div>
                    <form method="post" id="keadaan-prodi-edit-form-0">
                        <div class="modal-body">
                            <input type="hidden" id="keadaan_prodi_edit_id_ref" name="keadaan_prodi_edit_id_value">
                            <p>Isian Standar</p>
                            <select id ="keadaan_prodi_pilihan_standar_ref" name="keadaan_prodi_pilihan_standar_value" class="form-control" >
                                <option value="--">Pilih Standar</option>
                            </select>
                            <hr>
                            <p>Keadaan Prodi saat ini</p>
                            <textarea id="keadaan_prodi_edit_ref" name="keadaan_prodi_edit_value" class="form-control" id="inputEmiKeadaanProdi3" placeholder="Masukkan Keadaan Prodi saat ini"></textarea>
                            <hr>
                            <p>Bobot nilai</p>
                            <select id="keadaan_nilai_ref" name="keadaan_nilai_value" class="form-control" id="inputEmiBobotNilai3">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                            </select>
                            <hr>

                        </div>
                    
                    <div class="modal-footer">
                        
                        <button type="submit" class="btn btn-primary" >Simpan</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- modal form edit isian standar -->
        <div class="modal" id="modal_edit_isian_standar">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title">Mengubah data Isian standar</h4>
                    </div>
                    <form method="post" id="isian-standar-edit-form-0">
                        <div class="modal-body">
                            <input type="hidden" id="isian_standar_edit_id_ref" name="isian_standar_edit_id_value">
                            <p>Isian Standar</p>
                            <textarea id="isian_standar_edit_ref" name="isian_standar_edit_value" class="form-control"></textarea>
 
                        </div>
                    
                    <div class="modal-footer">
                        
                        <button type="submit" class="btn btn-primary" >Simpan</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>

@endif
    

@stop


@section('css')

    <link rel="stylesheet" href="/css/admin_custom.css">

@stop


@section('js')

<script>
@if ($state == 'view')
    //buat capaian ..~
    $(document).ready(function() {
       
        var tabel_capaian_standar = $('#tabel_emi_capaian_prodi').DataTable({
            "bServerSide": true,
            "searching":false,
            "ajax": {
                "url": "{{route('emi_capaian_standar_view',['emi_standar_id'=>$standar_id,'emi_tahun_standar'=>$standar_tahun])}}",
                "type": "POST"    
            },
            "columns": [
                {   "render" : function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1; }
                },
                { "data": "isian_standar" },
                {"data": "bobot" },
                { "data": "keadaan" },
                { "data": "nilai" },
                {  "data": "sebutan" } ,
                { 
                    "mData": "0", 
                    "mRender": function ( data, type, full ) {
                        return '<a href="#modal_edit_capaian" data-toggle="modal" data-edit-nilai-value="'+ full.nilai +'" data-edit-bobot-id="'+ full.bobot_id + '" data-edit-isian-id="'+ full.isian_id + '" ><i class="fa fa-edit"></i></a>';
                    }
                }
            ]

        });

        //edit -- form
        $('#capaian-edit-form-0').submit(function(e) {
            e.preventDefault();

            var data_form_capaian = $('#capaian-edit-form-0').serializeArray();

            $.ajax({
                type: "POST",
                url: "{{route('emi_capaian_standar_edit',['emi_standar_id'=>$standar_id,'emi_tahun_standar'=>$standar_tahun])}}",   
                data: data_form_capaian,
                success: function (result) {
                    $('#modal_edit_capaian').modal('toggle'); 
                    tabel_capaian_standar.ajax.reload();
                }
            });

        });

        $('#modal_edit_capaian').on('show.bs.modal', function(e) {
            
            var capaian_edit_id_ref = $(e.relatedTarget).data('edit-bobot-id');
            var capaian_nilai_ref =  $(e.relatedTarget).data('edit-nilai-value');
            var capaian_isian_ref =  $(e.relatedTarget).data('edit-isian-id'); 
          
            $(e.currentTarget).find('#capaian_edit_id_ref').val(capaian_edit_id_ref);
            $(e.currentTarget).find('#capaian_nilai_ref').val(capaian_nilai_ref);
            $(e.currentTarget).find('#capaian_isian_ref').val(capaian_isian_ref);
            
        });
    });


    //keadaan prodi standar
    function hapus_keadaan(keadaan_id)
    {
        var tabel_keadaan_standar = $('#tabel_emi_keadaan_prodi').DataTable();
        bootbox.confirm({
            title   : '<i class="fa fa-exclamation-triangle"></i> Konfirmasi',
            message : "Apakah anda ingin menghapus poin isian ini?",
            buttons : {
                confirm : {
                    label : 'Ya',
                    className: 'btn-primary'
                },
                cancel : {
                    label : 'Tidak',
                    className : 'btn-danger'
                }
            },
            callback : function (result) {
                if (result)
                {
                    $.ajax({
                        type: "POST",
                        url: "{{route('emi_keadaan_standar_delete',['emi_standar_id'=>$standar_id,'emi_tahun_standar'=>$standar_tahun])}}",   
                        data: {"keadaan_id":keadaan_id},
                        success: function (result) {
                            tabel_keadaan_standar.ajax.reload();
                        }
                    });
                }
            }
        });
    }

    $(document).ready(function() {
       
        var tabel_keadaan_standar = $('#tabel_emi_keadaan_prodi').DataTable({
            "bProcessing": true,
            "bServerSide": true,
            "ajax": {
                "url": "{{route('emi_keadaan_standar_view',['emi_standar_id'=>$standar_id,'emi_tahun_standar'=>$standar_tahun])}}",
                "type": "POST"    
            },
            "columns": [
                {   "render" : function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1; }
                },
                { "data": "emi_isian_standar" },
                { "data": "keadaan_prodi_saat_ini" },
                { "data": "emi_bobot_nilai" },
                { 
                    "mData": "0", 
                    "mRender": function ( data, type, full ) {
                        return '<a href="#modal_edit_keadaan_prodi" data-toggle="modal" data-edit-standar-id="'+ full.emi_isian_standar_id +'" data-edit-bobot="'+ full.emi_bobot_nilai + '" data-edit-keadaan="' + full.keadaan_prodi_saat_ini +'" data-edit-keadaanid="'+ full.keadaan_id +'" ><i class="fa fa-edit"></i></a> | <a onclick="hapus_keadaan(\''+ full.keadaan_id + '\')" ><i class="fa fa-trash" aria-hidden="true"></i></a>';
                    }
                }
            ]

        });

        $('#emi-keadaan-ini-form-0').submit(function(e) {
            e.preventDefault();
            var data_keadaan_form = $('#emi-keadaan-ini-form-0').serializeArray();

            $.ajax({
                type: "POST",
                url: "{{route('emi_keadaan_standar_create',['emi_standar_id'=>$standar_id,'emi_tahun_standar'=>$standar_tahun])}}",   
                data: data_keadaan_form,
                success: function (result) {
                    $('#inputEmiKeadaanProdi3').val('');
                   
                  
                    tabel_keadaan_standar.ajax.reload();
                }
            });

        });
        //edit -- form
        $('#keadaan-prodi-edit-form-0').submit(function(e) {
            e.preventDefault();

            var data_form_keadaan = $('#keadaan-prodi-edit-form-0').serializeArray();

            $.ajax({
                type: "POST",
                url: "{{route('emi_keadaan_standar_edit',['emi_standar_id'=>$standar_id,'emi_tahun_standar'=>$standar_tahun])}}",   
                data: data_form_keadaan,
                success: function (result) {
                    $('#modal_edit_keadaan_prodi').modal('toggle'); 
                    tabel_keadaan_standar.ajax.reload();
                }
            });

        });

        $('#modal_edit_keadaan_prodi').on('show.bs.modal', function(e) {
            
            var keadaan_prodi_edit_id_ref = $(e.relatedTarget).data('edit-keadaanid');
            var keadaan_prodi_edit_ref =  $(e.relatedTarget).data('edit-keadaan');
            var keadaan_prodi_pilihan_standar_ref = $(e.relatedTarget).data('edit-standar-id');
            var keadaan_nilai_ref = $(e.relatedTarget).data('edit-bobot');
           
            $(e.currentTarget).find('#keadaan_prodi_edit_id_ref').val(keadaan_prodi_edit_id_ref);
            $(e.currentTarget).find('#keadaan_prodi_edit_ref').val(keadaan_prodi_edit_ref);
            $(e.currentTarget).find('#keadaan_prodi_pilihan_standar_ref').val(keadaan_prodi_pilihan_standar_ref);
            $(e.currentTarget).find('#keadaan_nilai_ref').val(keadaan_nilai_ref);
       
        });
    });

  //isian standar
 function hapus_isian_standar(isian_id)
    {
        var tabel_isian_standar = $('#tabel_emi_isian_standar').DataTable();
        bootbox.confirm({
            title   : '<i class="fa fa-exclamation-triangle"></i> Konfirmasi',
            message : "Apakah anda ingin menghapus poin isian ini?",
            buttons : {
                confirm : {
                    label : 'Ya',
                    className: 'btn-primary'
                },
                cancel : {
                    label : 'Tidak',
                    className : 'btn-danger'
                }
            },
            callback : function (result) {
                if (result)
                {
                    $.ajax({
                        type: "POST",
                        url: "{{route('emi_standar_delete',['emi_standar_id'=>$standar_id,'emi_tahun_standar'=>$standar_tahun])}}",   
                        data: {"isian_id":isian_id},
                        success: function (result) {
                            tabel_isian_standar.ajax.reload();
                        }
                    });
                }
            }
        });
    }

    $(document).ready(function() {
       
        var tabel_isian_standar = $('#tabel_emi_isian_standar').DataTable({
            "bProcessing": true,
            "bServerSide": true,
            "ajax": {
                "url": "{{route('emi_standar_view',['emi_standar_id'=>$standar_id,'emi_tahun_standar'=>$standar_tahun])}}",
                "type": "POST"    
            },
            "columns": [
                {   "render" : function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1; }
                },
                { "data": "emi_isian_standar" },
                { 
                    "mData": "0", 
                    "mRender": function ( data, type, full ) {
                        return '<a href="#modal_edit_isian_standar" data-toggle="modal" data-edit-isian="'+ full.emi_isian_standar +'" data-edit-isianid="'+ full.id +'" ><i class="fa fa-edit"></i></a> | <a onclick="hapus_isian_standar(\''+ full.id + '\')" ><i class="fa fa-trash" aria-hidden="true"></i></a>';
                    }
                }
            ]

        });

        $('#emi-standar-form-0').submit(function(e) {
            e.preventDefault();
            var data_isian_standar_form = $('#emi-standar-form-0').serializeArray();

            $.ajax({
                type: "POST",
                url: "{{route('emi_standar_create',['emi_standar_id'=>$standar_id,'emi_tahun_standar'=>$standar_tahun])}}",   
                data: data_isian_standar_form,
                success: function (result) {
                    $('#inputEmiStandar3').val('');
                   
                  
                    tabel_isian_standar.ajax.reload();
                }
            });

        });

        $('#isian-standar-edit-form-0').submit(function(e) {
            e.preventDefault();

            var data_form_isian_standar = $('#isian-standar-edit-form-0').serializeArray();

            $.ajax({
                type: "POST",
                url: "{{route('emi_standar_edit',['emi_standar_id'=>$standar_id,'emi_tahun_standar'=>$standar_tahun])}}",   
                data: data_form_isian_standar,
                success: function (result) {
                    $('#modal_edit_isian_standar').modal('toggle'); 
                    tabel_isian_standar.ajax.reload();
                }
            });

        });

        $('#modal_edit_isian_standar').on('show.bs.modal', function(e) {
            
            var isian_standar_edit_id_ref = $(e.relatedTarget).data('edit-isianid');
            var isian_standar_edit_ref =  $(e.relatedTarget).data('edit-isian');
           
            $(e.currentTarget).find('#isian_standar_edit_id_ref').val(isian_standar_edit_id_ref);
            $(e.currentTarget).find('#isian_standar_edit_ref').val(isian_standar_edit_ref);
       
        });

        tabel_isian_standar.on( 'xhr', function ( e, settings, json ) {
            //console.log( 'Ajax event occurred. Returned data: ', json );
            //
            $.ajax({
                type: "POST",
                url: "{{route('emi_standar_pilihan_tabel',['emi_standar_id'=>$standar_id,'emi_tahun_standar'=>$standar_tahun])}}",   
                data: "hmm",
                success: function (result) {
                    $('#op_emi_pilihan_standar_ref').html(result); 
                    $('#keadaan_prodi_pilihan_standar_ref').html(result); 
                }
            });
        });

    });
@endif
</script>

@stop

