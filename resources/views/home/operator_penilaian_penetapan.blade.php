@extends('adminlte::page')


@section('title', 'SPMI dan SPME')


@section('content_header')

    <h1>SPMI dan SPME</h1>

<style>
.form_wizard .stepContainer {
  display: block;
  position: relative;
  margin: 0;
  padding: 0;
  border: 0 solid #CCC;
  overflow-x: hidden
}

.wizard_horizontal ul.wizard_steps {
  display: table;
  list-style: none;
  position: relative;
  width: 100%;
  margin: 0 0 20px
}

.wizard_horizontal ul.wizard_steps li {
  display: table-cell;
  text-align: center
}

.wizard_horizontal ul.wizard_steps li a,
.wizard_horizontal ul.wizard_steps li:hover {
  display: block;
  position: relative;
  -moz-opacity: 1;
  filter: alpha(opacity=100);
  opacity: 1;
  color: #666
}

.wizard_horizontal ul.wizard_steps li a:before {
  content: "";
  position: absolute;
  height: 4px;
  background: #ccc;
  top: 20px;
  width: 100%;
  z-index: 4;
  left: 0
}

.wizard_horizontal ul.wizard_steps li a.disabled .step_no {
  background: #ccc
}

.wizard_horizontal ul.wizard_steps li a .step_no {
  width: 40px;
  height: 40px;
  line-height: 40px;
  border-radius: 100px;
  display: block;
  margin: 0 auto 5px;
  font-size: 16px;
  text-align: center;
  position: relative;
  z-index: 5
}

.wizard_horizontal ul.wizard_steps li a.selected:before,
.step_no {
  background: #34495E;
  color: #fff
}

.wizard_horizontal ul.wizard_steps li a.done:before,
.wizard_horizontal ul.wizard_steps li a.done .step_no {
  background: #1ABB9C;
  color: #fff
}

.wizard_horizontal ul.wizard_steps li:first-child a:before {
  left: 50%
}

.wizard_horizontal ul.wizard_steps li:last-child a:before {
  right: 50%;
  width: 50%;
  left: auto
}

.wizard_verticle .stepContainer {
  width: 80%;
  float: left;
  padding: 0 10px
}

.actionBar {
  width: 100%;
  border-top: 1px solid #ddd;
  padding: 10px 5px;
  text-align: right;
  margin-top: 10px
}

.actionBar .buttonDisabled {
  cursor: not-allowed;
  pointer-events: none;
  opacity: .65;
  filter: alpha(opacity=65);
  box-shadow: none
}

.actionBar a {
  margin: 0 3px
}

.wizard_verticle .wizard_content {
  width: 80%;
  float: left;
  padding-left: 20px
}

.wizard_verticle ul.wizard_steps {
  display: table;
  list-style: none;
  position: relative;
  width: 20%;
  float: left;
  margin: 0 0 20px
}

.wizard_verticle ul.wizard_steps li {
  display: list-item;
  text-align: center
}

.wizard_verticle ul.wizard_steps li a {
  height: 80px
}

.wizard_verticle ul.wizard_steps li a:first-child {
  margin-top: 20px
}

.wizard_verticle ul.wizard_steps li a,
.wizard_verticle ul.wizard_steps li:hover {
  display: block;
  position: relative;
  -moz-opacity: 1;
  filter: alpha(opacity=100);
  opacity: 1;
  color: #666
}

.wizard_verticle ul.wizard_steps li a:before {
  content: "";
  position: absolute;
  height: 100%;
  background: #ccc;
  top: 20px;
  width: 4px;
  z-index: 4;
  left: 49%
}

.wizard_verticle ul.wizard_steps li a.disabled .step_no {
  background: #ccc
}

.wizard_verticle ul.wizard_steps li a .step_no {
  width: 40px;
  height: 40px;
  line-height: 40px;
  border-radius: 100px;
  display: block;
  margin: 0 auto 5px;
  font-size: 16px;
  text-align: center;
  position: relative;
  z-index: 5
}

.wizard_verticle ul.wizard_steps li a.selected:before,
.step_no {
  background: #34495E;
  color: #fff
}

.wizard_verticle ul.wizard_steps li a.done:before,
.wizard_verticle ul.wizard_steps li a.done .step_no {
  background: #1ABB9C;
  color: #fff
}

.wizard_verticle ul.wizard_steps li:first-child a:before {
  left: 49%
}

.wizard_verticle ul.wizard_steps li:last-child a:before {
  left: 49%;
  left: auto;
  width: 0
}

.form_wizard .loader {
  display: none
}

.form_wizard .msgBox {
  display: none
}

</style>
@stop


@section('content')


@if (Session::has('success'))
    <br>
        <div class="alert alert-success">
           
            {!! \Session::get('success') !!}
            
        </div>
@endif

@if (Session::has('pesan'))
    <br>
        <div class="alert alert-warning">
           
            {!! \Session::get('pesan') !!}
            
        </div>
@endif

<div class='row'>
        <div class='col-md-12'>
            <!-- Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Perencanaan </h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div id="wizard" class="form_wizard wizard_horizontal custom-wizard" style="100%;">
                        <ul class="wizard_steps" style="padding:0px;">
                            <li>
                            <a href="#step-1" onclick="lihat_sekarang('penetapan')">
                                <span class="step_no">P</span>
                                <span class="step_descr">
                                    Penetapan<br>
                                    <small></small>
                                </span>
                            </a>
                            </li>
                            <li>
                            <a href="#step-2"  onclick="lihat_sekarang('pelaksanaan')">
                                <span class="step_no">P</span>
                                <span class="step_descr">
                                    Pelaksanaan<br>
                                    <small></small>
                                </span>
                            </a>
                            </li>
                            <li>
                            <a href="#step-3"  onclick="lihat_sekarang('evaluasi')">
                                <span class="step_no">E</span>
                                <span class="step_descr">
                                    Evaluasi<br>
                                    <small></small>
                                </span>
                            </a>
                            </li>
                            <li>
                            <a href="#step-4"  onclick="lihat_sekarang('pengendalian')">
                                <span class="step_no">P</span>
                                <span class="step_descr">
                                    Pengendalian<br>
                                    <small></small>
                                </span>
                            </a>
                            </li>
                            <li>
                            <a href="#step-5"  onclick="lihat_sekarang('peningkatan')">
                                <span class="step_no">P</span>
                                <span class="step_descr">
                                    Peningkatan<br>
                                    <small></small>
                                </span>
                            </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="box-body">
                    <form method="post" enctype="multipart/form-data">

                    <input type="hidden" name="op_perencanaan_hidden1" value="@if ($profil_id != null){{$profil_id }}@endif">
                    <input type="hidden" name="op_perencanaan_hidden2" value="@if ($bagian != null){{$bagian }}@endif">

                        @csrf
                    <hr style="display: block; height: 1px; border: 0; border-top: 1px solid #ccc; margin: 1em 0;padding: 0; ">
                  
                    <div class="row">
                        <div class="col-md-12">
                        <table id="tabel_standar" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Standar</th>
                                <th>Hasil Peningkatan</th>
                                <th>Upload Peningkatan</th>
                                <th>Komentar</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>1</td>
                                <td> <div class="panel-group">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" href="#collapse1">Standar Isi</a>
                                        </h4>
                                        </div>
                                        <div id="collapse1" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Standar
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Indikator
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Pencapaian
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Mid Line
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Base Line
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                                                </div>
                                            </div>
                                        </div>
                                       
                                    </div>
                                    </div> 
                                </td>
                                <td>--</td>
                                <td><button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button></td>
                                <td><textarea></textarea></td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td> <div class="panel-group">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" href="#collapse2">Standar Proses</a>
                                        </h4>
                                        </div>
                                        <div id="collapse2" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Standar
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Indikator
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Pencapaian
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Mid Line
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Base Line
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                                                </div>
                                            </div>
                                        </div>
                                       
                                    </div>
                                    </div> 
                                </td>
                                <td>--</td>
                                <td><button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button></td>
                                <td><textarea></textarea></td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td> <div class="panel-group">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" href="#collapse3">Standar Kompetensi Lulusan</a>
                                        </h4>
                                        </div>
                                        <div id="collapse3" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Standar
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Indikator
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Pencapaian
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Mid Line
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Base Line
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                                                </div>
                                            </div>
                                        </div>
                                       
                                    </div>
                                    </div> 
                                </td>
                                <td>--</td>
                                <td><button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button></td>
                                <td><textarea></textarea></td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td> <div class="panel-group">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" href="#collapse4">Standar Pendidik dan Tenaga Kependidikan</a>
                                        </h4>
                                        </div>
                                        <div id="collapse4" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Standar
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Indikator
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Pencapaian
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Mid Line
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Base Line
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                                                </div>
                                            </div>
                                        </div>
                                       
                                    </div>
                                    </div> 
                                </td>
                                <td>--</td>
                                <td><button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button></td>
                                <td><textarea></textarea></td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td> <div class="panel-group">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" href="#collapse5">Ketersediaan Sarana dan Prasarana Pendidikan</a>
                                        </h4>
                                        </div>
                                        <div id="collapse5" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Standar
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Indikator
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Pencapaian
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Mid Line
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Base Line
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                                                </div>
                                            </div>
                                        </div>
                                       
                                    </div>
                                    </div> 
                                </td>
                                <td>--</td>
                                <td><button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button></td>
                                <td><textarea></textarea></td>
                            </tr>
                            <tr>
                                <td>6</td>
                                <td> <div class="panel-group">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" href="#collapse6">Pengelolaan Program Studi</a>
                                        </h4>
                                        </div>
                                        <div id="collapse6" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Standar
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Indikator
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Pencapaian
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Mid Line
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Base Line
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                                                </div>
                                            </div>
                                        </div>
                                       
                                    </div>
                                    </div> 
                                </td>
                                <td>--</td>
                                <td><button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button></td>
                                <td><textarea></textarea></td>
                            </tr>
                            <tr>
                                <td>7</td>
                                <td> <div class="panel-group">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" href="#collapse7">Standar Pembiayaan</a>
                                        </h4>
                                        </div>
                                        <div id="collapse7" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Standar
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Indikator
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Pencapaian
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Mid Line
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Base Line
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                                                </div>
                                            </div>
                                        </div>
                                       
                                    </div>
                                    </div> 
                                </td>
                                <td>--</td>
                                <td><button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button></td>
                                <td><textarea></textarea></td>
                            </tr>
                            <tr>
                                <td>8</td>
                                <td> <div class="panel-group">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" href="#collapse8">Standar Penilaian</a>
                                        </h4>
                                        </div>
                                        <div id="collapse8" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Standar
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Indikator
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Pencapaian
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Mid Line
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Base Line
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                                                </div>
                                            </div>
                                        </div>
                                       
                                    </div>
                                    </div> 
                                </td>
                                <td>--</td>
                                <td><button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button></td>
                                <td><textarea></textarea></td>
                            </tr>
                            <tr>
                                <td>9</td>
                                <td> <div class="panel-group">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" href="#collapse9">Standar Penelitian</a>
                                        </h4>
                                        </div>
                                        <div id="collapse9" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Standar
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Indikator
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Pencapaian
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Mid Line
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Base Line
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                                                </div>
                                            </div>
                                        </div>
                                       
                                    </div>
                                    </div> 
                                </td>
                                <td>--</td>
                                <td><button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button></td>
                                <td><textarea></textarea></td>
                            </tr>
                            <tr>
                                <td>10</td>
                                <td> <div class="panel-group">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" href="#collapse10">Standar Pengabdian Kepada Masyarakat</a>
                                        </h4>
                                        </div>
                                        <div id="collapse10" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Standar
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Indikator
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Pencapaian
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Mid Line
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Base Line
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                                                </div>
                                            </div>
                                        </div>
                                       
                                    </div>
                                    </div> 
                                </td>
                                <td>--</td>
                                <td><button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button></td>
                                <td><textarea></textarea></td>
                            </tr>
                          
                            </tbody>
                            
                        </table>
                        </div>
                    </div>
                  
                       
                 

                    </form>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->

    </div><!-- /.row -->

    

@stop


@section('css')

    <!-- <link rel="stylesheet" href="/css/admin_custom.css"> -->

@stop


@section('js')

<script>
    $(document).ready(function() {
        $('#tabel_standar').DataTable();
    } );

    function lihat_sekarang(bagian)
    {
        window.location = "?id=@if ($profil_id != null){{$profil_id }}@endif&bagian=" + bagian;
    }
</script>

@stop

