@extends('adminlte::page')


@section('title', 'SPMI dan SPME')


@section('content_header')

    <h1>SPMI dan SPME</h1>

@stop


@section('content')


@if (Session::has('success'))
    <br>
        <div class="alert alert-success">
           
            {!! \Session::get('success') !!}
            
        </div>
@endif

@if (Session::has('pesan'))
    <br>
        <div class="alert alert-warning">
           
            {!! \Session::get('pesan') !!}
            
        </div>
@endif

<div class='row'>
        <div class='col-md-12'>
            <!-- Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Perencanaan </h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <form method="post">

                    <input type="hidden" name="op_perencanaan_hidden1" value="@if ($profil_id != null){{$profil_id }}@endif">
                    <input type="hidden" name="op_perencanaan_hidden2" value="@if ($bagian != null){{$bagian }}@endif">

                        @csrf
                    <div class="row">
                    <div class="col-md-2"><label>Visi</label></div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <textarea id="op_perencanaan_x001" name="op_perencanaan_t001" class="form-control" disabled>@if ($profil_id != null){{$profil->tujuan}}@endif</textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                    <div class="col-md-2"><label>Misi</label></div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <textarea id="op_perencanaan_x002" name="op_perencanaan_t002" class="form-control" disabled>@if ($profil_id != null){{$profil->tujuan}}@endif</textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-2"><label>Tujuan</label></div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <textarea id="op_perencanaan_x1" name="op_perencanaan_t1" class="form-control" >@if ($profil_id != null){{$profil->tujuan}}@endif</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2"><label>Sasaran</label></div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <textarea id="op_perencanaan_x2" name="op_perencanaan_t2" class="form-control" >@if ($profil_id != null){{$profil->sasaran}}@endif</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4"></div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <button type="submit" class="btn btn-lg btn-primary"><i class="fa fa-save"></i> Simpan </button>
                            </div>
                        </div>
                        <div class="col-md-4"></div>
                    </div>

                    </form>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->

    </div><!-- /.row -->

    

@stop


@section('css')

    <link rel="stylesheet" href="/css/admin_custom.css">

@stop


@section('js')

@stop

