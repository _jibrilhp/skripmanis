@extends('adminlte::page')


@section('title', 'SPMI dan SPME')


@section('content_header')

    <h1>SPMI dan SPME</h1>

@stop


@section('content')

@if (Session::has('success'))
    <br>
        <div class="alert alert-success">
           
            {!! \Session::get('success') !!}
            
        </div>
@endif

@if (Session::has('pesan'))
    <br>
        <div class="alert alert-warning">
           
            {!! \Session::get('pesan') !!}
            
        </div>
@endif

<div class='row'>
        <div class='col-md-12'>
            <!-- Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Menambahkan Program Studi</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        <!-- <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button> -->
                    </div>
                </div>
                <div class="box-body">
                    <!-- mulai form -->
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-2 control-label">Kode PT</label>
                            <div class="col-sm-7">
                                <input type="date" class="form-control" id="op_tambah_x1" name="op_tambah_t1" placeholder="Kode perguruan tinggi" value="031037" required>
                            </div>
                            <div class="col-sm-3">
                                &nbsp;*
                            </div>
                        </div>
                    </div>
                    <!-- satu form -->
                    <!-- mulai form -->
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-2 control-label">Nama Perguruan Tinggi</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" id="op_tambah_x2" name="op_tambah_t2" placeholder="Nama perguruan tinggi" value="Universitas Gunadarma" required>
                            </div>
                            <div class="col-sm-3">
                                &nbsp;*
                            </div>
                        </div>
                    </div>
                    <!-- satu form -->
                    <!-- mulai form -->
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-2 control-label">Alamat pusat (jalan, kota, propinsi, kode pos)</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" id="op_tambah_x3" name="op_tambah_t3" placeholder="" value="Jl. Margonda Raya no.100, Pondok Cina, Depok , 16624" required>
                            </div>
                            <div class="col-sm-3">
                                &nbsp;*
                            </div>
                        </div>
                    </div>
                    <!-- satu form -->
                    <!-- mulai form -->
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-2 control-label">Status (negeri/swasta)</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" id="op_tambah_x4" name="op_tambah_t4" placeholder="" value="Swasta" required>
                            </div>
                            <div class="col-sm-3">
                                &nbsp;*
                            </div>
                        </div>
                    </div>
                    <!-- satu form -->
                    <!-- mulai form -->
                    <div class="form-group">
                    <h6>Pelaksana Proses Pembelajaran</h6>
                        <div class="row">
                            <label class="col-sm-2 control-label"><b>Nama Fakultas/Direktorat</b></label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" id="op_tambah_x5" name="op_tambah_t5" placeholder="Tahun dan nilai, contoh : 1998, A" value="" required>
                            </div>
                            <div class="col-sm-3">
                                &nbsp;*
                            </div>
                        </div>
                    </div>
                    <!-- satu form -->
                    <!-- mulai form -->
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-2 control-label">Telepon dan faximile</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" id="op_tambah_x6" name="op_tambah_t6" placeholder="" value="" required>
                            </div>
                            <div class="col-sm-3">
                                &nbsp;*
                            </div>
                        </div>
                    </div>
                    <!-- satu form -->
                    <!-- mulai form -->
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-2 control-label">Nama Jurusan/Program Studi</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" id="op_tambah_x7" name="op_tambah_t7" placeholder="" value="" required>
                            </div>
                            <div class="col-sm-3">
                                &nbsp;*
                            </div>
                        </div>
                    </div>
                    <!-- satu form -->

                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->

    </div><!-- /.row -->

    

@stop


@section('css')

    <link rel="stylesheet" href="/css/admin_custom.css">

@stop


@section('js')

@stop

