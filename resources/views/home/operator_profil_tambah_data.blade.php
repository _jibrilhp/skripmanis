@extends('adminlte::page')


@section('title', 'SPMI dan SPME')


@section('content_header')

    <h1>SPMI dan SPME</h1>

@stop


@section('content')

<div class='row'>
        <div class='col-md-12'>
            <!-- Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                @if ($borang == 'input')
                    <h3 class="box-title">Menginput borang Visitasi</h3>
                @elseif ($borang == 'ubah')
                    <h3 class="box-title">Mengubah borang Visitasi</h3>
                @endif
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        <!-- <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button> -->
                    </div>
                </div>

                @if ($borang == 'input')
                <form method="post" action="{{ route('operator_save_new_borang') }}">                
                @csrf
                    <div class="box-body">
                        <!-- mulai form -->
                        <div class="form-group">
                                <div class="row">
                                    <label class="col-sm-2 control-label">Kode PT</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" id="op_tambah_x25" name="op_tambah_t25" placeholder="Kode PT" value="" required>
                                    </div>
                                    <div class="col-sm-3">
                                        &nbsp;*
                                    </div>
                                </div>
                            </div>
                            <!-- satu form -->
                            <!-- mulai form -->
                        <div class="form-group">
                                <div class="row">
                                    <label class="col-sm-2 control-label">Nama Perguruan Tinggi</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" id="op_tambah_x26" name="op_tambah_t26" placeholder="Nama Perguruan Tinggi" value="" required>
                                    </div>
                                    <div class="col-sm-3">
                                        &nbsp;*
                                    </div>
                                </div>
                            </div>
                            <!-- satu form -->
                            <!-- mulai form -->
                            <div class="form-group">
                                <div class="row">
                                    <label class="col-sm-2 control-label">Alamat Pusat</label>
                                    <div class="col-sm-7">
                                        <textarea class="form-control" id="op_tambah_x27" name="op_tambah_t27" required></textarea>
                                    </div>
                                    <div class="col-sm-3">
                                        &nbsp;*
                                    </div>
                                </div>
                            </div>
                            <!-- satu form -->
                            <!-- mulai form -->
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">Status (Negeri/Swasta)</label>
                                <div class="col-sm-7">
                                    <select class="form-control" id="op_tambah_x28" name="op_tambah_t28" >
                                        <option value="Negeri">Negeri</option>
                                        <option value="Swasta">Swasta</option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                        <!-- mulai form -->
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">Nama Fakultas/Direktorat</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="op_tambah_x29" name="op_tambah_t29" placeholder="Nama Fakultas/ Direktorat" value="" required>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                        <!-- mulai form -->
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">Telepon dan faximile</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="op_tambah_x30" name="op_tambah_t30" placeholder="Telepon dan faximile" value="" required>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                        <!-- mulai form -->
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">Nama Jurusan/Program Studi</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="op_tambah_x31" name="op_tambah_t31" placeholder="Nama Program Studi" value="" required>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                       <!-- mulai form -->
                       <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">Nama Program Studi</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="op_tambah_x26b" name="op_tambah_t26b" placeholder="" value="" required>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                        <!-- mulai form -->
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">Tanggal Pendirian Program Studi</label>
                                <div class="col-sm-7">
                                    <input type="date" class="form-control" id="op_tambah_x1" name="op_tambah_t1" placeholder="Tanggal pendirian" value="" required>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                        <!-- mulai form -->
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">No SK Pendirian Program Studi</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="op_tambah_x2" name="op_tambah_t2" placeholder="Nomor pendirian" value="" required>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                        <!-- mulai form -->
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">No SK Operasional Program Studi (terakhir) </label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="op_tambah_x3" name="op_tambah_t3" placeholder="" value="" required>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                        <!-- mulai form -->
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">Tanggal pengisian data</label>
                                <div class="col-sm-7">
                                    <input type="date" class="form-control" id="op_tambah_x4" name="op_tambah_t4" placeholder="" value="" required>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                        <!-- mulai form -->
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">Peringkat Akreditasi Program Studi Diploma</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="op_tambah_x5" name="op_tambah_t5" placeholder="Tahun dan nilai, contoh : 1998, A" value="" required>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                        <!-- mulai form -->
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">Alamat website Program Studi</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="op_tambah_x6" name="op_tambah_t6" placeholder="" value="" required>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                        <!-- mulai form -->
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">Alamat email Program Studi</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="op_tambah_x7" name="op_tambah_t7" placeholder="" value="" required>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                        <!-- mulai form -->
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">Telepon Program Studi</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="op_tambah_x8" name="op_tambah_t8" placeholder="" value="" required>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                        <!-- mulai form -->
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">Visi Program Studi</label>
                                <div class="col-sm-7">
                                    <textarea class="form-control" id="op_tambah_x9" name="op_tambah_t9" placeholder=""  required></textarea>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                        <!-- mulai form -->
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">Misi Program Studi</label>
                                <div class="col-sm-7">
                                    <textarea class="form-control" id="op_tambah_x10" name="op_tambah_t10" placeholder=""  required></textarea>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                        <!-- mulai form -->
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">Tujuan Program Studi</label>
                                <div class="col-sm-7">
                                    <textarea class="form-control" id="op_tambah_x11" name="op_tambah_t11" placeholder=""  required></textarea>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                        <!-- mulai form -->
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">Kompetensi Lulusan Program Studi</label>
                                <div class="col-sm-7">
                                    <textarea class="form-control" id="op_tambah_x12" name="op_tambah_t12" placeholder=""  required></textarea>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                        <!-- mulai form -->
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">Peta matakuliah terhadap kompetensi lulusan</label>
                                <div class="col-sm-7">
                                <input type="text" class="form-control" id="op_tambah_x13" name="op_tambah_t13" placeholder="" value="" required>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                        <!-- mulai form -->
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">Kriteria pendaftaran calon mahasiswa</label>
                                <div class="col-sm-7">
                                <input type="text" class="form-control" id="op_tambah_x14" name="op_tambah_t14" placeholder="" value="" required>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                        <!-- mulai form -->
                        <div class="form-group">
                        <H6> Kriteria lulusan program studi</h6>
                            <div class="row">
                                <label class="col-sm-2 control-label">Jumlah SKS minimal</label>
                                <div class="col-sm-7">
                                <input type="text" class="form-control" id="op_tambah_x15" name="op_tambah_t15" placeholder="" value="" required>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                        <!-- mulai form -->
                        <div class="form-group">
                        <!-- <H6> Kriteria lulusan program studi</h6> -->
                            <div class="row">
                                <label class="col-sm-2 control-label">Jumlah nilai D maksimal</label>
                                <div class="col-sm-7">
                                <input type="text" class="form-control" id="op_tambah_x16" name="op_tambah_t16" placeholder="" value="" required>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                        <!-- mulai form -->
                        <div class="form-group">
                        <!-- <H6> Kriteria lulusan program studi</h6> -->
                            <div class="row">
                                <label class="col-sm-2 control-label">IPK minimal</label>
                                <div class="col-sm-7">
                                <input type="text" class="form-control" id="op_tambah_x17" name="op_tambah_t17" placeholder="" value="" required>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                        <!-- mulai form -->
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">Jumlah mahasiswa semester tahun ajaran ..</label>
                                <div class="col-sm-7">
                                <input type="text" class="form-control" id="op_tambah_x18a" name="op_tambah_t18a" placeholder="Tahun ajaran..." value="" required>
                                <br>
                                <input type="text" class="form-control" id="op_tambah_x18b" name="op_tambah_t18b" placeholder="Jumlah mahasiswa.." value="" required>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                        <!-- mulai form -->
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">Jumlah dosen tetap PS ..</label>
                                <div class="col-sm-7">
                                <input type="text" class="form-control" id="op_tambah_x19" name="op_tambah_t19" placeholder="" value="" required>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                        <!-- mulai form -->
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">Jumlah dosen tetap yang bidang keahliannya di luar PS</label>
                                <div class="col-sm-7">
                                <input type="text" class="form-control" id="op_tambah_x20" name="op_tambah_t20" placeholder="" value="" required>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                        <!-- mulai form -->
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">Jumlah tenaga kependidikan</label>
                                <div class="col-sm-7">
                                <input type="text" class="form-control" id="op_tambah_x21" name="op_tambah_t21" placeholder="" value="" required>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                        <!-- mulai form -->
                        <div class="form-group">
                        <H6> Identitas Ketua Program Studi </h6>
                            <div class="row">
                            
                                <label class="col-sm-2 control-label">Nama lengkap dengan gelar</label>
                                <div class="col-sm-7">
                                <input type="text" class="form-control" id="op_tambah_x22" name="op_tambah_t22" placeholder="" value="" required>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                        <!-- mulai form -->
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">NIP/NIDN</label>
                                <div class="col-sm-7">
                                <input type="text" class="form-control" id="op_tambah_x23" name="op_tambah_t23" placeholder="" value="" required>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                        <!-- mulai form -->
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">No Telp dan HP</label>
                                <div class="col-sm-7">
                                <input type="text" class="form-control" id="op_tambah_x24" name="op_tambah_t24" placeholder="" value="" required>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                         <!-- mulai form -->
                         <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">Tahun Pengisian</label>
                                <div class="col-sm-7">
                                <input type="text" class="form-control" id="op_tambah_x32" name="op_tambah_t32" placeholder="" value="{{date("Y")}}" required>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                         <div class="form-group">
                            <button type="submit" name="operator_submit_button" class="btn btn-primary">Simpan</button>
                        </div>

                    </div><!-- /.box-body -->
                </form>
                @elseif ($borang == 'ubah')    
                <form method="post" action="{{ route('operator_save_borang') }}"> 
                @csrf     
                <input type="hidden" name="op_tambah_hidden_t1" value="{{$profil->id}}">
                   <div class="box-body">
                        <!-- mulai form -->
                        <div class="form-group">
                                <div class="row">
                                    <label class="col-sm-2 control-label">Kode PT</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" id="op_tambah_x25" name="op_tambah_t25" placeholder="Kode PT" value="{{$profil->nama_prodi}}" required>
                                    </div>
                                    <div class="col-sm-3">
                                        &nbsp;*
                                    </div>
                                </div>
                            </div>
                            <!-- satu form -->
                            <!-- mulai form -->
                        <div class="form-group">
                                <div class="row">
                                    <label class="col-sm-2 control-label">Nama Perguruan Tinggi</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" id="op_tambah_x26" name="op_tambah_t26" placeholder="Nama Perguruan Tinggi" value="{{$profil->nama_pt}}" required>
                                    </div>
                                    <div class="col-sm-3">
                                        &nbsp;*
                                    </div>
                                </div>
                            </div>
                            <!-- satu form -->
                            <!-- mulai form -->
                            <div class="form-group">
                                <div class="row">
                                    <label class="col-sm-2 control-label">Alamat Pusat</label>
                                    <div class="col-sm-7">
                                        <textarea class="form-control" id="op_tambah_x27" name="op_tambah_t27" required>{{$profil->alamat_pt}}</textarea>
                                    </div>
                                    <div class="col-sm-3">
                                        &nbsp;*
                                    </div>
                                </div>
                            </div>
                            <!-- satu form -->
                            <!-- mulai form -->
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">Status (Negeri/Swasta)</label>
                                <div class="col-sm-7">
                                    <select class="form-control" id="op_tambah_x28" name="op_tambah_t28" > 
                                        <option @if ($profil->status_pt == "Negeri") selected="selected" @endif value="Negeri">Negeri</option>
                                        <option @if ($profil->status_pt == "Swasta") selected="selected" @endif value="Swasta">Swasta</option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                        <!-- mulai form -->
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">Nama Fakultas/Direktorat</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="op_tambah_x29" name="op_tambah_t29" placeholder="Nama Fakultas/ Direktorat" value="{{$profil->nama_fakultas}}" required>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                        <!-- mulai form -->
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">Telepon dan faximile</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="op_tambah_x30" name="op_tambah_t30" placeholder="Telepon dan faximile" value="{{$profil->telepon_pt}}" required>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                        <!-- mulai form -->
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">Nama Jurusan/Program Studi</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="op_tambah_x31" name="op_tambah_t31" placeholder="Nama Program Studi" value="{{$profil->jenjang}}" required>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                        <!-- mulai form -->
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">Nama Program Studi</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="op_tambah_x26b" name="op_tambah_t26b" placeholder="Nama Perguruan Tinggi" value="{{$profil->nama_prodi}}" required>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                        <!-- mulai form -->
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">Tanggal Pendirian Program Studi</label>
                                <div class="col-sm-7">
                                    <input type="date" class="form-control" id="op_tambah_x1" name="op_tambah_t1" placeholder="Tanggal pendirian" value="{{ date('Y-m-d',strtotime($profil->tgl_prog)) }}" required>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                        <!-- mulai form -->
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">No SK Pendirian Program Studi</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="op_tambah_x2" name="op_tambah_t2" placeholder="Nomor pendirian" value="{{$profil->no_sk}}" required>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                        <!-- mulai form -->
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">No SK Operasional Program Studi (terakhir) </label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="op_tambah_x3" name="op_tambah_t3" placeholder="" value="{{$profil->no_sk_operasional}}" required>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                        <!-- mulai form -->
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">Tanggal pengisian data</label>
                                <div class="col-sm-7">
                                    <input type="date" class="form-control" id="op_tambah_x4" name="op_tambah_t4" placeholder="" value="{{ date('Y-m-d',strtotime($profil->tgl_pengisian)) }}" required>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                        <!-- mulai form -->
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">Peringkat Akreditasi Program Studi Diploma</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="op_tambah_x5" name="op_tambah_t5" placeholder="Tahun dan nilai, contoh : 1998, A" value="{{$profil->akreditasi_prog}}" required>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                        <!-- mulai form -->
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">Alamat website Program Studi</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="op_tambah_x6" name="op_tambah_t6" placeholder="" value="{{$profil->alamat_website}}" required>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                        <!-- mulai form -->
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">Alamat email Program Studi</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="op_tambah_x7" name="op_tambah_t7" placeholder="" value="{{$profil->alamat_email}}" required>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                        <!-- mulai form -->
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">Telepon Program Studi</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="op_tambah_x8" name="op_tambah_t8" placeholder="" value="{{$profil->telepon_prog}}" required>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                        <!-- mulai form -->
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">Visi Program Studi</label>
                                <div class="col-sm-7">
                                    <textarea class="form-control" id="op_tambah_x9" name="op_tambah_t9" placeholder=""  required>{{$profil->visi_prog}}</textarea>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                        <!-- mulai form -->
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">Misi Program Studi</label>
                                <div class="col-sm-7">
                                    <textarea class="form-control" id="op_tambah_x10" name="op_tambah_t10" placeholder=""  required>{{$profil->misi_prog}}</textarea>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                        <!-- mulai form -->
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">Tujuan Program Studi</label>
                                <div class="col-sm-7">
                                    <textarea class="form-control" id="op_tambah_x11" name="op_tambah_t11" placeholder=""  required>{{$profil->tujuan_prog}}</textarea>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                        <!-- mulai form -->
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">Kompetensi Lulusan Program Studi</label>
                                <div class="col-sm-7">
                                    <textarea class="form-control" id="op_tambah_x12" name="op_tambah_t12" placeholder=""  required>{{$profil->kompetensi_prog}}</textarea>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                        <!-- mulai form -->
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">Peta matakuliah terhadap kompetensi lulusan</label>
                                <div class="col-sm-7">
                                <input type="text" class="form-control" id="op_tambah_x13" name="op_tambah_t13" placeholder="" value="{{$profil->peta_prog}}" required>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                        <!-- mulai form -->
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">Kriteria pendaftaran calon mahasiswa</label>
                                <div class="col-sm-7">
                                <input type="text" class="form-control" id="op_tambah_x14" name="op_tambah_t14" placeholder="" value="{{$profil->kriteria_mhs}}" required>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                        <!-- mulai form -->
                        <div class="form-group">
                        <H6> Kriteria lulusan program studi</h6>
                            <div class="row">
                                <label class="col-sm-2 control-label">Jumlah SKS minimal</label>
                                <div class="col-sm-7">
                                <input type="text" class="form-control" id="op_tambah_x15" name="op_tambah_t15" placeholder="" value="{{$profil->jumlah_sks_min}}" required>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                        <!-- mulai form -->
                        <div class="form-group">
                        <!-- <H6> Kriteria lulusan program studi</h6> -->
                            <div class="row">
                                <label class="col-sm-2 control-label">Jumlah nilai D maksimal</label>
                                <div class="col-sm-7">
                                <input type="text" class="form-control" id="op_tambah_x16" name="op_tambah_t16" placeholder="" value="{{$profil->jumlah_nilai_d}}" required>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                        <!-- mulai form -->
                        <div class="form-group">
                        <!-- <H6> Kriteria lulusan program studi</h6> -->
                            <div class="row">
                                <label class="col-sm-2 control-label">IPK minimal</label>
                                <div class="col-sm-7">
                                <input type="text" class="form-control" id="op_tambah_x17" name="op_tambah_t17" placeholder="" value="{{$profil->ipk_minimal}}" required>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                        <!-- mulai form -->
                        <div class="form-group">
                        <!-- <H6> Kriteria lulusan program studi</h6> -->
                            <div class="row">
                                <label class="col-sm-2 control-label">Syarat Lainnya</label>
                                <div class="col-sm-7">
                                <input type="text" class="form-control" id="op_tambah_x17a" name="op_tambah_t17a" placeholder="" value="{{$profil->syarat_lainnya}}" required>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                        <!-- mulai form -->
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">Jumlah mahasiswa semester tahun ajaran ..</label>
                                <div class="col-sm-7">
                                <input type="text" class="form-control" id="op_tambah_x18a" name="op_tambah_t18a" placeholder="Tahun ajaran..." value="{{$profil->jumlah_mhs_tahun}}" required>
                                <br>
                                <input type="text" class="form-control" id="op_tambah_x18b" name="op_tambah_t18b" placeholder="Jumlah mahasiswa.." value="{{$profil->jumlah_mhs}}" required>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                        <!-- mulai form -->
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">Jumlah dosen tetap PS ..</label>
                                <div class="col-sm-7">
                                <input type="text" class="form-control" id="op_tambah_x19" name="op_tambah_t19" placeholder="" value="{{$profil->jumlah_dosen_tetap_ps}}" required>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                        <!-- mulai form -->
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">Jumlah dosen tetap yang bidang keahliannya di luar PS</label>
                                <div class="col-sm-7">
                                <input type="text" class="form-control" id="op_tambah_x20" name="op_tambah_t20" placeholder="" value="{{$profil->jumlah_dosen_luar_ps}}" required>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                        <!-- mulai form -->
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">Jumlah tenaga kependidikan</label>
                                <div class="col-sm-7">
                                <input type="text" class="form-control" id="op_tambah_x21" name="op_tambah_t21" placeholder="" value="{{$profil->jumlah_tenaga_kependidikan}}" required>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                        <!-- mulai form -->
                        <div class="form-group">
                        <H6> Identitas Ketua Program Studi </h6>
                            <div class="row">
                            
                                <label class="col-sm-2 control-label">Nama lengkap dengan gelar</label>
                                <div class="col-sm-7">
                                <input type="text" class="form-control" id="op_tambah_x22" name="op_tambah_t22" placeholder="" value="{{$profil->identitas_nama_ketua_prod}}" required>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                        <!-- mulai form -->
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">NIP/NIDN</label>
                                <div class="col-sm-7">
                                <input type="text" class="form-control" id="op_tambah_x23" name="op_tambah_t23" placeholder="" value="{{$profil->identitas_nidn}}" required>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                        <!-- mulai form -->
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">No Telp dan HP</label>
                                <div class="col-sm-7">
                                <input type="text" class="form-control" id="op_tambah_x24" name="op_tambah_t24" placeholder="" value="{{$profil->identitas_nohp}}" required>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                         <!-- mulai form -->
                         <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 control-label">Tahun Pengisian</label>
                                <div class="col-sm-7">
                                <input type="text" class="form-control" id="op_tambah_x32" name="op_tambah_t32" placeholder="" value="{{$profil->tahun_pengisian}}" required>
                                </div>
                                <div class="col-sm-3">
                                    &nbsp;*
                                </div>
                            </div>
                        </div>
                        <!-- satu form -->
                        <div class="form-group">
                            <button type="submit" name="operator_submit_button" class="btn btn-primary">Simpan</button>
                        </div>
                    </div><!-- /.box-body -->
                    
                </form>
                @endif

            </div><!-- /.box -->
        </div><!-- /.col -->

    </div><!-- /.row -->

    

@stop


@section('css')

    <link rel="stylesheet" href="/css/admin_custom.css">

@stop


@section('js')

@stop

