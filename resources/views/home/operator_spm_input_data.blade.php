@extends('adminlte::page')


@section('title', 'SPMI dan SPME')


@section('content_header')

    <h1>SPMI dan SPME</h1>

@include('template.css_inline')

@stop


@section('content')


@if (Session::has('success'))
    <br>
        <div class="alert alert-success">
           
            {!! \Session::get('success') !!}
            
        </div>
@endif

@if (Session::has('pesan'))
    <br>
        <div class="alert alert-warning">
           
            {!! \Session::get('pesan') !!}
            
        </div>
@endif

<div class='row'>
        <div class='col-md-12'>
            <!-- Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{{$category->category_name}} - {{$standarmodel->nama_standar}}</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-4"></div>
                        <div class="col-sm-4">
                            <img class="img img-responsive" src="{{url('/') . '/assets/image/alur.png'}}">
                        </div>
                        <div class="col-sm-4"></div>
                    </div>
                    
                </div>
                <div class="box-body">
                    @if ($standaredit !== null) 
                    <form method="post" action="{{route('operator_borang_spm_view_edit_save',['category_ids'=>$standaredit->category_id,'standar_dikti_ids'=>$standaredit->standar_dikti_id,'id'=>$standaredit->id])}}">
                    @else
                    <form method="post">
                    @endif
                    <input type="hidden" value="ok2" name="sip2">
                    @csrf
                        <div class="row">
                            <div class="col-lg-4">
                                <h4>Standar</h4>
                            </div>
                            <div class="col-lg-4">
                                <h4>Indikator</h4>
                            </div>
                            <div class="col-lg-4">
                                <h4>Strategi*</h4>
                            </div>
                        </div>
                        @if ($standaredit !== null) 
                        <div class="row">
                            <div class="col-lg-4">
                                <textarea class="form-control" name="op_standar_input">{{$standaredit->s1}}</textarea>
                            </div>
                            <div class="col-lg-4">
                                <textarea class="form-control" name="op_indikator_input">{{$standaredit->s2}}</textarea>
                            </div>
                            <div class="col-lg-4">
                                <textarea class="form-control" name="op_strategi_input">{{$standaredit->s3}}</textarea>
                            </div>
                        </div>
                        @else
                        <div class="row">
                            <div class="col-lg-4">
                                <textarea class="form-control" name="op_standar_input"></textarea>
                            </div>
                            <div class="col-lg-4">
                                <textarea class="form-control" name="op_indikator_input"></textarea>
                            </div>
                            <div class="col-lg-4">
                                <textarea class="form-control" name="op_strategi_input"></textarea>
                            </div>
                        </div>
                        @endif
                        <br>
                        <div class="row">
                            <div class="col-lg-12">
                                <button type="submit" class="btn btn-block btn-lg btn-primary" name="ok_upload"> Simpan </button>
                            </div>
                        </div>
                    </form>

                    <form method="post" enctype="multipart/form-data">
                    <input type="hidden" value="ok" name="sip">

                        @csrf
                    <hr style="display: block; height: 1px; border: 0; border-top: 1px solid #ccc; margin: 1em 0;padding: 0; ">
                  
                    <div class="row">
                        <div class="col-md-12">
                        <table id="tabel_standar" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Standar</th>
                                <th>Indikator</th>
                                <th>Strategi</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php $angka = 0; @endphp
                            @foreach ($standarawal as $key => $st)
                            @php $angka = $angka + 1; @endphp
                            <tr>
                                <td>{{$angka}}</td>
                                <td>
                                    {{$st->s1}}
                                </td>
                                <td>{{$st->s2}}</td>
                                <td>{{$st->s3}}</td>
                                <td>
                                    <a href="{{route('operator_borang_spm_view_edit',['category_ids'=>$category_ids,'standar_dikti_ids'=>$standar_dikti_ids,'id'=>$st->id])}}"><i class="fa fa-pencil-square-o"></i></a> | <a href="{{route('operator_borang_spm_view_delete',['category_ids'=>$category_ids,'standar_dikti_ids'=>$standar_dikti_ids,'id'=>$st->id])}}"><i class="fa fa-trash"></i></a>
                                </td>
                                <!-- <td><textarea></textarea></td> -->
                            </tr>
                            @endforeach
                          
                            </tbody>
                            
                        </table>
                        </div>
                    </div>
                  
                       
                 

                    </form>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->

    </div><!-- /.row -->

    

@stop


@section('css')

    <!-- <link rel="stylesheet" href="/css/admin_custom.css"> -->

@stop


@section('js')

<script>
    $(document).ready(function() {
        $('#tabel_standar').DataTable();
    } );

  
</script>

@stop

