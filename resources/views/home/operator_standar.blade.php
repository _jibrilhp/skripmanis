@php use \App\Http\Controllers\HomeController; @endphp
@extends('adminlte::page')


@section('title', 'SPMI dan SPME')


@section('content_header')

    <h1>SPMI dan SPME</h1>


@stop


@section('content')


@if (Session::has('success'))
    <br>
        <div class="alert alert-success">
           
            {!! \Session::get('success') !!}
            
        </div>
@endif

@if (Session::has('pesan'))
    <br>
        <div class="alert alert-warning">
           
            {!! \Session::get('pesan') !!}
            
        </div>
@endif


<div class='row'>
        <div class='col-md-12'>
            <!-- Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{{$category->category_name}} - {{ $profilmodel->nama_prodi }} ({{ $profilmodel->tahun_pengisian }})</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-4"></div>
                        <div class="col-sm-4">
                            <img class="img img-responsive" src="{{url('/') . '/assets/image/alur.png'}}">
                        </div>
                        <div class="col-sm-4"></div>
                    </div>
                    
                </div>
                <div class="box-body">
                    <!-- <form method="post" enctype="multipart/form-data"> -->
                    <input type="hidden" value="ok" name="sip">

                        @csrf
                    <hr style="display: block; height: 1px; border: 0; border-top: 1px solid #ccc; margin: 1em 0;padding: 0; ">
                  
                    <div class="row">
                        <div class="col-md-12">
                        <table id="tabel_standar" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Standar</th>
                                <th>Isi Pernyataan Standar</th>
                                <th>Upload Standar</th>
                                <th>Download </th>
                            </tr>
                            </thead>
                            <tbody>
                            @php $angka = 0; @endphp
                            @foreach ($standarmodel as $key => $st)
                            @php $angka = $angka + 1; @endphp
                            <tr>
                                <td>{{$angka}}</td>
                                <td>
                                <div class="panel-group">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" href="#collapse{{$angka}}">{{$st->nama_standar}}</a>
                                        </h4>
                                        </div>
                                        <!-- <div id="collapse{{$angka}}" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Standar
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Indikator
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Pencapaian
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Mid Line
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Base Line
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                                                </div>
                                            </div>
                                        </div> -->
                                       
                                    </div>
                                </div> 
                                </td>
                                <td><a class="btn btn-primary" href="{{route('operator_borang_spm_view',['category_ids'=>$category_ids_id,'standar_dikti_ids'=>$st->id])}}"><i class="fa fa-plus-square-o" aria-hidden="true"></i> Isi</a></td>
                                <td>

                                <form method="post" enctype="multipart/form-data" action="{{route('operator_standar_mutu_berkas',['category_ids'=>$category_ids_id])}}">
                                    @csrf
                                        <input type="hidden" name="op_file_upload_standar_id" value="{{$st->id}}">
                                        <input type="file" name="op_file_upload_standar_{{$st->id}}"><button type="submit" name="sip" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button> 
                                        

                                    @if ($hasil = HomeController::operator_standar_mutu_check_file($st->id))
                                        <hr>
                                        <a href="{{asset('/unggah/unggah_spm')}}/{{$hasil->file_standar}}" class="btn btn-primary"><i class="fa fa-download" aria-hidden="true"></i> Download</a> 
                                    @endif
                                </form>

                                </td>
                                
                                <td><a href="{{route('operator_standar_mutu_download',['/'=>$st->id,'id'=>$category->id])}}" class="btn btn-primary"><i class="fa fa-download" aria-hidden="true"></i> Download XLS    </a></td>
                                <!-- <td><textarea></textarea></td> -->
                            </tr>
                            @endforeach
                          
                            </tbody>
                            
                        </table>
                        </div>
                    </div>
                  
                       
                 

                    <!-- </form> -->
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->

    </div><!-- /.row -->

    

@stop


@section('css')

    <!-- <link rel="stylesheet" href="/css/admin_custom.css"> -->

@stop


@section('js')

<script>
    $(document).ready(function() {
        $('#tabel_standar').DataTable();
    } );

  
</script>

@stop

