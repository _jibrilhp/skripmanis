@extends('adminlte::page')


@section('title', 'SPMI dan SPME')


@section('content_header')

    <h1>SPMI dan SPME</h1>

@stop


@section('content')


@if (Session::has('success'))
    <br>
        <div class="alert alert-success">
           
            {!! \Session::get('success') !!}
            
        </div>
@endif

@if (Session::has('pesan'))
    <br>
        <div class="alert alert-warning">
           
            {!! \Session::get('pesan') !!}
            
        </div>
@endif

<div class='row'>
        <div class='col-md-12'>
            <!-- Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <div class="row">
                        <div class="col-sm-4">
                            <h3 class="box-title">Borang Profil diri</h3>
                        </div>
                        <div class="col-sm-4">
                            <h3 class="box-title">{{$pilih}}</h3>
                        </div>
                        <div class="col-sm-4">
                            <h3 class="box-title">Borang Profil diri</h3>
                        </div>
                       
                    </div>
                    
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        <!-- <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button> -->
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <form method="post">
                        @csrf
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <h3>Melihat/Mengubah profil SPMI</h3>
                                </div>
                                <div class="form-group">
                                    <select name="op_borang_select1" class="form-control">
                                            <option value="kosong">--- Pilih ---</option>
                                            @foreach ($profil as $p)
                                            <option value="{{$p->id}}">{{$p->nama_prodi}}</option>
                                            @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                        <button type="submit" class="btn btn-lg btn-primary"> Lihat </button>
                                </div>
                            </div>
                            <div class="col-md-2"></div>
                        </form>
                    </div>
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <h3>Menambahkan profil diri SPMI</h3>
                            </div>
                            <div class="form-group">
                                <a target="_blank" href="{{route('operator_download_borang')}}" class="btn btn-primary"><i class="fa fa-plus"></i> Mengunduh template import data Profil diri</a>
                                <hr>
                                <a target="_blank" href="{{route('operator_upload_borang')}}" class="btn btn-primary"><i class="fa fa-upload"></i> Impor data Profil diri</a>
                            </div>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->

    </div><!-- /.row -->

    

@stop


@section('css')

    <link rel="stylesheet" href="/css/admin_custom.css">

@stop


@section('js')

@stop

