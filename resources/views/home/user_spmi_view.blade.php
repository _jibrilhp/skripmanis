@extends('adminlte::page')


@section('title', 'SPMI dan SPME')


@section('content_header')

    <h1>SPMI dan SPME</h1>

@stop


@section('content')


@if (Session::has('success'))
    <br>
        <div class="alert alert-success">
           
            {!! \Session::get('success') !!}
            
        </div>
@endif

@if (Session::has('pesan'))
    <br>
        <div class="alert alert-warning">
           
            {!! \Session::get('pesan') !!}
            
        </div>
@endif

<div class='row'>
        <div class='col-md-12'>
            <!-- Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{{$judul}}</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
              
                <div class="box-body">
                    <form method="post"  enctype="multipart/form-data" class="form-horizontal">
                    <input type="hidden" value="ok" name="sip">

                        @csrf
                    <!-- <hr style="display: block; height: 1px; border: 0; border-top: 1px solid #ccc; margin: 1em 0;padding: 0; "> -->
                  
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="inputUsername4" class="col-sm-2 control-label">Username</label>
                                <div class="col-sm-10">
                                    <input name="user_username_value" class="form-control" id="inputUsername4" placeholder="Silahkan masukan username" value="{{$profil->username}}">
                                </div>
                            </div>  

                            <div class="form-group">
                                <label for="inputFullname4" class="col-sm-2 control-label">Nama Lengkap</label>
                                <div class="col-sm-10">
                                    <input name="user_fullname_value" class="form-control" id="inputFullname4" placeholder="Silahkan masukan nama lengkap" value="{{$profil->name}}">
                                </div>
                            </div>
                      
                            <div class="form-group">
                                <label for="inputPasswordname4" class="col-sm-2 control-label">Password</label>
                                <div class="col-sm-10">
                                    <input type="password" name="user_password_value" class="form-control" id="inputPasswordname4" placeholder="Kosongkan field ini jika tidak ingin mengubah password" value="">
                                    <h6>Kosongkan field ini jika tidak ingin mengubah password</h6>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail4" class="col-sm-2 control-label">Email</label>
                                <div class="col-sm-10">
                                    <input name="user_email_value" class="form-control" id="inputEmail4" placeholder="Silahkan masukan email anda" value="{{$profil->email}}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputTelepon4" class="col-sm-2 control-label">Telepon</label>
                                <div class="col-sm-10">
                                    <input name="user_telepon_value" class="form-control" id="inputTelepon4" placeholder="Silahkan masukan telepon anda" value="{{$profil->telepon}}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputJabatan4" class="col-sm-2 control-label">Jabatan</label>
                                <div class="col-sm-10">
                                    <input name="user_jabatan_value" class="form-control" id="inputJabatan4" placeholder="Silahkan masukan jabatan" value="{{$profil->jabatan}}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputFoto4" class="col-sm-2 control-label">Foto</label>
                                <div class="col-sm-10">
                                    @if ($profil->foto !== null)
                                        <a href="{{url('/') . '/unggah/unggah_foto/' . $profil->foto }}"><img class="img-responsive" width="200px" height="500px" src="{{url('/') . '/unggah/unggah_foto/' . $profil->foto }}"></a>
                                    @endif
                                    <br>
                                    <input type="file" name="user_foto_value" id="inputFoto4">
                                    <br>
                                    
                                    <h6> Foto akan berubah jika mengupload file baru </h6>
                                </div>
                            </div>
                            @if ($judul == "Edit User")  
                            <div class="form-group">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-10">
                                    <h5> Pengaturan </h5>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputLevel4" class="col-sm-2 control-label">Level User</label>
                                <div class="col-sm-10">
                                    <select name="user_level_value" class="form-control" id="inputLevel4" >
                                        <option @if ($profil->level == 0) selected="selected" @endif value="0"> Super Admin </option>
                                        <option @if ($profil->level == 1) selected="selected" @endif value="1"> Admin </option>
                                        <option @if ($profil->level == 2) selected="selected" @endif value="2"> User </option>
                                    </select>
                                </div>
                            </div>
                        
                            <div class="form-group">
                                <label for="inputFakultas4" class="col-sm-2 control-label">Bagian</label>
                                <div class="col-sm-10">
                                    <select name="user_fakultas_value" class="form-control" id="inputFakultas4">
                                        <option value="-">Pilih Fakultas/Departemen/Prodi Anda</option>
                                        @foreach ($fakultas as $f)
                                        <option @if ($profil->fakultas_id == $f->id) selected="selected" @endif value="{{$f->id}}">{{$f->nama_bagian}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputProfilModel4" class="col-sm-2 control-label">Profil Prodi</label>
                                <div class="col-sm-10">
                                    <select name="user_prodi_value" class="form-control" id="inputProfilModel4">
                                        <option value="-">Pilih Profil Prodi Anda</option>
                                        @foreach ($profil_prodi as $p)
                                        <option @if ($profil->profil_id == $p->id) selected="selected" @endif value="{{$p->id}}">{{$p->nama_prodi}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            @endif
                            
                    <button type="submit" class="btn btn-primary"> Simpan </button>
                    <!-- end of div -->
                        </div>
                    </div>
                    <!-- end of div -->
                       
                 

                    </form>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->

    </div><!-- /.row -->

    

@stop


@section('css')

    <!-- <link rel="stylesheet" href="/css/admin_custom.css"> -->

@stop


@section('js')

<script>
  
</script>

@stop

