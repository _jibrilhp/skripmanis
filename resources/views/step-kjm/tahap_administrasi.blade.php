@php 
    use App\Http\Controllers\AuditorController;
@endphp
@extends('adminlte::page')


@section('title', 'SPMI dan SPME')


@section('content_header')

    <h1>SPMI dan SPME</h1>

@stop


@section('content')


@if (Session::has('success'))
    <br>
        <div class="alert alert-success">
           
            {!! \Session::get('success') !!}
            
        </div>
@endif

@if (Session::has('pesan'))
    <br>
        <div class="alert alert-warning">
           
            {!! \Session::get('pesan') !!}
            
        </div>
@endif

<div class='row'>
        <div class='col-md-12'>
            <!-- Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Administrasi</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- <div class="box-body">
                    <div class="row">
                        <div class="col-sm-4"></div>
                        <div class="col-sm-4">
                            <img class="img img-responsive" src="{{url('/') . '/assets/image/alur.png'}}">
                        </div>
                        <div class="col-sm-4"></div>
                    </div>
                    
                </div> -->
                <div class="box-body">
                    <form method="post" enctype="multipart/form-data">
                    <input type="hidden" value="ok" name="sip">

                        @csrf
                    <hr style="display: block; height: 1px; border: 0; border-top: 1px solid #ccc; margin: 1em 0;padding: 0; ">
                  
                    <div class="row">
                        <div class="col-md-12">
                        <table id="tabel_standar" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Nama Berkas</th>
                                <th>Tanggal Berkas</th>
                                <th>Unduh Berkas</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($berkas as $bs)
                            <tr>
                              
                                <td>{{$bs->adm_nama}}</td>
                                <td>{{$bs->adm_tanggal}}</td>
                                <td><a href="{{ url('/') . '/unggah/unggah_administrasi/' . $bs->adm_file}}">Unduh</a></td>
                            </tr>
                            @endforeach
                          
                            </tbody>
                            
                        </table>
                        </div>
                    </div>
                  
                       
                 

                    </form>
                </div><!-- /.box-body -->

                <div class="box-body">
                    <form method="post" enctype="multipart/form-data">
                        <input type="hidden" value="ok" name="sip">

                            @csrf
                        <hr style="display: block; height: 1px; border: 0; border-top: 1px solid #ccc; margin: 1em 0;padding: 0; ">
                    
                        <div class="row">
                            <div class="col-md-4">
                                Upload berkas
                            </div>
                            <div class="col-md-4">
                                <!-- <input type="hidden" class="form-control" name="ptk_upload_jadwal_id" value=""> -->

                                <input type="text" class="form-control" name="adm_upload_name" value="" placeholder="Nama berkas">
                                <hr>
                                <input type="date" class="form-control" name="adm_upload_tgl" value="" >
                                <hr>
                                <input type="file" class="form-control" name="adm_upload_data" multiple>
                                <hr>
                                <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                            </div>
                        </div>
                    </form>

                </div>

            </div><!-- /.box -->
        </div><!-- /.col -->

    </div><!-- /.row -->

    

@stop


@section('css')

    <!-- <link rel="stylesheet" href="/css/admin_custom.css"> -->

@stop


@section('js')

<script>
    $(document).ready(function() {
        $('#tabel_standar').DataTable();
    } );

  
</script>

@stop

