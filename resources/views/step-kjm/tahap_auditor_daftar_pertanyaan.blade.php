@extends('adminlte::page')


@section('title', 'SPMI dan SPME')


@section('content_header')

    <h1>Audit Mutu Internal</h1>

@stop


@section('content')


@if (Session::has('success'))
    <br>
        <div class="alert alert-success">
           
            {!! \Session::get('success') !!}
            
        </div>
@endif

@if (Session::has('pesan'))
    <br>
        <div class="alert alert-warning">
           
            {!! \Session::get('pesan') !!}
            
        </div>
@endif
    <div class='row'>
        <!-- daftar pertanyaan -->
        <div class='col-md-12'>
                <!-- Box -->
                    <div class="box primary-box"> 
                    <!-- <div class="box collapsed-box"> -->
                        <div class="box-header with-border">
                            <h3 class="box-title">Daftar Pertanyaan</h3>
                            <div class="box-tools pull-right">
                                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                        
                        <div class="box-body">

                        <div class="row">
                            <div class="col-md-12">
                                <!-- <hr style="display: block; height: 1px; border: 0; border-top: 1px solid #ccc; margin: 1em 0;padding: 0; "> -->
                                <form class="form-horizontal" id="pertanyaan-audit-form-0">

                                    <input type="hidden" id="pertanyaan_edit_jadwal_id_sip" name="pertanyaan_edit_jadwal_id">
                                    <input type="hidden" id="pertanyaan_tanggalan" name="pertanyaan_tanggalan">

                                    <div class="box-body">

                                        <div class="form-group">
                                            <label for="inputReferensi3" class="col-sm-2 control-label">Pilihan tanggal audit</label>

                                            <div class="col-sm-10">
                                                <select id="pertanyaan_edit_tanggal" class="form-control">
                                                        <option value="---">-- Pilih jadwal --</option>
                                                    @foreach ($audit_tanggal as $at)
                                                        
                                                        <option value="{{$at->pen_jadwal_tanggal}}">{{$at->pen_jadwal_tanggal}}</option>
                                                    @endforeach
                                                </select>    
                                            </div>
                                            
                                        </div>

                                        <div class="form-group">
                                            <label for="inputReferensi3" class="col-sm-2 control-label"></label>

                                            <div class="col-sm-10">
                                                <table class="table table-bordered display">
                                                <tbody>
                                                    <tr>
                                                        <td>Hari/Tanggal</td>
                                                        <td id="tampil_hari_tanggal">1</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                        <p>Pukul</p>
                                                        </td>
                                                        <td>
                                                        <p id="tampil_pukul">2</p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                        <p>Fak/Prodi</p>
                                                        </td>
                                                        <td>
                                                        <p id="tampil_fakultas">3</p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                        <p>Lingkup Audit</p>
                                                        </td>
                                                        <td>
                                                        <select name="pertanyaan_edit_lingkup" id="tampil_lingkup_audit">
                                                            @if ($audit_pertanyaan != null)
                                                                @foreach ($audit_pertanyaan as $ap)
                                                                    <option value="{{$ap->pen_lingkup_id}}">{{$ap->pen_lingkup_isian}}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            
                                        </div>

                                        <div class="form-group">
                                            <label for="inputReferensi3" class="col-sm-2 control-label">Auditor</label>

                                            <div class="col-sm-10">
                                                <table id="tampil_nama_auditor" class="table table-bordered display">
                                                    <thead>
                                                        <th>Nama Auditor</th>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="inputReferensi3" class="col-sm-2 control-label">Auditee</label>

                                            <div class="col-sm-10">
                                                <table id="tampil_nama_auditee" class="table table-bordered display">
                                                    <thead>
                                                        <th>Nama Auditee</th>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <h3>Buat daftar pertanyaan</h3>
                                        </div>

                                        <div class="form-group">
                                            <label for="inputReferensi3" class="col-sm-2 control-label">Referensi (Butir Mutu)</label>

                                            <div class="col-sm-10">
                                                <textarea name="op_pertanyaan_referensi" class="form-control" id="inputReferensi3" placeholder="Masukkan referensi (butir mutu) yang diinginkan"></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="inputHasilObservasi3" class="col-sm-2 control-label">Hasil Observasi</label>

                                            <div class="col-sm-10">
                                                <textarea  name="op_pertanyaan_hasilobs" class="form-control" id="inputHasilObservasi3" ></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="inputHasilObsCeklist3" class="col-sm-2 control-label">Ceklis</label>
                                            

                                            <div class="col-sm-10">
                                                <input type="checkbox" name="op_pertanyaan_ya" id="inputHasilObsCeklist3_ya" value="ok"> Ya <br>
                                                <input type="checkbox" name="op_pertanyaan_tidak" id="inputHasilObsCeklist3_tidak" value="ok"> Tidak
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="inputCatKhusus3" class="col-sm-2 control-label">Catatan khusus</label>

                                            <div class="col-sm-10">
                                                <textarea  name="op_pertanyaan_catkhusus" class="form-control" id="inputCatKhusus3" ></textarea>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label for="submitHasilObservasi" class="col-sm-2 control-label"></label>

                                            <div class="col-sm-10">
                                                <button type="submit" name="op_submit_pertanyaan" id="submitHasilObservasi" class="btn btn-primary"> Simpan </button>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="inputIsianReferensi3" class="col-sm-2 control-label">Isian Pertanyaan</label>

                                        
                                            <div class="col-sm-10">
                                            
                                                <table  id="tabel_pertanyaan_referensi" class="table table-bordered display">
                                                    <thead>
                                                    <tr>
                                                        <th>Referensi (butir mutu)</th>
                                                        <th>Hasil Observasi</th>
                                                        <th>Y</th>
                                                        <th>T</th>
                                                        <th>Catatan Khusus</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                    
                                                </table>
                                                
                                            </div> <!-- /.col-sm-10 -->
                                            
                                        </div>

                                        

                                    </div>
                                    <!-- /.box-body -->
                                
                                </form>
                            </div>
                        </div>

                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                
        </div><!-- /.col -->
    </div><!-- /.row -->

        <!-- modal form edit pertanyaan -->
        <div class="modal" id="modal_edit_pertanyaan">
            <div class="modal-dialog">
                <div class="modal-content">
                <form method="post" id="pertanyaan-edit-form-0">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title">Data pertanyaan </h4>

                            
                    </div>
                    
                        <div class="modal-body">
                           
                           

                            <input type="hidden" id="pertanyaan_edit_idx" name="pertanyaan_edit_ids">
                            <p>Referensi (butir mutu)</p>
                            <textarea id="pertanyaan_edit_ref" name="pertanyaan_edit_butir_mutu" class="form-control"></textarea>

                            <hr>
                            <p>Hasil observasi</p>
                            <textarea id="pertanyaan_edit_hasilobs" name="pertanyaan_edit_hasilobs" class="form-control"></textarea>

                            <hr>

                            <p>Ceklis</p>
                            <input type="checkbox" name="pertanyaan_edit_cek_ya" id="pertanyaan_edit_ceklist_ya" value="1" > Ya
                            <input type="checkbox" name="pertanyaan_edit_cek_tidak" id="pertanyaan_edit_ceklist_tidak" value="2" > Tidak

                            <hr>

                            <p>Catatan khusus</p>
                            <textarea id="pertanyaan_edit_catatan" name="pertanyaan_edit_catatan_khusus" class="form-control"></textarea>

                            
                        </div>
                    
                    <div class="modal-footer">
                        
                        <button type="submit" class="btn btn-primary" >Simpan</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>

    

@stop


@section('css')

    <!-- <link rel="stylesheet" href="/css/admin_custom.css"> -->

@stop


@section('js')

<script>
     //untuk bulir pertanyaan
     function hapus_pertanyaan(pertanyaan_id)
    {
        var tabel_pertanyaan = $('#tabel_pertanyaan_referensi').DataTable();
        bootbox.confirm({
            title   : '<i class="fa fa-exclamation-triangle"></i> Konfirmasi',
            message : "Apakah anda ingin menghapus poin tujuan ini?",
            buttons : {
                confirm : {
                    label : 'Ya',
                    className: 'btn-primary'
                },
                cancel : {
                    label : 'Tidak',
                    className : 'btn-danger'
                }
            },
            callback : function (result) {
                if (result)
                {
                    //  simulate click the link
                    $.ajax({
                        type: "POST",
                        url: "{{route('audit_borang_pertanyaan_delete',['audit_borang_id'=>'1'])}}",   
                        data: {"pertanyaan_id":pertanyaan_id},
                        success: function (result) {
                            tabel_pertanyaan.ajax.reload();
                        }
                    });
                }
            }
        });
    }

    $(document).ready(function() {
        
        var audit_tanggal = $("#pertanyaan_edit_tanggal").val();
        var jadwal_id     = $("#pertanyaan_edit_jadwal_id_sip").val();
        
        var tabel_auditor = $('#tampil_nama_auditor').DataTable({
            "bProcessing": true,
            "bServerSide": true,
            "ajax": {
                "url": "{{route('AuditorListView')}}/?audit_tanggal=" + audit_tanggal + "&jadwal_id=" + jadwal_id ,
            },
            "columns": [
                { "data": "name" },
            ]
        });

        var tabel_auditee = $('#tampil_nama_auditee').DataTable({
            "bProcessing": true,
            "bServerSide": true,
            "ajax": {
                "url": "{{route('AuditeeListView')}}/?audit_tanggal=" + audit_tanggal + "&jadwal_id=" + jadwal_id,
            },
            "columns": [
                { "data": "name" },
            ]
        });

        var tabel_pertanyaan = $('#tabel_pertanyaan_referensi').DataTable({
            "bProcessing": true,
            "bServerSide": true,
            "ajax": {
                "url": "{{route('AuditorDaftarPertanyaanView')}}/?audit_tanggal=" + audit_tanggal,
                "type": "POST"    
            },
            "columns": [
                { "data": "referensi_butir_mutu" },
                { "data": "hasil_observasi" },
                { 
                    "mData": "0", 
                    "mRender": function ( data, type, full ) {
                        if (full.yes_no_check == '1') {
                            return '<i class="fa fa-check" aria-hidden="true"></i>';
                        } else {
                            return '-';
                        }
                    }
                },
                { 
                    "mData": "0", 
                    "mRender": function ( data, type, full ) {
                        if (full.yes_no_check == '2') {
                            return '<i class="fa fa-check" aria-hidden="true"></i>';
                        } else {
                            return '-';
                        }
                    }
                },
                { "data": "catatan_khusus" },
                { 
                    "mData": "0", 
                    "mRender": function ( data, type, full ) {
                        return '<a href="#modal_edit_pertanyaan" data-toggle="modal" data-edit-pertanyaan-referensi="'+ full.referensi_butir_mutu +'" data-edit-pertanyaan-hasilobs="'+ full.hasil_observasi +'" data-edit-pertanyaan-yesnocheck="'+ full.yes_no_check + '" data-edit-pertanyaan-catatan-khusus="'+ full.catatan_khusus +'" data-edit-pertanyaanid="'+ full.id +'"><i class="fa fa-edit"></i></a> | <a class="delete-pertanyaan-data" onclick="hapus_pertanyaan(\''+ full.id + '\')" data-confirmation="Apakah anda ingin menghapus tujuan ini?" data-delete-id="'+ full.id +'"><i class="fa fa-trash" aria-hidden="true"></i></a>';
                    }
                }
            ]

        });
        
        $("#tampil_lingkup_audit").change(function() {
                var audit_tanggal1 = $("#pertanyaan_edit_tanggal").val();
                var jadwal_id     = $("#pertanyaan_edit_jadwal_id_sip").val();
                var lingkup_id = $("#tampil_lingkup_audit").val();

                tabel_pertanyaan.ajax.url("{{route('AuditorDaftarPertanyaanViewGet')}}/?audit_tanggal=" + audit_tanggal1 + "&jadwal_id=" + jadwal_id + "&lingkup_id=" + lingkup_id).load();
        });

        $("#pertanyaan_edit_tanggal").change(function() {
            //console.log($("#pertanyaan_edit_tanggal").val());

            var audit_tanggal = $("#pertanyaan_edit_tanggal").val();
            var jadwal_id     = $("#pertanyaan_edit_jadwal_id_sip").val();

            //mengambil data untuk si tabel2 dulu dah
            $.post("{{route('AuditorJadwalView')}}",
            {
                audit_tanggal: audit_tanggal,
                jadwal_id: jadwal_id
            },
            function(data,status){
                if (status != 404) {
                    $("#tampil_hari_tanggal").text(data.pen_jadwal_tanggal);
                    $("#tampil_pukul").text(data.pen_jadwal_pukul);
                    $("#tampil_fakultas").text(data.jenjang);
                    //$("#tampil_lingkup_audit").val(data.pen_lingkup_isian);

                    $("#pertanyaan_edit_jadwal_id_sip").val(data.pen_jadwal_id);
                    $("#pertanyaan_tanggalan").val(data.pen_jadwal_tanggal);

                    
                    var audit_tanggal1 = $("#pertanyaan_edit_tanggal").val();
                    var lingkup_id = $("#tampil_lingkup_audit").val();
                
                    tabel_auditee.ajax.url("{{route('AuditeeListView')}}/?audit_tanggal=" + audit_tanggal1 + "&jadwal_id=" + data.pen_jadwal_id).load();
                    tabel_auditor.ajax.url("{{route('AuditorListView')}}/?audit_tanggal=" + audit_tanggal1 + "&jadwal_id=" + data.pen_jadwal_id).load();
                    tabel_pertanyaan.ajax.url("{{route('AuditorDaftarPertanyaanViewGet')}}/?audit_tanggal=" + audit_tanggal1 + "&jadwal_id=" + data.pen_jadwal_id + "&lingkup_id=" + lingkup_id).load();


                } else {
                    $("#tampil_hari_tanggal").text("-");
                    $("#tampil_pukul").text("-");
                    $("#tampil_fakultas").text("-");
                    $("#tampil_lingkup_audit").text("-");
                }

                
                
            });

            
        });



        $('#pertanyaan-audit-form-0').submit(function(e) {
            e.preventDefault();
            var data_pertanyaan_audit_form = $('#pertanyaan-audit-form-0').serializeArray();

            $.ajax({
                type: "POST",
                url: "{{route('audit_borang_pertanyaan_create')}}",   
                data: data_pertanyaan_audit_form,
                success: function (result) {
                    $('#inputReferensi3').val('');
                    $('#inputHasilObservasi3').val('');
                    $('#inputCatKhusus3').val('');
                    $('#inputHasilObsCeklist3_ya').prop('checked',false);
                    $('#inputHasilObsCeklist3_tidak').prop('checked',false);

                    tabel_pertanyaan.ajax.reload();
                }
            });

        });

        $('#pertanyaan-edit-form-0').submit(function(e) {
            e.preventDefault();

            var data_form_pertanyaan = $('#pertanyaan-edit-form-0').serializeArray();

            $.ajax({
                type: "POST",
                url: "{{route('audit_borang_pertanyaan_edit',['audit_borang_id'=>'1'])}}",   
                data: data_form_pertanyaan,
                success: function (result) {
                    $('#modal_edit_pertanyaan').modal('toggle'); 
                    tabel_pertanyaan.ajax.reload();
                }
            });

        });

        $('#modal_edit_pertanyaan').on('show.bs.modal', function(e) {
            
            var referensi_edit_value = $(e.relatedTarget).data('edit-pertanyaan-referensi');
            var hasilobs_edit_value =  $(e.relatedTarget).data('edit-pertanyaan-hasilobs');
            var yesnocheck_edit_value =  $(e.relatedTarget).data('edit-pertanyaan-yesnocheck');
            var catkhusus_edit_value =  $(e.relatedTarget).data('edit-pertanyaan-catatan-khusus');
            var pertanyaan_edit_id = $(e.relatedTarget).data('edit-pertanyaanid');

            $(e.currentTarget).find('#pertanyaan_edit_idx').val(pertanyaan_edit_id);
            $(e.currentTarget).find('#pertanyaan_edit_ref').val(referensi_edit_value);
            $(e.currentTarget).find('#pertanyaan_edit_hasilobs').val(hasilobs_edit_value);
            $(e.currentTarget).find('#pertanyaan_edit_catatan').val(catkhusus_edit_value);

                if (yesnocheck_edit_value == 1) {
                    $(e.currentTarget).find('#pertanyaan_edit_ceklist_ya').prop('checked',true);
                } else if (yesnocheck_edit_value == 2) {
                    $(e.currentTarget).find('#pertanyaan_edit_ceklist_tidak').prop('checked',true);
                }
          

        });

    });
</script>

@stop


