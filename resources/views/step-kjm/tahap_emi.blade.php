@php
    use App\Http\Controllers\AuditorController;
@endphp
@extends('adminlte::page')


@section('title', 'SPMI dan SPME')


@section('content_header')

    <h1>SPMI dan SPME</h1>

@stop


@section('content')


@if (Session::has('success'))
    <br>
        <div class="alert alert-success">
           
            {!! \Session::get('success') !!}
            
        </div>
@endif

@if (Session::has('pesan'))
    <br>
        <div class="alert alert-warning">
           
            {!! \Session::get('pesan') !!}
            
        </div>
@endif
    <div class='row'>
     <!-- Box -->

        <div class='col-md-12'>
            <div class="box primary-box"> 
            <!-- <div class="box collapsed-box"> -->
                <div class="box-header with-border">
                    <h3 class="box-title">Membuat EMI</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                
                <div class="box-body">

                <div class="row">
                    <div class="col-md-12">
               
                        <form method="post" class="form-horizontal" id="input-emi-standar-form-0">
                            @csrf
                            <div class="box-body">

                                <div class="form-group">
                                    <label for="inputIsianReferensi3" class="col-sm-2 control-label">Jenjang</label>    

                                    <div class="col-sm-10">
                                        <select class="form-control" name="input_emi_program_studi">
                                            @foreach ($jenjang_list as $pl)
												@if ($jenjang_id == $pl->jenjang_id) 
													<option selected="selected" value="{{ $pl->jenjang_id }}">{{ $pl->jenjang_nama }}</option>
												@else
													<option value="{{ $pl->jenjang_id }}">{{ $pl->jenjang_nama }}</option>
												@endif												
                                            @endforeach
                                        </select>
                                    </div> <!-- /.col-sm-10 -->
                                    
                                </div> <!-- /.form-group -->

                                {{--<div class="form-group">
                                    <label for="inputIsianReferensi3" class="col-sm-2 control-label">Tahun Pengukuran Mutu</label>    

                                    <div class="col-sm-10">
                                            <input class="form-control"  type="text" name="input_emi_tahun_standar" placeholder="Contoh: {{ date("Y") }}" value="{{ $tahun_emi }}">
                                    </div> <!-- /.col-sm-10 -->
                                    
                                </div> <!-- /.form-group --> --}}

                                <div class="form-group">
                                    <label for="inputIsianReferensi3" class="col-sm-2 control-label">Tentang Standar</label>

                                    <div class="col-sm-10">
                                        <textarea class="form-control"  name="input_emi_tentang_standar"></textarea>
                                    </div> <!-- /.col-sm-10 -->
                                    
                                </div> <!-- /.form-group -->

                                <div class="form-group">
                                    <label for="inputIsianReferensi3" class="col-sm-2 control-label">Bobot (%)</label>    

                                    <div class="col-sm-10">
                                            <input class="form-control"  type="text" name="input_emi_bobot_terbobot" placeholder="%">
                                    </div> <!-- /.col-sm-10 -->
                                    
                                </div> <!-- /.form-group -->

                               {{-- <div class="form-group">
                                    <label for="inputIsianReferensi3" class="col-sm-2 control-label">Capaian terbobot</label>    

                                    <div class="col-sm-10">
                                            <input class="form-control"  type="text" name="input_emi_capaian_terbobot" placeholder="" >
                                    </div> <!-- /.col-sm-10 -->
                                    
                                </div> <!-- /.form-group -->
                                --}}

                                <div class="form-group">
                                    <label for="inputIsianReferensi3" class="col-sm-2 control-label"></label>    

                                    <div class="col-sm-5">
                                            <button type="submit" name="input_emi_simpan" class="btn btn-primary btn-lg"> Simpan </button>
                                    </div> <!-- /.col-sm-5 -->

                                    <div class="col-sm-5">
                                            <a href="{{ route('EmiRubrikDefault') }}" class="btn btn-warning btn-lg"> Ubah Rubrik </a>
                                    </div> <!-- /.col-sm-5 -->
                                    
                                </div> <!-- /.form-group -->

                                
                            </div>
                            <!-- /.box-body -->
                            
                        </form>
                    </div>
                </div>

                </div><!-- /.box-body -->
            </div><!-- /.box -->    
        </div><!-- /.col -->

        <div class='col-md-12'>
            <!-- Box -->     
            <div class="box box-primary">
                <div class="box-header with-border">
                        <h3 class="box-title">{{$nama_standar->nama_standar}}</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">   
                                <div class="box-body">
                                    <form method="post" enctype="multipart/form-data">
                                    @csrf
                                        <div class="row form-group">
                                            <label for="inputUsername4" class="col-sm-2 control-label">Upload Berkas EMI</label>
                                            <div class="col-sm-6">
                                                <input type="file" class="form-control" name="berkas_file"> 
                                            </div>
                                            <div class="col-sm-4">
                                                <button type="submit" class="btn btn-primary">Simpan</button>
                                            </div>
                                        </div>
                                        
                                        <div class="row form-group">
                                            <label for="inputUsername4" class="col-sm-2 control-label">Download Berkas EMI</label>
                                            <div class="col-sm-10">
                                                <a class="btn btn-warning" href="{{url('/') . '/unggah/unggah_emi/'}}/EMI UG-Prodi TInf.xls">Download berkas</a> 
                                            </div>
                                        </div>
                                    </form>

                                    <form method="post" action="{{ route('EmiSelection',['standar_emi' => $nama_standar->id]) }}">
                                    @csrf
                                        <hr>

                                        <div class="row form-group">
                                            <label for="inputIsianReferensi3" class="col-sm-2 control-label">Jenjang</label>    

                                            <div class="col-sm-10">
                                                    <select class="form-control" name="set_emi_program_studi">
                                                    @foreach ($profil_list as $pl)
                                                        @if ($pl->id == $profil_id)
                                                            <option selected="selected" value="{{ $pl->id }}">{{ $pl->jenjang }}</option>
                                                        @else
                                                            <option value="{{ $pl->id }}">{{ $pl->jenjang }}</option>
                                                        @endif
                                                    @endforeach
                                                    </select>
                                            </div> <!-- /.col-sm-10 -->
                                            
                                        </div> <!-- /.form-group -->

                                        <div class="row form-group">
                                            <label for="inputIsianReferensi3" class="col-sm-2 control-label">Tahun Pengukuran Mutu</label>    

                                            <div class="col-sm-6">
                                                    <select class="form-control" name="set_emi_tahun_standar">
                                                        @foreach ($tahun_pengukuran_list as $pl)
                                                            @if ($tahun_emi == $pl->tahun_emi)
                                                                <option selected="selected" value="{{ $pl->tahun_emi }}">{{ $pl->tahun_emi }}</option>
                                                            @else
                                                                <option value="{{ $pl->tahun_emi }}">{{ $pl->tahun_emi }}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                            </div> <!-- /.col-sm-10 --> 

                                            <div class="col-sm-4">
                                                    <button type="submit" class="btn btn-primary">Pilih</button>
                                            </div>

                                        </div> <!-- /.form-group -->


                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr>

                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">   
                                
                                    <div class="form-group">
                                        <label for="inputUsername4" class="col-sm-2 control-label">EMI {{$nama_standar->nama_standar}}</label>
                                        <div class="col-sm-10">
                                            <table style="overflow-x:auto;"  id="tabel_emi_capaian_prodi" class="table table-bordered display">
                                                <thead>
                                                    <tr>
                                                        <th>Tentang Standar</th>
                                                        <th>Bobot (%)</th>
                                                        <th>Keadaan Prodi Saat Ini</th>
                                                        <th>Nilai Capaian</th>
                                                        <th>Sebutan</th>
                                                        <th>Capaian Terbobot</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    @php
                                                        $total_nilai = 0;
                                                        $bobot_nilai = 0;
                                                        $capaian_nilai = 0;
                                                        $urutan = 0;
                                                    @endphp
                                                    @foreach ($standar_list as $sl)
                                                    <tr>
                                                    @php
                                                     $urutan++;
                                                     $au = AuditorController::kjm_step_emi_bobot_nilai($sl->isian_id);
                                                     if ($au != null) 
                                                     {
                                                         $ak = AuditorController::kjm_step_emi_keadaan_prodi($sl->isian_id,$au->nilai);
                                                     } else {
                                                         $ak = null;
                                                     }
                                                    @endphp
                                                        <td>{{$sl->emi_isian_standar}}</td>
                                                        <td>{{$sl->emi_bobot_persen}}%</td>
                                                        <td>@if ($ak != null) {{$ak->keadaan_prodi_saat_ini}} @endif </td>
                                                        <td>@if ($au != null) {{$au->nilai}} @endif | <a class="hmm_klik" data-isian-standar="{{$sl->id}}" data-toggle="modal" href="#modal_edit_keadaan_prodi"><i class="fa fa-edit"></i></a></td>
                                                        <td>@if ($au != null)
                                                            @if ($au->nilai > 6) Sangat Baik
                                                            @elseif ($au->nilai > 5) Baik
                                                            @elseif ($au->nilai > 4) Lebih dari cukup
                                                            @elseif ($au->nilai > 3) Cukup
                                                            @elseif ($au->nilai > 2) Perbaikan minor
                                                            @elseif ($au->nilai > 1) Perbaikan mayor
                                                            @elseif ($au->nilai > 0) Perbaikan menyeluruh dan  mendesak
                                                            @endif
                                                            @php $total_nilai +=  $au->nilai; @endphp
                                                            
                                                            @php $capaian_nilai += (float) $au->nilai * (float) $sl->emi_bobot_persen / 100; @endphp
                                                        @endif 
                                                        </td>
                                                        <td>@if ($au != null){{(float) $au->nilai * (float) $sl->emi_bobot_persen  / 100 }} @endif</td>  
                                                        <td></td>
                                                        @php $bobot_nilai += (float) $sl->emi_bobot_persen  / 100; @endphp
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                                <tfoot>
                                                     <tr>
                                                        <td><b>Rata-rata</b></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td>@if ($urutan > 0) {{ $total_nilai / $urutan }} @endif </td>
                                                        <td>@if ($urutan > 0) 
                                                                @if (($total_nilai / $urutan) > 0)
                                                                    @if (($total_nilai / $urutan) > 6) Sangat Baik
                                                                    @elseif (($total_nilai / $urutan) > 5) Baik
                                                                    @elseif (($total_nilai / $urutan) > 4) Lebih dari cukup
                                                                    @elseif (($total_nilai / $urutan) > 3) Cukup
                                                                    @elseif (($total_nilai / $urutan) > 2) Perbaikan minor
                                                                    @elseif (($total_nilai / $urutan) > 1) Perbaikan mayor
                                                                    @elseif (($total_nilai / $urutan) > 0) Perbaikan menyeluruh dan  mendesak
                                                                    @endif
                                                                @endif
                                                            @endif
                                                        </td>                                        
                                                     </tr>
                                                     <tr>
                                                        <td><b>Total</b></td>
                                                        <td>{{ (float) $bobot_nilai }}</td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td>{{ $capaian_nilai }}</td>
                                                        @php  $total_capaian_max = 7 * $bobot_nilai; @endphp
                                                     </tr>
                                                     <tr>
                                                        <td><b>Nilai Standar</b></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td>@if ($total_capaian_max > 0) {{$capaian_nilai / $total_capaian_max}} @endif</td>
                                                     </tr>
                                                </tfoot>
                                                
                                            </table>
                                        </div>
                                    </div>
                                   
                                
                            </div>
                        </div>
                    </div>

                    <!--end form-->
                </div>
            </div>
        </div>

        
    </div>

     <!-- modal form edit keadaan prodi -->
     <div class="modal" id="modal_edit_keadaan_prodi">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title">Ubah nilai capaian</h4>
                    </div>
                    <form method="post" id="keadaan-prodi-edit-form-0" action="{{ route('EmiUbahCapaian',['profil_id' => $profil_id,'standar_emi'=> $nama_standar->id,'tahun_emi' => $tahun_emi]) }}">
                    @csrf
                        <div class="modal-body">
                            <input type="hidden" id="keadaan_prodi_edit_id_ref" name="isian_standar">
                            <p>Bobot nilai</p>
                            <select id="keadaan_nilai_ref" name="keadaan_nilai_value" class="form-control" id="inputEmiBobotNilai3">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                            </select>
                            <hr>

                        </div>
                    
                    <div class="modal-footer">
                        
                        <button type="submit" class="btn btn-primary" >Simpan</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    

@stop


@section('css')

    <!-- <link rel="stylesheet" href="/css/admin_custom.css"> -->

@stop


@section('js')

<script>
  $(document).ready(function() {
      $(".hmm_klik").click(function() {
          var isian_standar = $(this).data('isian-standar');
          $("#keadaan_prodi_edit_id_ref").val(isian_standar);
      });
  });

  
</script>

@stop

