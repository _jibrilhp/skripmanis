@extends('adminlte::page')


@section('title', 'SPMI dan SPME')


@section('content_header')

<h1>Audit Mutu Internal</h1>

@stop


@section('content')


@if (Session::has('success'))
    <br>
        <div class="alert alert-success">
           
            {!! \Session::get('success') !!}
            
        </div>
@endif

@if (Session::has('pesan'))
    <br>
        <div class="alert alert-warning">
           
            {!! \Session::get('pesan') !!}
            
        </div>
@endif

    <div class='row'>
        <div class='col-md-12'>
            <!-- Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Pengisian Rubrik</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
              
                <div class="box-body">
                    <form id="rubrik_form_method" method="post" enctype="multipart/form-data" class="form-horizontal">
                        <input type="hidden" value="ok" name="sip">

                        @csrf
                  
                    <div class="row">
                        <div class="col-md-12">
                            

                            <div class="form-group">
                                <label for="inputUsername4" class="col-sm-2 control-label">Jenjang</label>

                                <div class="col-sm-10">
                                    <select class="form-control" name="input_rubrik_jenjang_id" >

                                        @foreach ($jenjang_list as $pl)

                                            @if ($pl->jenjang_id == $jenjang_id)
                                                <option selected="selected" value="{{ $pl->jenjang_id }}">{{ $pl->jenjang_nama }}</option>
                                            @else
                                                <option value="{{ $pl->jenjang_id }}">{{ $pl->jenjang_nama }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>  

                            {{-- 
                            <div class="form-group">
                                <label for="inputUsername4" class="col-sm-2 control-label">Tahun Pengukuran Mutu</label>

                                <div class="col-sm-10">
                                    <select class="form-control" name="set_emi_tahun_standar">
                                    @foreach ($tahun_pengukuran_list as $pl)
                                        @if ($tahun_emi == $pl->emi_tahun_standar)
                                            <option selected="selected" value="{{ $pl->emi_tahun_standar }}">{{ $pl->emi_tahun_standar }}</option>
                                        @else
                                            <option value="{{ $pl->emi_tahun_standar }}">{{ $pl->emi_tahun_standar }}</option>
                                        @endif
                                    @endforeach
                                    </select>
                                </div>
                            </div>   
                            --}}

                            <div class="form-group">
                                <label for="inputUsername4" class="col-sm-2 control-label">Standar Mutu</label>

                                <div class="col-sm-10">
                                    <select class="form-control" name="set_emi_standar_mutu">
                                        @foreach ($standar_mutu as $pl)
                                            @if ($standar_emi == $pl->id)
                                                <option selected="selected" value="{{$pl->id}}">{{ $pl->nama_standar }}</option>
                                            @else
                                                <option value="{{$pl->id}}">{{ $pl->nama_standar }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>  

                            @if ($isian_standar_mutu != null) 
                            

                            <div class="form-group">
                                <label for="inputUsername4" class="col-sm-2 control-label">Isian Standar Mutu</label>

                                <div class="col-sm-10">
                                    <select class="form-control" name="set_emi_isian_standar_mutu">
                                                <option value="0">-- Pilih --</option>
                                        @foreach ($isian_standar_mutu as $pl)
                                            @if ($isian_standar == $pl->id)
                                                <option selected="selected" value="{{$pl->id}}">{{ $pl->emi_isian_standar }}</option>
                                            @else
                                                <option value="{{$pl->id}}">{{ $pl->emi_isian_standar }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>  
                    
                            <div class="form-group">
                                <label for="inputUsername4" class="col-sm-2 control-label">Bobot Nilai</label>

                                <div class="col-sm-10"> 
                                    <select class="form-control" name="set_emi_bobot_nilai">
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                        <option>6</option>
                                        <option>7</option>
                                    </select>
                                </div>
                            </div>  

                            <div class="form-group">
                                <label for="inputUsername4" class="col-sm-2 control-label">Keadaan Prodi Ini</label>

                                <div class="col-sm-10"> 
                                    <textarea name="set_keadaan_prodi_saat_ini" class="form-control"></textarea>
                                </div>
                            </div>  

                            <div class="form-group">
                                <label for="inputUsername4" class="col-sm-2 control-label"></label>

                                <div class="col-sm-10"> 
                                    <button type="submit" name="rubrik_method_submit" class="btn btn-primary btn-lg"> Simpan </button>
                                </div>
                            </div>  

                            @endif

                    <!-- end of div -->
                        </div>
                    </div>
                    <!-- end of div -->
                       
                 

                    </form>
                </div><!-- /.box-body -->
            </div><!-- /.box -->

             <!-- Box -->
             <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Isian Rubrik</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
              
                <div class="box-body">
                    <form id="rubrik_isian_method" method="post" enctype="multipart/form-data" class="form-horizontal">
                        <input type="hidden" value="ok" name="sip">
                            @csrf
                        <div class="row">
                            <div class="col-md-12">
                                
                                <div class="form-group">
                                    <label for="inputUsername4" class="col-sm-2 control-label">Rubrik (Keadaan Prodi)</label>
                                    <div class="col-sm-10">
                                        <table style="overflow-x:auto;" id="tabel_emi_keadaan_prodi" class="table table-bordered display">
                                            <thead>
                                                <tr>
                                                    <th>Keadaan Prodi Saat Ini</th>
                                                    <th>Nilai Capaian</th>
                                                    <th></th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                            @if ($keadaan != null)
                                                @foreach ($keadaan as $kn)
                                                <tr>
                                                    <td>{{ $kn->keadaan_prodi_saat_ini }}</td>
                                                    <td>{{ $kn->emi_bobot_nilai }}</td>
                                                    <td></td>
                                                </tr>

                                                @endforeach
                                            @endif
                                            </tbody>
                                            
                                        </table>
                                    </div>
                                </div>  
         
                        <!-- end of div -->
                            </div>
                        </div>
                        <!-- end of div -->
                       
                 

                    </form>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->

    </div><!-- /.row -->

    

@stop


@section('css')

    <!-- <link rel="stylesheet" href="/css/admin_custom.css"> -->

@stop


@section('js')

<script>

    $(document).ready(function() {
        $("select[name=input_rubrik_jenjang_id]").change(function() {
             jalankan_rubrik();
        });

        $("select[name=set_emi_tahun_standar]").change(function() {
             jalankan_rubrik();
        });
        
        $("select[name=set_emi_standar_mutu]").change(function() {
             jalankan_rubrik();
        });

        $("select[name=set_emi_isian_standar_mutu]").change(function() {
             jalankan_rubrik_2();
        });

        

        function jalankan_rubrik () {
            var jenjang_id = $("select[name=input_rubrik_jenjang_id]").children("option:selected").val();
            // var tahun_standar = $("select[name=set_emi_tahun_standar]").children("option:selected").val();
            var standar_isi = $("select[name=set_emi_standar_mutu]").children("option:selected").val();

            window.location = "{{ url('/') }}/rs/emi/rubrik/"+  jenjang_id + "/" + standar_isi +"/set" ;
        }

        function jalankan_rubrik_2 () {
            var jenjang_id = $("select[name=input_rubrik_jenjang_id]").children("option:selected").val();
            // var tahun_standar = $("select[name=set_emi_tahun_standar]").children("option:selected").val();
            var standar_isi = $("select[name=set_emi_standar_mutu]").children("option:selected").val();
            var standar_rubrik = $("select[name=set_emi_isian_standar_mutu]").children("option:selected").val();

            window.location = "{{ url('/') }}/rs/emi/rubrik/"+  jenjang_id + "/" + standar_isi +"/"   + standar_rubrik +  "/set";
        }
    });
 
</script>

@stop



