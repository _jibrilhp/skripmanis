@extends('adminlte::page')


@section('title', 'SPMI dan SPME')


@section('content_header')

    <h1>Kantor Jaminan Mutu</h1>

@stop


@section('content')


@if (Session::has('success'))
    <br>
        <div class="alert alert-success">
           
            {!! \Session::get('success') !!}
            
        </div>
@endif

@if (Session::has('pesan'))
    <br>
        <div class="alert alert-warning">
           
            {!! \Session::get('pesan') !!}
            
        </div>
@endif

<div class='row'>
        <div class='col-md-12'>
            <!-- Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Pembuatan Form surat menyurat (KJM - Auditor)</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
              
                <div class="box-body">
                    <form id="auditor_anggota_post" method="post" enctype="multipart/form-data" class="form-horizontal">
                    <input type="hidden" value="ok" name="sip">

                        @csrf
                    <!-- <hr style="display: block; height: 1px; border: 0; border-top: 1px solid #ccc; margin: 1em 0;padding: 0; "> -->
                  
                    <div class="row">
                        <div class="col-md-12">

                            <div class="form-group">
                                <label for="inputUsername4" class="col-sm-2 control-label">Status form surat menyurat auditor-KJM</label>
                                <div class="col-sm-10">
                                    <table id="tabelSuratMenyurat" class="table table-bordered table-striped table-hover">
                                        <thead>
                                            <tr>    
                                            
                                                <th>Nama pemohon</th>  
                                                <th>Program Studi</th>  
                                                <th>Kirim berkas</th>  
                                                <th>Status berkas</th>
                                                <th></th>   
                                            </tr>
                                        </thead>
                                    </table>   
                                </div>
                            </div>  

                          

                   
                    <!-- end of div -->
                        </div>
                    </div>
                    <!-- end of div -->
                       
                 

                    </form>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->

    </div><!-- /.row -->

    

@stop


@section('css')

    <!-- <link rel="stylesheet" href="/css/admin_custom.css"> -->

@stop


@section('js')

<script>
    $(document).ready(function(){

        var rows_selected = [];

        var auditTable = $("#tabelAuditor").DataTable({
            "processing": true,
            "serverSide": true,
            ajax: '{{route('auditor_list_permintaan')}}',

            columns: [
                {data: 'permintaan_nama_audit'},
                {data: 'nama_prodi'},
                {   width: "100px",
                    orderable : false,
                    mRender   : function(data) {
                        return '<a href=""><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> | <a href=""><i class="fa fa-trash" aria-hidden="true"></i></a>';
                    }
                
                }
            ],
           
           

           

          
            order: [[1, 'asc']],

           

        });

    });
</script>

@stop



