@extends('adminlte::page')


@section('title', 'SPMI dan SPME')


@section('content_header')

<h1>Audit Mutu Internal</h1>

@stop


@section('content')


@if (Session::has('success'))
    <br>
        <div class="alert alert-success">
           
            {!! \Session::get('success') !!}
            
        </div>
@endif

@if (Session::has('pesan'))
    <br>
        <div class="alert alert-warning">
           
            {!! \Session::get('pesan') !!}
            
        </div>
@endif

<div class='row'>
        <div class='col-md-12'>
            <!-- Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Penentuan jadwal dan auditor</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
              
                <div class="box-body">
                    <form id="auditor_anggota_post" method="post" enctype="multipart/form-data" class="form-horizontal">
                    <input type="hidden" value="ok" name="sip">

                        @csrf
                    <!-- <hr style="display: block; height: 1px; border: 0; border-top: 1px solid #ccc; margin: 1em 0;padding: 0; "> -->
                  
                    <div class="row">
                        <div class="col-md-12">
                            

                            <div class="form-group">
                                <label for="inputUsername4" class="col-sm-2 control-label">Tabel jadwal dan auditor</label>
                                <div class="col-sm-10">
                                    <table id="tabelAuditorBersedia" class="table table-bordered table-striped table-hover">
                                        <thead>
                                            <tr>    
                                                <th>Tanggal</th>  
                                                <th>Program Studi</th>
                                                <th>Auditee</th>
                                                <th>Surat tugas Auditor</th>
                                                <th>Keterangan bersedia</th>
                                            </tr>
                                        </thead>
                                    </table>   
                                </div>
                            </div>  

                          

                   
                    <!-- end of div -->
                        </div>
                    </div>
                    <!-- end of div -->

                  
                       
                 

                    </form>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->

    </div><!-- /.row -->

    

@stop


@section('css')

    <!-- <link rel="stylesheet" href="/css/admin_custom.css"> -->

@stop


@section('js')

<script>
    $(document).ready(function(){

        var rows_selected = [];

        var auditTable = $("#tabelAuditorBersedia").DataTable({
            "processing": true,
            "serverSide": true,
            ajax: '{{route('PenentuanJadwalList')}}',

            columns: [
                {data: 'pen_jadwal_tanggal'},
                {data: 'nama_prodi'},
                {data: 'name'},
                {
                    width: "100px",
                    orderable : false,
                    mRender: function(data, type, full) {
                    return '<a href="{{url('/') }}/auditor/ami/kjm/unduh_surat_tugas/' + full.pen_jadwal_id + '">Lihat berkas</a>';
                    }
                },
                { 
                    width: "100px",
                    orderable : false,
                    mRender: function(data, type, full) {
                         if (full.pen_jadwal_list_auditor_user_kesediaan == 0) {
                             return "?";
                         } else if (full.pen_jadwal_list_auditor_user_kesediaan == 1) {
                             return "V";
                         } else if (full.pen_jadwal_list_auditor_user_kesediaan == 2) {
                             return "X";
                         }
                    }
                }
                
               
            ],
           
           

           

          
            order: [[1, 'asc']],

           

        });

    });
</script>

@stop



