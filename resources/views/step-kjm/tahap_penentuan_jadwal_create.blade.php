@extends('adminlte::page')


@section('title', 'SPMI dan SPME')


@section('content_header')

<h1>Audit Mutu Internal</h1>

@stop


@section('content')


@if (Session::has('success'))
    <br>
        <div class="alert alert-success">
           
            {!! \Session::get('success') !!}
            
        </div>
@endif

@if (Session::has('pesan'))
    <br>
        <div class="alert alert-warning">
           
            {!! \Session::get('pesan') !!}
            
        </div>
@endif

<div class='row'>
        <div class='col-md-12'>
            <!-- Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Penentuan jadwal dan auditor</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
              
                <div class="box-body">
                    <form id="auditor_anggota_post" method="post" enctype="multipart/form-data" class="form-horizontal">
                    <input type="hidden" value="ok" name="sip">

                        @csrf
                    <!-- <hr style="display: block; height: 1px; border: 0; border-top: 1px solid #ccc; margin: 1em 0;padding: 0; "> -->
                  
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="inputUsername4" class="col-sm-2 control-label">Tanggal pelaksanaan audit</label>
                                <div class="col-sm-10">
                                    <input class="form-control" type="date" name="penentuan_jadwal_tanggal_audit">
                                </div>
                            </div>  

                            <div class="form-group">
                                <label for="inputUsername4" class="col-sm-2 control-label">Pukul</label>
                                <div class="col-sm-10">
                                    <input class="form-control"  type="time" name="penentuan_jadwal_pukul_audit">
                                </div>
                            </div>  

                            <div class="form-group">
                                <label for="inputUsername4" class="col-sm-2 control-label">Prodi</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="penentuan_jadwal_prodi_audit">
                                            <option>-- Pilih Prodi --</option>
                                        @foreach ($profil_prodi as $pp)
                                            <option value="{{$pp->id}}">{{$pp->jenjang}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>  

                            <div class="form-group">
                                <label for="inputUsername4" class="col-sm-2 control-label">Lingkup audit</label>
                                <div class="col-sm-10">
                                    <input class="form-control"  type="text" name="penentuan_jadwal_lingkup_audit">
                                </div>
                            </div>  

                            <div class="form-group">
                                <label for="inputUsername4" class="col-sm-2 control-label">Auditee</label>
                                <div class="col-sm-10">
                                <table id="tabelAuditee" class="table table-bordered table-striped table-hover">
                                        <thead>
                                            <tr>    
                                                <th>Nama Auditor</th>  
                                                <th>Username</th>  
                                                <th>Email</th>  
                                                <th>Telepon</th>  
                                                <th></th>  
                                                
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>  

                            <div class="form-group">
                                <label for="inputUsername4" class="col-sm-2 control-label">Auditor</label>
                                <div class="col-sm-10">
                                    <table id="tabelAuditor" class="table table-bordered table-striped table-hover">
                                        <thead>
                                            <tr>    
                                                <th>Nama Auditor</th>  
                                                <th>Username</th>  
                                                <th>Email</th>  
                                                <th>Telepon</th>  
                                                <th></th>  
                                                <th></th>  
                                            </tr>
                                        </thead>
                                    </table> 
                                </div>
                            </div>  

                            <div class="form-group">
                                <label for="inputUsername4" class="col-sm-2 control-label">Surat Tugas</label>
                                <div class="col-sm-10">
                                    <input type="file" name="penentuan_jadwal_surat_tugas_audit" class="form-control">
                                </div>
                            </div>  

                            <div class="form-group">
                                <label for="inputUsername4" class="col-sm-2 control-label"></label>
                                <div class="col-sm-10">
                                    <button type="submit" name="penentuan_jadwal_submit" class="btn btn-primary">Simpan</button>
                                </div>
                            </div>  

                            


                    <!-- end of div -->
                        </div>
                    </div>
                    <!-- end of div -->
 
                    </form>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->

    </div><!-- /.row -->

    

@stop


@section('css')

    <!-- <link rel="stylesheet" href="/css/admin_custom.css"> -->

@stop


@section('js')

<script>
    //tabel auditee        
    var rows_auditee_selected = [];

    $(document).ready(function(){

       

        //tabel auditor
        var rows_selected = [];
        var select_selected = [];

        //tabel auditor
        var auditTable = $("#tabelAuditor").DataTable({
            "processing": true,
            "serverSide": true,
            ajax: '{{route('auditor_list_user')}}',

            columns: [
                {data: 'name'},
                {data: 'username'},
                {data: 'email'},
                {data: 'telepon'},
                {data: 'name'},
                {data: 'name'}
            ],
        
        

            'columnDefs': [
                {
                    'targets': 4,
                    'searchable': false,
                    'orderable': false,
                    'width': '1%',
                    'className': 'dt-body-center',
                    'render': function (data, type, full, meta){
                        return '<select id="status_data_anggota"><option value="0">-- Pilih Status--</option><option value="1">Ketua</option><option value="2">Anggota</option></select>';
                    }
                },
                {
                    'targets': 5,
                    'searchable': false,
                    'orderable': false,
                    'width': '1%',
                    'className': 'dt-body-center',
                    'render': function (data, type, full, meta){
                        return '<input type="checkbox">';
                    }
                }

            
            ],

            select:{
                style: 'multi'
            },

            order: [[1, 'asc']],

            'rowCallback': function(row, data, dataIndex){
                //console.log(dataIndex);
                //console.log(data['id']);
            
            var rowId = data['id'];
            

            // If row ID is in the list of selected row IDs
            if($.inArray(rowId, rows_selected) !== -1){
                $(row).find('input[type="checkbox"]').prop('checked', true);
                $(row).addClass('selected');
            } 
        }

        });

        $('#tabelAuditor tbody').on('click', 'input[type="checkbox"]', function(e){
            var $row = $(this).closest('tr');

            var $row_selection = $(this).closest('tr');
            var $td2 = $row.find('select'); 
            

           
            
            

            // Get row data
            var data = auditTable.row($row).data();

            // console.log( auditTable.row($td2[0]));
            // console.log(data);

            // Get row ID
            var rowId = data['id'];

            // console.log($td2[0].value + " dan " + rowId);

            // Determine whether row ID is in the list of selected row IDs
            var index = $.inArray(rowId, rows_selected);

            // If checkbox is checked and row ID is not in list of selected row IDs
            if(this.checked && index === -1){
                rows_selected.push(rowId);
                select_selected.push($td2[0].value);

            // Otherwise, if checkbox is not checked and row ID is in list of selected row IDs
            } else if (!this.checked && index !== -1){
                rows_selected.splice(index, 1);
                select_selected.splice(index,$td2[0].value);
            }

            if(this.checked){
                $row.addClass('selected');
                
            } else {
                $row.removeClass('selected');
                $td2[0].value =0;
            }

            // Prevent click event from propagating to parent
            e.stopPropagation();
        });

      

        //submit form
        $("#auditor_anggota_post").on('submit', function(e){
            var form = this;

                //iterasi untuk si tabel auditor

                // Iterate over all selected checkboxes
                $.each(rows_selected, function(index, rowId){
                // Create a hidden element
                $(form).append(
                    $('<input>')
                        .attr('type', 'hidden')
                        .attr('name', 'penentuan_jadwal_auditor_audit[]')
                        .val(rowId)
                    );
                });

                $.each(select_selected, function(index, rowId){
                // Create a hidden element
                $(form).append(
                    $('<input>')
                        .attr('type', 'hidden')
                        .attr('name', 'penentuan_jadwal_status_auditor_audit[]')
                        .val(rowId)
                    );
                });

                //tabel untuk iterasi si auditee
                $.each(rows_auditee_selected, function(index, rowId){
                
                $(form).append(
                    $('<input>')
                        .attr('type', 'hidden')
                        .attr('name', 'penentuan_jadwal_auditee_audit[]')
                        .val(rowId)
                    );
                });

             //e.preventDefault();
            console.log(rows_auditee_selected,select_selected,rows_selected);
    });
});
</script>

<script>
  $(document).ready(function(){

  //tabel auditee
  var auditTable = $("#tabelAuditee").DataTable({
            "processing": true,
            "serverSide": true,
            ajax: '{{route('auditor_list_user')}}',

            columns: [
                {data: 'name'},
                {data: 'username'},
                {data: 'email'},
                {data: 'telepon'},
                {data: 'name'},
                
            ],
        
        

            'columnDefs': [
              
                {
                    'targets': 4,
                    'searchable': false,
                    'orderable': false,
                    'width': '1%',
                    'className': 'dt-body-center',
                    'render': function (data, type, full, meta){
                        return '<input type="checkbox">';
                    }
                }

            
            ],

            select:{
                style: 'multi'
            },

            order: [[1, 'asc']],

            'rowCallback': function(row, data, dataIndex){
                //console.log(dataIndex);
                //console.log(data['id']);
            
            var rowId2 = data['id'];
            

            // If row ID is in the list of selected row IDs
            if($.inArray(rowId2, rows_auditee_selected) !== -1){
                $(row).find('input[type="checkbox"]').prop('checked', true);
                $(row).addClass('selected');
            } 
        }

        });

        $('#tabelAuditee tbody').on('click', 'input[type="checkbox"]', function(e){
            var $row2 = $(this).closest('tr');

                       // Get row data
            var data2 = auditTable.row($row2).data();

            var rowId2 = data2['id'];

            // Determine whether row ID is in the list of selected row IDs
            var index2 = $.inArray(rowId2, rows_auditee_selected);

            // If checkbox is checked and row ID is not in list of selected row IDs
            if(this.checked && index2 === -1){
                rows_auditee_selected.push(rowId2);
              
            // Otherwise, if checkbox is not checked and row ID is in list of selected row IDs
            } else if (!this.checked && index2 !== -1){
                rows_auditee_selected.splice(index2, 1);
              
            }

            if(this.checked){
                $row2.addClass('selected');
                
            } else {
                $row2.removeClass('selected');
              
            }

            // Prevent click event from propagating to parent
            e.stopPropagation();
        });
  });
</script>

@stop



