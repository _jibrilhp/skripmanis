@extends('adminlte::page')


@section('title', 'SPMI dan SPME')


@section('content_header')

    <h1>Audit Mutu Internal</h1>

@stop


@section('content')


@if (Session::has('success'))
    <br>
        <div class="alert alert-success">
           
            {!! \Session::get('success') !!}
            
        </div>
@endif

@if (Session::has('pesan'))
    <br>
        <div class="alert alert-warning">
           
            {!! \Session::get('pesan') !!}
            
        </div>
@endif

<div class='row'>
        <div class='col-md-12'>
            <!-- Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Permohonan Audit</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
              
                <div class="box-body">
                    <form id="auditor_anggota_post" method="post" enctype="multipart/form-data" class="form-horizontal">
                    <input type="hidden" value="ok" name="sip">

                        @csrf
                    <!-- <hr style="display: block; height: 1px; border: 0; border-top: 1px solid #ccc; margin: 1em 0;padding: 0; "> -->
                  
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="inputUsername4" class="col-sm-2 control-label">Nama Pemohon Audit</label>
                                <div class="col-sm-10">
                                    <input type="text" name="permintaan_audit_nama" class="form-control" id="inputUsername4" placeholder="Silahkan masukan nama pemohon" value="" required>
                                </div>
                            </div> 

                            <div class="form-group">
                                <label for="inputUsername4" class="col-sm-2 control-label">Program Studi</label>
                                <div class="col-sm-10">
                                    <select name="permintaan_audit_prodi" class="form-control" id="inputUsername4" required>
                                            <option> -- Pilih program studi --</option>
                                            @foreach ($profil_prodi as $pp)
                                            <option value="{{$pp->id}}">{{$pp->nama_prodi}}</option>
                                            @endforeach
                                    </select>
                                </div>
                            </div> 

                             <div class="form-group">
                                <label for="inputUsername4" class="col-sm-2 control-label">Upload surat permintaan audit</label>
                                <div class="col-sm-10">
                                    <input type="file" multiple name="permintaan_audit_upload_surat" class="form-control" id="inputUsername4" required>
                                </div>
                            </div>  

                           {{-- <div class="form-group">
                                <label for="inputUsername4" class="col-sm-2 control-label">Pilih auditor</label>
                                <div class="col-sm-10">
                                    <table id="tabelAuditor" class="table table-bordered table-striped table-hover">
                                        <thead>
                                            <tr>    
                                                <th></th>  
                                                <th>Nama Auditor</th>  
                                                <th>Username</th>  
                                                <th>Email</th>  
                                                <th>Telepon</th>  
                                            </tr>
                                        </thead>
                                    </table>   
                                </div>
                            </div>  
                            --}}

                          

                           
                    <button type="submit" class="btn btn-primary"> Simpan </button>
                    <!-- end of div -->
                        </div>
                    </div>
                    <!-- end of div -->
                       
                 

                    </form>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->

    </div><!-- /.row -->

    

@stop


@section('css')

    <!-- <link rel="stylesheet" href="/css/admin_custom.css"> -->

@stop


@section('js')

<script>
    $(document).ready(function(){

        var rows_selected = [];

        var auditTable = $("#tabelAuditor").DataTable({
            "processing": true,
            "serverSide": true,
            ajax: '{{route('auditor_list_user')}}',

            columns: [
                {data: 'name'},
                {data: 'name'},
                {data: 'username'},
                {data: 'email'},
                {data: 'telepon'}
            ],
           
           

            'columnDefs': [{
                'targets': 0,
                'searchable': false,
                'orderable': false,
                'width': '1%',
                'className': 'dt-body-center',
                'render': function (data, type, full, meta){
                    return '<input type="checkbox">';
                }
            }],

            select:{
                style: 'multi'
            },

            order: [[1, 'asc']],

            'rowCallback': function(row, data, dataIndex){
                //console.log(dataIndex);
                //console.log(data['id']);
            
            var rowId = data['id'];
            

            // If row ID is in the list of selected row IDs
            if($.inArray(rowId, rows_selected) !== -1){
                $(row).find('input[type="checkbox"]').prop('checked', true);
                $(row).addClass('selected');
            } 
        }

        });

        $('#tabelAuditor tbody').on('click', 'input[type="checkbox"]', function(e){
            var $row = $(this).closest('tr');

            // Get row data
            var data = auditTable.row($row).data();

            // Get row ID
            var rowId = data['id'];

            // Determine whether row ID is in the list of selected row IDs
            var index = $.inArray(rowId, rows_selected);

            // If checkbox is checked and row ID is not in list of selected row IDs
            if(this.checked && index === -1){
                rows_selected.push(rowId);

            // Otherwise, if checkbox is not checked and row ID is in list of selected row IDs
            } else if (!this.checked && index !== -1){
                rows_selected.splice(index, 1);
            }

            if(this.checked){
                $row.addClass('selected');
            } else {
                $row.removeClass('selected');
            }

            // Prevent click event from propagating to parent
            e.stopPropagation();
        });

        $("#auditor_anggota_post").on('submit', function(e){
            var form = this;
                // Iterate over all selected checkboxes
                $.each(rows_selected, function(index, rowId){
                // Create a hidden element
                $(form).append(
                    $('<input>')
                        .attr('type', 'hidden')
                        .attr('name', 'auditor_user_id[]')
                        .val(rowId)
                    );
                });
              // e.preventDefault();
            console.log(rows_selected);
        });
    });
</script>

@stop



