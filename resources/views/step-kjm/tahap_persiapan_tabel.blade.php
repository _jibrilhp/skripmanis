@extends('adminlte::page')


@section('title', 'SPMI dan SPME')


@section('content_header')

<h1>Audit Mutu Internal</h1>

@stop


@section('content')


@if (Session::has('success'))
    <br>
        <div class="alert alert-success">
           
            {!! \Session::get('success') !!}
            
        </div>
@endif

@if (Session::has('pesan'))
    <br>
        <div class="alert alert-warning">
           
            {!! \Session::get('pesan') !!}
            
        </div>
@endif

<div class='row'>
        <div class='col-md-12'>
            <!-- Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Permohonan Audit</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
              
                <div class="box-body">
                    <form id="auditor_anggota_post" method="post" enctype="multipart/form-data" class="form-horizontal">
                    <input type="hidden" value="ok" name="sip">

                        @csrf
                    <!-- <hr style="display: block; height: 1px; border: 0; border-top: 1px solid #ccc; margin: 1em 0;padding: 0; "> -->
                  
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="inputUsername4" class="col-sm-2 control-label"></label>
                                <div class="col-sm-10">
                                        <a href="{{route('auditor_permintaan_audit_add')}}" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> Tambah Permohonan audit</a>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUsername4" class="col-sm-2 control-label">Tabel permohonan audit</label>
                                <div class="col-sm-10">
                                    <table id="tabelAuditor" class="table table-bordered table-striped table-hover">
                                        <thead>
                                            <tr>    
                                            
                                                <th>Nama Pemohon</th>  
                                                <th>Program Studi</th>
                                                <th>Tanggal upload</th>  
                                                <th>Upload</th>  
                                                <th></th>   
                                            </tr>
                                        </thead>
                                    </table>   
                                </div>
                            </div>  

                          

                   
                    <!-- end of div -->
                        </div>
                    </div>
                    <!-- end of div -->
                       
                 

                    </form>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->

    </div><!-- /.row -->

    

@stop


@section('css')

    <!-- <link rel="stylesheet" href="/css/admin_custom.css"> -->

@stop


@section('js')

<script>
    $(document).ready(function(){

        var rows_selected = [];

        var auditTable = $("#tabelAuditor").DataTable({
            "processing": true,
            "serverSide": true,
            ajax: '{{route('auditor_list_permintaan')}}',

            columns: [
                {data: 'permintaan_nama_audit'},
                {data: 'nama_prodi'},
                {data: 'surat_dibuat_pada'},
                {
                    width: "100px",
                    orderable : false,
                    mRender: function(data, type, full) {
                    console.log(full);
                    return '<a href="{{asset('/unggah/unggah_permintaan/')}}/' + full.per_auditor_nama_file + '">Lihat berkas</a>';
                }},
                {   width: "100px",
                    orderable : false,
                    mRender   : function(data, type, full) {
                        return '<a href=""><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> | <a href=""><i class="fa fa-trash" aria-hidden="true"></i></a>';
                    }
                
                }
            ],
           
           

           

          
            order: [[1, 'asc']],

           

        });

    });
</script>

@stop



