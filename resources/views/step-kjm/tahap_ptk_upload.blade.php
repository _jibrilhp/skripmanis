@php 
    use App\Http\Controllers\AuditorController;
@endphp
@extends('adminlte::page')


@section('title', 'SPMI dan SPME')


@section('content_header')

    <h1>SPMI dan SPME</h1>

@stop


@section('content')


@if (Session::has('success'))
    <br>
        <div class="alert alert-success">
           
            {!! \Session::get('success') !!}
            
        </div>
@endif

@if (Session::has('pesan'))
    <br>
        <div class="alert alert-warning">
           
            {!! \Session::get('pesan') !!}
            
        </div>
@endif

<div class='row'>
        <div class='col-md-12'>
            <!-- Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Permintaan Tindakan Koreksi (PTK)</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- <div class="box-body">
                    <div class="row">
                        <div class="col-sm-4"></div>
                        <div class="col-sm-4">
                            <img class="img img-responsive" src="{{url('/') . '/assets/image/alur.png'}}">
                        </div>
                        <div class="col-sm-4"></div>
                    </div>
                    
                </div> -->
                <div class="box-body">
                    <form method="post" enctype="multipart/form-data">
                    <input type="hidden" value="ok" name="sip">

                        @csrf
                    <hr style="display: block; height: 1px; border: 0; border-top: 1px solid #ccc; margin: 1em 0;padding: 0; ">
                  
                    <div class="row">
                        <div class="col-md-12">
                        <table id="tabel_standar" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th width="10%">No</th>
                                <th width="90%">Tanggal Audit</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php $angka = 0; @endphp
                            @foreach ($ptkmodel as $key => $pm)
                            @php $angka = $angka + 1; @endphp
                            <tr>
                                <td>{{$angka}}</td>
                                <td>
                                <div class="panel-group">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" href="#collapse{{$angka}}">{{$pm->pen_jadwal_tanggal}} - {{$pm->jenjang}}</a>
                                        </h4>
                                        </div>
                                        <div id="collapse{{$angka}}" class="panel-collapse collapse">
                                        <form method="post" enctype="multipart/form-data">
                                        @csrf
                                          <div class="panel-body">
                                         
                                              <div class="row">
                                                  <div class="col-md-4">
                                                      Berkas PTK
                                                  </div>
                                                  <div class="col-md-4">
                                                     <input type="hidden" class="form-control" name="ptk_upload_jadwal_id" value="{{$pm->pen_jadwal_id}}">

                                                     <input type="text" class="form-control" name="ptk_upload_name" value="" placeholder="Isian Nama Upload">
                                                     <br>
                                                      <input type="file" class="form-control" name="ptk_upload_data" multiple>
                                                      <hr>
                                                      <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
                                                  </div>
                                              </div>
                                              <hr>
                                              <div class="row">
                                                  <div class="col-md-4">
                                                      List Berkas
                                                  </div>
                                                  <div class="col-md-4">
                                                     @php
                                                        $audit = AuditorController::auditor_step_ptk_list_data($pm->pen_jadwal_id);
                                                     @endphp
                                                     <table  class="table table-bordered table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th>Berkas</th>
                                                                <th></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        @if ($audit != null)
                                                            @foreach ($audit as $at)
                                                            <tr>
                                                                <td>{{$at->audit_ptk_nama}}</td>
                                                                <td><a href="{{url('/') . '/unggah/unggah_ptk/' . $at->audit_ptk_file}}">Lihat berkas</a></td>
                                                            </tr>
                                                            @endforeach
                                                        @endif
                                                        </tbody>

                                                     </table>
                                                     
                                                  </div>
                                              </div>
                                          
                                          </div>
                                       </form>
                                    </div>
                                </div> 
                                </td>
                            </tr>
                            @endforeach
                          
                            </tbody>
                            
                        </table>
                        </div>
                    </div>
                  
                       
                 

                    </form>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->

    </div><!-- /.row -->

    

@stop


@section('css')

    <!-- <link rel="stylesheet" href="/css/admin_custom.css"> -->

@stop


@section('js')

<script>
    $(document).ready(function() {
        $('#tabel_standar').DataTable();
    } );

  
</script>

@stop

