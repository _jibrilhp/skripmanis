<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', 'HomeController@home_index')->name('home_index_view');

// Authenticate ..

Route::get('/login','LoginController@login_view_index')->name('login_index_view');
Route::get('/logout','LoginController@logout_action');
Route::post('/logout','LoginController@logout_action');
Route::post('/login','LoginController@login_authenticate');

// User function

Route::get('/user/profile','UserController@user_list_view_index')->name('user_profile_index');
Route::post('/user/profile','UserController@user_list_view_save');

//Admin user management
Route::get('/user/admin/index','UserController@user_list_view_maintain')->name('user_admin_maintain');
Route::post('/user/admin/profile/new','UserController@user_admin_new_user_save')->name('user_admin_newuser_save');
Route::get('/user/admin/profile/edit/{user_id}','UserController@user_admin_edit_view')->name('user_admin_edit_view');
Route::post('/user/admin/profile/edit/{user_id}','UserController@user_list_view_save')->name('user_admin_edit_save');
Route::get('/user/admin/profile/delete/{user_id}','UserController@user_admin_delete_confirm')->name('user_admin_delete_confirm');


// Main function
Route::get('/borang', 'HomeController@operator_borang')->name('operator_lihat_borang');
Route::get('/borang/create','HomeController@operator_borang_create')->name('operator_buat_borang');
Route::post('/borang','HomeController@operator_borang_lihat');


Route::post('/borang/profil/update','HomeController@operator_borang_update_data')->name('operator_save_borang');
Route::post('/borang/profil/create','HomeController@operator_borang_create_data')->name('operator_save_new_borang');

Route::get('/borang/prodi/tambah', 'HomeController@operator_prodi_add_data');
Route::get('/borang/profil/tambah', 'HomeController@operator_profil_add_data');
Route::post('/borang/profil/tambah', 'HomeController@operator_profil_add_data');

Route::get('/borang/templat/download','HomeController@operator_borang_download_template')->name('operator_download_borang');

Route::get('/borang/templat/upload','HomeController@operator_profil_upload_data')->name('operator_upload_borang');
Route::post('/borang/templat/upload','HomeController@operator_profil_upload_data');

Route::get('/penilaian/pilihprodi','HomeController@operator_penilaian_pilih');
Route::post('/penilaian/pilihprodi','HomeController@operator_penilaian_pilih');

Route::get('/penilaian/rekapanalisis','HomeController@operator_penilaian_tampil')->name('operator_rekap_analisis_home');
Route::post('/penilaian/rekapanalisis','HomeController@operator_penilaian_tampil_save')->name('operator_rekap_analisis_home_save');
//new version

Route::get('/standar/{category_ids}','HomeController@operator_standar_mutu')->name('operator_standar_mutu');
//Route::post('/standar/{category_ids}','HomeController@operator_standar_mutu_save');
Route::post('/standar/{category_ids}/save/berkas','HomeController@operator_standar_mutu_borang_berkas_save')->name('operator_standar_mutu_berkas');
Route::get('/standar/download/{category_ids}','HomeController@operator_standar_mutu_download')->name('operator_standar_mutu_download');

Route::get('/standar/borang/{category_ids}/{standar_dikti_ids}','HomeController@operator_standar_mutu_borang')->name('operator_borang_spm_view');
Route::post('/standar/borang/{category_ids}/{standar_dikti_ids}','HomeController@operator_standar_mutu_borang_save')->name('operator_borang_spm_view_save');
Route::get('/standar/borang/{category_ids}/{standar_dikti_ids}/edit','HomeController@operator_standar_mutu_borang_edit')->name('operator_borang_spm_view_edit');
Route::post('/standar/borang/{category_ids}/{standar_dikti_ids}/edit','HomeController@operator_standar_mutu_borang_edit_save')->name('operator_borang_spm_view_edit_save');
Route::get('/standar/borang/{category_ids}/{standar_dikti_ids}/delete','HomeController@operator_standar_mutu_borang_delete')->name('operator_borang_spm_view_delete');


Route::get('/manual/{category_ids}','ManualMutuController@operator_manual_mutu_index')->name('operator_manual_mutu');
Route::post('/manual/{category_ids}','ManualMutuController@operator_manual_mutu_save')->name('operator_manual_mutu_save');

Route::get('/manual/{category_ids}/{category_standar_ids}/{standar_dikti_ids}','ManualMutuController@operator_manual_download')->name('operator_manual_mutu_download'); 

Route::get('/auditor/ami','AuditorController@auditor_audit_mutu_internal_index');
Route::get('/auditor/ami/edit/{audit_borang_id}','AuditorController@auditor_audit_mutu_internal_edit')->name('audit_borang_edit');
Route::get('/auditor/ami/delete/{audit_borang_id}','AuditorController@auditor_audit_mutu_internal_delete')->name('audit_borang_delete');
Route::post('/auditor/ami/create','AuditorController@auditor_mutu_internal_create')->name('audit_borang_create');

Route::post('/auditor/ami/borang/tujuan/create/{audit_borang_id}','AuditorController@auditor_ami_tujuan_create')->name('audit_borang_tujuan_create');
Route::post('/auditor/ami/borang/tujuan/view/{audit_borang_id}','AuditorController@auditor_ami_tujuan_view')->name('audit_borang_tujuan_view');
Route::post('/auditor/ami/borang/tujuan/delete/{audit_borang_id}','AuditorController@auditor_audit_mutu_internal_tujuan_delete')->name('audit_borang_tujuan_delete');
Route::post('/auditor/ami/borang/tujuan/edit/{audit_borang_id}','AuditorController@auditor_audit_mutu_internal_tujuan_edit')->name('audit_borang_tujuan_edit');
Route::post('/auditor/ami/userlist','AuditorController@auditor_audit_user_list')->name('audit_list_user');

Route::post('/auditor/ami/borang/pertanyaan/create','AuditorController@auditor_ami_pertanyaan_create')->name('audit_borang_pertanyaan_create');
Route::post('/auditor/ami/borang/pertanyaan/view/{audit_borang_id}','AuditorController@auditor_ami_pertanyaan_view')->name('audit_borang_pertanyaan_view');
Route::post('/auditor/ami/borang/pertanyaan/edit/{audit_borang_id}','AuditorController@auditor_ami_pertanyaan_edit')->name('audit_borang_pertanyaan_edit');
Route::post('/auditor/ami/borang/pertanyaan/delete/{audit_borang_id}','AuditorController@auditor_ami_pertanyaan_delete')->name('audit_borang_pertanyaan_delete');

Route::post('/auditor/ami/borang/lingkup/create/{audit_borang_id}','AuditorController@auditor_ami_lingkup_create')->name('audit_borang_lingkup_create');
Route::post('/auditor/ami/borang/lingkup/view/{audit_borang_id}','AuditorController@auditor_ami_lingkup_view')->name('audit_borang_lingkup_view');
Route::post('/auditor/ami/borang/lingkup/edit/{audit_borang_id}','AuditorController@auditor_ami_lingkup_edit')->name('audit_borang_lingkup_edit');
Route::post('/auditor/ami/borang/lingkup/delete/{audit_borang_id}','AuditorController@auditor_ami_lingkup_delete')->name('audit_borang_lingkup_delete');

Route::post('/auditor/ami/borang/jadwal/create/{audit_borang_id}','AuditorController@auditor_ami_jadwal_create')->name('audit_borang_jadwal_create');
Route::post('/auditor/ami/borang/jadwal/view/{audit_borang_id}','AuditorController@auditor_ami_jadwal_view')->name('audit_borang_jadwal_view');
Route::post('/auditor/ami/borang/jadwal/edit/{audit_borang_id}','AuditorController@auditor_ami_jadwal_edit')->name('audit_borang_jadwal_edit');
Route::post('/auditor/ami/borang/jadwal/delete/{audit_borang_id}','AuditorController@auditor_ami_jadwal_delete')->name('audit_borang_jadwal_delete');

Route::post('/auditor/ami/borang/jadwal/create/{audit_borang_id}','AuditorController@auditor_ami_jadwal_create')->name('audit_borang_jadwal_create');
Route::post('/auditor/ami/borang/jadwal/view/{audit_borang_id}','AuditorController@auditor_ami_jadwal_view')->name('audit_borang_jadwal_view');
Route::post('/auditor/ami/borang/jadwal/edit/{audit_borang_id}','AuditorController@auditor_ami_jadwal_edit')->name('audit_borang_jadwal_edit');
Route::post('/auditor/ami/borang/jadwal/delete/{audit_borang_id}','AuditorController@auditor_ami_jadwal_delete')->name('audit_borang_jadwal_delete');

Route::post('/auditor/ami/borang/temuan/create/{audit_borang_id}','AuditorController@auditor_ami_temuan_create')->name('audit_borang_temuan_create');
Route::post('/auditor/ami/borang/temuan/view/{audit_borang_id}','AuditorController@auditor_ami_temuan_view')->name('audit_borang_temuan_view');
Route::post('/auditor/ami/borang/temuan/edit/{audit_borang_id}','AuditorController@auditor_ami_temuan_edit')->name('audit_borang_temuan_edit');
Route::post('/auditor/ami/borang/temuan/delete/{audit_borang_id}','AuditorController@auditor_ami_temuan_delete')->name('audit_borang_temuan_delete');

Route::post('/auditor/ami/borang/saran/create/{audit_borang_id}','AuditorController@auditor_ami_saran_create')->name('audit_borang_saran_create');
Route::post('/auditor/ami/borang/saran/view/{audit_borang_id}','AuditorController@auditor_ami_saran_view')->name('audit_borang_saran_view');
Route::post('/auditor/ami/borang/saran/edit/{audit_borang_id}','AuditorController@auditor_ami_saran_edit')->name('audit_borang_saran_edit');
Route::post('/auditor/ami/borang/saran/delete/{audit_borang_id}','AuditorController@auditor_ami_saran_delete')->name('audit_borang_saran_delete');

Route::post('/auditor/ami/borang/kesimpulan/create/{audit_borang_id}','AuditorController@auditor_ami_kesimpulan_create')->name('audit_borang_kesimpulan_create');
Route::post('/auditor/ami/borang/kesimpulan/view/{audit_borang_id}','AuditorController@auditor_ami_kesimpulan_view')->name('audit_borang_kesimpulan_view');
Route::post('/auditor/ami/borang/kesimpulan/edit/{audit_borang_id}','AuditorController@auditor_ami_kesimpulan_edit')->name('audit_borang_kesimpulan_edit');
Route::post('/auditor/ami/borang/kesimpulan/delete/{audit_borang_id}','AuditorController@auditor_ami_kesimpulan_delete')->name('audit_borang_kesimpulan_delete');

Route::get('/emi/{emi_standar_id}','EmiController@emi_standar_index')->name('emi_standar_index');
Route::post('/emi/{emi_standar_id}','EmiController@emi_standar_index')->name('emi_standar_index');
Route::get('/emi/{emi_standar_id}/view/{emi_tahun_standar}','EmiController@emi_standar_view_index')->name('emi_standar_index_pilih');

Route::post('/emi/{emi_standar_id}/table/create/{emi_tahun_standar}','EmiController@emi_standar_table_create')->name('emi_standar_create');
Route::post('/emi/{emi_standar_id}/table/view/{emi_tahun_standar}','EmiController@emi_standar_table_view')->name('emi_standar_view');
Route::post('/emi/{emi_standar_id}/table/edit/{emi_tahun_standar}','EmiController@emi_standar_table_edit')->name('emi_standar_edit');
Route::post('/emi/{emi_standar_id}/table/delete/{emi_tahun_standar}','EmiController@emi_standar_table_delete')->name('emi_standar_delete');
Route::post('/emi/{emi_standar_id}/table/pilihan/{emi_tahun_standar}/view','EmiController@emi_standar_option_pilihan_standar')->name('emi_standar_pilihan_tabel');
Route::post('/emi/{emi_standar_id}/table/pilihan/{emi_tahun_standar}/view/{emi_pilihan}','EmiController@emi_standar_option_pilihan_standar')->name('emi_standar_pilihan_tabel_terpilih');

Route::post('/emi/{emi_standar_id}/table/keadaan/create/{emi_tahun_standar}','EmiController@emi_keadaan_table_create')->name('emi_keadaan_standar_create');
Route::post('/emi/{emi_standar_id}/table/keadaan/view/{emi_tahun_standar}','EmiController@emi_keadaan_table_view')->name('emi_keadaan_standar_view');
Route::post('/emi/{emi_standar_id}/table/keadaan/edit/{emi_tahun_standar}','EmiController@emi_keadaan_table_edit')->name('emi_keadaan_standar_edit');
Route::post('/emi/{emi_standar_id}/table/keadaan/delete/{emi_tahun_standar}','EmiController@emi_keadaan_table_delete')->name('emi_keadaan_standar_delete');

Route::post('/emi/{emi_standar_id}/table/capaian/view/{emi_tahun_standar}','EmiController@emi_capaian_table_view')->name('emi_capaian_standar_view');
Route::post('/emi/{emi_standar_id}/table/capaian/edit/{emi_tahun_standar}','EmiController@emi_capaian_table_edit')->name('emi_capaian_standar_edit');

// Auth::routes(['register' => false]);

Route::get('/home', 'HomeController@home_index')->name('home');

//Steppy Route
Route::get('/auditor/ami/tahap/list_user','AuditorController@auditor_step_view_user')->name('auditor_list_user');
Route::get('/auditor/ami/tahap/list_permintaan','AuditorController@auditor_step_view_permintaan')->name('auditor_list_permintaan');
Route::get('/auditor/ami/tahap/permintaan','AuditorController@auditor_step_permintaan_audit_index')->name('auditor_permintaan_audit');
Route::get('/auditor/ami/tahap/permintaan/tambah','AuditorController@auditor_step_permintaan_audit_index_add')->name('auditor_permintaan_audit_add');
Route::post('/auditor/ami/tahap/permintaan/tambah','AuditorController@auditor_step_permintaan_audit_save')->name('auditor_permintaan_audit_save');

Route::get('/auditor/ami/tahap/kjm/permohonan','AuditorController@auditor_step_permohonan_index')->name("PermohoanAudit");
Route::get('/auditor/ami/tahap/kjm/permohonan/lihat','AuditorController@auditor_step_view_read_pemohonan')->name("auditor_step_view_read_pemohonan");
Route::get('/auditor/ami/kjm/unduh_berkas/{permintaan_id}','AuditorController@auditor_step_permohonan_buka_berkas')->name('DownloadBerkasPermohonan');
Route::get('/auditor/ami/tahap/kjm/penentuan','AuditorController@auditor_step_penentuan_jadwal_index')->name('PenentuanJadwalIndex');
Route::get('/auditor/ami/tahap/kjm/penentuan/create','AuditorController@auditor_step_penentuan_jadwal_create')->name('PenentuanJadwalCreate');
Route::post('/auditor/ami/tahap/kjm/penentuan/create','AuditorController@auditor_step_penentuan_jadwal_save')->name('PenentuanJadwalCreateSave');
Route::get('/auditor/ami/tahap/kjm/data_pelaksana','AuditorController@auditor_step_data_pelaksana_audit')->name('PenentuanDataPelaksana');
Route::get('/auditor/ami/tahap/kjm/data_pelaksana/view','AuditorController@auditor_step_data_pelaksana_view')->name('PenentuanDataPelaksanaView');
Route::get('/auditor/ami/tahap/kjm/penentuan/list','AuditorController@auditor_step_penentuan_jadwal_listing')->name('PenentuanJadwalList');
Route::get('/auditor/ami/kjm/unduh_surat_tugas/{permintaan_id}','AuditorController@auditor_step_permohonan_surat_tugas')->name('DownloadBerkasPermohonanAuditor');

//auditor
Route::get('/auditor/ami/tahap/auditor/form/daftar_pertanyaan','AuditorController@auditor_step_daftar_pertanyaan_audit')->name('AuditorDaftarPertanyaan');
Route::post('/auditor/ami/tahap/auditor/form/daftar_pertanyaan/view','AuditorController@auditor_step_daftar_pertanyaan_view')->name('AuditorDaftarPertanyaanView');
Route::get('/auditor/ami/tahap/auditor/form/daftar_pertanyaan/view','AuditorController@auditor_step_daftar_pertanyaan_view')->name('AuditorDaftarPertanyaanViewGet');
Route::get('/auditor/ami/tahap/auditor/form/daftar_pertanyaan/view/auditor','AuditorController@auditor_step_daftar_pertanyaan_auditor_view')->name('AuditorView');
Route::post('/auditor/ami/tahap/auditor/form/daftar_pertanyaan/view/header','AuditorController@auditor_step_view_tabel_jadwal')->name('AuditorJadwalView');
Route::get('/auditor/ami/tahap/auditor/form/daftar_pertanyaan/view/auditor/list','AuditorController@auditor_step_view_tabel_auditor')->name('AuditorListView');
Route::get('/auditor/ami/tahap/auditor/form/daftar_pertanyaan/view/auditee/list','AuditorController@auditor_step_view_tabel_auditee')->name('AuditeeListView');

//ptk new
Route::get('/auditor/ami/tahap/ptk/form','AuditorController@auditor_step_ptk_index')->name('PTK_index');
Route::post('/auditor/ami/tahap/ptk/form','AuditorController@auditor_step_ptk_submit');

Route::get('/auditor/ami/tahap/administrasi','AuditorController@auditor_step_administrasi')->name('Adm_index');
Route::post('/auditor/ami/tahap/administrasi','AuditorController@auditor_step_administrasi_save');

Route::get('/rs/emi/{profil_id}/{standar_emi}/{tahun_emi}','AuditorController@kjm_step_emi')->name('EmiSementara');
Route::post('/rs/emi/{standar_emi}/pilih','AuditorController@kjm_step_emi_selection')->name('EmiSelection');
Route::get('/rs/emi/{profil_id}/{standar_emi}','AuditorController@kjm_step_emi')->name('EmiSementara1');
Route::post('/rs/emi/{profil_id}/{standar_emi}/','AuditorController@kjm_step_emi_save')->name('EmiSementaraSave');
Route::post('/rs/emi/{profil_id}/{standar_emi}/{tahun_emi}','AuditorController@kjm_step_emi_save')->name('EmiSementaraSave2');

Route::get('/rs/emi/rubrik/','AuditorController@kjm_step_emi_rubrik')->name('EmiRubrikDefault');
Route::post('/rs/emi/rubrik/','AuditorController@kjm_step_emi_rubrik_redirect')->name('EmiRubrikDefaultPost');
Route::get('/rs/emi/rubrik/{jenjang_id}/{standar_emi}/set','AuditorController@kjm_step_emi_rubrik')->name('EmiRubrikSelection0');
Route::get('/rs/emi/rubrik/{jenjang_id}/{standar_emi}/{isian_standar}/set','AuditorController@kjm_step_emi_rubrik')->name('EmiRubrikSelection1');
Route::post('/rs/emi/rubrik/{jenjang_id}/{standar_emi}/{isian_standar}/set','AuditorController@kjm_step_emi_rubrik_save')->name('EmiRubrikSelection2');
Route::post('/rs/emi/rubrik/{jenjang_id}/{standar_emi}/set','UserController@OraIso');
Route::post('/rs/emi/rubrik/nilai/post/{profil_id}/{standar_emi}/{tahun_emi}/set','AuditorController@kjm_step_emi_post_nilai_capaian')->name('EmiUbahCapaian');

//Emi baru
Route::get('/rs/emi/grafik/{profil_id}/{tahun_emi}/lihat','HomeController@emi_grafik_standar')->name('GrafikStandar0');
Route::get('/rs/emi/rekap/{profil_id}/{tahun_emi}/lihat','HomeController@emi_rekap_standar')->name('RekapStandar0');
Route::get('/analisis/rekap/getdata','HomeController@emi_rekap_get_data')->name('RekapStandar1');
Route::post('/analisis/rekap/postdata','HomeController@emi_rekap_set_data')->name('RekapStandar2');

Route::get('/ar/ami/laporan_ami','AuditorController@auditor_ami_program_studi')->name('Auditor0');
Route::get('/ar/ami/get_info_laporan','AuditorController@auditor_ami_get_info_laporan')->name('Auditor1');

//AMI baru
Route::post('/ar/ami/tujuan_post','AuditorController@auditor_ami_tujuan_audit')->name('Auditor2');
Route::post('/ami/tujuan/ajax/view','AuditorController@auditor_ami_tujuan_ajax_view')->name('audit_ajax_tujuan_view');
Route::post('/ami/tujuan/ajax/edit','AuditorController@auditor_ami_tujuan_ajax_edit')->name('audit_ajax_tujuan_edit');
Route::get('/ami/tujuan/ajax/delete','AuditorController@auditor_ami_tujuan_ajax_delete')->name('audit_ajax_tujuan_delete');
Route::get('/ami/tujuan/ajax/check_tanggal','AuditorController@auditor_ami_tujuan_ajax_view_2')->name('Auditor1d');
Route::post('/ar/ami/lingkup_post','AuditorController@auditor_ami_lingkup_audit')->name('Auditor3');
Route::post('/ami/lingkup/ajax/view','AuditorController@auditor_ami_lingkup_ajax_view')->name('audit_ajax_lingkup_view');
Route::post('/ami/lingkup/ajax/edit','AuditorController@auditor_ami_lingkup_ajax_edit')->name('audit_ajax_lingkup_edit');
Route::get('/ami/lingkup/ajax/delete','AuditorController@auditor_ami_lingkup_ajax_delete')->name('audit_ajax_lingkup_delete');
Route::post('/ar/ami/kegiatan_post','AuditorController@auditor_ami_kegiatan_audit')->name('Auditor4');
Route::post('/ami/kegiatan/ajax/view','AuditorController@auditor_ami_kegiatan_ajax_view')->name('audit_ajax_kegiatan_view');
Route::post('/ami/kegiatan/ajax/edit','AuditorController@auditor_ami_kegiatan_ajax_edit')->name('audit_ajax_kegiatan_edit');
Route::get('/ami/kegiatan/ajax/delete','AuditorController@auditor_ami_kegiatan_ajax_delete')->name('audit_ajax_kegiatan_delete');

Route::post('/ami/temuan/ajax/view','AuditorController@auditor_ami_temuan_ajax_view')->name('audit_ajax_temuan_view');
Route::post('/ami/temuan/ajax/edit','AuditorController@auditor_ami_temuan_ajax_edit')->name('audit_ajax_temuan_edit');
Route::get('/ami/temuan/ajax/delete','AuditorController@auditor_ami_temuan_ajax_delete')->name('audit_ajax_temuan_delete');
Route::get('/ami/temuan/ajax/reload','AuditorController@auditor_ami_temuan_ajax_reload')->name('auditor_ajax_temuan_reload'); // untuk ngapus data si temuan load ulang..

Route::post('/ar/ami/saran_post','AuditorController@auditor_ami_saran_audit')->name('Auditor5');
Route::post('/ami/saran/ajax/view','AuditorController@auditor_ami_saran_ajax_view')->name('audit_ajax_saran_view');
Route::post('/ami/saran/ajax/edit','AuditorController@auditor_ami_saran_ajax_edit')->name('audit_ajax_saran_edit');
Route::get('/ami/saran/ajax/delete','AuditorController@auditor_ami_saran_ajax_delete')->name('audit_ajax_saran_delete');

Route::post('/ami/kesimpulan/ajax/view','AuditorController@auditor_ami_kesimpulan2_view')->name('audit_ajax_kesimpulan_view');
Route::post('/ami/kesimpulan/ajax/edit','AuditorController@auditor_ami_kesimpulan_ajax_edit')->name('audit_ajax_kesimpulan_edit');

//PTK new
Route::get('/ar/ami/ptk_form','AuditorController@auditor_ami_ptk')->name('Auditor6');
Route::post('/ami/ptk/ajax/ptk_submit','AuditorController@auditor_ami_ptk_edit')->name('Auditor6a');
Route::post('/ami/ptk/ajax/ptk_check','AuditorController@auditor_ami_ptk_check')->name('Auditor6b');
Route::get('/ami/ptk/ajax/view','AuditorController@auditor_ami_ptk_ajax_view')->name('audit_ajax_ptk_view');

//Download Laporan Final !!!
Route::get('/dw/emi/download_laporan','HomeController@emi_download_page');
Route::get('/dw/emi/download_laporan/{profil_id}/{tahun_emi}','HomeController@emi_download_data_set');
Route::get('/dw/ami/download_laporan','AuditorController@ami_download_page');
Route::get('/dw/ami/download_laporan/{jadwal_id}','AuditorController@ami_download_data_set');
Route::get('/th/emi/matriks_analisis','EmiController@emi_matriks_analisis');
Route::post('/th/emi/matriks_analisis/okesip','EmiController@emi_matriks_submit')->name('post_data_matriks');




//debug mode
Route::get('/coba/hmm','Controller@getChart');
Route::get('/coba/hmm2','Controller@getExcel');

