-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 17 Agu 2019 pada 12.55
-- Versi server: 10.1.37-MariaDB
-- Versi PHP: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `spmi`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(86, '2014_10_12_100000_create_password_resets_table', 1),
(87, '2019_03_25_105227_create_profil_data', 1),
(88, '2019_03_31_161321_create_penilaian_table', 1),
(89, '2019_04_01_065639_create_perencanaan_table', 1),
(90, '2019_04_30_155246_create_menu_table', 1),
(91, '2019_04_30_161654_create_category_table', 1),
(92, '2019_04_30_164056_create_standard_category_table', 1),
(96, '2019_04_30_165944_create_standar_awal', 2),
(98, '2019_05_05_060520_create_standar_dikti', 3),
(99, '2019_05_08_143432_create_manual_mutu', 4),
(100, '2019_05_12_134813_create_audit_pertanyaan', 5),
(101, '2019_05_12_142801_create_audit_borang', 5),
(102, '2019_05_12_143427_create_audit_tujuan', 5),
(103, '2019_05_12_143743_create_audit_jadwal', 5),
(104, '2019_05_12_144039_create_audit_temuan', 5),
(105, '2019_05_12_144244_create_audit_saran_perbaikan', 5),
(106, '2019_05_12_144442_create_audit_kesimpulan', 5),
(107, '2019_05_12_144603_create_audit_lampiran', 5),
(111, '2019_05_21_233016_create_tbl_fakultas', 9),
(112, '2014_10_12_000000_create_users_table', 10),
(117, '2019_05_21_231425_create_tbl_emi_standar', 13),
(120, '2019_05_22_004421_create_tbl_emi_bobot_nilai', 15),
(122, '2019_05_21_232130_create_tbl_emi_keadaan_prodi', 17),
(123, '2019_05_22_004753_create_tbl_emi_isian_standar', 18),
(124, '2019_05_14_163916_create_tbl_audit_anggota', 19),
(125, '2019_05_19_133742_create_tbl_audit_lingkup', 20),
(126, '2019_05_21_233448_create_tbl_akreditasi', 21),
(127, '2019_07_08_200949_create_new_tabel_audit_borang_permintaan', 21),
(128, '2019_07_08_201529_create_new_tabel_audit_borang_permintaan_auditor', 21),
(129, '2019_07_08_202209_create_new_tabel_audit_borang_permintaan_auditor', 22),
(130, '2019_07_08_201529_create_new_tabel_audit_borang_permintaan_auditor_file', 23),
(131, '2019_07_10_103418_create_tbl_audit_borang_penentuan', 24),
(132, '2019_07_10_173829_create_tbl_audit_borang_penentuan_jadwal_auditor', 24),
(133, '2019_07_10_174129_create_tbl_audit_borang_penentuan_jadwal_auditor_list', 24),
(134, '2019_07_10_174428_create_tbl_audit_borang_penentuan_jadwal_auditee_list', 24);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_administrasi`
--

CREATE TABLE `tbl_administrasi` (
  `adm_id` int(11) NOT NULL,
  `adm_nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `adm_file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `adm_tanggal` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tbl_administrasi`
--

INSERT INTO `tbl_administrasi` (`adm_id`, `adm_nama`, `adm_file`, `adm_tanggal`, `created_at`, `updated_at`) VALUES
(1, '0 Surat Pemberitahuan Rencana Audit ke Prodi', 'd34fcbf5039c9925d27d3e0651473ce5.pdf', '2018-07-20', '2019-07-14 14:06:38', '2019-07-14 14:06:38'),
(2, '1 Surat Tugas AUDIT PS-TInf', 'f85d906ac2fb5a1a26fd25cd6036b49c.pdf', '2018-07-23', '2019-07-14 14:08:36', '2019-07-14 14:08:36'),
(3, '2 Surat pemberitahuan jadwal Audit dari Auditor', '77d5b1fe5425a02a7e7b508f870b9d02.pdf', '2018-07-25', '2019-07-14 14:10:43', '2019-07-14 14:10:43');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_akreditasi`
--

CREATE TABLE `tbl_akreditasi` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tahun` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `akreditasi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sk_operasional` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_audit_anggota`
--

CREATE TABLE `tbl_audit_anggota` (
  `anggota_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `anggota_status` int(11) NOT NULL,
  `profil_id` int(11) NOT NULL,
  `audit_borang_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_audit_borang`
--

CREATE TABLE `tbl_audit_borang` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tanggalan` datetime NOT NULL COMMENT 'hari, tanggal, pukul..',
  `auditee_user_id` int(11) NOT NULL,
  `ketua_user_id` int(11) NOT NULL,
  `alamat` text COLLATE utf8_unicode_ci NOT NULL,
  `fakultas_direktorat` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `program_studi` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telepon_program` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `audit_program_unit` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telepon_audit_program` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `tbl_audit_borang`
--

INSERT INTO `tbl_audit_borang` (`id`, `tanggalan`, `auditee_user_id`, `ketua_user_id`, `alamat`, `fakultas_direktorat`, `program_studi`, `telepon_program`, `audit_program_unit`, `telepon_audit_program`, `created_at`, `updated_at`) VALUES
(4, '2019-05-20 00:00:00', 2, 1, 'Kampus Margonda, Gedung 4 lantai 1 Jl. Margonda Raya no. 100, Pondok Cina Depok', 'Lembaga Penelitian', '-', '085781601196', NULL, '0857181601196', '2019-05-15 07:16:04', '2019-05-15 07:16:04'),
(5, '2019-05-20 00:00:00', 2, 1, 'Kampus Margonda, Gedung 4 lantai 1 Jl. Margonda Raya no. 100, Pondok Cina Depok', 'Lembaga Penelitian', '-', '085781601196', 'Bajamtu', '0857181601196', '2019-05-15 07:20:13', '2019-05-15 07:20:13'),
(6, '2019-05-20 00:00:00', 2, 1, 'Kampus Margonda, Gedung 4 lantai 1 Jl. Margonda Raya no. 100, Pondok Cina Depok', 'Lembaga Penelitian', '-', '085781601196', 'Bajamtu', '0857181601196', '2019-05-15 07:21:01', '2019-05-15 07:21:01');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_audit_borang_penentuan`
--

CREATE TABLE `tbl_audit_borang_penentuan` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_audit_borang_penentuan_jadwal`
--

CREATE TABLE `tbl_audit_borang_penentuan_jadwal` (
  `pen_jadwal_id` bigint(20) UNSIGNED NOT NULL,
  `pen_jadwal_prodi_id` int(11) NOT NULL COMMENT 'diambil dari profil prodi',
  `pen_jadwal_tanggal` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pen_jadwal_tanggal_sampai` date NOT NULL,
  `pen_jadwal_pukul` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pen_jadwal_lingkup_audit` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pen_jadwal_upload_surat_tugas` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `tbl_audit_borang_penentuan_jadwal`
--

INSERT INTO `tbl_audit_borang_penentuan_jadwal` (`pen_jadwal_id`, `pen_jadwal_prodi_id`, `pen_jadwal_tanggal`, `pen_jadwal_tanggal_sampai`, `pen_jadwal_pukul`, `pen_jadwal_lingkup_audit`, `pen_jadwal_upload_surat_tugas`, `created_at`, `updated_at`) VALUES
(2, 7, '2018-08-06', '2018-08-07', '08:30-15.30', '1', 'af9c0e61d3c18c771c921fc26bca6bd8.pdf', '2019-07-13 06:20:40', '2019-07-13 06:20:40');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_audit_borang_penentuan_jadwal_lingkup`
--

CREATE TABLE `tbl_audit_borang_penentuan_jadwal_lingkup` (
  `pen_lingkup_id` int(11) NOT NULL,
  `pen_lingkup_ref_id` int(11) NOT NULL,
  `pen_lingkup_isian` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tbl_audit_borang_penentuan_jadwal_lingkup`
--

INSERT INTO `tbl_audit_borang_penentuan_jadwal_lingkup` (`pen_lingkup_id`, `pen_lingkup_ref_id`, `pen_lingkup_isian`) VALUES
(1, 2, 'Standar Pendidikan Standar 1. Kompetensi Lulusan'),
(2, 2, 'Standar Pendidikan\r\nStandar 2. Isi Pembelajaran\r\n'),
(3, 2, 'Standar Pendidikan\r\nStandar 3. Proses  Pembelajaran\r\n'),
(4, 2, 'Standar Pendidikan\r\nStandar 4. Penilaian Pembelajaran'),
(5, 2, 'Standar Pendidikan\r\nStandar 5. Dosen dan Tenaga Kependidikan'),
(6, 2, 'Standar Pendidikan\r\nStandar 6. Sarana dan Prasarana\r\n'),
(7, 2, 'Standar Pendidikan\r\nStandar 7. Pengelolaan Pembelajaran'),
(8, 2, 'Standar Pendidikan\r\nStandar 8. Pembiayaan Pembelajaran');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_audit_borang_penentuan_jadwal_listauditee`
--

CREATE TABLE `tbl_audit_borang_penentuan_jadwal_listauditee` (
  `pen_jadwal_list_auditee_id` bigint(20) UNSIGNED NOT NULL,
  `pen_jadwal_list_auditee_ref_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pen_jadwal_list_auditee_user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `tbl_audit_borang_penentuan_jadwal_listauditee`
--

INSERT INTO `tbl_audit_borang_penentuan_jadwal_listauditee` (`pen_jadwal_list_auditee_id`, `pen_jadwal_list_auditee_ref_id`, `pen_jadwal_list_auditee_user_id`, `created_at`, `updated_at`) VALUES
(2, '2', 4, '2019-07-13 06:20:40', '2019-07-13 06:20:40');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_audit_borang_penentuan_jadwal_listauditor`
--

CREATE TABLE `tbl_audit_borang_penentuan_jadwal_listauditor` (
  `pen_jadwal_list_auditor_id` bigint(20) UNSIGNED NOT NULL,
  `pen_jadwal_list_auditor_ref_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pen_jadwal_list_auditor_user_id` int(11) NOT NULL,
  `pen_jadwal_list_auditor_user_status` int(11) NOT NULL,
  `pen_jadwal_list_auditor_user_kesediaan` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `tbl_audit_borang_penentuan_jadwal_listauditor`
--

INSERT INTO `tbl_audit_borang_penentuan_jadwal_listauditor` (`pen_jadwal_list_auditor_id`, `pen_jadwal_list_auditor_ref_id`, `pen_jadwal_list_auditor_user_id`, `pen_jadwal_list_auditor_user_status`, `pen_jadwal_list_auditor_user_kesediaan`, `created_at`, `updated_at`) VALUES
(2, '1', 3, 2, 1, '2019-07-10 11:50:19', '2019-07-10 11:50:19'),
(3, '2', 6, 2, 1, '2019-07-13 06:20:40', '2019-07-13 06:20:40'),
(4, '2', 5, 1, 1, '2019-07-13 06:20:40', '2019-07-13 06:20:40'),
(5, '2', 7, 2, 1, '2019-07-13 06:20:40', '2019-07-13 06:20:40');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_audit_borang_permintaan`
--

CREATE TABLE `tbl_audit_borang_permintaan` (
  `permintaan_id` bigint(20) UNSIGNED NOT NULL,
  `permintaan_nama_audit` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permintaan_profil_id` int(11) NOT NULL,
  `permintaan_status_read` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `tbl_audit_borang_permintaan`
--

INSERT INTO `tbl_audit_borang_permintaan` (`permintaan_id`, `permintaan_nama_audit`, `permintaan_profil_id`, `permintaan_status_read`, `created_at`, `updated_at`) VALUES
(1, 'Siapa', 1, 1, '2019-07-09 02:02:52', '2019-07-10 07:47:21'),
(2, 'Rektor', 7, 1, '2019-07-09 11:44:00', '2019-07-09 17:45:37'),
(3, 'Rektor', 1, 0, '2019-07-24 09:49:26', '2019-07-24 09:49:26');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_audit_borang_permintaan_auditor`
--

CREATE TABLE `tbl_audit_borang_permintaan_auditor` (
  `per_audit_id` bigint(20) UNSIGNED NOT NULL,
  `per_audit_permintaan_id` int(11) NOT NULL,
  `per_audit_anggota_id` int(11) NOT NULL COMMENT 'id user dari si anggota ini siapa aja',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `tbl_audit_borang_permintaan_auditor`
--

INSERT INTO `tbl_audit_borang_permintaan_auditor` (`per_audit_id`, `per_audit_permintaan_id`, `per_audit_anggota_id`, `created_at`, `updated_at`) VALUES
(1, 1, 2, '2019-07-09 02:02:52', '2019-07-09 02:02:52'),
(2, 2, 2, '2019-07-09 11:44:01', '2019-07-09 11:44:01');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_audit_borang_permintaan_auditor_file`
--

CREATE TABLE `tbl_audit_borang_permintaan_auditor_file` (
  `per_auditor_id` bigint(20) UNSIGNED NOT NULL,
  `per_auditor_permintaan_id` int(11) NOT NULL,
  `per_auditor_nama_file` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `tbl_audit_borang_permintaan_auditor_file`
--

INSERT INTO `tbl_audit_borang_permintaan_auditor_file` (`per_auditor_id`, `per_auditor_permintaan_id`, `per_auditor_nama_file`, `created_at`, `updated_at`) VALUES
(1, 1, 'b18cfcf7ef25a9d2a7881667dd7844d4.docx', '2019-07-09 02:02:52', '2019-07-09 02:02:52'),
(2, 2, 'eb8463b64255dd2dfcafa201c62516de.docx', '2019-07-09 11:44:00', '2019-07-09 11:44:00'),
(3, 3, '8674c8711fb698f128cc806eff52ce96.pdf', '2019-07-24 09:49:27', '2019-07-24 09:49:27');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_audit_jadwal`
--

CREATE TABLE `tbl_audit_jadwal` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `pukul` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kegiatan_audit` text COLLATE utf8_unicode_ci NOT NULL,
  `audit_borang_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `tbl_audit_jadwal`
--

INSERT INTO `tbl_audit_jadwal` (`id`, `pukul`, `kegiatan_audit`, `audit_borang_id`, `created_at`, `updated_at`) VALUES
(2, 'w', 'w', 6, '2019-05-20 04:02:50', '2019-05-20 04:02:50'),
(3, '09.00-10.00', 'Audit internal', 5, '2019-05-24 15:51:24', '2019-05-24 15:51:24'),
(5, '12', '12', 2, '2019-08-13 01:53:22', '2019-08-13 01:53:22'),
(6, '22', 'si[[', 2, '2019-08-13 04:35:50', '2019-08-13 04:35:50');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_audit_jenjang_kesimpulan`
--

CREATE TABLE `tbl_audit_jenjang_kesimpulan` (
  `jk_id` int(11) NOT NULL,
  `jk_jenjang_id` int(11) NOT NULL,
  `jk_isi_kesimpulan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `jk_json_check` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `jk_json_value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `urutan` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tbl_audit_jenjang_kesimpulan`
--

INSERT INTO `tbl_audit_jenjang_kesimpulan` (`jk_id`, `jk_jenjang_id`, `jk_isi_kesimpulan`, `jk_json_check`, `jk_json_value`, `urutan`, `created_at`, `updated_at`) VALUES
(1, 2, 'Sistem dokumentasi cukup lengkap dan terstruktur untuk mendukung pelaksanaan Sistem Penjaminan Mutu Internal', '[\"Ya\",\"Tidak\",\"Lainnya\"]', '[\"X\",\"X\",\"V\"]', 1, '2019-08-14 09:43:23', '2019-08-14 09:43:23'),
(2, 2, 'Program Studi telah menjalankan Sistem Penjaminan Mutu Internal secara konsisten dan berkelanjutan', '[\"Ya\",\"Tidak\",\"Lainnya\"]', '[\"X\",\"X\",\"V\"]', 2, '2019-08-14 09:51:19', '2019-08-14 09:51:19'),
(3, 2, 'Temuan pada periode audit ini adalah ', '[\"Major\",\"Minor\",\"Observasi\"]', '[\"X\",\"X\",\"X\"]', 3, '2019-08-14 09:51:54', '2019-08-14 09:51:54'),
(4, 2, 'PTK pada temuan audit sebelumnya telah ditindak-lanjuti secara efektif.\r\nJika tidak, sebutkan rekomendasi tim auditor : ', '[\"Ya\",\"Tidak\"]', '[\"X\",\"V\"]', 4, '2019-08-14 09:52:26', '2019-08-14 09:52:26'),
(5, 2, 'Program Studi menunjukkan komitmennya terhadap impementasi Sistem Penjaminan Mutu Internal untuk tercapainya kepuasan stakeholder', '[\"Ya\",\"Tidak\",\"Lainnya\"]', '[\"X\",\"X\",\"V\"]', 5, '2019-08-14 09:53:11', '2019-08-14 09:53:11');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_audit_kesimpulan`
--

CREATE TABLE `tbl_audit_kesimpulan` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kesimpulan` text COLLATE utf8_unicode_ci NOT NULL,
  `yes_no_other_check` int(11) NOT NULL,
  `lainnya` longtext COLLATE utf8_unicode_ci,
  `audit_borang_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `tbl_audit_kesimpulan`
--

INSERT INTO `tbl_audit_kesimpulan` (`id`, `kesimpulan`, `yes_no_other_check`, `lainnya`, `audit_borang_id`, `created_at`, `updated_at`) VALUES
(1, '1', 1, NULL, 2, '2019-05-20 15:54:03', '2019-08-15 03:59:31'),
(2, '2', 2, NULL, 2, '2019-08-15 03:59:46', '2019-08-15 03:59:46'),
(3, '3', 3, 'hmm', 2, '2019-08-15 04:00:04', '2019-08-15 04:00:04'),
(4, '4', 2, 'ya udah', 2, '2019-08-15 04:00:19', '2019-08-15 04:00:19'),
(5, '5', 1, 'sip alhamdulillah', 2, '2019-08-15 04:00:35', '2019-08-15 04:00:35');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_audit_lampiran`
--

CREATE TABLE `tbl_audit_lampiran` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `lampiran` text COLLATE utf8_unicode_ci,
  `audit_borang_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_audit_lingkup`
--

CREATE TABLE `tbl_audit_lingkup` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `audit_lingkup` text COLLATE utf8_unicode_ci NOT NULL,
  `audit_borang_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `tbl_audit_lingkup`
--

INSERT INTO `tbl_audit_lingkup` (`id`, `audit_lingkup`, `audit_borang_id`, `created_at`, `updated_at`) VALUES
(2, 'sip', 2, '2019-08-13 04:35:11', '2019-08-13 04:35:11'),
(3, 'Hmm', 2, '2019-08-13 04:35:24', '2019-08-13 04:35:24');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_audit_pertanyaan`
--

CREATE TABLE `tbl_audit_pertanyaan` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `referensi_butir_mutu` text COLLATE utf8_unicode_ci,
  `hasil_observasi` longtext COLLATE utf8_unicode_ci NOT NULL,
  `yes_no_check` int(11) NOT NULL COMMENT 'kalau 1 ya, 0 tidak',
  `catatan_khusus` text COLLATE utf8_unicode_ci,
  `audit_pen_jadwal_id` int(11) NOT NULL,
  `audit_pen_lingkup_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `tbl_audit_pertanyaan`
--

INSERT INTO `tbl_audit_pertanyaan` (`id`, `referensi_butir_mutu`, `hasil_observasi`, `yes_no_check`, `catatan_khusus`, `audit_pen_jadwal_id`, `audit_pen_lingkup_id`, `created_at`, `updated_at`) VALUES
(6, 'Z', 'z', 1, 'z', 1, 0, '2019-07-13 06:05:59', '2019-07-13 06:05:59'),
(7, 'Standar Mutu Pendidikan :  Standar Kompetensi Lulusan (std-1)\r\n\r\nKurikulum PS T.Informatika', 'Apakah PS memiliki Visi, Misi dan Tujuan PS ?', 1, 'Kata kunci Visi :\r\nInternasionalisasi\r\nKeunggulan Tridharma PT secara holistic dan integrative\r\nDaya saing bangsa\r\nMemadukan iptek menghadapi Revoulusi 4.0, Komputasi Big Data dan Ekonomi Bisnis Digital', 2, 1, '2019-07-13 06:47:13', '2019-07-13 06:47:13'),
(8, NULL, 'Apakah PS telah menurunkan  VMT PS ke dalam kompetensi lulusan yang diharapkan (kompetensi umum dan kompetensi pendukung) ?', 1, NULL, 2, 1, '2019-07-13 06:48:02', '2019-07-13 06:48:02'),
(9, NULL, 'Apakah dalam menuangkan kompetensi lulusan yang diharapkan, PS melakukan analisis kebutuhan dilihat dari perkembangan ilmu TI dan kebutuhan pasar tenaga kerja ?', 1, NULL, 2, 1, '2019-07-13 06:48:21', '2019-07-13 06:48:21'),
(10, NULL, 'Apakah PS sudah memiliki CP-PS ?', 1, NULL, 2, 1, '2019-07-13 06:48:54', '2019-07-13 06:48:54'),
(11, NULL, 'Apakah PS sudah menuangkan CP-PS (terkait Sikap, Pengetahuan, Ketrampilan Umum/Khusus) dalam RPS untuk tiap mata kuliah ?', 1, 'Semua mata kuliah sudah memiliki RPS yang di dalamnya sudah mencantumkan CP-PS', 2, 1, '2019-07-13 06:49:07', '2019-07-13 06:49:07'),
(12, NULL, 'Apakah PS melakukan pemetaan mata kuliah terkait kompetensi lulusan yang diharapkan ?', 1, 'Pemetaan mata kuliah sudah memperhatikan perkembangan keilmuwan Informatika (KKNI) dan kebutuhan pasar tenaga kerja (SKKNI)', 2, 1, '2019-07-13 06:50:47', '2019-07-13 06:50:47'),
(13, NULL, 'Apakah PS memberikan pembekalan tambahan terkait penguatan kompetensi lulusan kepada mahasiswa ?', 1, 'Dalam bentuk kursus/ workshop yang wajib diikuti mahasiswa setiap semester', 2, 1, '2019-07-13 06:51:32', '2019-07-13 06:51:32'),
(15, NULL, 'Apakah PS memberikan pembekalan tambahan terkait penguatan kurikulum kepada dosen untuk menunjang kompetensi lulusan?', 1, 'Dalam bentuk refreshing mata kuliah', 2, 1, '2019-07-13 06:52:05', '2019-07-13 06:52:05'),
(16, NULL, 'Apakah PS melakukan penelusuran tracer study terhadap alumni  ?', 1, 'Bekerja sama dengan Career Center', 2, 1, '2019-07-13 06:52:27', '2019-07-13 06:52:27'),
(17, NULL, 'Apakah PS melakkan penelusuran tracer study terhadap pengguna lulusan ?', 1, 'Bekerja sama dengan Career Center', 2, 1, '2019-07-13 06:52:49', '2019-07-13 06:52:49'),
(18, 'Standar Mutu Pendidikan :  Standar Isi Pembelajaran (std-2)\r\n\r\nKurikulum PS T.Informatika (2017-2022)', 'Apakah PS memiliki dokumen kurikulum ?', 1, NULL, 2, 2, '2019-07-13 09:29:46', '2019-07-13 09:29:46'),
(19, NULL, 'Apakah PS melakukan pemetaan kurikulum, meetakannya ke dalam mata kuliah dan terdokumentasi ?', 1, 'Pemetaan mata kuliah sudah memperhatikan perkembangan keilmuwan Informatika (KKNI) dan kebutuhan pasar tenaga kerja (SKKNI)', 2, 2, '2019-07-13 09:30:24', '2019-07-13 09:30:24'),
(20, NULL, 'Apakah PS sudah memiliki CP-PS ?', 1, NULL, 2, 2, '2019-07-13 09:30:44', '2019-07-13 09:30:44'),
(21, NULL, 'Apakah PS menuangkan CP-PS (terkait Sikap, Pengetahuan, Ketrampilan Umum/Khusus) dalam RPS untuk tiap mata kuliah termasuk modul praktikum ?', 1, 'Semua mata kuliah sudah memiliki RPS yang di dalamnya sudah mencantumkan CP-PS\r\n    Daftar RPS', 2, 2, '2019-07-13 09:31:00', '2019-07-13 09:31:00'),
(22, NULL, 'Apakah PS melakukan peninjauan kurikulum secara rutin  ?', 1, 'Dilakukan oleh tim khusus\r\nBerita Acara peninjauan kurikulum / RPS', 2, 2, '2019-07-13 09:31:22', '2019-07-13 09:31:22'),
(23, NULL, 'Apakah PS melakukan peninjauan RPS secara rutin?', 1, 'Melalui kegiatan refreshing mata kuliah pada saat libur panjang semester genap.\r\nBerita Acara peninjauan kurikulum / RPS', 2, 2, '2019-07-13 09:31:42', '2019-07-13 09:31:42'),
(24, NULL, 'Apakah PS memiliki mekanisme penentuan dosen pengampu mata kuliah sesuai kompetensinya  ?', 1, 'Berkoordinasi dengan bagian BAU terkait kompetensi dosen', 2, 2, '2019-07-13 09:31:57', '2019-07-13 09:31:57'),
(25, NULL, 'Apakah PS memiliki mekanisme pemantauan dosen memberikan perkuliahan selama 14x pertemuan dan sesuai dengan RPS yang telah ditentukan ?', 1, 'Berkoordinasi dengan bagian monitoring secretariat dosen\r\nDaftar monitoring dosen \r\nBerita Acara perkuliahan', 2, 2, '2019-07-13 09:32:13', '2019-07-13 09:32:13'),
(26, NULL, 'Apakah PS memiliki mekanisme penentuan dosen pembimbing tugas akhir ?', 1, 'Berkoordinasi dengan bagian BAU terkait kompetensi dosen', 2, 2, '2019-07-13 09:32:33', '2019-07-13 09:32:33'),
(27, NULL, 'Apakah PS memiliki mekanisme penentuan topik-topik termutakhir untuk penelitian tugas akhir ?', 1, 'Dilakukan oleh tim khusus', 2, 2, '2019-07-13 09:32:49', '2019-07-13 09:32:49'),
(28, NULL, 'Apakah PS  memiliki mekanisme pemantauan kegiatan pembimbngan tugas akhir mahasiswa ?', 1, 'Buku Pembimbingan', 2, 2, '2019-07-13 09:33:01', '2019-07-13 09:33:01'),
(29, NULL, 'Apakah PS mendokumentasikan materi/modul praktikum ?', 1, 'Modul praktikum', 2, 2, '2019-07-13 09:33:14', '2019-07-13 09:33:14'),
(30, NULL, 'Apakah PS mendokumentasikan materi/modul praktikum ?', 1, 'Modul praktikum', 2, 2, '2019-07-13 09:33:14', '2019-07-13 09:33:14'),
(31, NULL, 'Apakah PS memiliki mekanisme pemantauan  pelaksanaan praktikum dari jumlah pertemuan dan materi yang diberikan apakah sudah sesuai dengan AP (Acara Praktikum) yang telah ditentukan ?', 1, 'Berkoordinasi dengan coordinator Laboratorium\r\nKartu praktikum\r\nDaftar hadir mahasiswa (praktikum)\r\nBerita Acara Praktikum', 2, 2, '2019-07-13 09:33:27', '2019-07-13 09:33:27'),
(32, NULL, 'Apakah PS menyediakan sarana dan prasarana praktikum yang memadai ?', 1, 'Berkoordinasi dengan bagian pengelola lingkungan kampus/ pengelola sarpras', 2, 2, '2019-07-13 09:33:45', '2019-07-13 09:33:45'),
(33, 'Standar Mutu Pendidikan :  Standar Proses Pembelajaran Pembelajaran (std-3)\r\n\r\nKurikulum PS T.Informatika (2017-2022)', 'Apakah PS menerapkan berbagai metode pembelajaran (tatap muka, FGD, diskusi, problem based solving, project based solving) dan menuangkannya dalam RPS setiap mata kuliah ?', 1, NULL, 2, 3, '2019-07-13 09:35:47', '2019-07-13 09:35:47'),
(34, NULL, 'Apakah setiap mata kuliah dilengkapi dengan RPS ?', 1, 'Semua mata kuliah sudah memiliki RPS', 2, 3, '2019-07-13 09:36:04', '2019-07-13 09:36:04'),
(35, NULL, 'Apakah PS menuangkan CP-PS (terkait Sikap, Pengetahuan, Ketrampilan Umum/Khusus) dalam RPS untuk tiap mata kuliah termasuk modul praktikum ?', 1, 'Semua mata kuliah sudah memiliki RPS yang di dalamnya sudah mencantumkan CP-PS', 2, 3, '2019-07-13 09:37:33', '2019-07-13 09:37:33'),
(36, NULL, 'Apakah PS melakukan peninjauan kurikulum secara rutin  ?', 1, 'Dilakukan oleh tim khusus\r\nBerita Acara peninjauan kurikulum/RPS', 2, 3, '2019-07-13 09:38:12', '2019-07-13 09:38:12'),
(37, NULL, 'Apakah PS melakukan peninjauan RPS secara rutin?', 1, 'Melalui kegiatan refreshing mata kuliah pada saat libur panjang semester genap.', 2, 3, '2019-07-13 09:38:51', '2019-07-13 09:38:51'),
(38, NULL, 'Apakah PS memiliki mekanisme pemantauan dosen memberikan perkuliahan selama 14x pertemuan dan sesuai dengan RPS yang telah ditentukan ?', 1, 'Berkoordinasi dengan bagian monitoring secretariat dosen\r\nDaftar hadir dosen\r\nBerita acara perkuliahan', 2, 3, '2019-07-13 09:39:09', '2019-07-13 09:39:09'),
(39, NULL, 'Apakah PS memiliki mekanisme pemantauan  pelaksanaan praktikum dari jumlah pertemuan dan materi yang diberikan apakah sudah sesuai dengan AP (Acara Praktikum) yang telah ditentukan ?', 1, 'Berkoordinasi dengan coordinator Laboratorium\r\nBerita Acara praktikum', 2, 3, '2019-07-13 09:39:22', '2019-07-13 09:39:22'),
(40, NULL, 'Kartu/buku bimbinganApakah PS memiliki mekanisme pemantauan proses pembimbingan tugas akhir ?', 1, 'Kartu/buku bimbingan', 2, 3, '2019-07-13 09:39:51', '2019-07-13 09:39:51'),
(41, NULL, 'Apakah PS memiliki mekanisme pemantauan kehadiran mahasiswa dalam perkuliahan dan praktek ?', 1, 'Berkoordinasi degnan bagian monitoring di sekdos dan coordinator laboratoium\r\nDaftar hadir mahasiswa (perkuliahan dan praktikum)\r\nKartu praktikum', 2, 3, '2019-07-13 09:40:04', '2019-07-13 09:40:04'),
(42, NULL, 'Apakah PS sudah menentukan (beban mahasiswa) jumlah sks  tatap muka dan praktikum ?', 1, 'Daftar FRS mahasiswa', 2, 3, '2019-07-13 09:40:18', '2019-07-13 09:40:18'),
(43, NULL, 'Apakah PS sudah menentukan bobot tugas akhir bagi mahasiswa ?', 1, NULL, 2, 3, '2019-07-13 09:40:29', '2019-07-13 09:40:29'),
(44, NULL, 'Apakah PS sudah menentukan masa studi mahasiswa ?', 1, NULL, 2, 3, '2019-07-13 09:40:39', '2019-07-13 09:40:39'),
(45, 'Standar Mutu Pendidikan :  Standar Penilaian Pembelajaran (std-4)\r\n\r\nKurikulum PS T.Informatika (2017-2022)', 'Apakah PS memiliki standar penilaian dalam bentuk rubik untuk setiap metode pembelajaran ?', 1, 'Tertuang dalam Grade Schema yang menjadi satu kesatuan dalam rancangan tugas di RPS.', 2, 4, '2019-07-13 09:42:02', '2019-07-13 09:42:02'),
(46, NULL, 'Apakah PS memiliki standar bobot untuk nilai UTS, praktikum, vclass maupun softskill ?', 1, NULL, 2, 4, '2019-07-13 09:42:13', '2019-07-13 09:42:13'),
(47, NULL, 'Apakah PS memiliki standar nilai mutu untuk kualifikasi keberhasilan mahasiswa ?', 1, NULL, 2, 4, '2019-07-13 09:42:25', '2019-07-13 09:42:25'),
(48, NULL, 'Apakah PS memiliki standar kelulusan mahasiswa ?', 1, NULL, 2, 4, '2019-07-13 09:42:33', '2019-07-13 09:42:33'),
(49, NULL, 'Apakah PS memiliki standar sidang tugas akhir, PI dan sidang komprehensif mahasiswa ?', 1, NULL, 2, 4, '2019-07-13 09:42:40', '2019-07-13 09:42:40'),
(50, NULL, 'Apakah PS memiliki standar dokumen yang menunjukkan kelulusan mahasiswa', 1, NULL, 2, 4, '2019-07-13 09:42:50', '2019-07-13 09:42:50'),
(51, NULL, 'Apakah PS melakukan sosialisasi terkait standar penilaian kepada dosen, mahasiswa dan sivitas akademika ?', 1, 'Melalui surat edaran, rapat, orientasi mahasiswa baru', 2, 4, '2019-07-13 09:43:02', '2019-07-13 09:43:02'),
(52, 'Standar Mutu Pendidikan :  Standar Dosen dan Tenaga Kependidikan  (std-5)', 'Apakah PS memiliki standar pengelolaan SDM ?', 1, 'Mengikuti ketentuan pengelolaan SDM Universitas yang dikoordinir Wakil Rektor II dan Biro Administasi Umum', 2, 5, '2019-07-13 10:10:11', '2019-07-13 10:10:11'),
(53, NULL, 'Apakah PS memiliki standar rektrutmen dan seleksi dosen serta tenaga kependidikan ?', 1, 'Mengikuti ketentuan pengelolaan SDM Universitas yang dikoordinir Wakil Rektor II dan Biro Administasi Umum\r\n', 2, 5, '2019-07-13 10:10:23', '2019-07-13 10:10:23'),
(54, NULL, 'Apakah PS memiliki standar beban kerja dosen ?', 1, 'Mengikuti ketentuan pemerintah (kemenristekdikti ) dan ketentuan univeristias yang dikoordinir Wakil Rektor II dan Biro Administasi Umum', 2, 5, '2019-07-13 10:11:47', '2019-07-13 10:11:47'),
(55, NULL, 'Apakah PS memiliki program Pengembangan dosen dan tenaga kependidikan ?', 1, 'Berkoordinasi dengan Wakil rector II, Wakil Dekan II dan Biro Administrasi Umum\r\nPengembangan SDM : studi lanjut, mengikuti pelatihan, workshop, seminar', 2, 5, '2019-07-13 10:11:58', '2019-07-13 10:11:58'),
(56, NULL, 'Apakah PS melakukan penilaian performa kinerja dosen ?', 1, 'Melalui pemantauan pengisian BKD (Beban Kerja Dosen) yang haus diisi dosen setiap  semester dan dilaporkan ke kemenristekdikti melalui  BAU\r\nMelalui monitoring kehadiran mengajar di sekdos\r\nMelalui monitoring keaktifan penelitian di Lemlit\r\nMelalui monitoring keaktifan monitoring abdimas di LPM\r\nMelalui penilaian umum terkait kondite dosen dan tenaga kependidikan  bekoordinasi dengan Bajamtu', 2, 5, '2019-07-13 10:12:15', '2019-07-13 10:12:15'),
(57, NULL, 'Apakah PS memiliki aturan terkait reward dan punishement bagi dosen dan tenaga kependidikan ?', 1, 'Mengikuti aturan yang berlaku di Universitas Gunadarma', 2, 5, '2019-07-13 10:12:35', '2019-07-13 10:12:35'),
(58, 'Standar Mutu Pendidikan :  Standar Sarana dan Prasarana  (std-6)', 'Apakah PS memiliki ruang kelas yang memadai terkait ukuran dan jumlah ruang ?', 1, NULL, 2, 6, '2019-07-13 10:15:48', '2019-07-13 10:15:48'),
(59, NULL, 'Apakah PS memiliki ruang layanan mahasiswa ?', 1, NULL, 2, 6, '2019-07-13 10:16:08', '2019-07-13 10:16:08'),
(60, NULL, 'Apakah PS memiliki ruang dosen yang memadai ?', 1, NULL, 2, 6, '2019-07-13 10:16:17', '2019-07-13 10:16:17'),
(61, NULL, 'Apakah PS memiliki laboratorium yang memadai   ?', 1, 'Meliputi laboratorium computer (umum), laboratorium multimedia, laboratorium machine/deep learning', 2, 6, '2019-07-13 10:16:31', '2019-07-13 10:16:31'),
(62, NULL, 'Apakah PS memiliki sarana laboratorium yang memadai ?', 1, 'Komputer, perangkat multimedia, super computer, perangkat virtual reality', 2, 6, '2019-07-13 10:16:43', '2019-07-13 10:16:43'),
(63, NULL, 'Apakah PS menerapkan K3 untuk kegiatan yang dilakukan di laboratorium ?', 1, NULL, 2, 6, '2019-07-13 10:16:53', '2019-07-13 10:16:53'),
(64, NULL, 'Apakah PS memiliki koleksi buku, jurnal, dan publikasi  bidang informatika yang memadai ?', 1, 'Tersedia di perpustakaan', 2, 6, '2019-07-13 10:17:03', '2019-07-13 10:17:03'),
(65, NULL, 'Apakah PS memiliki koleksi buku, jurnal dan publikasi digital bidang informatika yang memadai ?', 1, 'Tersedia di perpustakaan dan ada akun khusus untuk mengakses informasi digital tersebut.', 2, 6, '2019-07-13 10:18:00', '2019-07-13 10:18:00'),
(66, NULL, 'Apakah PS memiliki sarana pembelajaran yang memadai (misal : viewer )?', 1, 'Dikoordinir oleh Badan Pengelola Lingkungan Kampus', 2, 6, '2019-07-13 10:18:13', '2019-07-13 10:18:13'),
(68, 'Standar Mutu Pendidikan :  Standar Pengelolaan Pembelajaran  (std-7)', 'Apakah PS memiliki struktur organisasi di tingkat PS dan kaitannya dengan struktur di atasnya dan unit-unit kerja lainnya ?', 1, 'Mengacu pada struktur organisasi di Universitas Gunadarma', 2, 7, '2019-07-13 10:21:57', '2019-07-13 10:21:57'),
(69, NULL, 'Apakah PS memiliki job desk diantara SDM yang ada di dalamnya (dosen, tenaga kependidikan) ?', 1, NULL, 2, 7, '2019-07-13 10:22:51', '2019-07-13 10:22:51'),
(70, NULL, 'Apakah PS memiliki pedoman pelaksanaan pengelolaan kurikulum ?', 1, NULL, 2, 7, '2019-07-13 10:23:03', '2019-07-13 10:23:03'),
(71, NULL, 'Apakah PS memiliki Renstra dan Renop ?', 1, NULL, 2, 7, '2019-07-13 10:23:13', '2019-07-13 10:23:13'),
(72, NULL, 'Apakah PS memiliki angaran dan biaya operasional ?', 1, 'Setiap kegiatan yang membutuhkan pendanaan diajukan ke wakil dekan II untuk dimintai persetujuannya.', 2, 7, '2019-07-13 10:23:44', '2019-07-13 10:23:44'),
(73, NULL, 'Apakah PS menyusun Kalender Akademik setiap semester', 1, 'Mengikuti ketentuan kalender akademik yang telah disusun BAAK dan disetujui pimpinan universitas', 2, 7, '2019-07-13 10:23:57', '2019-07-13 10:23:57'),
(74, NULL, 'Apakah PS mengelola sarana dan prasarana pembelajaran ?', 1, 'Berkoordinasi dnegan Badan Penelolan Lingkungan Kampus', 2, 7, '2019-07-13 10:24:13', '2019-07-13 10:24:13'),
(75, NULL, 'Apakah melakukan evaluasi proses pembelajaran ?', 1, 'Melalui wadah rapat PS dengan dosen, tenaga kependidikan dan unit kerja terkait  sebelum tahun ajaran dimulai.', 2, 7, '2019-07-13 10:24:41', '2019-07-13 10:24:41'),
(76, NULL, 'Apakah melaporkan kegiatan PS secara rutin kepada pimpinan ?', 1, 'Melalui rapat dengan fakultas sebelum tahun ajaran dimulai\r\nMelalui rapat kerja Universitas setiap tahun', 2, 7, '2019-07-13 10:24:53', '2019-07-13 10:24:53'),
(77, 'Standar Mutu Pendidikan :  Standar Pembiayaan Pembelajaran  (std-8', 'Apakah PS memiliki standar pembiayaan  pembelajaran ?', 2, 'Diatur di tingkat universitas dan dekanat, tetapi apabila ada kegiatan yang membutuhkan biaya mengajukan proposal ke Wakil dekan II dan apabila disetuji dana akan turun dan PS wajib melaporkan kegitan yang telah dilakukan termasuk pengeluaran biayanya.', 2, 8, '2019-07-13 10:29:01', '2019-07-13 10:29:01'),
(78, NULL, 'Apakah PS memiliki standar biaya investasi ?', 2, NULL, 2, 8, '2019-07-13 10:33:59', '2019-07-13 10:33:59'),
(79, NULL, 'Apakah PS memiliki standar biaya operasional ?', 2, NULL, 2, 8, '2019-07-13 10:34:07', '2019-07-13 10:34:07'),
(80, NULL, 'Apakah PS memiliki standar implementasi pembiayaan', 2, NULL, 2, 8, '2019-07-13 10:34:16', '2019-07-13 10:34:16'),
(81, NULL, 'Apakah PS memiliki standar monitoring pembiayaan?', 2, NULL, 2, 8, '2019-07-13 10:34:25', '2019-07-13 10:34:25'),
(82, NULL, 'Apakah PS memiliki standar sumber dana ?', 2, NULL, 2, 8, '2019-07-13 10:34:34', '2019-07-13 10:34:34'),
(83, NULL, 'Apakah PS memiliki standar penggunaan dana', 1, 'PS memiliki daftar penggunaan dana untuk setiap kegiatan, namun standar besaran dana untuk setiap kegiatan ditentukan oleh universitas dan dicairkan melalui wakil dekan II dan bendahara, tergantung tingkat kepentingannya', 2, 8, '2019-07-13 10:35:01', '2019-07-13 10:35:01');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_audit_ptk`
--

CREATE TABLE `tbl_audit_ptk` (
  `audit_ptk_id` int(11) NOT NULL,
  `audit_ptk_ref_id` int(11) DEFAULT NULL,
  `audit_ptk_nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `audit_ptk_file` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tbl_audit_ptk`
--

INSERT INTO `tbl_audit_ptk` (`audit_ptk_id`, `audit_ptk_ref_id`, `audit_ptk_nama`, `audit_ptk_file`, `created_at`, `updated_at`) VALUES
(5, 2, 'Hai', 'dd86664b32ab30d7867548a939d7cca2.doc', '2019-07-13 17:00:22', '2019-07-13 17:00:22'),
(6, 2, 'Hai', '196d23dd86598a9e3b288cdd657e279b.doc', '2019-07-13 17:01:31', '2019-07-13 17:01:31');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_audit_ptk_form`
--

CREATE TABLE `tbl_audit_ptk_form` (
  `ptk_id` int(11) NOT NULL,
  `ptk_audit_jadwal_ref` int(11) NOT NULL,
  `ptk_input_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Bajamtu-UG/AMI-PTK/PS/01.01/2018',
  `ptk_input_uraian_temuan` longtext COLLATE utf8mb4_unicode_ci,
  `ptk_input_rtk` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tbl_audit_ptk_form`
--

INSERT INTO `tbl_audit_ptk_form` (`ptk_id`, `ptk_audit_jadwal_ref`, `ptk_input_no`, `ptk_input_uraian_temuan`, `ptk_input_rtk`, `created_at`, `updated_at`) VALUES
(4, 2, 'Bajamtu-UG/AMI-PTK/PS/01.01/2018', 'aa\r\n\r\nbb', 'sd', '2019-08-17 07:53:40', '2019-08-17 10:45:25');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_audit_saran_perbaikan`
--

CREATE TABLE `tbl_audit_saran_perbaikan` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `bidang` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kelebihan` text COLLATE utf8_unicode_ci,
  `peluang_peningkatan` text COLLATE utf8_unicode_ci,
  `audit_borang_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `tbl_audit_saran_perbaikan`
--

INSERT INTO `tbl_audit_saran_perbaikan` (`id`, `bidang`, `kelebihan`, `peluang_peningkatan`, `audit_borang_id`, `created_at`, `updated_at`) VALUES
(1, '314', '3', '3', 2, '2019-08-14 04:57:40', '2019-08-14 07:28:01');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_audit_temuan`
--

CREATE TABLE `tbl_audit_temuan` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kts_ob` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `referensi_butir_mutu` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pernyataan` longtext COLLATE utf8_unicode_ci NOT NULL,
  `audit_borang_id` int(11) NOT NULL,
  `audit_pertanyaan_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `tbl_audit_temuan`
--

INSERT INTO `tbl_audit_temuan` (`id`, `kts_ob`, `referensi_butir_mutu`, `pernyataan`, `audit_borang_id`, `audit_pertanyaan_id`, `created_at`, `updated_at`) VALUES
(163, 's', 'Standar Mutu Pendidikan :  Standar Pembiayaan Pembelajaran  (std-8', 'Ok sip', 2, 77, '2019-08-13 04:37:47', '2019-08-13 04:46:43'),
(164, NULL, NULL, 'ya punyalah', 2, 78, '2019-08-13 04:37:47', '2019-08-13 04:47:20'),
(165, NULL, NULL, 'Apakah PS memiliki standar biaya operasional ?', 2, 79, '2019-08-13 04:37:47', '2019-08-13 04:37:47'),
(166, NULL, NULL, 'Apakah PS memiliki standar implementasi pembiayaan', 2, 80, '2019-08-13 04:37:47', '2019-08-13 04:37:47'),
(167, NULL, NULL, 'Apakah PS memiliki standar monitoring pembiayaan?', 2, 81, '2019-08-13 04:37:47', '2019-08-13 04:37:47'),
(168, NULL, NULL, 'Apakah PS memiliki standar sumber dana ?', 2, 82, '2019-08-13 04:37:47', '2019-08-13 04:37:47');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_audit_tujuan`
--

CREATE TABLE `tbl_audit_tujuan` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tujuan` text COLLATE utf8_unicode_ci NOT NULL,
  `yes_no_check` int(11) NOT NULL,
  `audit_jadwal_ref_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `tbl_audit_tujuan`
--

INSERT INTO `tbl_audit_tujuan` (`id`, `tujuan`, `yes_no_check`, `audit_jadwal_ref_id`, `created_at`, `updated_at`) VALUES
(3, 'Siap ok!! K', 1, 6, '2019-05-15 16:23:51', '2019-05-19 06:31:06'),
(8, 'HM', 1, 2, '2019-08-12 07:21:21', '2019-08-12 07:25:42'),
(11, 'Sip', 0, 2, '2019-08-13 04:32:30', '2019-08-13 04:32:30'),
(12, 'Ok', 0, 2, '2019-08-13 04:33:28', '2019-08-13 04:33:28'),
(13, 'Ok2', 0, 2, '2019-08-13 04:34:36', '2019-08-13 04:34:36');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_category`
--

CREATE TABLE `tbl_category` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `tbl_category`
--

INSERT INTO `tbl_category` (`id`, `category_name`, `created_at`, `updated_at`) VALUES
(1, 'Pendidikan', '2019-05-05 15:45:27', '2019-05-05 15:45:27'),
(2, 'Penelitian', '2019-05-05 15:45:27', '2019-05-05 15:45:27'),
(3, 'PKM', '2019-05-05 15:45:27', '2019-05-05 15:45:27');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_emi_bobot_nilai`
--

CREATE TABLE `tbl_emi_bobot_nilai` (
  `bobot_id` bigint(20) UNSIGNED NOT NULL,
  `profil_id` int(11) DEFAULT NULL,
  `tahun_emi` int(11) DEFAULT NULL,
  `isian_standar_id` int(11) NOT NULL,
  `nilai` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `tbl_emi_bobot_nilai`
--

INSERT INTO `tbl_emi_bobot_nilai` (`bobot_id`, `profil_id`, `tahun_emi`, `isian_standar_id`, `nilai`, `created_at`, `updated_at`) VALUES
(1, 7, 2018, 1, 1, '2019-07-27 16:14:37', '2019-07-27 16:15:47'),
(2, 7, 2018, 2, 1, '2019-07-28 06:22:59', '2019-07-28 06:22:59'),
(3, 7, 2018, 3, 1, '2019-07-28 06:59:04', '2019-07-28 06:59:04'),
(4, 7, 2018, 4, 1, '2019-07-28 07:00:48', '2019-07-28 07:00:48'),
(5, 7, 2018, 6, 1, '2019-07-28 07:05:52', '2019-07-28 07:05:52'),
(6, 7, 2018, 7, 1, '2019-07-28 07:07:09', '2019-07-28 07:07:09'),
(7, 7, 2018, 10, 1, '2019-07-28 07:10:33', '2019-07-28 07:10:33'),
(8, 7, 2018, 9, 1, '2019-07-28 07:10:43', '2019-07-28 07:10:43'),
(9, 7, 2018, 8, 1, '2019-07-28 07:10:50', '2019-07-28 07:10:50'),
(10, 7, 2018, 11, 1, '2019-07-28 07:14:47', '2019-07-28 07:14:47'),
(11, 7, 2018, 12, 2, '2019-07-31 15:14:34', '2019-07-31 15:14:35'),
(12, 7, 2018, 13, 5, '2019-07-31 15:21:04', '2019-07-31 15:21:04');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_emi_isian_standar`
--

CREATE TABLE `tbl_emi_isian_standar` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `emi_jenjang_id` int(11) DEFAULT NULL,
  `emi_standar_id` int(11) NOT NULL,
  `emi_isian_standar` longtext COLLATE utf8_unicode_ci NOT NULL,
  `emi_bobot_persen` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `emi_capaian_terbobot` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `tbl_emi_isian_standar`
--

INSERT INTO `tbl_emi_isian_standar` (`id`, `emi_jenjang_id`, `emi_standar_id`, `emi_isian_standar`, `emi_bobot_persen`, `emi_capaian_terbobot`, `created_at`, `updated_at`) VALUES
(1, 2, 1, 'Haha', '0.02', NULL, '2019-07-27 12:08:02', '2019-07-27 12:08:02'),
(2, 2, 2, 'hmm', '0.3', NULL, '2019-07-28 06:19:49', '2019-07-28 06:19:49'),
(3, 2, 3, 'WWW', '0.01', NULL, '2019-07-28 06:58:05', '2019-07-28 06:58:05'),
(4, 2, 4, 'DIDADAM', '0.01', NULL, '2019-07-28 06:59:45', '2019-07-28 06:59:45'),
(5, 2, 5, 'panaas', '0.09', NULL, '2019-07-28 07:02:16', '2019-07-28 07:02:16'),
(6, 2, 6, 'materi', '0.08', NULL, '2019-07-28 07:04:39', '2019-07-28 07:04:39'),
(7, 2, 7, 'tidak adanya korupsi', '1', NULL, '2019-07-28 07:06:22', '2019-07-28 07:06:22'),
(8, 2, 8, 'kata siap', '0.03', NULL, '2019-07-28 07:07:34', '2019-07-28 07:07:34'),
(9, 2, 9, 'peneliti cakep', '0.3', NULL, '2019-07-28 07:09:08', '2019-07-28 07:09:08'),
(10, 2, 10, 'aaa', '0.03', NULL, '2019-07-28 07:09:45', '2019-07-28 07:09:45'),
(11, 2, 7, 'uhuyyy', '0.25', NULL, '2019-07-28 07:14:06', '2019-07-28 07:14:06'),
(12, 2, 1, 'yha', '0.5', NULL, '2019-07-31 15:14:11', '2019-07-31 15:14:11'),
(13, 2, 10, 'cui', '0.09', NULL, '2019-07-31 15:17:02', '2019-07-31 15:17:02');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_emi_keadaan_prodi`
--

CREATE TABLE `tbl_emi_keadaan_prodi` (
  `keadaan_id` bigint(20) UNSIGNED NOT NULL,
  `emi_jenjang_id` int(11) NOT NULL,
  `emi_standar_id` int(11) NOT NULL,
  `emi_isian_standar_id` int(11) NOT NULL,
  `emi_bobot_nilai` int(11) NOT NULL,
  `keadaan_prodi_saat_ini` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `tbl_emi_keadaan_prodi`
--

INSERT INTO `tbl_emi_keadaan_prodi` (`keadaan_id`, `emi_jenjang_id`, `emi_standar_id`, `emi_isian_standar_id`, `emi_bobot_nilai`, `keadaan_prodi_saat_ini`, `created_at`, `updated_at`) VALUES
(1, 2, 1, 1, 1, 'aass', '2019-07-27 16:11:31', '2019-07-27 16:14:18'),
(2, 2, 1, 1, 2, 'aa', '2019-07-27 16:13:54', '2019-07-27 16:13:54'),
(3, 2, 2, 2, 1, 'para si', '2019-07-28 06:20:07', '2019-07-28 06:20:07'),
(4, 2, 3, 3, 1, 'zzz', '2019-07-28 06:58:47', '2019-07-28 06:58:47'),
(5, 2, 4, 4, 1, 'atahiyaa', '2019-07-28 07:00:29', '2019-07-28 07:00:29'),
(6, 2, 5, 5, 1, 'ruang terbuka', '2019-07-28 07:03:10', '2019-07-28 07:03:10'),
(7, 2, 6, 6, 1, 'dari dulu sampai sekarang sama', '2019-07-28 07:05:25', '2019-07-28 07:05:25'),
(8, 2, 7, 7, 1, 'banyak kasus korupsi', '2019-07-28 07:06:44', '2019-07-28 07:06:44'),
(9, 2, 8, 8, 1, 'hmm ngantuk', '2019-07-28 07:08:10', '2019-07-28 07:08:10'),
(10, 2, 9, 9, 1, 'cakep', '2019-07-28 07:09:28', '2019-07-28 07:09:28'),
(11, 2, 10, 10, 1, 'galat', '2019-07-28 07:10:12', '2019-07-28 07:10:12'),
(12, 2, 7, 11, 1, 'nyoba ah', '2019-07-28 07:15:09', '2019-07-28 07:15:09'),
(13, 2, 10, 10, 5, 'yoi', '2019-07-31 15:16:47', '2019-07-31 15:16:47');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_emi_rekap_standar`
--

CREATE TABLE `tbl_emi_rekap_standar` (
  `rekap_id` int(11) NOT NULL,
  `rekap_standar_id` int(11) NOT NULL,
  `rekap_profil_id` int(11) NOT NULL,
  `rekap_tahun_pengukuran` int(11) NOT NULL,
  `rekap_temuan` text COLLATE utf8mb4_unicode_ci,
  `rekap_hasil_temuan_sebab` text COLLATE utf8mb4_unicode_ci,
  `rekap_hasil_temuan_akibat` text COLLATE utf8mb4_unicode_ci,
  `rekap_rekomendasi` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tbl_emi_rekap_standar`
--

INSERT INTO `tbl_emi_rekap_standar` (`rekap_id`, `rekap_standar_id`, `rekap_profil_id`, `rekap_tahun_pengukuran`, `rekap_temuan`, `rekap_hasil_temuan_sebab`, `rekap_hasil_temuan_akibat`, `rekap_rekomendasi`, `created_at`, `updated_at`) VALUES
(1, 5, 7, 2018, 'huff', 'hii', NULL, NULL, '2019-08-01 15:17:56', '2019-08-01 15:19:04');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_emi_standar`
--

CREATE TABLE `tbl_emi_standar` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_standar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `tbl_emi_standar`
--

INSERT INTO `tbl_emi_standar` (`id`, `nama_standar`, `created_at`, `updated_at`) VALUES
(1, 'Standar Isi', '2019-05-20 17:00:00', '2019-05-20 17:00:00'),
(2, 'Standar Proses', '2019-05-20 17:00:00', '2019-05-20 17:00:00'),
(3, 'Standar Kompetensi Lulusan (SKL)', '2019-05-20 17:00:00', '2019-05-20 17:00:00'),
(4, 'Standar Pendidik dan Tenaga Kependidikan (PTK)', '2019-05-20 17:00:00', '2019-05-20 17:00:00'),
(5, 'Ketersediaan Sarana dan Prasarana Pendidikan', '2019-05-20 17:00:00', '2019-05-20 17:00:00'),
(6, 'Pengelolaan Program Studi', '2019-05-20 17:00:00', '2019-05-20 17:00:00'),
(7, 'Standar Pembiayaan', '2019-05-20 17:00:00', '2019-05-20 17:00:00'),
(8, 'Standar Penilaian', '2019-05-20 17:00:00', '2019-05-20 17:00:00'),
(9, 'Standar Penelitian', '2019-05-20 17:00:00', '2019-05-20 17:00:00'),
(10, 'Standar Pengabdian Kepada Masyarakat', '2019-05-20 17:00:00', '2019-05-20 17:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_fakultas_dep_prodi`
--

CREATE TABLE `tbl_fakultas_dep_prodi` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT 'id',
  `nama_bagian` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'bisa fakultas, departemen, prodi, lembaga..',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `tbl_fakultas_dep_prodi`
--

INSERT INTO `tbl_fakultas_dep_prodi` (`id`, `nama_bagian`, `created_at`, `updated_at`) VALUES
(1, 'Fakultas Ilmu Komputer dan Teknologi Informasi', '2019-05-29 22:00:00', '2019-05-29 22:00:00'),
(2, 'Teknik Industri', '2019-07-12 17:00:00', '2019-07-12 17:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_jenjang`
--

CREATE TABLE `tbl_jenjang` (
  `jenjang_id` int(11) NOT NULL,
  `jenjang_nama` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tbl_jenjang`
--

INSERT INTO `tbl_jenjang` (`jenjang_id`, `jenjang_nama`, `created_at`, `updated_at`) VALUES
(1, 'D3', '2019-07-27 04:15:11', '0000-00-00 00:00:00'),
(2, 'S1', '2019-07-27 04:15:11', '0000-00-00 00:00:00'),
(3, 'S2', '2019-07-27 04:15:22', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_manual_mutu`
--

CREATE TABLE `tbl_manual_mutu` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` int(11) NOT NULL,
  `profil_id` int(11) NOT NULL DEFAULT '7',
  `standar_category_id` int(11) NOT NULL,
  `standar_dikti_id` int(11) NOT NULL,
  `file_location` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `tbl_manual_mutu`
--

INSERT INTO `tbl_manual_mutu` (`id`, `category_id`, `profil_id`, `standar_category_id`, `standar_dikti_id`, `file_location`, `created_at`, `updated_at`) VALUES
(1, 3, 7, 11, 17, 'cefd247e49ca66ae8c2af4692a428b49.xlsx', '2019-05-08 08:56:05', '2019-05-08 09:16:11'),
(2, 1, 7, 1, 1, 'd9f4508c1d45ead0a9ee6792f8e4e1ea.docx', '2019-05-09 07:22:14', '2019-07-09 11:31:20'),
(3, 1, 7, 2, 1, '8939e13c340ee150dc43a6fbd4c31c17.docx', '2019-07-09 11:25:30', '2019-07-09 11:25:30'),
(4, 1, 7, 3, 1, '1c89f017d3f6f8b9b8165f89de1b9298.docx', '2019-07-09 11:25:46', '2019-07-09 11:25:46'),
(5, 1, 7, 4, 1, '9e680e94609fdd4ccad58cf61a565181.docx', '2019-07-09 11:25:53', '2019-07-09 11:25:53'),
(6, 1, 7, 5, 1, '60acd70e3366a0cf9cfba49d48ec406b.docx', '2019-07-09 11:26:01', '2019-07-09 11:26:01'),
(7, 1, 7, 1, 2, 'ec2ce6ca672620f0ecfe1e45c61db092.docx', '2019-07-09 11:26:14', '2019-07-09 11:26:14'),
(8, 1, 7, 2, 2, '678eb7a6e6f66b3bb4292e1536385b1d.docx', '2019-07-09 11:26:23', '2019-07-09 11:26:23'),
(9, 1, 7, 3, 2, 'dfcd7c5a7198f4adacc6cb37d76ae04c.docx', '2019-07-09 11:26:33', '2019-07-09 11:26:33'),
(10, 1, 7, 4, 2, '512656192740e000a7f93f403639c0f0.docx', '2019-07-09 11:26:47', '2019-07-09 11:26:47'),
(11, 1, 7, 5, 2, '56887cca4556c0c2bfe52de06a177bf0.docx', '2019-07-09 11:26:56', '2019-07-09 11:26:56'),
(12, 1, 7, 1, 3, 'e07ba6d952ca52f2b6b0baaf43078424.docx', '2019-07-09 11:27:07', '2019-07-09 11:27:07'),
(13, 1, 7, 2, 3, '614ccefe23ba3caac2d384a5be3e513c.docx', '2019-07-09 11:27:17', '2019-07-09 11:27:17'),
(14, 1, 7, 3, 3, '923567994204cae79d81c5bdfd2294af.docx', '2019-07-09 11:27:25', '2019-07-09 11:27:25'),
(15, 1, 7, 4, 3, 'c372e5bec381ddd36c51484b73ccdf12.docx', '2019-07-09 11:27:47', '2019-07-09 11:27:47'),
(16, 1, 7, 5, 3, '0822ee9c0e774a16099ca8c9e1e28250.docx', '2019-07-09 11:27:56', '2019-07-09 11:27:56'),
(17, 1, 7, 1, 4, 'a959cbb87079f787120eec23ab064570.docx', '2019-07-09 11:28:30', '2019-07-09 11:28:30'),
(18, 1, 7, 2, 4, 'b0b72c860fd40322da29f67e6566faaf.docx', '2019-07-09 11:28:39', '2019-07-09 11:28:39'),
(19, 1, 7, 3, 4, 'c97ebaf8f15aeb1a60da1bec11b26979.docx', '2019-07-09 11:28:46', '2019-07-09 11:28:46'),
(20, 1, 7, 4, 4, '8b87a89da1d536f872d34777c22e973f.docx', '2019-07-09 11:28:53', '2019-07-09 11:28:53'),
(21, 1, 7, 5, 4, '9c4a298b038a7b2ec3766043a58f3fc1.docx', '2019-07-09 11:29:05', '2019-07-09 11:29:05'),
(22, 1, 7, 1, 5, '1e79a6a7ae949d94171e5528a636f0fc.docx', '2019-07-09 11:29:18', '2019-07-09 11:30:00'),
(23, 1, 7, 2, 5, 'ea6e157f411ff47b752a836c51c9219c.docx', '2019-07-09 11:29:27', '2019-07-09 11:29:27'),
(24, 1, 7, 3, 5, '11ea3975c17ad761bed010d9dd2dbfac.docx', '2019-07-09 11:30:29', '2019-07-09 11:30:29'),
(25, 1, 7, 4, 5, '439710496ac06f3242cc549e66b0b6a4.docx', '2019-07-09 11:30:38', '2019-07-09 11:30:38'),
(26, 1, 7, 5, 5, '3db857c6c62ddf4bfe39bdb1b1d2c4ef.docx', '2019-07-09 11:30:48', '2019-07-09 11:30:48'),
(27, 1, 7, 1, 6, 'dbaa736bb4d0240c63c08a6fc2ef33ff.docx', '2019-07-09 11:31:33', '2019-07-09 11:31:33'),
(28, 1, 7, 2, 6, '17f0c26168df1e40330d7703a8a95526.docx', '2019-07-09 11:31:43', '2019-07-09 11:31:43'),
(29, 1, 7, 3, 6, '0d4a8f1a72d921e20b61c69bf56eddad.docx', '2019-07-09 11:31:51', '2019-07-09 11:31:51'),
(30, 1, 7, 4, 6, '717928dd4e367c69aecae46149c80d60.docx', '2019-07-09 11:32:10', '2019-07-09 11:32:10'),
(31, 1, 7, 5, 6, 'f93d85f0ced944df2756ec3fd923c605.docx', '2019-07-09 11:32:20', '2019-07-09 11:32:20'),
(32, 1, 7, 1, 7, 'f89ff3061902754bbb04a79286e51635.docx', '2019-07-09 11:32:35', '2019-07-09 11:32:35'),
(33, 1, 7, 2, 7, 'f29321ea27bffc86e0e162d06c78ecd4.docx', '2019-07-09 11:32:45', '2019-07-09 11:32:45'),
(34, 1, 7, 3, 7, 'addef398a061b9c75f99decb7d86a947.docx', '2019-07-09 11:32:54', '2019-07-09 11:32:54'),
(35, 1, 7, 4, 7, 'd2a50bd652cec59ba670aa1dec293d6b.docx', '2019-07-09 11:33:17', '2019-07-09 11:33:17'),
(36, 1, 7, 5, 7, '4d0a2e305555be026ca993f1f38dfba5.docx', '2019-07-09 11:33:27', '2019-07-09 11:33:27'),
(37, 1, 7, 1, 8, '10ff834b962ebe5222c2522b555c6afa.docx', '2019-07-09 11:33:37', '2019-07-09 11:33:37'),
(38, 1, 7, 2, 8, '03f54ec8649b57359d5d13abfc12e797.docx', '2019-07-09 11:33:46', '2019-07-09 11:33:46'),
(39, 1, 7, 3, 8, '68cf90e6b889b854c6b9d006d626f0eb.docx', '2019-07-09 11:33:55', '2019-07-09 11:33:55'),
(40, 1, 7, 4, 8, 'eb8463b64255dd2dfcafa201c62516de.docx', '2019-07-09 11:34:03', '2019-07-09 11:34:03'),
(41, 1, 7, 5, 8, '9ea95b417960a2aa3f75f18017d9b178.docx', '2019-07-09 11:34:09', '2019-07-09 11:34:09'),
(42, 1, 1, 1, 1, 'ce5ac8277ec8d2543ba97a16706ef8c5.pdf', '2019-08-01 15:38:36', '2019-08-01 15:38:36');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_menu`
--

CREATE TABLE `tbl_menu` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `menu_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `menu_icon` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `menu_enabled` int(11) NOT NULL DEFAULT '1',
  `route_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `tbl_menu`
--

INSERT INTO `tbl_menu` (`id`, `parent_id`, `level`, `menu_name`, `menu_icon`, `menu_enabled`, `route_name`, `created_at`, `updated_at`) VALUES
(1, -1, 0, 'Prodi', 'edit', 1, '#', NULL, NULL),
(2, 1, 1, 'Profil Prodi', 'edit', 1, 'borang/', NULL, NULL),
(3, 1, 1, 'Standar Penjaminan Mutu', 'edit', 1, '#', NULL, NULL),
(4, 3, 2, 'Pendidikan', 'edit', 1, 'standar/1/', NULL, NULL),
(5, 3, 2, 'Pengabdian Masyarakat', 'edit', 1, 'standar/2/', NULL, NULL),
(6, 3, 2, 'Penelitian', 'edit', 1, 'standar/3/', NULL, NULL),
(7, 1, 1, 'Manual mutu', 'edit', 1, 'manual/', '2019-05-05 17:00:00', '2019-05-05 17:00:00'),
(8, 7, 2, 'Pendidikan', 'edit', 1, 'manual/1/', NULL, NULL),
(9, 7, 2, 'Pengabdian Masyarakat', 'edit', 1, 'manual/2/', NULL, NULL),
(10, 7, 2, 'Penelitian', 'edit', 1, 'manual/3/', NULL, NULL),
(11, -1, 0, 'EMI', 'edit', 1, 'emi/', '2019-05-11 17:00:00', '2019-05-11 17:00:00'),
(12, 11, 1, 'Standar Isi', 'edit', 1, 'rs/emi/{profil_id}/1/{tahun_pengisian}', '2019-05-12 17:00:00', '2019-05-12 17:00:00'),
(13, -1, 0, 'Audit Mutu Internal', 'edit', 1, 'auditor/', '2019-05-11 17:00:00', '2019-05-11 17:00:00'),
(15, 13, 1, 'Data Auditor', 'edit', 0, 'auditor/data_audit/', '2019-05-11 17:00:00', '2019-05-11 17:00:00'),
(16, 13, 1, 'Penunjukan Auditor', 'edit', 0, 'auditor/penunjukkan_audit/', '2019-05-11 17:00:00', '2019-05-11 17:00:00'),
(17, 13, 1, 'SK Auditor', 'edit', 0, 'auditor/sk_audit/', '2019-05-11 17:00:00', '2019-05-11 17:00:00'),
(18, 13, 1, 'Proses Audit Mutu Internal', 'edit', 0, 'auditor/ami/', '2019-05-11 17:00:00', '2019-05-11 17:00:00'),
(19, 11, 1, 'Standar Proses', 'edit', 1, 'rs/emi/{profil_id}/2/{tahun_pengisian}', '2019-05-12 17:00:00', '2019-05-12 17:00:00'),
(20, 11, 1, 'Standar Kompetensi Lulusan (SKL)', 'edit', 1, 'rs/emi/{profil_id}/3/{tahun_pengisian}', '2019-05-12 17:00:00', '2019-05-12 17:00:00'),
(21, 11, 1, 'Standar Pendidik dan Tenaga Kependidikan (PTK)', 'edit', 1, 'rs/emi/{profil_id}/4/{tahun_pengisian}', '2019-05-12 17:00:00', '2019-05-12 17:00:00'),
(22, 11, 1, 'Ketersediaan Sarana dan Prasarana Pendidikan', 'edit', 1, 'rs/emi/{profil_id}/5/{tahun_pengisian}', '2019-05-12 17:00:00', '2019-05-12 17:00:00'),
(23, 11, 1, 'Pengelolaan Program Studi', 'edit', 1, 'rs/emi/{profil_id}/6/{tahun_pengisian}', '2019-05-12 17:00:00', '2019-05-12 17:00:00'),
(24, 11, 1, 'Standar Pembiayaan', 'edit', 1, 'rs/emi/{profil_id}/7/{tahun_pengisian}', '2019-05-12 17:00:00', '2019-05-12 17:00:00'),
(25, 11, 1, 'Standar Penilaian', 'edit', 1, 'rs/emi/{profil_id}/8/{tahun_pengisian}', '2019-05-12 17:00:00', '2019-05-12 17:00:00'),
(26, 11, 1, 'Standar Penelitian', 'edit', 1, 'rs/emi/{profil_id}/9/{tahun_pengisian}', '2019-05-12 17:00:00', '2019-05-12 17:00:00'),
(27, 11, 1, 'Standar Pengabdian Kepada Masyarakat', 'edit', 1, 'rs/emi/{profil_id}/10/{tahun_pengisian}', '2019-05-12 17:00:00', '2019-05-12 17:00:00'),
(28, -1, 0, 'Pengguna', 'user', 1, '/user/#', '2019-05-27 17:00:00', '2019-05-27 17:00:00'),
(29, 28, 1, 'Profil saya', 'check', 1, '/user/profile', '2019-05-27 17:00:00', '2019-05-27 17:00:00'),
(30, 28, 1, 'Pengaturan pengguna SPMI', 'check', 1, '/user/admin/index', '2019-05-27 17:00:00', '2019-05-27 17:00:00'),
(31, 18, 2, 'Permohonan melaksanakan awal', 'check', 1, 'auditor/ami/step/permohonan', '2019-05-30 02:00:00', '2019-05-30 02:00:00'),
(32, 18, 2, 'Pencatatan KJM', 'check', 1, 'auditor/ami/step/pencatatan', '2019-05-30 02:00:00', '2019-05-30 02:00:00'),
(33, 18, 2, 'Perantara Auditor', 'check', 1, 'auditor/ami/step/surat_menyurat', '2019-05-30 02:00:00', '2019-05-30 02:00:00'),
(34, 18, 2, 'Pembuatan Jadwal', 'check', 1, 'auditor/ami/step/buat_jadwal', '2019-05-30 02:00:00', '2019-05-30 02:00:00'),
(35, 18, 2, 'PTK', 'check', 1, 'auditor/ami/step/ptk', '2019-05-30 02:00:00', '2019-05-30 02:00:00'),
(36, 35, 3, 'Rencana Penyelesaian', 'check', 1, 'auditor/ami/step/ptk/rencana', '2019-05-30 02:00:00', '2019-05-30 02:00:00'),
(37, 35, 3, 'Pantauan', 'check', 1, 'auditor/ami/step/ptk/pantauan', '2019-05-30 02:00:00', '2019-05-30 02:00:00'),
(38, 35, 3, 'Laporan', 'check', 1, 'auditor/ami/step/ptk', '2019-05-30 02:00:00', '2019-05-30 02:00:00'),
(39, 13, 1, 'Data Permohonan', 'edit', 1, '/auditor/ami/tahap/kjm/permohonan', NULL, NULL),
(40, 13, 1, 'Buat penentuan jadwal dan auditor', 'edit', 1, '/auditor/ami/tahap/kjm/penentuan/create', NULL, NULL),
(41, 13, 1, 'Data Kesediaan Auditor', 'edit', 1, '/auditor/ami/tahap/kjm/penentuan', NULL, NULL),
(42, 13, 1, 'Data Pelaksana Audit', 'edit', 1, '/auditor/ami/tahap/kjm/data_pelaksana', NULL, NULL),
(43, 11, 1, 'Rekapitulasi dan Analisis per Standar', 'check', 1, 'rs/emi/rekap/{profil_id}/{tahun_pengisian}/lihat', '2019-07-28 05:00:08', '2019-07-28 05:00:08'),
(44, 11, 1, 'Download Laporan', 'check', 1, 'rs/emi/grafik/{profil_id}/{tahun_pengisian}/lihat', '2019-07-28 05:00:08', '2019-07-28 05:00:08'),
(45, 13, 1, 'Daftar Pertanyaan', 'check', 1, '/auditor/ami/tahap/auditor/form/daftar_pertanyaan', NULL, NULL),
(46, 13, 1, 'Laporan Audit Mutu Internal Program Studi', 'check', 1, '/ar/ami/laporan_ami', '2019-08-01 17:00:00', '2019-08-01 17:00:00'),
(47, 13, 1, 'PTK', 'check', 1, '/ar/ami/ptk_form', '2019-08-01 17:00:00', '2019-08-01 17:00:00'),
(48, 13, 1, 'Buat Permohonan Audit', 'plus', 1, '/auditor/ami/tahap/permintaan', '2019-08-01 17:00:00', '2019-08-01 17:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_penilaian`
--

CREATE TABLE `tbl_penilaian` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `profil_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `bagian` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rekap_nilai` text COLLATE utf8_unicode_ci NOT NULL,
  `bobot_standar` double DEFAULT NULL,
  `nilai_bobot_standar` double DEFAULT NULL,
  `nilai_capaian_standar` double DEFAULT NULL,
  `sebutan` text COLLATE utf8_unicode_ci,
  `temuan` text COLLATE utf8_unicode_ci,
  `hasil_temuan` text COLLATE utf8_unicode_ci,
  `hasil_temuan_disebabkan` text COLLATE utf8_unicode_ci,
  `rekomendasi` text COLLATE utf8_unicode_ci,
  `format_standar` text COLLATE utf8_unicode_ci,
  `format_indikator` text COLLATE utf8_unicode_ci,
  `format_pencapaian` text COLLATE utf8_unicode_ci,
  `format_midline` text COLLATE utf8_unicode_ci,
  `format_baseline` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_perencanaan`
--

CREATE TABLE `tbl_perencanaan` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `profil_id` int(11) NOT NULL,
  `tujuan` text COLLATE utf8_unicode_ci NOT NULL,
  `sasaran` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_profil`
--

CREATE TABLE `tbl_profil` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_prodi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jenjang` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jenjang_plot_id` int(11) DEFAULT NULL COMMENT 'sambungin sama tabel jenjang, deteksi otomatis',
  `nama_fakultas` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telepon_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kode_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nama_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alamat_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tgl_prog` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `no_sk` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `no_sk_operasional` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tgl_pengisian` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `akreditasi_prog` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alamat_website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alamat_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telepon_prog` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `visi_prog` text COLLATE utf8_unicode_ci,
  `misi_prog` text COLLATE utf8_unicode_ci,
  `tujuan_prog` text COLLATE utf8_unicode_ci,
  `kompetensi_prog` text COLLATE utf8_unicode_ci,
  `peta_prog` text COLLATE utf8_unicode_ci,
  `kriteria_mhs` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jumlah_sks_min` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jumlah_nilai_d` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ipk_minimal` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `syarat_lainnya` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jumlah_mhs_tahun` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jumlah_mhs` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jumlah_dosen_tetap_ps` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jumlah_dosen_luar_ps` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jumlah_tenaga_kependidikan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `identitas_nama_ketua_prod` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `identitas_nidn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `identitas_nohp` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tahun_pengisian` int(11) NOT NULL DEFAULT '2019',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `tbl_profil`
--

INSERT INTO `tbl_profil` (`id`, `nama_prodi`, `jenjang`, `jenjang_plot_id`, `nama_fakultas`, `telepon_pt`, `kode_pt`, `nama_pt`, `status_pt`, `alamat_pt`, `tgl_prog`, `no_sk`, `no_sk_operasional`, `tgl_pengisian`, `akreditasi_prog`, `alamat_website`, `alamat_email`, `telepon_prog`, `visi_prog`, `misi_prog`, `tujuan_prog`, `kompetensi_prog`, `peta_prog`, `kriteria_mhs`, `jumlah_sks_min`, `jumlah_nilai_d`, `ipk_minimal`, `syarat_lainnya`, `jumlah_mhs_tahun`, `jumlah_mhs`, `jumlah_dosen_tetap_ps`, `jumlah_dosen_luar_ps`, `jumlah_tenaga_kependidikan`, `identitas_nama_ketua_prod`, `identitas_nidn`, `identitas_nohp`, `tahun_pengisian`, `created_at`, `updated_at`) VALUES
(1, 'D3 Manajemen Keuangan', 'D3 Manajemen Keuangan', 1, 'Direktorat Diploma Tiga Bisnis dan Kewirausahaan', '021-78881112 /021-7872829', '031-037', 'Universitas Gunadarma', 'Swasta', 'Jl. Margonda Raya no.100, Pondok Cina, Depok , 16624', '1990-06-14 00:00:00', '0408/O/1990', '103/SK/BAN-PT/AK-XII/Spl-III/V/2013', '2017-09-20 00:00:00', '2013, A', 'http://diploma.gunadarma.ac.id/bisnis/', 'd3_bk@gunadarma.ac.id', '021- 8710561, 8727541, 8613819 ext. 111', 'Pada tahun 2017 menjadi program studi Manajemen  Keuangan terkemuka, yang bereputasi internasional, memiliki jejaring global,dan memberikan kontribusi signifikan bagi peningkatan daya saing bangsa.', '1. Menyelenggarakan kegiatan pendidikan dan pelatihan di bidang Manajemen Keuangan yang selalu mempertimbangkan kebutuhan industri, masyarakat dan profesional serta  erkembangan IPTEK sehingga dapat menghasilkan lulusan berkualitas tinggi dengan memiliki pengetahuan dan   trampilan profesional di bidang Manajemen Keuangan\n2. Melaksanakan kegiatan pengembangan serta penelitian  alam bidang Manajemen keuangan dan pengelolaan lembaga keuangan seperti lembaga perbankan, asuransi, investasi,  ehingga dapat memberikan kontribusi kepada kemajuan ilmu\npengetahuan\n3. Melaksanakan kegiatan pengabdian pada masyarakat  dengan memanfaatkan keilmuan dibidang Manajemen Keuangan dalam rangka meningkatkan produktivitas serta nilai tambah bagi masyarakat.\n4. Menumbuhkembangkan jiwa kewirausahaan dan  engetahuan bisnis syariah bagi sivitas akademika melalui program pendidikan, pelatihan, praktek dan penyediaan sarana prasarana praktek kewirausahaan yang memadai.\n5. Membangun jejaring dalam rangka mendukung  terlaksananya Tridharma perguruan tinggi, pada tingkat  nasional maupun global', '1) menghasilkan lulusan yang memiliki kompetensi :\n Mampu bekerja dalam bidang keuangan baik di institusi swasta maupun pemerintah,\n Mampu berperan aktif dalam memajukan lembaga di mana mereka bekerja,\n Mampu mengamati, menganalisis, serta melakukan pekerjaan praktek di bidang manajemen keuangan,       Mampu memberikan penjelasan dan informasi tentang berbagai hal yang berkaitan dengan manajemen keuangan,\n Memahami dasar ilmu pengetahuan dan teknologi yang berkaitan dengan manajemen keuangan, diantaranya sektor perbankan, asuransi, serta investasi yang kebutuhannya sangat dirasakan saat ini dan di masa yang akan datang.\n Mampu memanfaatkan perangkat teknologi informasi yang relevan dengan kebutuhan masa kini dan masa yang akan datang\n2) Menghasilkan penelitian di bidang manajemen keuangan, baik teori meupun terapan yang dapat membantu menyelesaikan berbagai permasalahan yang ada di masyarakat\n3) Membantu menyelesaikan berbagai permasalahan di masyarakat melalui penerapan bidang ilmu manajemen keuangan dalam kegiatan pengabdian masyarakat\n4) Menghasilkan lulusan yang memiliki sikap kemandirian dan kewirausahaan yang dapat diandalkan dalam arti tidak selalu tergantung pada orang/pihak lain, namun justru mampu menciptakan lapangan pekerjaan sendiri dengan memanfaatkan\nberbagai peluang usaha yang ada \n5) Memperluas akses dan pengakuan dari intitusi dan lembaga lainnya, baik di dalam negeri mapun di luar negeri', 'Kompeteisis Utama  : \nMenghasilkan lulusan yang  memiliki\nketrampilan ilmu pengelolaan keuangan sehingga mampu menganalisis dan mengaplikasikan ilmunya dalam pekerjaan bidang keuangan baik di institusi swasta maupun pemerintah.\n\nKompetensi Pendukung :\n1. Melakukan analisis dan menerapkan dasar-dasar praktis dalam permasalahan sehari-hari yang merupakan terapan dari manajemen keuangan dengan dukungan keahlian di bidang kewirausahaan dan penerapan teknologi informasi.\n2. Memahami dasar ilmu pengetahuan dan teknologi yang berkaitan dengan manajemen keuangan, diantaranya sektor perbankan, asuransi, serta investasi yang kebutuhannya sangat dirasakan saat ini dan di masa yang akan datang.\n3. Memanfaatkan perangkat lunak komputer yang relevan dengan kebutuhan masa kini dan masa yang akan datang, memiliki sikap kemandirian dan kewirausahaan yang dapat diandalkan.\n4. Mengembangkan keahlian penggunaan perangkat lunak dalam bidang ilmu manajemen keuangan sehingga sangat berguna untuk membantu menyelesaikan masalah-masalah yang berhubungan dengan manajemen keuangan dan kewirausahaan.', 'Ada/Tidak', '(Sesuai panduan penerimaan mahasiswa baru)', '112 sks', '2', '3.26', NULL, '2016/2017', '361', '7', '3', '17', 'Dr. Herry Sussanto', ' 900118/0325026603', '0856-9985299', 2018, '2019-05-06 03:06:30', '2019-08-01 15:30:31'),
(7, 'Teknik Informatika', 'S1 Teknik Informatika', 2, 'Teknologi Industri', '021-78881112', 'Teknik Informatika', 'Universitas Gunadarma', 'Swasta', 'Jl. Margonda Raya no.100, Pondok Cina, Depok , 16624', '1996-04-03', '92 / DIKTI / Kep / 1996', '13957/D/T/K-III/2012', '2012-12-21', '2012,A', 'http://fti.gunadarma.ac.id/informatika', 'ti@gunadarma.ac.id', '021-78881112', 'Visi Program Studi Teknik Informatika yaitu \"Pada tahun 2022 menjadi Program Studi, Teknik Informatika bereputasi Internasional, berbasis keunggulan dalam kegiatan tridharma perguruan tinggi yang holisik dan integratif guna peningkatan daya saing bangsa dengan memadukan ilmu dan terapan Informatika menghadapi Revolusi Industri 4.0 termasuk Komputasi Big Data serta Ekonomi dan Bisnis Digital\". Reputasi Internasional yang dimaksud dalam visi adalah dengan memanfaatkan secara optimal jejaring Internasional yang telah dibangun hingga saat ini dengan berdasarkan pada keunggulan (excellence) bidang kegiatan tridharma perguruan tinggi-pendidikan dan pengajaran, penelitian, dan pengabdian kepada masyarakat-yang bersifat menyeluruh (holistik) dan terpadu (integratif) dengan berupaya menghasilkan produk penelitian yang bersifat multi disiplin keilmuan yang menjadikan visi program studi Teknik Informatika Universitas Gunadarma dalam lima tahun mendatang disertai dengan komitmen untuk meningkatkan daya saing bangsa.', 'Pencapaian Visi didukung oleh empat misi Program Studi Teknik Informatika yaitu:\r\n1. menyelenggarakan program pendidikan dan pelatihan bidang Informatika yang\r\nberkualitas untuk menghasilkan lulusan yang kompetitif dan berkarakter sebagai\r\nwujud peningkatan daya saing bangsa dalam bidang Informatika yang mampu\r\nmengikuti perkembangan jaman dengan memadukan ilmu dan terapan\r\nInformatika menghadapi Revolusi Industri 4.0 termasuk Komputasi Big Data serta\r\nEkonomi dan Bisnis Digital\r\n2. menciptakan suasana akademik yang mendukung kegiatan penelitian bertaraf\r\nnasional dan Intenasional yang medorong perkembangan ilmu pengetahuan dan\r\nteknologi bidang Informatika yang holistik dan integratif termasuk Komputasi Big\r\nData serta Ekonomi dan Bisnis Digital\r\n3. menyelenggarakan kegiatan pengabdian kepada masyarakat sebagai ujud\r\npengejawantahan tanggung jawab sosial (social responsibility) dalam rangka\r\nmeningkatkan kesejahteraaan masyarakat\r\n4. menyelenggarakan dan mengembangkan jejaring kerjasama dengan berbagai\r\ninstitusi, baik di dalam maupun di luar negeri guna meningkatkan kapasitas dan\r\nreputasi program studi dengan mengutamakan kepentingan nasional dan\r\nmemperkuat jati diri bangsa.\r\nDengan berbasiskan pada visi dan misi tersebut, program studi bertekad secara\r\nbersama-sama untuk meraih posisi yang lebih baik di masa datang. Keberhasilan\r\npencapaian tersebut diukur berdasarkan indikator kinerja yang tertuang dalam\r\nRenstra Teknik Informatika 2017-2021, yang secara umum mengacu kepada Renstra\r\nUniversitas Gunadarma pada periode perencanaan yang sama.\r\nHingga saat ini capaian target proses pengembangan program studi terhadap\r\nindikator dan rentang waktu yang ditetapkan telah tercapai sesuai perencanaan yang\r\nditetapkan. Hal ini ditunjukkan berdasarkan meningkatnya peran dan kontribusi\r\nprogram studi pada tingkat nasional ataupun kepercayaan institusi Internasional\r\nterhadap program studi (yang ditunjukkan oleh jumlah kerjasama dan program\r\nkerjasama dengan Institusi nasional maupun Internasional). Dengan demikian,\r\nprogram pengembangan yang dilaksanakan menunjukkan bahwa visi, misi, dan\r\nperangkat pendukung lainnya dalam renstra Program Studi Teknik Informatika sesuai\r\ndengan kondisi dan arah pengembangan program studi di masa yang akan datang.\r\nSehingga program pengembangan yang ditetapkan tersebut sangat realistis.', 'Program Studi Teknik Informatika bertujuan :\r\n1. menjadi program studi yang menyelenggarakan program pendidikan dan\r\npelatihan bidang Informatika yang berkualitas untuk menghasilkan lulusan yang\r\nkompetitif dan berkarakter sebagai wujud peningkatan daya saing bangsa dalam\r\nbidang Informatika yang mampu mengikuti perkembangan jaman dengan\r\nmemadukan ilmu dan terapan Informatika menghadapi Revolusi Industri 4.0\r\ntermasuk Komputasi Big Data serta Ekonomi dan Bisnis Digital;\r\nBorang Program Studi S1 Teknik Informatika, 2019\r\nSTANDAR 1 - 5\r\n2. menghasilkan penelitian bertaraf nasional dan Internasional yang medorong\r\nperkembangan ilmu pengetahuan dan teknologi bidang Informatika yang holistik\r\ndan integratif termasuk Komputasi Big Data serta Ekonomi dan Bisnis Digital;\r\n3. menghasilkan kegiatan pengabdian kepada masyarakat sebagai ujud\r\npengejawantahan tanggung jawab sosial (social responsibility) dalam rangka\r\nmeningkatkan kesejahteraaan masyarakat;\r\n4. meningkatnya kualitas dan kuantitas jejaring kerjasama dengan berbagai institusi,\r\nbaik di dalam maupun di luar negeri sebagai usaha untuk meningkatkan\r\nkapasitas dan reputasi program studi dengan mengutamakan kepentingan\r\nnasional dan memperkuat jati diri bangsa', 'belum diisi', 'belum diisi', 'belum diisi', 'belum diisi', 'belum diisi', 'belum diisi', 'belum diisi', '1000', 'belum diisi', 'belum diisi', 'belum diisi', 'belum diisi', 'hmm', 'belum diisi', 'belum diisi', 2018, '2019-07-09 08:08:03', '2019-08-17 10:51:23');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_standard_category`
--

CREATE TABLE `tbl_standard_category` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` int(11) NOT NULL,
  `category_standard_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `urutan` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `tbl_standard_category`
--

INSERT INTO `tbl_standard_category` (`id`, `category_id`, `category_standard_name`, `urutan`, `created_at`, `updated_at`) VALUES
(1, 1, 'Penetapan', 0, '2019-05-06 09:48:44', '2019-05-06 09:48:44'),
(2, 1, 'Pelaksanaan', 0, '2019-05-06 09:48:44', '2019-05-06 09:48:44'),
(3, 1, 'Evaluasi', 0, '2019-05-06 09:48:44', '2019-05-06 09:48:44'),
(4, 1, 'Pengendalian', 0, '2019-05-06 09:48:44', '2019-05-06 09:48:44'),
(5, 1, 'Peningkatan', 0, '2019-05-06 09:48:44', '2019-05-06 09:48:44'),
(6, 2, 'Penetapan', 0, '2019-05-06 09:48:44', '2019-05-06 09:48:44'),
(7, 2, 'Pelaksanaan', 0, '2019-05-06 09:48:44', '2019-05-06 09:48:44'),
(8, 2, 'Evaluasi', 0, '2019-05-06 09:48:44', '2019-05-06 09:48:44'),
(9, 2, 'Pengendalian', 0, '2019-05-06 09:48:44', '2019-05-06 09:48:44'),
(10, 2, 'Peningkatan', 0, '2019-05-06 09:48:44', '2019-05-06 09:48:44'),
(11, 3, 'Penetapan', 0, '2019-05-06 09:48:44', '2019-05-06 09:48:44'),
(12, 3, 'Pelaksanaan', 0, '2019-05-06 09:48:44', '2019-05-06 09:48:44'),
(13, 3, 'Evaluasi', 0, '2019-05-06 09:48:44', '2019-05-06 09:48:44'),
(14, 3, 'Pengendalian', 0, '2019-05-06 09:48:44', '2019-05-06 09:48:44'),
(15, 3, 'Peningkatan', 0, '2019-05-06 09:48:44', '2019-05-06 09:48:44');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_standar_awal`
--

CREATE TABLE `tbl_standar_awal` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` int(11) NOT NULL,
  `profil_id` int(11) NOT NULL,
  `standar_dikti_id` int(11) NOT NULL,
  `s1` longtext COLLATE utf8_unicode_ci NOT NULL,
  `s2` longtext COLLATE utf8_unicode_ci NOT NULL,
  `s3` longtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `tbl_standar_awal`
--

INSERT INTO `tbl_standar_awal` (`id`, `category_id`, `profil_id`, `standar_dikti_id`, `s1`, `s2`, `s3`, `created_at`, `updated_at`) VALUES
(10, 1, 7, 1, 'Bertakwa kepada Tuhan Yang Maha Esa dan mampu menunjukkan sikap religius', 'Lulusan memiliki sikap bertaqwa, berperi kemanusiaan, berkontribusi terhadap masyarakat, mencintai tanah air, memiliki toleransi yang baik, peka terhadap kondisi sosial, taat pada hukum yang berlaku, menjunjung tinggi etika akademik, bertanggung jawab sesuai bidang keahlian, dan mandiri', 'a. Peningkatan mutu akademik sesuai dengan baku mutu akademik nasional \r\n\r\nb. Meningkatkan kompetensi sumberdaya manusia, baik tenaga akademik maupun tenaga kependidikan \r\n\r\nc. Menciptakan suasana akademik yang kondusif \r\n\r\nd. Meningkatkan kualitas sarana dan prasarana pendukung proses pembelajaran \r\n\r\ne. Meningkatkan secara berkesinambungan sistem pengelolaan institusi', '2019-07-09 08:34:34', '2019-07-09 09:00:02'),
(11, 1, 7, 1, 'Menjunjung tinggi nilai kemanusiaan dalam menjalankan tugas berdasarkan agama,moral, dan etika.', 'Lulusan memiliki sikap bertaqwa, berperi kemanusiaan, berkontribusi terhadap masyarakat, mencintai tanah air, memiliki toleransi yang baik, peka terhadap kondisi sosial, taat pada hukum yang berlaku, menjunjung tinggi etika akademik, bertanggung jawab sesuai bidang keahlian, dan mandiri', 'a. Peningkatan mutu akademik sesuai dengan baku mutu akademik nasional \r\n\r\nb. Meningkatkan kompetensi sumberdaya manusia, baik tenaga akademik maupun tenaga kependidikan \r\n\r\nc. Menciptakan suasana akademik yang kondusif \r\n\r\nd. Meningkatkan kualitas sarana dan prasarana pendukung proses pembelajaran \r\n\r\ne. Meningkatkan secara berkesinambungan sistem pengelolaan institusi', '2019-07-09 08:35:09', '2019-07-09 09:00:44'),
(12, 1, 7, 1, 'Berkontribusi dalam peningkatan mutu kehidupan bermasyarakat, berbangsa, bernegara, dan kemajuan peradaban berdasarkan Pancasila.', 'Lulusan memiliki sikap bertaqwa, berperi kemanusiaan, berkontribusi terhadap masyarakat, mencintai tanah air, memiliki toleransi yang baik, peka terhadap kondisi sosial, taat pada hukum yang berlaku, menjunjung tinggi etika akademik, bertanggung jawab sesuai bidang keahlian, dan mandiri', 'a. Peningkatan mutu akademik sesuai dengan baku mutu akademik nasional \r\n\r\nb. Meningkatkan kompetensi sumberdaya manusia, baik tenaga akademik maupun tenaga kependidikan \r\n\r\nc. Menciptakan suasana akademik yang kondusif \r\n\r\nd. Meningkatkan kualitas sarana dan prasarana pendukung proses pembelajaran \r\n\r\ne. Meningkatkan secara berkesinambungan sistem pengelolaan institusi', '2019-07-09 08:35:31', '2019-07-09 09:00:58'),
(13, 1, 7, 1, 'Berperan sebagai warga negara yang bangga dan cinta tanah air, memiliki nasionalisme serta rasa tanggungjawab pada negara dan bangsa', 'Lulusan memiliki sikap bertaqwa, berperi kemanusiaan, berkontribusi terhadap masyarakat, mencintai tanah air, memiliki toleransi yang baik, peka terhadap kondisi sosial, taat pada hukum yang berlaku, menjunjung tinggi etika akademik, bertanggung jawab sesuai bidang keahlian, dan mandiri', 'a. Peningkatan mutu akademik sesuai dengan baku mutu akademik nasional \r\n\r\nb. Meningkatkan kompetensi sumberdaya manusia, baik tenaga akademik maupun tenaga kependidikan \r\n\r\nc. Menciptakan suasana akademik yang kondusif \r\n\r\nd. Meningkatkan kualitas sarana dan prasarana pendukung proses pembelajaran \r\n\r\ne. Meningkatkan secara berkesinambungan sistem pengelolaan institusi', '2019-07-09 08:36:05', '2019-07-09 09:01:06'),
(14, 1, 7, 1, 'Menghargai keanekaragaman budaya, pandangan, agama, dan kepercayaan, serta pendapat atau temuan orisinal orang lain', 'Lulusan memiliki sikap bertaqwa, berperi kemanusiaan, berkontribusi terhadap masyarakat, mencintai tanah air, memiliki toleransi yang baik, peka terhadap kondisi sosial, taat pada hukum yang berlaku, menjunjung tinggi etika akademik, bertanggung jawab sesuai bidang keahlian, dan mandiri', 'a. Peningkatan mutu akademik sesuai dengan baku mutu akademik nasional \r\n\r\nb. Meningkatkan kompetensi sumberdaya manusia, baik tenaga akademik maupun tenaga kependidikan \r\n\r\nc. Menciptakan suasana akademik yang kondusif \r\n\r\nd. Meningkatkan kualitas sarana dan prasarana pendukung proses pembelajaran \r\n\r\ne. Meningkatkan secara berkesinambungan sistem pengelolaan institusi', '2019-07-09 08:36:25', '2019-07-09 09:01:13'),
(15, 1, 7, 1, 'Bekerja sama dan memiliki kepekaan sosial serta kepedulian terhadap masyarakat dan lingkungan', 'Lulusan memiliki sikap bertaqwa, berperi kemanusiaan, berkontribusi terhadap masyarakat, mencintai tanah air, memiliki toleransi yang baik, peka terhadap kondisi sosial, taat pada hukum yang berlaku, menjunjung tinggi etika akademik, bertanggung jawab sesuai bidang keahlian, dan mandiri', 'a. Peningkatan mutu akademik sesuai dengan baku mutu akademik nasional \r\n\r\nb. Meningkatkan kompetensi sumberdaya manusia, baik tenaga akademik maupun tenaga kependidikan \r\n\r\nc. Menciptakan suasana akademik yang kondusif \r\n\r\nd. Meningkatkan kualitas sarana dan prasarana pendukung proses pembelajaran \r\n\r\ne. Meningkatkan secara berkesinambungan sistem pengelolaan institusi', '2019-07-09 08:36:44', '2019-07-09 09:01:20'),
(16, 1, 7, 1, 'Taat hukum dan disiplin dalam kehidupan bermasyarakat dan bernegara', 'Lulusan memiliki sikap bertaqwa, berperi kemanusiaan, berkontribusi terhadap masyarakat, mencintai tanah air, memiliki toleransi yang baik, peka terhadap kondisi sosial, taat pada hukum yang berlaku, menjunjung tinggi etika akademik, bertanggung jawab sesuai bidang keahlian, dan mandiri', 'a. Peningkatan mutu akademik sesuai dengan baku mutu akademik nasional \r\n\r\nb. Meningkatkan kompetensi sumberdaya manusia, baik tenaga akademik maupun tenaga kependidikan \r\n\r\nc. Menciptakan suasana akademik yang kondusif \r\n\r\nd. Meningkatkan kualitas sarana dan prasarana pendukung proses pembelajaran \r\n\r\ne. Meningkatkan secara berkesinambungan sistem pengelolaan institusi', '2019-07-09 08:37:25', '2019-07-09 09:01:27'),
(17, 1, 7, 1, 'Menginternalisasi nilai, norma, dan etika akademik', 'Lulusan memiliki sikap bertaqwa, berperi kemanusiaan, berkontribusi terhadap masyarakat, mencintai tanah air, memiliki toleransi yang baik, peka terhadap kondisi sosial, taat pada hukum yang berlaku, menjunjung tinggi etika akademik, bertanggung jawab sesuai bidang keahlian, dan mandiri', 'a. Peningkatan mutu akademik sesuai dengan baku mutu akademik nasional \r\n\r\nb. Meningkatkan kompetensi sumberdaya manusia, baik tenaga akademik maupun tenaga kependidikan \r\n\r\nc. Menciptakan suasana akademik yang kondusif \r\n\r\nd. Meningkatkan kualitas sarana dan prasarana pendukung proses pembelajaran \r\n\r\ne. Meningkatkan secara berkesinambungan sistem pengelolaan institusi', '2019-07-09 08:39:43', '2019-07-09 09:01:35'),
(18, 1, 7, 1, 'Menunjukkan sikap bertanggungjawab atas pekerjaan di bidang keahliannya secara mandiri', 'Lulusan memiliki sikap bertaqwa, berperi kemanusiaan, berkontribusi terhadap masyarakat, mencintai tanah air, memiliki toleransi yang baik, peka terhadap kondisi sosial, taat pada hukum yang berlaku, menjunjung tinggi etika akademik, bertanggung jawab sesuai bidang keahlian, dan mandiri', 'a. Peningkatan mutu akademik sesuai dengan baku mutu akademik nasional \r\n\r\nb. Meningkatkan kompetensi sumberdaya manusia, baik tenaga akademik maupun tenaga kependidikan \r\n\r\nc. Menciptakan suasana akademik yang kondusif \r\n\r\nd. Meningkatkan kualitas sarana dan prasarana pendukung proses pembelajaran \r\n\r\ne. Meningkatkan secara berkesinambungan sistem pengelolaan institusi', '2019-07-09 08:40:01', '2019-07-09 09:01:43'),
(19, 1, 7, 1, 'Menginternalisasi semangat kemandirian, kejuangan, dan kewirausahaan', 'Lulusan memiliki sikap bertaqwa, berperi kemanusiaan, berkontribusi terhadap masyarakat, mencintai tanah air, memiliki toleransi yang baik, peka terhadap kondisi sosial, taat pada hukum yang berlaku, menjunjung tinggi etika akademik, bertanggung jawab sesuai bidang keahlian, dan mandiri', 'a. Peningkatan mutu akademik sesuai dengan baku mutu akademik nasional \r\n\r\nb. Meningkatkan kompetensi sumberdaya manusia, baik tenaga akademik maupun tenaga kependidikan \r\n\r\nc. Menciptakan suasana akademik yang kondusif \r\n\r\nd. Meningkatkan kualitas sarana dan prasarana pendukung proses pembelajaran \r\n\r\ne. Meningkatkan secara berkesinambungan sistem pengelolaan institusi', '2019-07-09 08:40:14', '2019-07-09 09:01:51'),
(20, 1, 7, 1, 'Memiliki penguasaan ilmu pengetahuan berdasarkan standar profesi', 'Lulusan menguasai ilmu pengetahuan dan memenuhi kualifikasi standar profesi yang berlaku sesuai bidangnya', 'a. Peningkatan mutu akademik sesuai dengan baku mutu akademik nasional \r\n\r\nb. Meningkatkan kompetensi sumberdaya manusia, baik tenaga akademik maupun tenaga kependidikan \r\n\r\nc. Menciptakan suasana akademik yang kondusif \r\n\r\nd. Meningkatkan kualitas sarana dan prasarana pendukung proses pembelajaran \r\n\r\ne. Meningkatkan secara berkesinambungan sistem pengelolaan institusi', '2019-07-09 08:40:39', '2019-07-09 09:05:10'),
(21, 1, 7, 1, 'Mampu menerapkan pemikiran logis, kritis, sistematis, dan inovatif dalam konteks pengembangan atau implementasi ilmu pengetahuan dan teknologi yang memperhatikan dan menerapkan nilai humaniora yang sesuai dengan bidang keahliannya', 'Lulusan memiliki pemikiran logis, kritis, sistematis, dan inovatif dalam konteks pengembangan atau implementasi ilmu pengetahuan dan teknologi yang memperhatikan dan menerapkan nilai humaniora yang sesuai dengan bidang keahliannya', 'a. Peningkatan mutu akademik sesuai dengan baku mutu akademik nasional \r\n\r\nb. Meningkatkan kompetensi sumberdaya manusia, baik tenaga akademik maupun tenaga kependidikan \r\n\r\nc. Menciptakan suasana akademik yang kondusif \r\n\r\nd. Meningkatkan kualitas sarana dan prasarana pendukung proses pembelajaran \r\n\r\ne. Meningkatkan secara berkesinambungan sistem pengelolaan institusi', '2019-07-09 08:45:31', '2019-07-09 09:05:03'),
(22, 1, 7, 1, 'Mampu menunjukkan kinerja mandiri, bermutu, dan terukur', 'Lulusan memiliki kinerja mandiri, bermutu, dan terukur', 'a. Peningkatan mutu akademik sesuai dengan baku mutu akademik nasional \r\n\r\nb. Meningkatkan kompetensi sumberdaya manusia, baik tenaga akademik maupun tenaga kependidikan \r\n\r\nc. Menciptakan suasana akademik yang kondusif \r\n\r\nd. Meningkatkan kualitas sarana dan prasarana pendukung proses pembelajaran \r\n\r\ne. Meningkatkan secara berkesinambungan sistem pengelolaan institusi', '2019-07-09 08:45:43', '2019-07-09 09:04:56'),
(23, 1, 7, 1, 'Mampu mengkaji implikasi pengembangan atau implementasi ilmu pengetahuan teknologi yang memperhatikan dan menerapkan nilai humaniora sesuai dengan keahliannya berdasarkan kaidah, tata cara dan etika ilmiah dalam rangka menghasilkan solusi, gagasan, desain atau kritik seni, menyusun deskripsi saintifik hasil kajiannya dalam bentuk skripsi atau laporan tugas akhir, dan mengunggahnya dalam laman perguruan tinggi', 'Lulusan memiliki kemampuan mengkaji implikasi pengembangan atau implementasi ilmu pengetahuan teknologi yang memperhatikan dan menerapkan nilai humaniora sesuai dengan keahliannya berdasarkan kaidah, tata cara dan etika ilmiah dalam rangka menghasilkan solusi, gagasan, desain atau kritik seni, menyusun deskripsi saintifik hasil kajiannya dalam bentuk skripsi atau laporan tugas akhir, dan mengunggahnya dalam laman perguruan tinggi', 'a. Peningkatan mutu akademik sesuai dengan baku mutu akademik nasional \r\n\r\nb. Meningkatkan kompetensi sumberdaya manusia, baik tenaga akademik maupun tenaga kependidikan \r\n\r\nc. Menciptakan suasana akademik yang kondusif \r\n\r\nd. Meningkatkan kualitas sarana dan prasarana pendukung proses pembelajaran \r\n\r\ne. Meningkatkan secara berkesinambungan sistem pengelolaan institusi', '2019-07-09 08:46:44', '2019-07-09 09:04:49'),
(24, 1, 7, 1, 'Menyusun deskripsi saintifik hasil kajian tersebut di atas dalam bentuk skripsi atau laporan tugas akhir, dan mengunggahnya dalam laman perguruan tinggi.', 'Lulusan memiliki kemampuan menyusun deskripsi saintifik hasil kajian tersebut di atas dalam bentuk skripsi atau laporan tugas akhir, dan mengunggahnya dalam laman perguruan tinggi', 'a. Peningkatan mutu akademik sesuai dengan baku mutu akademik nasional \r\n\r\nb. Meningkatkan kompetensi sumberdaya manusia, baik tenaga akademik maupun tenaga kependidikan \r\n\r\nc. Menciptakan suasana akademik yang kondusif \r\n\r\nd. Meningkatkan kualitas sarana dan prasarana pendukung proses pembelajaran \r\n\r\ne. Meningkatkan secara berkesinambungan sistem pengelolaan institusi', '2019-07-09 08:47:09', '2019-07-09 09:04:41'),
(25, 1, 7, 1, 'Mampu mengambil keputusan secara tepat dalam konteks penyelesaian masalah di bidang keahliannya, berdasarkan hasil analisis informasi dan data', 'Lulusan memiliki kemampuan mengambil keputusan secara tepat dalam konteks penyelesaian masalah di bidang keahliannya, berdasarkan hasil analisis informasi dan data', 'a. Peningkatan mutu akademik sesuai dengan baku mutu akademik nasional \r\n\r\nb. Meningkatkan kompetensi sumberdaya manusia, baik tenaga akademik maupun tenaga kependidikan \r\n\r\nc. Menciptakan suasana akademik yang kondusif \r\n\r\nd. Meningkatkan kualitas sarana dan prasarana pendukung proses pembelajaran \r\n\r\ne. Meningkatkan secara berkesinambungan sistem pengelolaan institusi', '2019-07-09 08:47:22', '2019-07-09 09:04:34'),
(26, 1, 7, 1, 'Mampu memelihara dan mengembang-kan jaringan kerja dengan pembimbing, kolega, sejawat baik di dalam maupun di luar lembaganya', 'Lulusan memiliki kemampuan memelihara dan mengembangkan jaringan kerja dengan pembimbing, kolega, sejawat baik di dalam maupun di luar lembaganya', 'a. Peningkatan mutu akademik sesuai dengan baku mutu akademik nasional \r\n\r\nb. Meningkatkan kompetensi sumberdaya manusia, baik tenaga akademik maupun tenaga kependidikan \r\n\r\nc. Menciptakan suasana akademik yang kondusif \r\n\r\nd. Meningkatkan kualitas sarana dan prasarana pendukung proses pembelajaran \r\n\r\ne. Meningkatkan secara berkesinambungan sistem pengelolaan institusi', '2019-07-09 08:47:35', '2019-07-09 09:04:27'),
(27, 1, 7, 1, 'Mampu bertanggungjawab atas pencapaian hasil kerja kelompok dan melakukan supervisi dan evaluasi terhadap penyelesaian pekerjaan yang ditugaskan kepada pekerja yang berada di bawah tanggungjawabnya', 'Lulusan bertanggungjawab atas pencapaian hasil kerja kelompok dan melakukan supervisi dan evaluasi terhadap penyelesaian pekerjaan yang ditugaskan kepada pekerja yang berada di bawah tanggungjawabnya', 'a. Peningkatan mutu akademik sesuai dengan baku mutu akademik nasional \r\n\r\nb. Meningkatkan kompetensi sumberdaya manusia, baik tenaga akademik maupun tenaga kependidikan \r\n\r\nc. Menciptakan suasana akademik yang kondusif \r\n\r\nd. Meningkatkan kualitas sarana dan prasarana pendukung proses pembelajaran \r\n\r\ne. Meningkatkan secara berkesinambungan sistem pengelolaan institusi', '2019-07-09 08:47:58', '2019-07-09 09:02:51'),
(28, 1, 7, 1, 'Mampu melakukan proses evaluasi diri terhadap kelompok kerja yang berada dibawah tanggung jawabnya, dan mampu mengelola pembelajaran secara mandiri', 'Lulusan memiliki kemampuan melakukan proses evaluasi diri terhadap kelompok kerja yang berada dibawah tanggung jawabnya, dan mampu mengelola pembelajaran secara mandiri', 'a. Peningkatan mutu akademik sesuai dengan baku mutu akademik nasional \r\n\r\nb. Meningkatkan kompetensi sumberdaya manusia, baik tenaga akademik maupun tenaga kependidikan \r\n\r\nc. Menciptakan suasana akademik yang kondusif \r\n\r\nd. Meningkatkan kualitas sarana dan prasarana pendukung proses pembelajaran \r\n\r\ne. Meningkatkan secara berkesinambungan sistem pengelolaan institusi', '2019-07-09 08:48:10', '2019-07-09 09:02:43'),
(29, 1, 7, 1, 'Mampu mendokumentasikan, menyimpan, mengamankan, dan menemukan kembali data untuk menjamin kesahihan dan mencegah plagiasi', 'Lulusan mampu mendokumentasikan, menyimpan, mengamankan, dan menemukan kembali data untuk menjamin kesahihan dan mencegah plagiasi', 'a. Peningkatan mutu akademik sesuai dengan baku mutu akademik nasional \r\n\r\nb. Meningkatkan kompetensi sumberdaya manusia, baik tenaga akademik maupun tenaga kependidikan \r\n\r\nc. Menciptakan suasana akademik yang kondusif \r\n\r\nd. Meningkatkan kualitas sarana dan prasarana pendukung proses pembelajaran \r\n\r\ne. Meningkatkan secara berkesinambungan sistem pengelolaan institusi', '2019-07-09 08:48:31', '2019-07-09 09:02:34'),
(30, 1, 7, 1, 'Mampu menggunakan teknologi modern dalam menyelesaikan masalah.', 'Lulusan mampu menggunakan teknologi modern dalam menyelesaikan masalah memanfaatkan teknologi informasi dan komunikasi terkini untuk melakukan berbagai penyelesain masalah dalam domain setiap bidang ilmu dan ketrampilan, dan menggunakan piranti lunak yang bersesuaian dengan bidang ilmu dan keterampilan', 'a. Peningkatan mutu akademik sesuai dengan baku mutu akademik nasional \r\n\r\nb. Meningkatkan kompetensi sumberdaya manusia, baik tenaga akademik maupun tenaga kependidikan \r\n\r\nc. Menciptakan suasana akademik yang kondusif \r\n\r\nd. Meningkatkan kualitas sarana dan prasarana pendukung proses pembelajaran \r\n\r\ne. Meningkatkan secara berkesinambungan sistem pengelolaan institusi', '2019-07-09 08:49:08', '2019-07-09 09:02:27'),
(31, 1, 7, 1, 'Mampu memanfaatkan teknologi informasi dan komunikasi terkini untuk melakukan berbagai penyelesain masalah dalam domain setiap bidang ilmu dan ketrampilan', 'Lulusan mampu menggunakan teknologi modern dalam menyelesaikan masalah memanfaatkan teknologi informasi dan komunikasi terkini untuk melakukan berbagai penyelesain masalah dalam domain setiap bidang ilmu dan ketrampilan, dan menggunakan piranti lunak yang bersesuaian dengan bidang ilmu dan keterampilan', 'a. Peningkatan mutu akademik sesuai dengan baku mutu akademik nasional \r\n\r\nb. Meningkatkan kompetensi sumberdaya manusia, baik tenaga akademik maupun tenaga kependidikan \r\n\r\nc. Menciptakan suasana akademik yang kondusif \r\n\r\nd. Meningkatkan kualitas sarana dan prasarana pendukung proses pembelajaran \r\n\r\ne. Meningkatkan secara berkesinambungan sistem pengelolaan institusi', '2019-07-09 08:49:48', '2019-07-09 09:02:19'),
(32, 1, 7, 1, 'Mampu menggunakan piranti lunak yang bersesuaian dengan bidang ilmu dan keterampilan', 'Lulusan mampu menggunakan teknologi modern dalam menyelesaikan masalah memanfaatkan teknologi informasi dan komunikasi terkini untuk melakukan berbagai penyelesain masalah dalam domain setiap bidang ilmu dan ketrampilan, dan menggunakan piranti lunak yang bersesuaian dengan bidang ilmu dan keterampilan', 'a. Peningkatan mutu akademik sesuai dengan baku mutu akademik nasional \r\n\r\nb. Meningkatkan kompetensi sumberdaya manusia, baik tenaga akademik maupun tenaga kependidikan \r\n\r\nc. Menciptakan suasana akademik yang kondusif \r\n\r\nd. Meningkatkan kualitas sarana dan prasarana pendukung proses pembelajaran \r\n\r\ne. Meningkatkan secara berkesinambungan sistem pengelolaan institusi', '2019-07-09 08:50:18', '2019-07-09 09:02:10'),
(33, 1, 7, 1, 'Mampu menggunakan Bahasa Inggris dalam bidang ilmu dan ketrampilan masing-masing', 'Lulusan Universitas Gunadarma memiliki kemampuan Bahasa Inggris yang baik', 'a. Peningkatan mutu akademik sesuai dengan baku mutu akademik nasional \r\n\r\nb. Meningkatkan kompetensi sumberdaya manusia, baik tenaga akademik maupun tenaga kependidikan \r\n\r\nc. Menciptakan suasana akademik yang kondusif \r\n\r\nd. Meningkatkan kualitas sarana dan prasarana pendukung proses pembelajaran \r\n\r\ne. Meningkatkan secara berkesinambungan sistem pengelolaan institusi', '2019-07-09 08:50:30', '2019-07-09 09:02:03'),
(35, 1, 7, 2, 'Ketersediaan dokumen kurikulum yang dimutahirkan secara periodik dan beriorientasi ke depan sesuai dengan visi, misi, tujuan dan sasaran program studi.', 'a. Dokumen kurikulum memuat penjabaran profil, capaian pembelajaran serta berorientasi ke depan sesuai dengan visi, misi, tujuan dan sasaran program studi.  \r\n\r\nb. Dokumen kurikulum memuat matriks/peta kurikulum. \r\n\r\nc. Seluruh mata kuliah dilengkapi dengan deskripsi, silabus dan  Rencana Pembelajaran Semester (RPS) mata kuliah yang selalu dimutahirkan. \r\n\r\nd. Seluruh mata kuliah praktikum dilengkapi dengan deskripsi, silabus dan  Rencana Pembelajaran Semester (RPS) mata kuliah yang selalu dimutahirkan serta modul praktikum.\r\n\r\ne. Peninjauan kurikulum minimal 2 tahun sekali dengan melibatkan/ mempertimbang-kan masukan dari pemangku kepentingan internal dan eksternal, dimutahirkan dengan perkembangan keilmuan dan teknologi di bidangnya serta hasil penelitian dan pengabdian masyarakat. \r\n\r\nf. Peninjauan Rencana Pembelajaran Semester (RPS)  setiap 1 tahun sekali.', '1. Menyelenggarakan pelatihan yang berkaitan dengan isi pembelajaran .\r\n\r\n2. Ketua Program Studi perlu membina hubungan dengan organisasi profesi, alumni, pemerintah dan perusahaan.', '2019-07-09 09:51:38', '2019-07-09 10:03:53'),
(36, 1, 7, 2, 'Kompetensi dosen pengampu mata kuliah.', 'a. Program studi memiliki mekanisme penentuan dosen pengampu \r\n\r\nb. Seluruh dosen pengampu mengajar sesuai dengan kompetensinya. \r\n\r\nc. Secara periodik (minimal 1x dalam satu semester)  mengikuti kegiatan refresing dan pendalaman materi mata kuliah. \r\n\r\nd. Melakukan Proses belajar mengajar 14 kali pertemuan tatap muka dalam satu semester.', '1. Menyelenggarakan pelatihan yang berkaitan dengan isi pembelajaran .\r\n\r\n2. Ketua Program Studi perlu membina hubungan dengan organisasi profesi, alumni, pemerintah dan perusahaan.', '2019-07-09 09:52:30', '2019-07-09 10:04:09'),
(37, 1, 7, 2, 'Kompetensi dosen pembimbing tugas akhir bagi setiap mahasiswa  untuk melakukan pembimbingan penyelesaian tugas akhir.', 'a. Program studi memiliki mekanisme penentuan dosen pembimbing tugas akhir dan pembimbingan penyelesaian tugas akhir.\r\n\r\nb. Seluruh dosen pembimbing tugas akhir  program studi S1 berpendidikan minimal S2 dan sesuai dengan bidang keahliannya. \r\n\r\nc. Jumlah mahasiswa per dosen pembimbing tugas akhir maksimal 6 orang per angkatan.\r\n\r\nd. Rata-rata jumlah pertemuan/ pembimbingan selama penyelesaian tugas akhir minimal 8 kali.  \r\n\r\ne. Rata-rata penyelesaian tugas akhir mahasiswa maksimal 6 bulan. \r\n\r\nf. Satu karya ilmiah setiap Tugas Akhir.', '1. Menyelenggarakan pelatihan yang berkaitan dengan isi pembelajaran .\r\n\r\n2. Ketua Program Studi perlu membina hubungan dengan organisasi profesi, alumni, pemerintah dan perusahaan.', '2019-07-09 09:56:52', '2019-07-09 10:04:16'),
(38, 1, 7, 2, 'Ketersediaan materi praktikum yang mendukung peningkatan penguasaan materi pembelajaran mahasiswa.', 'a. Setiap materi praktikum dilengkapi modul praktikum. \r\n\r\nb. Modul praktikum sesuai dengan capaian pembelajaran.\r\n\r\nc. Pelaksanaan praktikum didukung dengan peralatan yang mutakhir. \r\n\r\nd. Jumlah pertemuan praktikum sesuai dengan materi modul.', '1. Menyelenggarakan pelatihan yang berkaitan dengan isi pembelajaran .\r\n\r\n2. Ketua Program Studi perlu membina hubungan dengan organisasi profesi, alumni, pemerintah dan perusahaan.', '2019-07-09 09:57:38', '2019-07-09 10:04:24'),
(39, 1, 7, 2, 'Kontribusi Hasil Penelitian dan pengabdian masyarakat untuk pengayaan bahan kajian.', 'a. Hasil penelitian menjadi bahan referensi peninjauan kurikulum \r\n\r\nb. Hasil pengabdian masyarakat menjadi bahan referensi peninjauan kurikulum', '1. Menyelenggarakan pelatihan yang berkaitan dengan isi pembelajaran .\r\n\r\n2. Ketua Program Studi perlu membina hubungan dengan organisasi profesi, alumni, pemerintah dan perusahaan.', '2019-07-09 09:58:11', '2019-07-09 10:04:30'),
(40, 1, 7, 3, 'Karakteristik proses pembelajaran', '• Materi mata kuliah wajib ditampilkan pada digital locker setiap pengampu mata kuliah \r\n• Mata kuliah dasar umum wajib menerapkan metode problem based learning dan project based learning (PBL) \r\n• Pelaksanaan PBL setara dengan 4 kali tatap muka', '1. Wakil Rektor 1 berkoordinasi dengan ketua program studi dan pihak-pihak terkait \r\n2. Ketua program studi mendiskusikan berbagai hal perencanaan proses pembelajaran setiap semester\r\n 3. Ketua program studi menugasi pihak-pihak terkait untuk melaksanakan tugas seperti yang tertulis dalam standar', '2019-07-09 10:07:04', '2019-07-09 10:07:04'),
(41, 1, 7, 3, 'Perencanaan proses pembelajaran', '• Kurikulum memuat jabaran kompetensi lulusan secara lengkap (pengetahuan, keterampilan dan sikap) serta berorientasi ke depan sesuai dengan visi, misi, tujuan dan sasaran program studi. • Kurikulum mencantumkan peta kurikulum  • Seluruh mata kuliah (kuliah dan praktikum)dilengkapi dengan RPS  mata kuliah yang selaludimutahirkan. • Program studi melakukan peninjauan kurikulum minimal 2  tahun sekali dengan  mempertimbangkan masukan dari pemangku kepentingan internal daneksternal, serta dimutahirkan dengan perkembangan keilmuan dan teknologi dibidangnya.', '1. Wakil Rektor 1 berkoordinasi dengan ketua program studi dan pihak-pihak terkait \r\n2. Ketua program studi mendiskusikan berbagai hal perencanaan proses pembelajaran setiap semester \r\n3. Ketua program studi menugasi pihak-pihak terkait untuk melaksanakan tugas seperti yang tertulis dalam standar', '2019-07-09 10:08:50', '2019-07-09 10:08:50'),
(42, 1, 7, 3, 'Pelaksanaan proses pembelajaran', '• Kegiatan kuliah dan praktikum dilengkapi dengan buku referensi yang mutahir dan bahan ajar handout/modul/ penuntun praktikum). • Program studi menerapkan mekanisme penyusunan dan peninjauan materi kuliahan dengan melibatkan kelompok dosen dalam satu bidang ilmu setiap semester (mencakup materi kuliah, metode pem-belajaran, penggunaan teknologi pembelajaran dan cara-cara evaluasinya • Kegiatan perkuliahan dan praktikum dilaksanakan secara penuh (14 kali pertemuan) dan sesuai dengan beban kreditnya. • Tatap muka minimal dilakukan sebanyak 11 kali • Kegiatan perkuliahan  dengan sistem elearning (blended system) maksimal dilakukan 3 kali • Kegiatan praktikum mahasiswa menggunakanfasilitas laboratorium yang dimiliki oleh  Universitas Gunadarma • Program studi menerapkan mekanisme monitoring kehadiran mahasiswa, kehadiran dosen, dan kesesuaian materi kuliah yang diajarkan dengan RPS di setiap semester.', '1. Wakil Rektor 1 berkoordinasi dengan ketua program studi dan pihak-pihak terkait \r\n2. Ketua program studi mendiskusikan berbagai hal perencanaan proses pembelajaran setiap semester \r\n3. Ketua program studi menugasi pihak-pihak terkait untuk melaksanakan tugas seperti yang tertulis dalam standar', '2019-07-09 10:09:42', '2019-07-09 10:09:42'),
(43, 1, 7, 3, 'Beban mahasiswa', '• Jumlah  SKS matakuliahumum 10 SKS danmatakuliahkeahlian 134 SKS\r\n • Setiap mata kuliah paling sedikit memiliki bobot 1 (satu) SKS \r\n• Beban normal  belajar mahasiswa adalah 8 (delapan) jam per hari atau 48 (empat puluh delapan) jam per minggu setara dengan 18 (delapan belas) SkS per semester,  sampai dengan 9 (sembilan) jam per hari atau 54 (lima puluh empat) jam per minggu setara dengan 20 (duapuluh) SKS per semester \r\n• Skripsi/ tugas akhir/ karya seni/ bentuk lain yang setara, diberi bobot 6-8 sks dan merupakan bagian dari mata kuliah keahlian.\r\n • Lama studi minimal  3 tahun untuk Diploma Tiga dan 4 tahun untuk Sarjana, sedangkan  lama studi maksimal diatur oleh masing-masing program studi.', '1. Wakil Rektor 1 berkoordinasi dengan ketua program studi dan pihak-pihak terkait \r\n2. Ketua program studi mendiskusikan berbagai hal perencanaan proses pembelajaran setiap semester \r\n3. Ketua program studi menugasi pihak-pihak terkait untuk melaksanakan tugas seperti yang tertulis dalam standar', '2019-07-09 10:11:25', '2019-07-09 10:11:25'),
(44, 1, 7, 4, 'Standar Prinsip Penilaian', '1) Prinsip penilaian mencakup prinsip edukatif, otentik, objektif, akuntabel, dan transparan yang dilakukan secara terintegrasi,\r\n 2) Prinsip edukatif merupakan penilaian yang memotivasi mahasiswa agar mampu: \r\na. memperbaiki perencanaan dan cara belajar; dan \r\nb. meraih capaian pembelajaran lulusan.\r\n  3) Prinsip otentik merupakan penilaian yang berorientasi pada proses belajar yang berkesinambungan dan hasil belajar yang mencerminkan kemampuan mahasiswa pada saat proses pembelajaran berlangsung.  \r\n4) Prinsip otentik merupakan penilaian yang berorientasi pada proses belajar yang berkesinambungan dan hasil belajar yang mencerminkan kemampuan mahasiswa pada saat proses pembelajaran berlangsung.\r\n  5) Prinsip objektif merupakan penilaian yang didasarkan pada standar yang disepakati antara dosen dan mahasiswa serta bebas dari pengaruh subjektivitas penilai dan yang dinilai. \r\n 6) Prinsip akuntabel merupakan penilaian yang dilaksanakan sesuai dengan prosedur dan kriteria yang jelas, disepakati pada awal kuliah, dan dipahami oleh mahasiswa.  \r\n7) Prinsip transparan merupakan penilaian yang prosedur dan hasil penilaiannya dapat diakses oleh semua pemangku kepentingan.', '1. Melakukan sosialisasi standar penilaian kepada dosen, staf administrasi yang menangani system akademik\r\n2. Melaksanakan evaluasi dan pengawasan dalam proses penilaian pembelajaran', '2019-07-09 10:15:11', '2019-07-09 10:15:11'),
(45, 1, 7, 4, 'Standar Teknik dan Instrumen Penilaian', '1) Teknik penilaian terdiri atas: observasi, partisipasi,  unjuk kerja, tes tertulis, tes lisan, dan angket.  \r\n2) Instrumen penilaian terdiri atas penilaian proses dalam bentuk rubrik dan/atau penilaian hasil dalam  bentuk portofolio atau karya \r\ndesain.   \r\n3) Penilaian sikap dapat menggunakan teknik penilaian observasi.   \r\n4) Penilaian penguasaan pengetahuan, keterampilan  umum, dan keterampilan khusus dilakukan dengan memilih satu atau kombinasi dari berbagi teknik dan instrumen penilaian.  \r\n 5) Hasil akhir penilaian merupakan integrasi antara berbagai teknik dan instrumen penilaian yang digunakan.', '1. Melakukan sosialisasi standar penilaian kepada dosen, staf administrasi yang menangani system akademik\r\n2. Melaksanakan evaluasi dan pengawasan dalam proses penilaian pembelajaran', '2019-07-09 10:16:25', '2019-07-09 10:22:31'),
(46, 1, 7, 4, 'Standar Mekanisme penilaian', '4.3.1 Mekanisme penilaian terdiri atas: \r\n a. menyusun, menyampaikan, menyepakati tahap,  teknik, instrumen, kriteria, indikator, dan bobot penilaian antara penilai dan yang dinilai sesuai dengan rencana pembelajaran;  \r\n b. melaksanakan proses penilaian sesuai dengan tahap, teknik, instrumen, kriteria, indikator, dan bobot penilaian yang memuat prinsip penilaian;\r\n  c. memberikan umpan balik dan kesempatan untuk mempertanyakan hasil penilaian kepada mahasiswa; dan   \r\nd. mendokumentasikan penilaian proses dan hasil belajar mahasiswa secara akuntabel dan transparan.   \r\n4.3.2 Prosedur penilaian mencakup tahap perencanaan, kegiatan pemberian tugas atau soal, observasi kinerja, pengembalian hasil observasi, dan pemberian nilai akhir.   \r\n4.3.3 Prosedur penilaian pada tahap perencanaan dapat dilakukan melalui penilaian bertahap dan/atau penilaian ulang', '1. Melakukan sosialisasi standar penilaian kepada dosen, staf administrasi yang menangani system akademik\r\n2. Melaksanakan evaluasi dan pengawasan dalam proses penilaian pembelajaran', '2019-07-09 10:17:14', '2019-07-09 10:17:14'),
(47, 1, 7, 4, 'Standar Pelaksanaan penilaian', '4.4.1 Pelaksanaan penilaian dilakukan sesuai dengan  rencana pembelajaran.   \r\n4.4.2 Pelaksanaan penilaian dapat dilakukan oleh:  \r\na. dosen pengampu atau tim dosen pengampu;\r\n  b. dosen pengampu atau tim dosen pengampu dengan mengikutsertakan mahasiswa; dan/atau   \r\nc. dosen pengampu atau tim dosen pengampu dengan mengikutsertakan pemangku kepentingan yang relevan', '1. Melakukan sosialisasi standar penilaian kepada dosen, staf administrasi yang menangani system akademik\r\n2. Melaksanakan evaluasi dan pengawasan dalam proses penilaian pembelajaran', '2019-07-09 10:17:56', '2019-07-09 10:17:56'),
(48, 1, 7, 4, 'Standar Penilaian Mata Kuliah', '4.5.1 Bobot Penilaian mata kuliah tanpa praktikum penunjang adalah 70% Nilai UTS + 30% Nilai UAS.\r\n 4.5.2 Bobot Penilaian mata kuliah dengan praktikum penunjang adalah 50% Nilai UTS + 30 % Nilai UAS + 20% Nilai Praktikum Penunjang.\r\n 4.5.3 Komposisi Nilai Tugas dapat diambil sebesar 20% dari nilai UAS. Tugas dapat berupa Kuis, Makalah, Pembuatan Program, Presentasi dan diskusi, Pekerjaan Rumah,\r\n  4.5.4 Bobot Penilaian Mata kuliah Utama adalah 50% Nilai UTS + 50% Nilai Ujian Utama', '1. Melakukan sosialisasi standar penilaian kepada dosen, staf administrasi yang menangani system akademik\r\n2. Melaksanakan evaluasi dan pengawasan dalam proses penilaian pembelajaran', '2019-07-09 10:19:55', '2019-07-09 10:19:55'),
(49, 1, 7, 4, 'Standar Pelaporan Penilaian', '4.6.1 Pelaporan penilaian berupa kualifikasi keberhasilan mahasiswa dalam menempuh suatu mata kuliah yang dinyatakan dalam kisaran:  \r\n\r\n- huruf A setara dengan angka 4 (empat) berkategori  sangat baik;  \r\n- huruf B setara dengan angka 3 (tiga) berkategori baik;  \r\n- huruf C setara dengan angka 2 (dua) berkategori  cukup;  \r\n- huruf D setara dengan angka 1 (satu) berkategori  kurang; atau  \r\n- huruf E setara dengan angka 0 (nol) berkategori  sangat kurang. \r\n\r\n4.6.2 Huruf antara dan  angka antara untuk nilai pada kisaran 0 (nol) sampai 4  (empat).  \r\n\r\n4.6.3 Hasil penilaian diumumkan kepada mahasiswa setelah  satu tahap pembelajaran sesuai dengan rencana pembelajaran.\r\n\r\n 4.6.4 Hasil penilaian capaian pembelajaran lulusan di tiap  semester dinyatakan dengan indeks prestasi semester (IPS). \r\n\r\n4.6.5 Hasil penilaian capaian pembelajaran lulusan pada akhir program studi dinyatakan dengan indeks prestasi kumulatif (IPK).   \r\n\r\n4.6.6 Indeks prestasi semester (IPS) dinyatakan dalam besaran yang dihitung dengan cara menjumlahkan perkalian antara nilai huruf setiap mata kuliah yang ditempuh dan sks mata kuliah bersangkutan dibagi dengan jumlah sks mata kuliah yang diambil dalam satu semester.   \r\n\r\n4.6.7 Indeks prestasi kumulatif (IPK) dinyatakan dalam besaran yang dihitung dengan cara menjumlahkan perkalian antara nilai huruf setiap mata kuliah yang ditempuh dan sks mata kuliah bersangkutan dibagi dengan jumlah sks mata kuliah yang diambil yang telah ditempuh.', '1. Melakukan sosialisasi standar penilaian kepada dosen, staf administrasi yang menangani system akademik\r\n2. Melaksanakan evaluasi dan pengawasan dalam proses penilaian pembelajaran', '2019-07-09 10:21:44', '2019-07-09 10:21:44'),
(50, 1, 7, 4, 'Standar Kelulusan', '4.7.1 Mahasiswa program diploma dan program sarjana Universitas Gunadarma dinyatakan lulus apabila telah menempuh seluruh beban belajar yang ditetapkan dan memiliki capaian pembelajaran lulusan yang ditargetkan oleh program studi dengan indeks prestasi kumulatif (IPK) lebih besar atau sama dengan 2,00 (dua koma nol nol).\r\n\r\n4.7.2 Kelulusan mahasiswa dari program diploma dan program sarjana dapat diberikan predikat memuaskan, sangat memuaskan, atau pujian dengan kriteria:  \r\na. mahasiswa dinyatakan lulus dengan predikat  memuaskan apabila mencapai indeks prestasi kumulatif (IPK) 2,76 (dua koma tujuh enam) sampai dengan 3,00 (tiga koma nol nol);\r\n  b. mahasiswa dinyatakan lulus dengan predikat sangat memuaskan apabila mencapai indeks prestasi kumulatif (IPK) 3,01 (tiga koma nol satu) sampai dengan 3,50 (tiga koma lima nol); atau \r\n c. mahasiswa dinyatakan lulus dengan predikat pujian apabila mencapai indeks prestasi kumulatif (IPK) lebih dari 3,50 (tiga koma nol).', '1. Melakukan sosialisasi standar penilaian kepada dosen, staf administrasi yang menangani system akademik\r\n2. Melaksanakan evaluasi dan pengawasan dalam proses penilaian pembelajaran', '2019-07-09 10:23:37', '2019-07-09 10:23:37'),
(51, 1, 7, 4, 'Standar Sidang Tugas Akhir Skripsi', '4.8.1 Sidang Tugas Akhir Skripsi dilakukan oleh 2 (dua) penguji dan dosen pembimbing.\r\n\r\n 4.8.2 Penilaian tugas akhir mencakup: Isi Penulisan, Penguasaan Materi, dan Presntasi. \r\n\r\n4.8.3 Mahasiswa dinyatakan lulus sidang apabila memperoleh minimal rata-rata 70', '1. Melakukan sosialisasi standar penilaian kepada dosen, staf administrasi yang menangani system akademik\r\n2. Melaksanakan evaluasi dan pengawasan dalam proses penilaian pembelajaran', '2019-07-09 10:24:27', '2019-07-09 10:24:27'),
(52, 1, 7, 4, 'Standar Sidang Tugas Akhir Penulisan Ilmiah', '4.9.1 Sidang Tugas Akhir Penulisan Ilmiah dilakukan oleh 3 (dua) penguji.  \r\n\r\n4.9.2 Penilaian tugas akhir penulisan ilmiah mencakup: Isi Penulisan, Penguasaan Materi, dan Presntasi. \r\n\r\n4.9.3 Mahasiswa dinyatakan lulus sidang apabila memperoleh minimal rata-rata 65', '1. Melakukan sosialisasi standar penilaian kepada dosen, staf administrasi yang menangani system akademik\r\n2. Melaksanakan evaluasi dan pengawasan dalam proses penilaian pembelajaran', '2019-07-09 10:25:07', '2019-07-09 10:25:07'),
(53, 1, 7, 4, 'Standar Sidang Komprehensif', '4.10.1 Program Sarjana (S1) dapat menyelenggarakan ujian komprehensif sebagai syarat kelulusan selain skripsi dengan memenuhi ketentuan syarat akademik kelulusan.\r\n 4.10.2 Ujian Komprehensif teridiri dari tiga (3) mata ujian ditetapkan oleh Program Studi sesuai dengan kompetensi keilmuan Program Studi. \r\n4.10.3 Pelaksanaan Sidang Komprehensif diuji oleh 3 Dosen penguji untuk masing-masing penguji menguji 1 materi ujian dengan kualifikasi Doktor (S3) atau Magister (S2) dengan jabatan fungsional akademik Lektor Kepala dengan bidang ilmu yang sesuai dengan materi yang diujikan. \r\n4.10.4 Mahasiswa dinyatakan lulus apabila ratarata nilai ujian dari 3 materi yang diujikan minimal 60.', '1. Melakukan sosialisasi standar penilaian kepada dosen, staf administrasi yang menangani system akademik\r\n2. Melaksanakan evaluasi dan pengawasan dalam proses penilaian pembelajaran', '2019-07-09 10:25:42', '2019-07-09 10:25:42'),
(54, 1, 7, 4, 'Standar Dokumen Kelulusan', '4.11.1 Dokumen yang diterima oleh lulusan adalah: \r\na. ijazah,\r\n b. sertifikat kompetensi yang diterbitkan oleh perguruan tinggi bekerja sama dengan organisasi profesi, lembaga pelatihan, atau lembaga sertifikasi yang terakreditasi  \r\n c. gelar; dan  \r\n d. surat keterangan pendamping ijazah, kecuali  ditentukan lain oleh peraturan perundang- undangan.', '1. Melakukan sosialisasi standar penilaian kepada dosen, staf administrasi yang menangani system akademik\r\n2. Melaksanakan evaluasi dan pengawasan dalam proses penilaian pembelajaran', '2019-07-09 10:26:16', '2019-07-09 10:26:16'),
(55, 1, 7, 5, 'Standar Umum Pengelolaan SDM', '1. Institusi Universitas Gunadarma memiliki Renstra pengelolaan SDM tingkat Universitas, merujuk pada dokumen SK 710.2/SK/REK/UG/2016, serta petunjuk teknisnya. \r\n2. Setiap Prodi memiliki renstra pengelolaan SDM di lingkungan akademiknya, merujuk pada dokumen SK 710.2/SK/REK/UG/2016, serta petunjuk teknisnya. \r\n3. Setiap unit kerja memiliki renstra pengelolaan SDM di lingkungan kerjanya, merujuk pada dokumen SK 710.2/SK/REK/UG/2016, serta petunjuk teknisnya. \r\n4. Setiap Sivitas akademika memahami dan bertugas dan bekerja sesuai dengan dokumen-dokumen pada poin 1 s.d. 3 tersebut.\r\n 5. Institusi memiliki ratio Dosen tetap terhadap Total Dosen di instutusi maksimal: \r\n• Untuk ilmu pasti maksimal 1:45 \r\n• Untuk ilmu sosial maksimal 1:30 6. Institusi memiliki ratio tenaga kependidikan maksimal 1:50', 'a. Pimpinan Program Sarjana dan Diploma Tiga mengawasi pelaksanaan proses pembelajaran yang diselenggarakan oleh Program Studi; \r\n b. Pimpinan Program Studi memonitor dan mengevaluasi pelaksanaan proses pembelajaran yang diampu oleh dosen;\r\n c. Pimpinan Program Sarjana dan Diploma Tiga dan Program Studi melakukan sosialisasi standar proses pembelajaran kepada seluruh dosen.', '2019-07-09 10:28:41', '2019-07-09 10:28:41'),
(56, 1, 7, 5, 'Rekrutmen dan Seleksi Dosen dan Tenaga kependidikan', '1. Institusi Universitas Gunadarma memiliki dokumen standar minimal kualifikasi dan kompetensi tenga Dosen dan Tenaga kependidikan yang menjadi rujukan bagi setiap Prodi dan unit kerja di lingkungan Universitas Gunadarma, yang mengacu pada SK 711.1/SK/REK/UG/2016 \r\n2. Setiap Program studi di lingkungan Universitas Gunadarma memiliki dokumen standar minimal kualifikasi dan kompetensi tenga Dosen yang mengacu pada SK 711.1/SK/REK/UG/2016 \r\n3. Setiap Program studi di lingkungan Universitas Gunadarma memiliki dokumen SOP rekruitmen dan seleksi Dosen, megacu pada SOP universitas \r\n4. Setiap Prodi memiliki Jumlah dosen tetap pada perguruan tinggi paling sedikit60%(enampuluh persen) dari jumlah seluruh dosen dan memiliki keahlian dibidang ilmu yang sesuai dengan disiplin ilmu pada program studi \r\n5. Setiap Prodi memiliki Jumlah dosen tetap pada perguruan tinggi paling sedikit 60%(enampuluh persen) dari jumlah seluruh dosen dan memiliki keahlian dibidang ilmu yang sesuai dengan disiplin ilmu pada program studi \r\n6. Setiap Dosen yang diterima menjadi Dosen di setiap Prodi sudah memiliki kualifikasi dan kompetensi sesuai dengan dokumen-dokumen pada poin 1 dan 2 tersebut \r\n7. Setiap unit kerja di lingkungan Universitas Gunadarma memiliki dokumen standar minimal kualifikasi dan kompetensi tenga kependidikan yang mengacu pada SK 711.1/SK/REK/UG/2016 \r\n8. Setiap Unit kerja di lingkungan Universitas Gunadarma memiliki dokumen SOP rekruitmen dan seleksi Tenaga kependidikan, megacu pada SOP universitas \r\n9. Setiap unit kerja di lingkungan Universitas Gunadarma memiliki Tenagakependidikanmemilikikualifikasiakademik paling rendah lulusan program diploma 3 (tiga) yang dinyatakan denganijazah sesuaidengan kualifikasi tugas pokok dan fungsinya \r\n\r\n10. Khusus untuk tenaga administratif, Setiap unit kerja di lingkungan Universitas Gunadarma memiliki dengan kualifikasi kualifikasi akademik paling rendah SMA atau sederajat\r\n 11. Bila diperlukan, setiap unit kerja di lingkungan Universitas Gunadarma memiliki Tenaga  kependidikan  dengan  keahlian khusus wajib memiliki sertifikat kompetensi sesuai dengan bidang tugas dan keahliannya\r\n 12. Setiap Tenaga kependidikan yang diterima di setiap unnit kerja sudah memiliki kualifikasi dan kompetensi sesuai dengan dokumen-dokumen pada poin 7 dan 8 tersebut', 'a. Pimpinan Program Sarjana dan Diploma Tiga mengawasi pelaksanaan proses pembelajaran yang diselenggarakan oleh Program Studi;  \r\nb. Pimpinan Program Studi memonitor dan mengevaluasi pelaksanaan proses pembelajaran yang diampu oleh dosen; \r\nc. Pimpinan Program Sarjana dan Diploma Tiga dan Program Studi melakukan sosialisasi standar proses pembelajaran kepada seluruh dosen.', '2019-07-09 10:31:22', '2019-07-09 10:31:22'),
(57, 1, 7, 5, 'Analisis Beban Kerja Dosen dan Tenaga kependidikan Di Lingkungan Universitas Gunadarma', '1. Setiap Prodi di lingkungan Universitas Gunadarma memiliki standar beban kerja mengaju pada dokumen standar beban kerja yang dituangkan dalam SK nomor 712.1/SK/REK/UG/2016 \r\n2. Setiap Dosen Prodi di lingkungan Universitas Gunadarma memiliki beban kerja akademik yang sesuai atau mengacu pada dokumen Buku Pedoman Sertifikasi Dosen \r\n3. Setiap Prodi memiliki ratio Dosen tetap terhadap Total Dosen di instutusi maksimal: \r\n• Untuk ilmu pasti maksimal 1:45 \r\n• Untuk ilmu sosial maksimal 1:30\r\n\r\n4. Setiap unit kerja di lingkungan Universitas Gunadarma memiliki standar beban kerja mengaju pada dokumen standar beban kerja yang dituangkan dalam SK nomor 712.1/SK/REK/UG/2016 \r\n5. Setiap unit kerja di lingkungan Universitas Gunadarma memiliki beban kerja sesuai dokumen yang dimaksud dalam poin 1 di atas, dan dengan ratio tenaga kependidikan dengan Sivitas akademika maksimal 1:50', 'a. Pimpinan Program Sarjana dan Diploma Tiga mengawasi pelaksanaan proses pembelajaran yang diselenggarakan oleh Program Studi;  \r\nb. Pimpinan Program Studi memonitor dan mengevaluasi pelaksanaan proses pembelajaran yang diampu oleh dosen; \r\nc. Pimpinan Program Sarjana dan Diploma Tiga dan Program Studi melakukan sosialisasi standar proses pembelajaran kepada seluruh dosen.', '2019-07-09 10:32:42', '2019-07-09 10:33:07'),
(58, 1, 7, 5, 'Pengembangan Dosen dan Tenaga kependidikan Universitas Gunadarma', '1. Institusi Universitas Gunadarma memiliki dokumen standar pengembagan SDM tingkat universitas dengan merujuk pada dokumen SK 711.2/SK/REK/UG/2016.\r\n 2. Setiap Prodi memiliki dokumen pengembangan Dosen di lingkungan akademiknya, merujuk pada dokumen SK 711.2/SK/REK/UG/2016 \r\n3. Setiap Dosen di setiap Prodi memiliki pengembangan karir/jenjang pendidikan/kepangkatan akademik sesuai dengan dokumen pada poin 1 dan 2. \r\n4. Setiap unit kerja memiliki dokumen pengembangan Tenaga kependidikan di lingkungan kerjanya, merujuk pada dokumen SK 711.2/SK/REK/UG/2016 \r\n5. Setiap Tenaga kependidikan memiliki jenjang karir sesuai dengan dokumen pada poin 4 di atas', 'a. Pimpinan Program Sarjana dan Diploma Tiga mengawasi pelaksanaan proses pembelajaran yang diselenggarakan oleh Program Studi;  \r\nb. Pimpinan Program Studi memonitor dan mengevaluasi pelaksanaan proses pembelajaran yang diampu oleh dosen; \r\nc. Pimpinan Program Sarjana dan Diploma Tiga dan Program Studi melakukan sosialisasi standar proses pembelajaran kepada seluruh dosen.', '2019-07-09 10:35:50', '2019-07-09 10:35:50'),
(59, 1, 7, 5, 'Penilaian Performa Kerja Dosen dan Tenaga Kependidikan Universitas Gunadarma', '1. Institusi Universitas Gunadarma memiliki dokumen penilaian kinerja SDM tingkat universitas dengan merujuk pada dokumen SK 712.2/SK/REK/UG/2016, dengan indikator utama: \r\na) Jumlah Dosen dengan nilai Indeks bidang pengajaran di atas 80 \r\nb) Jumlah Dosen dengan nilai Indeks bidang penelitian di atas 70\r\n c) Jumlah Dosen dengan nilai Indeks bidang pengabdian kepada masyarakat di atas 70 \r\nd) Pemenuhan persyaratan beban kinerja dosen\r\n 2. Setiap Prodi memiliki dokumen Penilaian Performa Kerja Dosen di lingkungan akademiknya, merujuk pada dokumen SK 712.2/SK/REK/UG/2016, dengan indikator utama:\r\n a) Jumlah Dosen dengan nilai Indeks bidang pengajaran di atas 80\r\n b) Jumlah Dosen dengan nilai Indeks bidang penelitian di atas 70 \r\nc) Jumlah Dosen dengan nilai Indeks bidang pengabdian kepada masyarakat di atas 70 \r\nd) Pemenuhan beban kerja dosen \r\n3. Institusi Universitas Gunadarma, Prodi dan Unit kerja memiliki sistem reward dan punishmen, serta petunjuk teknisnya.\r\n 4. Setiap Dosen di setiap Prodi memiliki kinerja akademik seperti dimaksud dalam dokumen pada poin 1 dan 2 di atas. \r\n5. Setiap unit kerja memiliki dokumen Penilaian Performa Kerja Tenaga kependidikan di lingkungan kerjanya, merujuk pada dokumen SK 712.2/SK/REK/UG/2016, dengan fokus pada jumlah tenga kependidikan dengan nilai indeks pelayanan di atas 80\r\n 6. Setiap Tenaga kependidikan di setiap unit kerja memiliki kinerja sesuai yang dimaksud pada dokumen pada ppoin no. 5.', 'a. Pimpinan Program Sarjana dan Diploma Tiga mengawasi pelaksanaan proses pembelajaran yang diselenggarakan oleh Program Studi;  \r\nb. Pimpinan Program Studi memonitor dan mengevaluasi pelaksanaan proses pembelajaran yang diampu oleh dosen; \r\nc. Pimpinan Program Sarjana dan Diploma Tiga dan Program Studi melakukan sosialisasi standar proses pembelajaran kepada seluruh dosen.', '2019-07-09 10:37:16', '2019-07-09 10:37:16'),
(60, 1, 7, 5, 'Pensiun Dan Pemberhentian Dosen dan Tenaga kependidikan Universitas Gunadarma', '1. Institusi Universitas Gunadarma memiliki dokumen standar pengelolaan pensiun dan pemberhentian SDM tingkat universitas dengan merujuk pada dokumen SK 097.2/SK/YPG/2016 \r\n2. Setiap Prodi memiliki dokumen standar pengelolaan pensiun dan pemberhentian Dosen di lingkungan akademiknya, merujuk pada dokumen SK 097.2/SK/YPG/2016 \r\n3. Setiap Dosen dapat mengakhiri masa tugasnya di setiap Prodi sesuai yang dimaksud dalam dokumen pada poin 1 dan 2. \r\n4. Setiap unit kerja memiliki dokumen standar pengelolaan pensiun dan pemberhentian Tenaga kependidikan di lingkungan kerjanya, merujuk pada dokumen SK 097.2/SK/YPG/2016 \r\n5. Setiap Tenaga kependidikan dapat mengakhiri masa tugasnya di setiap Prodi sesuai yang dimaksud dalam dokumen pada poin 4.', 'a. Pimpinan Program Sarjana dan Diploma Tiga mengawasi pelaksanaan proses pembelajaran yang diselenggarakan oleh Program Studi;  \r\nb. Pimpinan Program Studi memonitor dan mengevaluasi pelaksanaan proses pembelajaran yang diampu oleh dosen; \r\nc. Pimpinan Program Sarjana dan Diploma Tiga dan Program Studi melakukan sosialisasi standar proses pembelajaran kepada seluruh dosen.', '2019-07-09 10:38:19', '2019-07-09 10:38:19'),
(61, 1, 7, 6, 'Lahan', '1. Status kepemilikan milik sendiri dan bersertifikat. 2. Lokasi mudah dijangkau dan berada pada lingkungan yang sesuai dengan master plan kota', '1. Pimpinan universitas, fakultas dan program studi secara berkala melakukan evaluasi keberadaan sarana dan prasarana yang ada \r\n2. Menginventaris kebutuhan sarana dan prasarana', '2019-07-09 10:42:12', '2019-07-09 10:42:12');
INSERT INTO `tbl_standar_awal` (`id`, `category_id`, `profil_id`, `standar_dikti_id`, `s1`, `s2`, `s3`, `created_at`, `updated_at`) VALUES
(62, 1, 7, 6, 'Bangunan Gedung/Ruang', '1. Kekuatan fisik • Strukturbangunankuatdankokoh • Stabildalammemikulbeban/kombinasibeba n • Memenuhipersyaratankelayanan(serviceability)dengan mempertimbangkanfungsigedung,lokasi&keawetan • Memilikidokumenrencanainduk(masterpl an,perencanaan strukturgedunglengkapdenganspesifikasite knis) \r\n \r\n2. Kecukupan Sesuaidenganstandarratioluasterhadappemakai: •  Ruang kelas:1.5-2m2/mahasiswa •  Ruang kantor:2m2/dosenataukaryawan •  Ruang rapat:2m2/pesertarapat •  Ruang perpustakaan:1.6 m2/orang •  Ruang komputer:2 m2/orang •  Laboratorium: sesuai dengan kurikulum dan jumlah pemakaian yang direncanakan serta standar kebutuhan dan pemanfaatan ruangkhususlaboratorium/hari •  Masjid dan atau mushola disetiaplokasi kampus:sesuaijumlah maksimaljama’ahdankegiatankeagamaanrutin. PKM:sesuaidenganrataratajumlahkunjunganmahasiswadan karyawan/hari •  Ruang kegiatan mahasiswa:memenuhirencanadanjeniskegiata n mahasiswa(teater,senitari,ruangsenatmahasis wa,carier developmentcentre,danlain-lain) •  Gedung olahraga:memenuhikriteriagedung(indoor)untukpe makaian jeniscabangolaharagatertentudanstadionuntukca bangsepakbola •  Gudang:sesuaidenganrencanadayatampungperpe riode(umurpenyimpanan) •  Bengkel:sesuaijenisdanjumlahkendaraanunive rsitasserta kebutuhanruangperalatanbengkel • Rumah sakit:sesuaistandaruntukkelasrumahsakitd an mengakomodasikegiatanpendidikan 3. Kesesuaian Disaindanpenataansesuaidenganfungsiban gunangedung/ruang danpersyaratanlingkungan. 4. Keselamatan Memenuhipersyaratankemampuanbangunangedu nguntuk: • mendukungbebanmuatan • mencegahdanmenanggulangibahayakebakaran danpetir(memiliki dokumenpedomandanstandarteknisyangberla ku)mengenai: • pembebanan,ketahananterhadapgempa dan/atauangin • sistempengamanankebakaran • sistempenangkalpetir 5. Kemudahan • Hubunganke,dari,dandidalambangunange dung:tersedia fasilitasdanaksebilitasyangmudah,aman,d annyaman termasukuntukpenyandangcacatdanlanjut usia • Mempertimbangkantersedianyahubunganhorizo ntal(pintudan/atau koridor)danvertikalantarruangdalambangunange dung(tangga,ram, lift,danlainlain),aksesevakuasi(sistembahaya,pintukeluarda rurat, danlainlain),termasukbagipenyandangcacatdanlanjut usia', '1. Pimpinan universitas, fakultas dan program studi secara berkala melakukan evaluasi keberadaan sarana dan prasarana yang ada \r\n2. Menginventaris kebutuhan sarana dan prasarana', '2019-07-09 10:43:17', '2019-07-09 10:43:17'),
(63, 1, 7, 6, 'Kesehatan Lingkuangan dan Keamanan Lingkungan', '1. Kesehatan Memenuhipersyaratan: -   sistempenghawaan -   sistempencahayaan -   sistemsanitasi -   penggunaanbahanbangunangedung Persyaratanpenghawaan:tersediaventilasialamid an/ataubangunan ventilasimekanik/buatansesuaidenganfungsinyad anmempertimbangkan prinsipprinsippenghematanenergidalambangunanged ung \r\n•  \r\nPersyaratanpencahayaan:setiapbangunange dungharus mempunyaipencahayaanalamidan/ataupenc ahayaanbuatan, termasukpencahayaamdaruratsesuaidenganf ungsinya \r\n•  \r\nPesyaratansistemsanitasimencakupsistemair bersih,sistem pembuanganairkotordan/atauairlimbah,kotor andansampah, sertapenyaluranairhujan,termasuksistempla mbing \r\n•  \r\nPersyaratanbahanbangunan:menggunakanbah anbangunan yangamanbagikesehatan(tidakmengandungB3 )dantidak menimbulkandampaknegatifterhadaplingkung an(efeksilau, pantulan,peningkatansuhu,konservasienergi,s erasidanselaras denganlingkungan) (Memilikidokumenpedomandanstandarteknis yangberlakuuntuk sistempenghawaan,sistempencahayaan,sistem sanitasidan penggunaanbahanbangunangedung) 2. Kenyamanan • Memenuhipersyaratan: • Kenyamananruanggerak:mempertimb angkanfungsiruang, jumlahpengguna,perabot/peralatan,ak sebilitasruang • Hubunganantarruang • Tempatduduk,mejamemenuhipersyaratanergonomi • Kondisiudaradalamruang(pertimban gantemperaturdan kelembaban)nyaman,berAC • Pandangan:kenyamananpandangandaridal amgedungkeluar • Tingkatgetaran • Tingkatkebisingan • (Memilikidokumenpedomandanstandarteknis yangberlakuuntuk hubunganantarruang,temperaturdankelembab an,pandangan, tingkatgetaran,tingkatkebisingan) \r\n \r\n3. Keamanan Lingkungan  • Tersedianyaunitpenanggungjawabkeamananling kungan(UPT-PLK) • AdanyaProgramkeamananlingkungankampus yangdilaksanakan dandievaluasisecararutin • Tidakadatindakkriminalitasdanasusiladilingku ngankampus', '1. Pimpinan universitas, fakultas dan program studi secara berkala melakukan evaluasi keberadaan sarana dan prasarana yang ada \r\n2. Menginventaris kebutuhan sarana dan prasarana', '2019-07-09 10:43:46', '2019-07-09 10:43:46'),
(64, 1, 7, 6, 'Efektivitas Pemakian Bangunan/Gedung', '1. Efektifitas • Mempunyaipedomanpemakaiansarana • Memilikitargetpemakaian • Memilikidatapemakaiandandinilaiefisiendala mpemakaiannya • Dibuatrekomendasiperbaikan', '1. Pimpinan universitas, fakultas dan program studi secara berkala melakukan evaluasi keberadaan sarana dan prasarana yang ada \r\n	2. Menginventaris kebutuhan sarana dan prasarana', '2019-07-09 10:44:08', '2019-07-09 10:44:08'),
(65, 1, 7, 6, 'Pemeliharaan dan Perawatan Bangunan/  Gedung', '1. Pelaksanaan Pemeliharaan Tersediaunitdansdmpemeliharadanperawatanbangun angedungatau menggunakanjasapemeliharaandanperawatangedun gyangbersertifikat \r\n \r\n2. Pemeliharaan Tersediaunitdansdmpemeliharadanperawatanbang unangedungatau menggunakanjasapemeliharaandanperawatangedu ngyangbersertifikat \r\n 3. Perawatan Perawatanmeliputiperbaikandan/ataupenggantian bagian bangunan,bahanbangunan,dan/atauprasaranadans arana berdasarkandokumenrencanateknisperawatanban gunangedung \r\n \r\n4. Sertfikat Laik Fungsi Terdapatlaporanhasilkegiatanpemeliharaandanper awatanyang digunakanuntukpertimbanganpenetapanperpanjan gansertifikat laikfungsiyangditetapkanpemda(setiap5tahun). \r\n \r\n5. K3 Kegiatanpelaksanaanpemeliharaandanperawatanb angunan gedungharusmenerapkanprinsipprinsipkeselamatandan kesehatankerja(K3) \r\n \r\n6. Pemeriksaan Berkala Pemeriksaanberkaladilakukanterhadapseluruhata usebagian bangunangedung,komponen,bahanbangunan,dan/ atauprasarana dansaranadalamrangkapemeliharaandanperawata nbangunan gedung,gunamemperolehperpanjangansertifikatla ikfungsi', '1. Pimpinan universitas, fakultas dan program studi secara berkala melakukan evaluasi keberadaan sarana dan prasarana yang ada \r\n	2. Menginventaris kebutuhan sarana dan prasarana', '2019-07-09 10:44:41', '2019-07-09 10:44:41'),
(66, 1, 7, 6, 'Air', '1. Pemenuhan persyaratanteknis Sistempenyediaanairbersih,reservoir,perpipa an,dan perlengkapannya,memenuhipersyaratantekni s  2. Kontinuitasaliran  Jumlahairyangtersediamemenuhikebutuhanpe makai \r\n \r\n3. Kuantitas Kualitasairmemenuhipersyaratanairbersih \r\n \r\n4. Kualitas Aliranairmengalirsecaramenerus \r\n \r\n5. Kepuasan Tidakadakeluhandaripemakai', '1. Pimpinan universitas, fakultas dan program studi secara berkala melakukan evaluasi keberadaan sarana dan prasarana yang ada \r\n	2. Menginventaris kebutuhan sarana dan prasarana', '2019-07-09 10:45:03', '2019-07-09 10:45:03'),
(67, 1, 7, 6, 'Sanitasi', '1. Pemenuhan persyaratanteknis WC/toiletmemenuhipersyaratanteknis \r\n \r\n2. Ketersediaanairbersih Tersediaairbersihdalamjumlahcukup \r\n \r\n3. KebersihanWC/toilet  WC/toiletdalamkeadaanbersihdanberfungsi \r\n \r\n4. Kepuasanpemakai Tidakadakeluhandaripemakai', '1. Pimpinan universitas, fakultas dan program studi secara berkala melakukan evaluasi keberadaan sarana dan prasarana yang ada \r\n	2. Menginventaris kebutuhan sarana dan prasarana', '2019-07-09 10:45:20', '2019-07-09 10:45:20'),
(68, 1, 7, 6, 'Drainase', '1. Pemenuhan persyaratanteknis • Salurandrainasedanbangunanairlainnyamemenu hipersyaratanteknis • Salurandrainasemampumengatasialiranairpu ncak(tidak terjadigenanganair,banjir) \r\n \r\n2. Kebersihansaluran • Salurandrainaseyangbersih/terpelihara', '1. Pimpinan universitas, fakultas dan program studi secara berkala melakukan evaluasi keberadaan sarana dan prasarana yang ada \r\n	2. Menginventaris kebutuhan sarana dan prasarana', '2019-07-09 10:45:35', '2019-07-09 10:45:35'),
(69, 1, 7, 6, 'Listrik', '1. Pemenuhan persyaratanteknis gardudan perlengkapan peralatanlistrik. • Perlengkapanlistrikmemenuhipersyaratantekn is • Tersediagardulistrikdanperalatanlistrikdengan kondisibaik(laporanpemeriksaansecaraberkala ) 2. Kecukupan • Prosespembelajarantidaktergangguolehkurang nyadayalistrik \r\n \r\n3. Efisiensi • Pemakaiansesuaikebutuhan(dokumenlaporanpe nggunaanlistrik)', '1. Pimpinan universitas, fakultas dan program studi secara berkala melakukan evaluasi keberadaan sarana dan prasarana yang ada \r\n	2. Menginventaris kebutuhan sarana dan prasarana', '2019-07-09 10:46:04', '2019-07-09 10:46:04'),
(70, 1, 7, 6, 'Jaringan Telekomunikasi', '1. Tersediajaringan telekomunikasi • Tersediasambungandaninstalasitelepondengan kondisibaik • (laporanpemeriksaansecaraberkala) • Tersediajaringaninformasidankomunikasilainny a(MisalBTS) \r\n \r\n2. Kecukupan • Tidaktergangguproseskomunikasidaninforma sikarena minimnyajumlahsalurantelepondanlainnya \r\n \r\n3. Efisiensi • Pemakaiansesuaikebutuhan(dokumenlaporan penggunaan telepon,danlainnya)', '1. Pimpinan universitas, fakultas dan program studi secara berkala melakukan evaluasi keberadaan sarana dan prasarana yang ada \r\n2. Menginventaris kebutuhan sarana dan prasarana', '2019-07-09 10:46:29', '2019-07-09 10:46:29'),
(71, 1, 7, 6, 'Parkir', '1. Pemenuhandaya tampung • Memenuhidayatampungkendaraansivitasakad emika (berdasarkanpendataan) \r\n 2. Pengaturanparker • Tataletakdanpengaturanyangtepat \r\n 3. Keamanan kendaraan • Keamanankendaraanditempatparker', '1. Pimpinan universitas, fakultas dan program studi secara berkala melakukan evaluasi keberadaan sarana dan prasarana yang ada \r\n2. Menginventaris kebutuhan sarana dan prasarana', '2019-07-09 10:46:50', '2019-07-09 10:46:50'),
(72, 1, 7, 6, 'Taman', '1. Penataantaman  • Penataantamanyangmenunjangsuasanabelaj aryangnyaman \r\n \r\n2. Pemilihan tanaman • Pemilihantanamananyangtepatuntuklingkungan,keindahan dankemudahanperawatan/pemeliharaan', '1. Pimpinan universitas, fakultas dan program studi secara berkala melakukan evaluasi keberadaan sarana dan prasarana yang ada \r\n2. Menginventaris kebutuhan sarana dan prasarana', '2019-07-09 10:47:13', '2019-07-09 10:47:13'),
(73, 1, 7, 6, 'Instalasi Pengolahan Limbah Laboratorium', 'Persyaratanteknis Instalasipengelolaan LimbahLabroratorium • Memilikipengolahanlimbahdarilaboratorium yangterpisahdari limbahdomestik • Hasilpengolahanyangdibuangkesalurandrai nase/badanair memenuhibakuperuntukanbadanairsetemp at • Adanyapengawasanterhadappengelolaanlimbah B3dariLaboratorium', '1. Pimpinan universitas, fakultas dan program studi secara berkala melakukan evaluasi keberadaan sarana dan prasarana yang ada \r\n2. Menginventaris kebutuhan sarana dan prasarana', '2019-07-09 10:47:55', '2019-07-09 10:47:55'),
(74, 1, 7, 6, 'Pengelolaan Sampah', '• Pengelolaansampahterpadu • Memiliki peralatan/perlengkapan pengelolaan sampah dengan kualitas baik', '1. Pimpinan universitas, fakultas dan program studi secara berkala melakukan evaluasi keberadaan sarana dan prasarana yang ada \r\n2. Menginventaris kebutuhan sarana dan prasarana', '2019-07-09 10:48:14', '2019-07-09 10:48:14'),
(75, 1, 7, 6, 'Peralatan Ruang Kuliah', '1. Ketersedian peralatankuliah • Tersedianyaperalatankuliahlengkap(seperti LCD,OHP, whiteboard,soundsytem,danlain-lain) \r\n \r\n2. Ketersedian peralatancadangan • Tersediaperalatankuliahcadangan \r\n \r\n3. Ketersedianruang kuliahcadangan • Tersediaruang kuliahcadangan', '1. Pimpinan universitas, fakultas dan program studi secara berkala melakukan evaluasi keberadaan sarana dan prasarana yang ada \r\n2. Menginventaris kebutuhan sarana dan prasarana', '2019-07-09 10:48:45', '2019-07-09 10:48:45'),
(76, 1, 7, 6, 'Peralatan Ruang Perkantoran', '1. Ketersediaan peralatangedung perkantoran • Tersedianyaperlatankantorcukupmoderndanle ngkap \r\n \r\n2. Usiaperalatankantor • Usiaperalatankantormaksimal5tahun', '1. Pimpinan universitas, fakultas dan program studi secara berkala melakukan evaluasi keberadaan sarana dan prasarana yang ada \r\n2. Menginventaris kebutuhan sarana dan prasarana', '2019-07-09 10:49:02', '2019-07-09 10:49:02'),
(77, 1, 7, 6, 'Bahan dan Perlengkapan Perpustakaan', '• Kesesuaian/relevansi dankemutahiran bahanpustaka • Jumlahjudulbahanpustakalengkap,relevandan mutakhirdengan cakupanyangluas \r\n \r\n• Variasijenis,bentuk bahanpustaka • Bahanpustakasesuaidengankebutuhanprogram studidan bervariasi:Buku,CDROM,jurnalilmiah. \r\n \r\n• Ketersediaanbuku bermutu • Tersediabukureferensiinternasionalminimal25 % • Tersediadokumendisertasi,thesis,skripsidantugas akhirmahasiswa \r\n \r\n• Tahunterbitan • Tersediabukuteks,jurnal,majalahilmiahterbita n3tahunterakhir \r\n \r\n• Rata-rata perbandinganjumlah bukuterhadap mahasiswadalam semuabidangkajian • Rasiojumlahbukuterhadapmahasiswadalamse muabidangkajian memenuhiPedomanPerpustakaanPerguruanTi nggi \r\n \r\n• Aksesibilitasdengan sumberpustakalain • Memilikiaksesdari/keperpustakaandaerah,nasi onal.', '1. Pimpinan universitas, fakultas dan program studi secara berkala melakukan evaluasi keberadaan sarana dan prasarana yang ada \r\n2. Menginventaris kebutuhan sarana dan prasarana', '2019-07-09 10:49:47', '2019-07-09 10:49:47'),
(78, 1, 7, 6, 'Peralatan Laboratorium, Bengkel,Studio, Lahan percobaan, Klinik', '1. Ketersediaandan kecukupan • Peralatanlaboratoriumlengkap,moderndancuk upmutakhirserta sesuaidengankebutuhan  \r\n \r\n2. Kesesuaian • Adaperencanaandengandanayangmemadaiunt ukpengadaan, pemeliharaandanpeningkatanmutuperalatan \r\n \r\n3. Intensitas penggunaan \r\n \r\n4. Keberfungsian&kemutakhiran  \r\n• Ruanganmemenuhistandarkeamanan,keselam atandan kenyamanankerja \r\n \r\n5. Usiaperalatanyangtersedia • Usiaperalatanmaksimal5tahun \r\n \r\n• 6.  Presentasialat yangmutakhir • Jumlahperalatanyangmutakhirminimal25%', '1. Pimpinan universitas, fakultas dan program studi secara berkala melakukan evaluasi keberadaan sarana dan prasarana yang ada \r\n2. Menginventaris kebutuhan sarana dan prasarana', '2019-07-09 10:50:19', '2019-07-09 10:50:19'),
(79, 1, 7, 6, 'Fasilitas Komputer', '1. Jumlah,jenis&kemutakhiran perangkatkeras danlunak • Tersediakomputerdanperangkatlunakyangle ngkapdan canggih • Sistemteknologiinformasiharusselaluditatadan diupgrade minimal1tahun1kali \r\n \r\n2. Aksesibilitas dan Waktu Pelayanan • Dihubungkandenganjaringanlokaldaninternet • Aksesuntukdosen,mahasiswadanpegawailain nyaminimal18 jam \r\n \r\n3. Dukungankebijakan dan Pemerilahaan Sistem • Adakebijakanpemeliharaandanmodernisasiko mputerserta didukungdanayangmemadai \r\n \r\n4. Rasiocomputer/mahasiswa • Rasiojumlahkomputer/mhsmaksimal1:10 \r\n \r\n5. Pemanfaatandalam pembelajaran • Pemakaiankomputertinggi', '1. Pimpinan universitas, fakultas dan program studi secara berkala melakukan evaluasi keberadaan sarana dan prasarana yang ada \r\n2. Menginventaris kebutuhan sarana dan prasarana', '2019-07-09 10:50:40', '2019-07-09 10:50:40'),
(80, 1, 7, 6, 'Efektifitas Pemakaian', '1. Pedoman pemakaiansarana • Mempunyaipedomanpemakaianprasarana • Memilikitargetpemakaian \r\n \r\n2. Efisiensi pemakaian • Memilikitargetpemakaian • Memilikidatapemakaiandandinilaiefisiendala mpemakaiannya \r\n \r\n3. Tindakan perbaikanmutu • Dibuatrekomendasiperbaikan', '1. Pimpinan universitas, fakultas dan program studi secara berkala melakukan evaluasi keberadaan sarana dan prasarana yang ada \r\n2. Menginventaris kebutuhan sarana dan prasarana', '2019-07-09 10:50:59', '2019-07-09 10:50:59'),
(81, 1, 7, 6, 'Pemeliharaan dan Perawatan Sarana', '1. Pelaksana pemeliharaan • Tersediaunitdansdmyangdapatmemeliharasara nakampus (Universitas Gunadarma),seperti operatorkomputer,pustakawan,laboran,danlai n-lain \r\n \r\n2. Pemeliharaan • Terselenggarakegiatanpemeliharaansaranase caraberkala, meliputi:pembersihan,perapian,pemeriksaan, pengujian,perbaikan dan/ataupenggantianbahanatauperlengkapan sarana,dan kegiatansejenislainnyaberdasarkanpedoman pengoperasiandan pemeliharaansarana (misalmemilikidokumentatacara pemeliharaanbahandanperlengkapanlaborato rium)', '1. Pimpinan universitas, fakultas dan program studi secara berkala melakukan evaluasi keberadaan sarana dan prasarana yang ada \r\n2. Menginventaris kebutuhan sarana dan prasarana', '2019-07-09 10:51:16', '2019-07-09 10:51:16'),
(82, 1, 7, 6, 'Sumberbelajar', '1. Kelayakandan keberagaman • Tersediasumbersumberpemelajaran/intruksionalyanglayak/ses uaidengankebutuhandalamSAP • Adadokumensumbersumberbelajaryangdimiliki • Sumberbelajaryangtersediadandimanfaatkanb eragam \r\n \r\n2. SOP penggunaan sumberbelajar • Ada SOP penggunaan perpustakaan • Setiapkegiatandalampemanfaatansumberbelaj aryangada memilkiSOP \r\n \r\n3. Dokumentasi • Tersediadokumentasitentangaksesdanvariasi penggunaan sumbersumberpembelajaranolehmahasiswadanstaf akademik • Tersediadokumentasisumbersumberbelajaryangditawarkan padadosendanmahasiswa \r\n \r\n4. Perpustakaan digital • Tersediaperpustakaanyangdapatdiaksesolehm ahasiswa dengan mudah • Tersediaatauminimaladarencanauntukmenge mbangkan perpustakaandigitalyangmudahdiaksesolehdo sendan mahasiswa', '1. Pimpinan universitas, fakultas dan program studi secara berkala melakukan evaluasi keberadaan sarana dan prasarana yang ada \r\n2. Menginventaris kebutuhan sarana dan prasarana', '2019-07-09 10:51:38', '2019-07-09 10:51:38'),
(83, 1, 7, 6, 'Media pembelajaran', 'Isimedia • Isisejalandengankurikulumdanmampumemba ngkitkanmotivasi', '1. Pimpinan universitas, fakultas dan program studi secara berkala melakukan evaluasi keberadaan sarana dan prasarana yang ada \r\n2. Menginventaris kebutuhan sarana dan prasarana', '2019-07-09 10:51:57', '2019-07-09 10:51:57'),
(84, 1, 7, 7, 'Penerapan	Biro	Administrasi	Umum', '-	Adanya	SK	penetapan	Universitas	Gunadarma	sebagai	Biro	Administrasi	Umum		-	Tersedia	berbagai	kebijakan	yang	terkait	dengan	pengelolaan	Universitas	Gunadarma	sebagai	Biro	Administrasi	Umum		-	Adanya	SK	Rektor	dan	pedoman	pelaksanaan	Biro	Administrasi	Umum	di	Universitas	Gunadarma', 'a. Pimpinan Program Sarjana&Diploma 3 mengawasi pelaksanaan proses pembelajaran yang diselenggarakan oleh Program Studi;\r\nb. Pimpinan Program Studi memonitor dan mengevaluasi pelaksanaan proses pembelajaran yang diampu oleh dosen; \r\nc. Pimpinan Program  Sarjana&Diploma 3  dan Program Studi melakukan sosialisasi standar proses pembelajaran kepada seluruh dosen.', '2019-07-09 10:53:33', '2019-07-09 10:53:33'),
(85, 1, 7, 7, 'Struktur	organisasi	di	tingkat Universitas,	Fakultas,	Program	Studi', '-	Tersedia	SK	Rektor	untuk	setiap	unit	organisasi', 'a. Pimpinan Program Sarjana&Diploma 3 mengawasi pelaksanaan proses pembelajaran yang diselenggarakan oleh Program Studi;\r\nb. Pimpinan Program Studi memonitor dan mengevaluasi pelaksanaan proses pembelajaran yang diampu oleh dosen; \r\nc. Pimpinan Program  Sarjana&Diploma 3  dan Program Studi melakukan sosialisasi standar proses pembelajaran kepada seluruh dosen.', '2019-07-09 10:54:07', '2019-07-09 10:54:07'),
(86, 1, 7, 7, 'Pedoman	yang	mengatur	pelaksanaan	pengelolaan	pendidikan	(kurikulum,	kalender	akademik,	tugas	dan	pembagian	tugas	tenaga	pendidik	dan	kependidikan)', '-	Tersedia	pedoman	yang	lengkap	untuk	setiap	pengelolaan	pendidikan	yang	memiliki	SK	Rektor', 'a. Pimpinan Program Sarjana&Diploma 3 mengawasi pelaksanaan proses pembelajaran yang diselenggarakan oleh Program Studi;\r\nb. Pimpinan Program Studi memonitor dan mengevaluasi pelaksanaan proses pembelajaran yang diampu oleh dosen; \r\nc. Pimpinan Program  Sarjana&Diploma 3  dan Program Studi melakukan sosialisasi standar proses pembelajaran kepada seluruh dosen.', '2019-07-09 10:54:23', '2019-07-09 10:54:23'),
(87, 1, 7, 7, 'Kode	etik	sivitas	akademik', '-	Tersedia	pedoman	yang	mengatur	etika	akademik	yang	memiliki	SK	Rektor', 'a. Pimpinan Program Sarjana&Diploma 3 mengawasi pelaksanaan proses pembelajaran yang diselenggarakan oleh Program Studi;\r\nb. Pimpinan Program Studi memonitor dan mengevaluasi pelaksanaan proses pembelajaran yang diampu oleh dosen; \r\nc. Pimpinan Program  Sarjana&Diploma 3  dan Program Studi melakukan sosialisasi standar proses pembelajaran kepada seluruh dosen.', '2019-07-09 10:54:40', '2019-07-09 10:54:40'),
(88, 1, 7, 7, 'Biaya	operasional	satuan	pendidikan', '-	Tersedia	SK	Rektor	tentang	Biaya	Operasional	Pendidikan	(BOP)	untuk	setiap	jenjang	pendidikan	di	Universitas	Gunadarma	yang	mudah	diakses	oleh	calon	peserta	didik', 'a. Pimpinan Program Sarjana&Diploma 3 mengawasi pelaksanaan proses pembelajaran yang diselenggarakan oleh Program Studi;\r\nb. Pimpinan Program Studi memonitor dan mengevaluasi pelaksanaan proses pembelajaran yang diampu oleh dosen; \r\nc. Pimpinan Program  Sarjana&Diploma 3  dan Program Studi melakukan sosialisasi standar proses pembelajaran kepada seluruh dosen.', '2019-07-09 10:54:54', '2019-07-09 10:54:54'),
(89, 1, 7, 7, 'Rencana	kerja	menengah	dan	rencana	kerja	tahunan', '-	Tersedia	Renstra	dan	Rencana	Kerja	Anggaran	Tahunan	di	setiap	unit	kerja', 'a. Pimpinan Program Sarjana&Diploma 3 mengawasi pelaksanaan proses pembelajaran yang diselenggarakan oleh Program Studi;\r\nb. Pimpinan Program Studi memonitor dan mengevaluasi pelaksanaan proses pembelajaran yang diampu oleh dosen; \r\nc. Pimpinan Program  Sarjana&Diploma 3  dan Program Studi melakukan sosialisasi standar proses pembelajaran kepada seluruh dosen.', '2019-07-09 10:55:14', '2019-07-09 10:55:14'),
(90, 1, 7, 7, 'Pengelolaan	satuan	pendidikan	yang	mandiri,	efisien,	efektif	dan	akuntabel', '-	Setiap	unit	kerja	melakukan	evaluasi	internal	secara	periodik		-	Memanfaatkan	teknologi	informasi	dan	komunikasi	(TIK)	sbagai	media	pengelolaan', 'a. Pimpinan Program Sarjana&Diploma 3 mengawasi pelaksanaan proses pembelajaran yang diselenggarakan oleh Program Studi;\r\nb. Pimpinan Program Studi memonitor dan mengevaluasi pelaksanaan proses pembelajaran yang diampu oleh dosen; \r\nc. Pimpinan Program  Sarjana&Diploma 3  dan Program Studi melakukan sosialisasi standar proses pembelajaran kepada seluruh dosen.', '2019-07-09 10:55:29', '2019-07-09 10:55:29'),
(91, 1, 7, 7, 'Pengaturan	kegiatan	yang	tidak	tercantum	dalam	Rencana	Kerja	Anggaran	Tahunan', '-	Pelaksanaan	kegiatan	yang	sangat	penting	dan	harus	dilaksanakan	yang	tidak	sesuai	dengan	Rencana	Kerja	Anggaran	Tahunan	merupakan	kebijakan	Pimpinan	Universitas', 'a. Pimpinan Program Sarjana&Diploma 3 mengawasi pelaksanaan proses pembelajaran yang diselenggarakan oleh Program Studi;\r\nb. Pimpinan Program Studi memonitor dan mengevaluasi pelaksanaan proses pembelajaran yang diampu oleh dosen; \r\nc. Pimpinan Program  Sarjana&Diploma 3  dan Program Studi melakukan sosialisasi standar proses pembelajaran kepada seluruh dosen.', '2019-07-09 10:55:47', '2019-07-09 10:55:47'),
(92, 1, 7, 7, 'Pertanggungjawaban	pelaksanaan	pegelolaan	pendidikan	di	Universitas	Gunadarma', '-	Tersedia	laporan	yang	memuat	capaian	kinerja	Universitas	Gunadarma	setiap	tahun	dan	dipertanggungjawabkan	pada	sidang	paripurna	senat	Universitas	Gunadarm', 'a. Pimpinan Program Sarjana&Diploma 3 mengawasi pelaksanaan proses pembelajaran yang diselenggarakan oleh Program Studi;\r\nb. Pimpinan Program Studi memonitor dan mengevaluasi pelaksanaan proses pembelajaran yang diampu oleh dosen; \r\nc. Pimpinan Program  Sarjana&Diploma 3  dan Program Studi melakukan sosialisasi standar proses pembelajaran kepada seluruh dosen.', '2019-07-09 10:56:06', '2019-07-09 10:56:06'),
(93, 1, 7, 7, 'Pengawasan	satuan	pendidikan	(pemantauan,	supervise,	evaluasi,	laporan	dan	tindak	lanjut	hasil	pengawasan)', '-	Tersedia	laporan	pengawasan	dan	ada	tindak	lanjut	hasil	pengawasan', 'a. Pimpinan Program Sarjana&Diploma 3 mengawasi pelaksanaan proses pembelajaran yang diselenggarakan oleh Program Studi;\r\nb. Pimpinan Program Studi memonitor dan mengevaluasi pelaksanaan proses pembelajaran yang diampu oleh dosen; \r\nc. Pimpinan Program  Sarjana&Diploma 3  dan Program Studi melakukan sosialisasi standar proses pembelajaran kepada seluruh dosen.', '2019-07-09 10:56:32', '2019-07-09 10:56:32'),
(94, 1, 7, 7, 'Supervisi	manajerial	dan	akademik', '-	Dilakukan	secara	teratur	dan	berkesinambungan	oleh	petugas	pelaksanayang	ditetapkan	oleh	pimpinan	universitas/fakultas	-	Tersedia	sistem	supervisi	manajemen	dan	akademik		-	Tersedia	laporan	hasil	supervisi	oleh	pimpinan	Universitas	Gunadarma	/fakultas	atau	unit	kerja	lainnya', 'a. Pimpinan Program Sarjana&Diploma 3 mengawasi pelaksanaan proses pembelajaran yang diselenggarakan oleh Program Studi;\r\nb. Pimpinan Program Studi memonitor dan mengevaluasi pelaksanaan proses pembelajaran yang diampu oleh dosen; \r\nc. Pimpinan Program  Sarjana&Diploma 3  dan Program Studi melakukan sosialisasi standar proses pembelajaran kepada seluruh dosen.', '2019-07-09 10:57:01', '2019-07-09 10:57:01'),
(95, 1, 7, 7, 'Pelaporan	oleh	pendidik,	tenaga	kependidikan,	pimpinan	satuan	pendidikan', '-	Tersedia	format	laporan	sesuai	lingkup	tugas	masing-	masing	-	Tersedia	laporan	sesuai	lingkup	tugas	masing-masing', 'a. Pimpinan Program Sarjana&Diploma 3 mengawasi pelaksanaan proses pembelajaran yang diselenggarakan oleh Program Studi;\r\nb. Pimpinan Program Studi memonitor dan mengevaluasi pelaksanaan proses pembelajaran yang diampu oleh dosen; \r\nc. Pimpinan Program  Sarjana&Diploma 3  dan Program Studi melakukan sosialisasi standar proses pembelajaran kepada seluruh dosen.', '2019-07-09 10:57:27', '2019-07-09 10:57:27'),
(96, 1, 7, 8, 'Standar Pembiayaan Pembelajaran', '1. Institusi Universitas Gunadarma memiliki Renstra Pembiayaan Pembelajaran tingkat Universitas, merujuk pada dokumen SK Rektor 2. Setiap Prodi memiliki renstra pengelolaan SDM di lingkungan akademiknya, merujuk pada dokumen SK Rektor 3. Setiap unit kerja memiliki renstra pengelolaan SDM di lingkungan kerjanya, merujuk pada dokumen SK Rektor', 'a. Pimpinan Program Sarjana&Diploma 3 mengawasi pelaksanaan proses pembelajaran yang diselenggarakan oleh Program Studi;\r\nb. Pimpinan Program Studi memonitor dan mengevaluasi pelaksanaan proses pembelajaran yang diampu oleh dosen; \r\nc. Pimpinan Program  Sarjana&Diploma 3  dan Program Studi melakukan sosialisasi standar proses pembelajaran kepada seluruh dosen.', '2019-07-09 10:58:39', '2019-07-09 10:58:39'),
(97, 1, 7, 8, 'Biaya Investasi', '1. Institusi Universitas Gunadarma memiliki dokumen standar pembiayaan sarana dan prasarana yang meliputi standar kecukupan pembiayaan investasi sarana dan prasarana  \r\n \r\n2. Institusi Universitas Gunadarma memiliki dokumen standar investasi pengembangan dosen yang meliputi standar pembiayaan studi lanjut, standar biaya seminar/workshop/lokakarya/ magang \r\n \r\n3. Institusi Universitas Gunadarma memiliki dokumen standar investasi pembiayaan tenaga kependidikan yang meliputi standar pembiayaan studi lanjut, standar biaya seminar/workshop/lokakarya/ magang', 'a. Pimpinan Program Sarjana&Diploma 3 mengawasi pelaksanaan proses pembelajaran yang diselenggarakan oleh Program Studi;\r\nb. Pimpinan Program Studi memonitor dan mengevaluasi pelaksanaan proses pembelajaran yang diampu oleh dosen; \r\nc. Pimpinan Program  Sarjana&Diploma 3  dan Program Studi melakukan sosialisasi standar proses pembelajaran kepada seluruh dosen.', '2019-07-09 10:58:59', '2019-07-09 10:58:59'),
(98, 1, 7, 8, 'Biaya Operasional', 'Setiap Program studi di lingkungan Gunadarma telah memperhatikan: 1. Kecukupan pada biaya dosen,  biaya bahan operasional pembelajaran, biaya perkuliahan/tatap muka,  biaya laboratorium/praktek, biaya ujuian semester dan biaya operasional lainnya \r\n \r\nPenentuan kecukupan biaya dosen tertuang dalam POB Biaya dosen Universitas Gunadarma yang mengacu pada Permenristek Dikti No 44 tahun 2015 tetnang SNPT \r\n \r\n2. Rata-rata dana operasional (pendidikan, penelitian, pengabdian pada masyarakat, termasuk gaji dan upah> lebih dari 20 juta per mahasiswa per tahun', 'a. Pimpinan Program Sarjana&Diploma 3 mengawasi pelaksanaan proses pembelajaran yang diselenggarakan oleh Program Studi;\r\nb. Pimpinan Program Studi memonitor dan mengevaluasi pelaksanaan proses pembelajaran yang diampu oleh dosen; \r\nc. Pimpinan Program  Sarjana&Diploma 3  dan Program Studi melakukan sosialisasi standar proses pembelajaran kepada seluruh dosen.', '2019-07-09 10:59:22', '2019-07-09 10:59:22'),
(99, 1, 7, 8, 'Standar Penganggaran Pembiayaan Pembelajaran', '1. Institusi Universitas Gunadarma memiliki dokumen standar Penganggaran Pembiayaan Pembelajaran tingkat universitas dengan merujuk pada dokumen SK Rektor 2. Setiap Prodi memiliki dokumen Penganggaran Pembiayaan Pembelajaran lingkungan akademiknya, merujuk pada dokumen SK Rektor 3. Setiap unit kerja memiliki dokumen Penganggaran Pembiayaan Pembelajaran di lingkungan kerjanya, merujuk pada dokumen SK Rektor', 'a. Pimpinan Program Sarjana&Diploma 3 mengawasi pelaksanaan proses pembelajaran yang diselenggarakan oleh Program Studi;\r\nb. Pimpinan Program Studi memonitor dan mengevaluasi pelaksanaan proses pembelajaran yang diampu oleh dosen; \r\nc. Pimpinan Program  Sarjana&Diploma 3  dan Program Studi melakukan sosialisasi standar proses pembelajaran kepada seluruh dosen.', '2019-07-09 10:59:35', '2019-07-09 10:59:35'),
(100, 1, 7, 8, 'Standar Implementasi pembiayaan', 'Universitas Gunadarma memiliki Standar Implementasi pembiayaan yang tertuang di SK Rektor Setiap unit pelaksana berkewajiban melaksanakan anggaran yang sudah ditetapkan dan wajib memberikan laporan pelaksanaan kegiatan', 'a. Pimpinan Program Sarjana&Diploma 3 mengawasi pelaksanaan proses pembelajaran yang diselenggarakan oleh Program Studi;\r\nb. Pimpinan Program Studi memonitor dan mengevaluasi pelaksanaan proses pembelajaran yang diampu oleh dosen; \r\nc. Pimpinan Program  Sarjana&Diploma 3  dan Program Studi melakukan sosialisasi standar proses pembelajaran kepada seluruh dosen.', '2019-07-09 10:59:56', '2019-07-09 10:59:56'),
(101, 1, 7, 8, 'Standar Monitoring Pembiayaan', 'Universitas Gunadarma memiliki Standar monitoring pembiayaan yang terstuang di SK Rektor \r\n \r\nSetiap unit pelaksana melakukan monitoring internal atas penggunaan dana. Monitoring selanjutnya dilakukan oleh unit yang lebih tinggi dan secara periodic akan dilakukan oleh monitoring oleh BAJAMTU', 'a. Pimpinan Program Sarjana&Diploma 3 mengawasi pelaksanaan proses pembelajaran yang diselenggarakan oleh Program Studi;\r\nb. Pimpinan Program Studi memonitor dan mengevaluasi pelaksanaan proses pembelajaran yang diampu oleh dosen; \r\nc. Pimpinan Program  Sarjana&Diploma 3  dan Program Studi melakukan sosialisasi standar proses pembelajaran kepada seluruh dosen.', '2019-07-09 11:01:17', '2019-07-09 11:01:17'),
(102, 1, 7, 8, 'Standar sumber dana', 'Universitas Gunadarma memiliki Standar Sumber yang tertuang di SK Rektor \r\n \r\nSumber dana berasal dari: 1. Dana masyarakat 2. Dana Unit produksi 3. Dana Hibah   4. Dana dari mitra/institusi melalui kerjasa', 'a. Pimpinan Program Sarjana&Diploma 3 mengawasi pelaksanaan proses pembelajaran yang diselenggarakan oleh Program Studi;\r\nb. Pimpinan Program Studi memonitor dan mengevaluasi pelaksanaan proses pembelajaran yang diampu oleh dosen; \r\nc. Pimpinan Program  Sarjana&Diploma 3  dan Program Studi melakukan sosialisasi standar proses pembelajaran kepada seluruh dosen.', '2019-07-09 11:01:35', '2019-07-09 11:01:35'),
(103, 1, 7, 8, 'Standar penggunaan dana', 'Universitas Gunadarma memiliki Standar penggunaan dana yang tertuang di SK Rektor Alokasi penggunaan dana untuk: 1. Pendidikan 2. Penelitian 3. Pengabdian kepada masyarakat', 'a. Pimpinan Program Sarjana&Diploma 3 mengawasi pelaksanaan proses pembelajaran yang diselenggarakan oleh Program Studi;\r\nb. Pimpinan Program Studi memonitor dan mengevaluasi pelaksanaan proses pembelajaran yang diampu oleh dosen; \r\nc. Pimpinan Program  Sarjana&Diploma 3  dan Program Studi melakukan sosialisasi standar proses pembelajaran kepada seluruh dosen.', '2019-07-09 11:01:54', '2019-07-09 11:01:54'),
(104, 2, 1, 9, '2', '2', '2', '2019-07-27 17:00:04', '2019-07-27 17:00:04');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_standar_dikti`
--

CREATE TABLE `tbl_standar_dikti` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` int(11) NOT NULL,
  `nama_standar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `tbl_standar_dikti`
--

INSERT INTO `tbl_standar_dikti` (`id`, `category_id`, `nama_standar`, `created_at`, `updated_at`) VALUES
(1, 1, 'Standar Kompetensi Lulusan', NULL, '2019-07-10 07:44:04'),
(2, 1, 'Standar Isi Pembelajaran', NULL, '2019-07-10 07:44:42'),
(3, 1, 'Standar Proses Pembelajaran', NULL, '2019-07-10 07:44:52'),
(4, 1, 'Standar Penilaian Pembelajaran', NULL, '2019-07-10 07:45:02'),
(5, 1, 'Standar Dosen dan Tenaga Kependidikan', NULL, '2019-07-10 07:45:12'),
(6, 1, 'Standar Sarana dan Prasarana Pembelajaran', NULL, '2019-07-10 07:45:25'),
(7, 1, 'Standar Pengelolaan Pembelajaran', NULL, '2019-07-10 07:45:41'),
(8, 1, 'Standar Pembiayaan Pembelajaran', NULL, '2019-07-10 07:45:53'),
(9, 2, 'Standar Hasil Penelitian', NULL, NULL),
(10, 2, 'Standar Isi Penelitian', NULL, NULL),
(11, 2, 'Standar Proses Penelitian', NULL, NULL),
(12, 2, 'Standar Penilaian Penelitian', NULL, NULL),
(13, 2, 'Standar Peneliti', NULL, NULL),
(14, 2, 'Standar Sarpras Penelitian', NULL, NULL),
(15, 2, 'Standar Pengelolaan Penelitian', NULL, NULL),
(16, 2, 'Standar Pendanaan & Pembiayaan Penelitian', NULL, NULL),
(17, 3, 'Standar Hasil PKM', NULL, NULL),
(18, 3, 'Standar Isi PKM', NULL, NULL),
(19, 3, 'Standar Proses PKM', NULL, NULL),
(20, 3, 'Standar Penilaian PKM', NULL, NULL),
(21, 3, 'Standar Pelaksana PKM', NULL, NULL),
(22, 3, 'Standar Sarpras PKM', NULL, NULL),
(23, 3, 'Standar Pengelolaan PKM', NULL, NULL),
(24, 3, 'Standar Pendanaan & Pembiayaan PKM', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_standar_dikti_file`
--

CREATE TABLE `tbl_standar_dikti_file` (
  `dikti_file_id` int(11) NOT NULL,
  `dikti_id` int(11) NOT NULL,
  `profil_id` int(11) NOT NULL,
  `file_standar` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tbl_standar_dikti_file`
--

INSERT INTO `tbl_standar_dikti_file` (`dikti_file_id`, `dikti_id`, `profil_id`, `file_standar`, `created_at`, `updated_at`) VALUES
(1, 1, 7, '96c8a8d8851d2c55beb18edb7a55c4e7.pdf', '2019-07-27 16:30:12', '0000-00-00 00:00:00'),
(2, 2, 7, 'f0ee0fd7b2cfbf2ac08243b1fbfa1dd5.pdf', '2019-07-27 16:30:12', '0000-00-00 00:00:00'),
(3, 3, 7, '05da6a19f15268d3ad54c317fc3eb52a.pdf', '2019-07-27 16:30:12', '0000-00-00 00:00:00'),
(4, 4, 7, 'e40cd35812520df9af197e37c9c7f38d.pdf', '2019-07-27 16:30:12', '0000-00-00 00:00:00'),
(5, 5, 7, '6655b1a354a803a2f3502f87f2f77c2a.pdf', '2019-07-27 16:30:12', '0000-00-00 00:00:00'),
(6, 6, 7, 'd3457a3b344ca7cd8af6778c36f65a38.pdf', '2019-07-27 16:30:12', '0000-00-00 00:00:00'),
(7, 7, 7, '0e73e58b07951615d4e77668e2711e70.pdf', '2019-07-27 16:30:12', '0000-00-00 00:00:00'),
(8, 8, 7, '509ff0dad2049ef7b80f17457b905882.pdf', '2019-07-27 16:30:12', '0000-00-00 00:00:00'),
(9, 1, 1, 'ce5ac8277ec8d2543ba97a16706ef8c5.pdf', '2019-08-01 15:37:41', '2019-08-01 15:37:41');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `level` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `profil_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fakultas_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jabatan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `foto` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telepon` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `password`, `level`, `profil_id`, `fakultas_id`, `jabatan`, `email`, `foto`, `telepon`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Jibril Hartri', 'jibrilhp', '$2y$10$TIv.13ItNgY9gs0DTf3g/O.BL2nuNnaIL8ZJMZwlTGdzcoc3kYdK2', '0', '7', '1', 'Lektor', 'jibrilhp@outlook.com', '95e811c2bba42d6a6fd58c858295e90a.png', '085781601196', NULL, NULL, '2019-08-17 05:54:44'),
(2, 'Putra ', 'putra', '$2y$10$TIv.13ItNgY9gs0DTf3g/O.BL2nuNnaIL8ZJMZwlTGdzcoc3kYdK2', '1', '1', '1', 'Lektor II', 'jibrilhp@outlook.com', 'a2e2fd32adc83b5abe6a4fe89eca1928.png', '085781601196', NULL, NULL, '2019-06-30 15:57:22'),
(3, 'Putra', 'putra2', '$2y$10$TIv.13ItNgY9gs0DTf3g/O.BL2nuNnaIL8ZJMZwlTGdzcoc3kYdK2', '1', '1', '1', 'Lektor ', 'jibrilhp@outlook.com', 'a2e2fd32adc83b5abe6a4fe89eca1928.png', '085781601196', NULL, NULL, '2019-06-30 15:57:22'),
(4, 'Prof. Ing. Adang Suhendra', 'adang', '$2y$10$si76YXqdPp9tQlWQGBwaIuKTyRGyQbnH/F0Enk6avAvVG1edBc9dS', '1', '7', '2', 'Kepala Jurusan TI', 'adang@gunadarma.ac.id', NULL, '085781601196', NULL, '2019-07-13 06:07:37', '2019-07-13 06:09:01'),
(5, 'Dr. Karmilasari', 'karmilasari', '$2y$10$0Ydniqzj76OJqJy/Pb7tq.xkE3i9PB2ymvXmDKcOEo65XvMl1iZby', '1', '7', '2', 'KJM', 'karmilasari@gunadarma.ac.id', NULL, '085781601196', NULL, '2019-07-13 06:11:09', '2019-07-13 06:11:09'),
(6, 'Dr. Hustinawaty', 'hustinawaty', '$2y$10$YGk91zjYL2q1XNalNKsP2.eVdiIR2YUrqqXx5bQ9VU6I4ag1O2P72', '1', '7', '2', 'KJM', 'hustinawaty@gunadarma.ac.id', NULL, '085781601196', NULL, '2019-07-13 06:13:12', '2019-07-24 08:26:40'),
(7, 'Dr. Setia Wirawan', 'setia', '$2y$10$FwRlsVVv5WvUFGxM/as0Heq..IOggbQ6aeik.w5Xuw5Ww2JmnjLRO', '1', '7', '2', 'KJM', 'setia@gunadarma.ac.id', NULL, '085781601196', NULL, '2019-07-13 06:14:11', '2019-07-13 06:14:11');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `tbl_administrasi`
--
ALTER TABLE `tbl_administrasi`
  ADD PRIMARY KEY (`adm_id`);

--
-- Indeks untuk tabel `tbl_akreditasi`
--
ALTER TABLE `tbl_akreditasi`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_audit_anggota`
--
ALTER TABLE `tbl_audit_anggota`
  ADD PRIMARY KEY (`anggota_id`);

--
-- Indeks untuk tabel `tbl_audit_borang`
--
ALTER TABLE `tbl_audit_borang`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_audit_borang_penentuan`
--
ALTER TABLE `tbl_audit_borang_penentuan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_audit_borang_penentuan_jadwal`
--
ALTER TABLE `tbl_audit_borang_penentuan_jadwal`
  ADD PRIMARY KEY (`pen_jadwal_id`);

--
-- Indeks untuk tabel `tbl_audit_borang_penentuan_jadwal_lingkup`
--
ALTER TABLE `tbl_audit_borang_penentuan_jadwal_lingkup`
  ADD PRIMARY KEY (`pen_lingkup_id`);

--
-- Indeks untuk tabel `tbl_audit_borang_penentuan_jadwal_listauditee`
--
ALTER TABLE `tbl_audit_borang_penentuan_jadwal_listauditee`
  ADD PRIMARY KEY (`pen_jadwal_list_auditee_id`);

--
-- Indeks untuk tabel `tbl_audit_borang_penentuan_jadwal_listauditor`
--
ALTER TABLE `tbl_audit_borang_penentuan_jadwal_listauditor`
  ADD PRIMARY KEY (`pen_jadwal_list_auditor_id`);

--
-- Indeks untuk tabel `tbl_audit_borang_permintaan`
--
ALTER TABLE `tbl_audit_borang_permintaan`
  ADD PRIMARY KEY (`permintaan_id`);

--
-- Indeks untuk tabel `tbl_audit_borang_permintaan_auditor`
--
ALTER TABLE `tbl_audit_borang_permintaan_auditor`
  ADD PRIMARY KEY (`per_audit_id`);

--
-- Indeks untuk tabel `tbl_audit_borang_permintaan_auditor_file`
--
ALTER TABLE `tbl_audit_borang_permintaan_auditor_file`
  ADD PRIMARY KEY (`per_auditor_id`);

--
-- Indeks untuk tabel `tbl_audit_jadwal`
--
ALTER TABLE `tbl_audit_jadwal`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_audit_jenjang_kesimpulan`
--
ALTER TABLE `tbl_audit_jenjang_kesimpulan`
  ADD PRIMARY KEY (`jk_id`);

--
-- Indeks untuk tabel `tbl_audit_kesimpulan`
--
ALTER TABLE `tbl_audit_kesimpulan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_audit_lampiran`
--
ALTER TABLE `tbl_audit_lampiran`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_audit_lingkup`
--
ALTER TABLE `tbl_audit_lingkup`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_audit_pertanyaan`
--
ALTER TABLE `tbl_audit_pertanyaan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_audit_ptk`
--
ALTER TABLE `tbl_audit_ptk`
  ADD PRIMARY KEY (`audit_ptk_id`);

--
-- Indeks untuk tabel `tbl_audit_ptk_form`
--
ALTER TABLE `tbl_audit_ptk_form`
  ADD PRIMARY KEY (`ptk_id`),
  ADD UNIQUE KEY `ptk_audit_jadwal_ref` (`ptk_audit_jadwal_ref`);

--
-- Indeks untuk tabel `tbl_audit_saran_perbaikan`
--
ALTER TABLE `tbl_audit_saran_perbaikan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_audit_temuan`
--
ALTER TABLE `tbl_audit_temuan`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `audit_pertanyaan_id` (`audit_pertanyaan_id`);

--
-- Indeks untuk tabel `tbl_audit_tujuan`
--
ALTER TABLE `tbl_audit_tujuan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_category`
--
ALTER TABLE `tbl_category`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_emi_bobot_nilai`
--
ALTER TABLE `tbl_emi_bobot_nilai`
  ADD PRIMARY KEY (`bobot_id`);

--
-- Indeks untuk tabel `tbl_emi_isian_standar`
--
ALTER TABLE `tbl_emi_isian_standar`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_emi_keadaan_prodi`
--
ALTER TABLE `tbl_emi_keadaan_prodi`
  ADD PRIMARY KEY (`keadaan_id`);

--
-- Indeks untuk tabel `tbl_emi_rekap_standar`
--
ALTER TABLE `tbl_emi_rekap_standar`
  ADD PRIMARY KEY (`rekap_id`);

--
-- Indeks untuk tabel `tbl_emi_standar`
--
ALTER TABLE `tbl_emi_standar`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_fakultas_dep_prodi`
--
ALTER TABLE `tbl_fakultas_dep_prodi`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_jenjang`
--
ALTER TABLE `tbl_jenjang`
  ADD PRIMARY KEY (`jenjang_id`);

--
-- Indeks untuk tabel `tbl_manual_mutu`
--
ALTER TABLE `tbl_manual_mutu`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_menu`
--
ALTER TABLE `tbl_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_penilaian`
--
ALTER TABLE `tbl_penilaian`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_perencanaan`
--
ALTER TABLE `tbl_perencanaan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_profil`
--
ALTER TABLE `tbl_profil`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tbl_profil_nama_prodi_unique` (`nama_prodi`);

--
-- Indeks untuk tabel `tbl_standard_category`
--
ALTER TABLE `tbl_standard_category`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_standar_awal`
--
ALTER TABLE `tbl_standar_awal`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_standar_dikti`
--
ALTER TABLE `tbl_standar_dikti`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_standar_dikti_file`
--
ALTER TABLE `tbl_standar_dikti_file`
  ADD PRIMARY KEY (`dikti_file_id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=135;

--
-- AUTO_INCREMENT untuk tabel `tbl_administrasi`
--
ALTER TABLE `tbl_administrasi`
  MODIFY `adm_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `tbl_akreditasi`
--
ALTER TABLE `tbl_akreditasi`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tbl_audit_anggota`
--
ALTER TABLE `tbl_audit_anggota`
  MODIFY `anggota_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tbl_audit_borang`
--
ALTER TABLE `tbl_audit_borang`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `tbl_audit_borang_penentuan`
--
ALTER TABLE `tbl_audit_borang_penentuan`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tbl_audit_borang_penentuan_jadwal`
--
ALTER TABLE `tbl_audit_borang_penentuan_jadwal`
  MODIFY `pen_jadwal_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tbl_audit_borang_penentuan_jadwal_lingkup`
--
ALTER TABLE `tbl_audit_borang_penentuan_jadwal_lingkup`
  MODIFY `pen_lingkup_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `tbl_audit_borang_penentuan_jadwal_listauditee`
--
ALTER TABLE `tbl_audit_borang_penentuan_jadwal_listauditee`
  MODIFY `pen_jadwal_list_auditee_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tbl_audit_borang_penentuan_jadwal_listauditor`
--
ALTER TABLE `tbl_audit_borang_penentuan_jadwal_listauditor`
  MODIFY `pen_jadwal_list_auditor_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `tbl_audit_borang_permintaan`
--
ALTER TABLE `tbl_audit_borang_permintaan`
  MODIFY `permintaan_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `tbl_audit_borang_permintaan_auditor`
--
ALTER TABLE `tbl_audit_borang_permintaan_auditor`
  MODIFY `per_audit_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tbl_audit_borang_permintaan_auditor_file`
--
ALTER TABLE `tbl_audit_borang_permintaan_auditor_file`
  MODIFY `per_auditor_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `tbl_audit_jadwal`
--
ALTER TABLE `tbl_audit_jadwal`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `tbl_audit_jenjang_kesimpulan`
--
ALTER TABLE `tbl_audit_jenjang_kesimpulan`
  MODIFY `jk_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `tbl_audit_kesimpulan`
--
ALTER TABLE `tbl_audit_kesimpulan`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `tbl_audit_lampiran`
--
ALTER TABLE `tbl_audit_lampiran`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tbl_audit_lingkup`
--
ALTER TABLE `tbl_audit_lingkup`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `tbl_audit_pertanyaan`
--
ALTER TABLE `tbl_audit_pertanyaan`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;

--
-- AUTO_INCREMENT untuk tabel `tbl_audit_ptk`
--
ALTER TABLE `tbl_audit_ptk`
  MODIFY `audit_ptk_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `tbl_audit_ptk_form`
--
ALTER TABLE `tbl_audit_ptk_form`
  MODIFY `ptk_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `tbl_audit_saran_perbaikan`
--
ALTER TABLE `tbl_audit_saran_perbaikan`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tbl_audit_temuan`
--
ALTER TABLE `tbl_audit_temuan`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=169;

--
-- AUTO_INCREMENT untuk tabel `tbl_audit_tujuan`
--
ALTER TABLE `tbl_audit_tujuan`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `tbl_category`
--
ALTER TABLE `tbl_category`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `tbl_emi_bobot_nilai`
--
ALTER TABLE `tbl_emi_bobot_nilai`
  MODIFY `bobot_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `tbl_emi_isian_standar`
--
ALTER TABLE `tbl_emi_isian_standar`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `tbl_emi_keadaan_prodi`
--
ALTER TABLE `tbl_emi_keadaan_prodi`
  MODIFY `keadaan_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `tbl_emi_rekap_standar`
--
ALTER TABLE `tbl_emi_rekap_standar`
  MODIFY `rekap_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tbl_emi_standar`
--
ALTER TABLE `tbl_emi_standar`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `tbl_fakultas_dep_prodi`
--
ALTER TABLE `tbl_fakultas_dep_prodi`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id', AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tbl_jenjang`
--
ALTER TABLE `tbl_jenjang`
  MODIFY `jenjang_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `tbl_manual_mutu`
--
ALTER TABLE `tbl_manual_mutu`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT untuk tabel `tbl_menu`
--
ALTER TABLE `tbl_menu`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT untuk tabel `tbl_penilaian`
--
ALTER TABLE `tbl_penilaian`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tbl_perencanaan`
--
ALTER TABLE `tbl_perencanaan`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tbl_profil`
--
ALTER TABLE `tbl_profil`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `tbl_standard_category`
--
ALTER TABLE `tbl_standard_category`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT untuk tabel `tbl_standar_awal`
--
ALTER TABLE `tbl_standar_awal`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;

--
-- AUTO_INCREMENT untuk tabel `tbl_standar_dikti`
--
ALTER TABLE `tbl_standar_dikti`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT untuk tabel `tbl_standar_dikti_file`
--
ALTER TABLE `tbl_standar_dikti_file`
  MODIFY `dikti_file_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
